var addBtn = $("#add-btn"),
    loadData = window.base_url + "income_detail/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "income_detail/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker(),Helper.selectField($(".pilih"), "Pilih");
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), addBtn.click(function() {
                    Page.add()
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1
                }), $(document).on("submit", "#form-unverify", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitUnVerify($("#form-unverify"), a), !1

                }), $(document).on("click", ".btn-export", function() {
                    Page.export()
                }), $(document).on("click", ".btn-export-month", function() {
                    Page.export_date()
                }), $(document).on("click", ".btn-verify", function() {
                    Page.verify()
                }), $(document).on("click", ".btn-unverify", function() {
                    Page.un_verify()
                }), $(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e)
                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1
                }),$(document).on("submit", "#form-verify", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitVerify($("#form-verify"), a), !1
                })
            },
             submitForm: function(e, a) {
                            if (confirm('Anda Yakin ?')) {
                                var i = e.find(".submit");
                                var file_data = $('#path_bukti_bayar').prop('files')[0];
                                var form = $('form')[0];
                                var form_data = new FormData(form);
                                form_data.append('file', file_data);

                                if (Helper.validateForm(e, {
                                        jml_bayar: {
                                            required: !0,
                                            number: !0
                                        },
                                        tgl_bayar: {
                                            required: !0
                                        },
                                        catatan: {
                                            required: !0
                                        }
                                    }), e.valid()) {
                                    var t = Helper.baseUrl("income_detail/add");
                                    a && (t = Helper.baseUrl("income_detail/edit")), 
                                    Helper.blockElement(e.parent()), i.attr("disabled", !0); 

                                    $.ajax({
                                        url: t, // point to server-side PHP script
                                        dataType: 'json',  // what to expect back from the PHP script, if anything
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'POST',
                                        error: function(t) {
                                            swal(a.status.toString(), a.statusText, "error")
                                        }
                                    })

                                    .error(function(a){Helper.unblockElement(e.parent()), i.attr("disabled", !1)}).done(function(a) 
                                    {
                                        a.status ? (e.parent().parent().find(".close").click(), 
                                            swal(a.action, a.message, "success"), 
                                            grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), 
                                            Helper.unblockElement(e.parent()), swal({
                                            title: a.action,
                                            text: a.message,
                                            type: "error",
                                            html: !0
                                        }))
                                    })
                                } else {
                                alert("Data Belum Lengkap");
                                }
                            }
                        

            },
            export: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Date');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('income_detail/load_export'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            export_date: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Month');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('income_detail/load_export_date'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            add: function(e) {
                var e = Helper.loadModal("lg"),
                    a = e.find(".modal-body"),
                    i = e.find(".modal-title");

                i.text("Add Data Income Other"), Helper.blockElement($(a)), Helper.ajax(Helper.baseUrl("income_detail/load_add_form"), "get", "html").error(function(a) {
                    e.find(".close").click()
                }).done(function(e) {
                    a.html(e), Helper.selectField($(".pilih"), "Pilih");
                    var i = null;
                    Helper.datePicker(i), Helper.unblockElement($(a));
                })
            },
            edit: function(e) {
                var a = Helper.loadModal("lg"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Data Income"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("income_detail/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    var a = null;
                    Helper.datePicker(a), Helper.unblockElement($(i))
                })
            },
            verify: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Verify Based On Date');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('income_detail/load_verify'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            un_verify: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('UnVerify Based On Date');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('income_detail/load_unverify'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select");
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            submitUnVerify: function(e, a) {
                var i = e.find(".submit");
                if (Helper.validateForm(e, {
                    tgl_bayar: {
                    required: !0
                    },
                    kd_cabang: {
                    required: !0
                         }
                    }), e.valid()) {
                    var t = Helper.baseUrl("income_detail/unverify");
                    a && (t = Helper.baseUrl("income_detail/unverify")), Helper.blockElement(e.parent()), i.attr("disabled", !0), Helper.ajax(t, "post", "json", Helper.serializeForm(e)).error(function(a) {
                        Helper.unblockElement(e.parent()), i.attr("disabled", !1)
                    }).done(function(a) {
                        var status = true;
                        a.status ? (e.parent().parent().find(".close").click(), swal(a.action, a.message, "success"), grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), Helper.unblockElement(e.parent()), 
                            
                            e.parent().parent().find(".close").click(),
                            swal({
                            title: 'Un Verify Succes',
                            text: a.message,
                            type: "success",
                            html: !0
                        }))
                    location.reload();
                    })
                }
            },
            submitVerify: function(e, a) {
                var i = e.find(".submit");
                if (Helper.validateForm(e, {
                    tgl_bayar: {
                    required: !0
                    },
                    kd_cabang: {
                    required: !0
                         }
                    }), e.valid()) {
                    var t = Helper.baseUrl("income_detail/verify");
                    a && (t = Helper.baseUrl("income_detail/verify")), Helper.blockElement(e.parent()), i.attr("disabled", !0), Helper.ajax(t, "post", "json", Helper.serializeForm(e)).error(function(a) {
                        Helper.unblockElement(e.parent()), i.attr("disabled", !1)
                    }).done(function(a) {
                        var status = true;
                        a.status ? (e.parent().parent().find(".close").click(), swal(a.action, a.message, "success"), grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1),  Helper.unblockElement(e.parent()), 
                            
                            e.parent().parent().find(".close").click(),
                            swal({
                            title: 'Verify Succes',
                            text: a.message,
                            type: "success",
                            html: !0
                        }))
                    location.reload();
                    })
                }
            },
        }
    }();