var addBtn = $("#add-btn"),
    loadData = window.base_url + "benefit_marketing/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "benefit_marketing/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker(), Helper.selectField($(".jadwal"), "Select Schedule");
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), addBtn.click(function() {
                    Page.add()
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1

                }), $(document).on("click", ".btn-payment", function() {
                    var e = $(this).data("id");
                    Page.payment(e)

                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1

                }), $(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e)

                }),$(document).on("click", ".btn-detail", function() {
                    var e = $(this).data("id");
                    Page.detail(e)

                }),$(document).on("click", ".btn-export", function() {
                    Page.export()

                }),$(document).on("click", ".btn-between", function() {
                    Page.export_between()
                }), $(document).on("submit", "#form-cb", function() {
                    return Page.submitFormcb($("#form-cb"), null, null), !1
                })
            },
            submitFormcb: function(e, a) {
                var i = e.find(".submit");
                if (Helper.validateForm(e, {
                        sales:{required:!0}
                    }), e.valid()) {
                    var t = Helper.baseUrl("benefit_marketing/update_multiple");
                    a && (t = Helper.baseUrl("benefit_marketing/update_multiple")),

                    Helper.blockElement(e.parent()), i.attr("disabled", !0), 

                    Helper.ajax(t, "post", "json", Helper.serializeForm(e))

                    .error(function(a) {
                        Helper.unblockElement(e.parent()), i.attr("disabled", !1)
                    })

                    .done(function(a) {
                        a.status ? (e.parent().parent().find(".close").click(), 
                            swal(a.action, a.message, "success"), 
                            location.reload()) : (i.attr("disabled", !1), 
                            Helper.unblockElement(e.parent()), 
                            swal({
                                title: a.action,
                                text: a.message,
                                type: "error",
                                html: !0
                                })
                        )
                    })
                }
            },
            submitForm: function(e, a) {
                if (confirm("Are you sure ?")) {
                    var i = e.find(".submit");
                    if (Helper.validateForm(e, {
                            jml_benefit: {
                                required: !0,
                                number: !0
                            },
                        }), e.valid()) {
                        var t = Helper.baseUrl("benefit_marketing/add");
                        a && (t = Helper.baseUrl("benefit_marketing/edit")), Helper.blockElement(e.parent()), i.attr("disabled", !0), Helper.ajax(t, "post", "json", Helper.serializeForm(e)).error(function(a) {
                            Helper.unblockElement(e.parent()), i.attr("disabled", !1)
                        }).done(function(a) {
                            a.status ? (e.parent().parent().find(".close").click(), swal(a.action, a.message, "success"), grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), Helper.unblockElement(e.parent()), swal({
                                title: a.action,
                                text: a.message,
                                type: "error",
                                html: !0
                            }))
                        })
                    }
                }

            },        
            add: function(e) {
                var e = Helper.loadModal("lg"),
                    a = e.find(".modal-body"),
                    i = e.find(".modal-title");

                i.text("Add Data payment"), Helper.blockElement($(a)), Helper.ajax(Helper.baseUrl("benefit_marketing/load_payment_form"), "get", "html").error(function(a) {
                    e.find(".close").click()
                }).done(function(e) {
                    a.html(e), Helper.selectField($(".jadwal"), "Pilih"), Helper.selectField($(".pilihbanyak"), "Pilih"), Helper.selectField($(".cabang"), "Pilih");
                    var i = null;
                    Helper.datePicker(i), Helper.unblockElement($(a));
                })



            },
            edit: function(e) {
                var a = Helper.loadModal("lg"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Benefit"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("benefit_marketing/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    var a = null;
                    Helper.datePicker(a), Helper.unblockElement($(i));
                })
            },
            detail: function(e) {
                var a = Helper.loadModal("lg"),
                    t = a.find(".modal-body"),
                    i = a.find(".modal-title");
                i.text("Detail"), Helper.blockElement($(t)), Helper.ajax(Helper.baseUrl("registrasi/load_detail"), "get", "html", {
                    id: e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    t.html(e), Helper.unblockElement($(t))
                })
            },

            payment: function(e) {
                var a = Helper.loadModal("lg"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Payment"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("benefit_marketing/load_payment_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    benefit= document.getElementById('jml_benefit').value;
                    $(".harga-pph").val(0);
                    $(".jml_bayar").val(benefit);

                    $(document).on("click", ".pph", function() {
                        var ischecked= $(this).is(':checked'),

                            benefit= document.getElementById('jml_benefit').value;
                            //benefit = $("#jml_benefit").autoNumeric("get");

                        if(ischecked) {
                            r = (parseFloat(benefit) / 100 )* 2;
                            $(".harga-pph").val(r);
                            $(".jml_bayar").val(benefit-r);
                          //alert('checkd ' + benefit);
                        } else {
                            r = 0;
                            $(".harga-pph").val(r);
                            $(".jml_bayar").val(benefit);
                          //alert('uncheckd ' + benefit);
                        }
                    }); 
                    var a = null;
                    Helper.datePicker(a), Helper.unblockElement($(i));
                })
            },
            export: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Schedule');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('benefit_marketing/load_export'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            export_between: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Between');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('benefit_marketing/load_export_between'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            
        }
    }();