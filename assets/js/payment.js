var addBtn = $("#add-btn"),
    loadData = window.base_url + "payment/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "payment/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker(),Helper.selectField($(".pilih"), "Pilih");
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), addBtn.click(function() {
                    Page.add()
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1

                }), $(document).on("submit", "#form-multi", function() {
                    return Page.submitFormMulti($("#form-multi"), null, null), !1

                }), $(document).on("click", ".btn-payment", function() {
                    var e = $(this).data("id");
                    Page.payment(e)

                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1

                }),$(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e)

                }),$(document).on("click", ".btn-print", function() {
                    var e = $(this).data("id");
                    Page.print(e)

                }),$(document).on("click", ".btn-export", function() {
                    Page.export()

                 }),$(document).on("click", ".btn-export-tabungan", function() {
                    Page.export_tabungan()

                }),$(document).on("click", ".btn-export-excel", function() {
                    Page.export_excel()

                }),$(document).on("click", ".btn-multi", function() {
                    Page.multi()
                })
            },
            submitForm: function(e, a) {
                var select = document.getElementById("tunai");
                var isi = select.options[select.selectedIndex].value;
                var bayar = document.getElementById("jml_bayar").value;
                var sisa = document.getElementById("sisa2").value;
                var id_pendaftar = document.getElementById("id_pendaftar").value;
                var id_paket = document.getElementById("id_paket").value;
                var catatan = document.getElementById("catatan").value;
                var userLevel = document.getElementById("userLevel").value;


                if (bayar >= 10000000 && isi == 'Tunai') {
                    alert("Pembayaran Diatas Rp.10.000.000,- Harus Melalui Transfer");
                } else if (parseFloat(bayar) > parseFloat(sisa)) {
                    alert('Pembayaran Melebihi yang Harus Dibayar, yakni '+sisa+'');
                } else {
                    $.ajax({
                        url : Helper.baseUrl("payment/cek_first/")+id_pendaftar,
                        method : "POST",
                        // data : {id: id_pendaftar},
                        async : false,
                        dataType : 'json',
                        success: function(data){
                        if (bayar <= 999999 && id_paket == 0) {
                            alert("Setoran Awal Paket Tabungan Minimal Rp.1.000.000,-");
                            
                        } else if (data.cek_first == 0 && bayar <= 4999000 && id_paket != 1 && bayar != 0 && userLevel == 0) {
                            alert("Pembayaran Pertama Minimal Rp.5.000.000,-");
                        } else if (catatan == 'Pelunasan' && bayar != sisa) {
                            alert("Jumlah Peluansan tidak sesuai sisa bayar !");
                        } else {                            
                            if (confirm('Anda Yakin ?')) {
                                var i = e.find(".submit");
                                var file_data = $('#path_bukti_bayar').prop('files')[0];
                                var form = $('form')[0];
                                var form_data = new FormData(form);
                                form_data.append('file', file_data);

                                if (Helper.validateForm(e, {
                                        jml_bayar: {
                                            required: !0,
                                            number: !0
                                        },
                                        tgl_bayar: {
                                            required: !0
                                        },
                                        catatan: {
                                            required: !0
                                        },
                                        path_bukti_bayar: {
                                            required: !0
                                        },
                                    }), e.valid()) {
                                    var t = Helper.baseUrl("payment/add");
                                    a && (t = Helper.baseUrl("payment/edit")), 
                                    Helper.blockElement(e.parent()), i.attr("disabled", !0); 

                                    $.ajax({
                                        url: t, // point to server-side PHP script
                                        dataType: 'json',  // what to expect back from the PHP script, if anything
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'POST',
                                        error: function(t) {
                                            swal(a.status.toString(), a.statusText, "error")
                                        }
                                    })

                                    .error(function(a){Helper.unblockElement(e.parent()), i.attr("disabled", !1)}).done(function(a) 
                                    {
                                        a.status ? (e.parent().parent().find(".close").click(), 
                                            swal(a.action, a.message, "success"), 
                                            grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), 
                                            Helper.unblockElement(e.parent()), swal({
                                            title: a.action,
                                            text: a.message,
                                            type: "error",
                                            html: !0
                                        }))
                                    })
                                } else {
                                alert("Data Belum Lengkap");
                                }
                            }
                        } 
                        }
                    });

                     
                }
            },

            add: function(e) {
                var e = Helper.loadModal("lg"),
                    a = e.find(".modal-body"),
                    i = e.find(".modal-title");

                i.text("Add Data payment"), Helper.blockElement($(a)), Helper.ajax(Helper.baseUrl("payment/load_payment_form"), "get", "html").error(function(a) {
                    e.find(".close").click()
                }).done(function(e) {
                    a.html(e), Helper.selectField($(".jadwal"), "Pilih"), Helper.selectField($(".pilihbanyak"), "Pilih"), Helper.selectField($(".cabang"), "Pilih");
                    var i = null;
                    Helper.datePicker(i), Helper.unblockElement($(a));
                })
            },
            edit: function(e) {
                window.location.href = Helper.baseUrl("payment_detail/pendaftaran/") + e;
            },
            detail: function(e) {
                var a = Helper.loadModal("lg"),
                    t = a.find(".modal-body"),
                    i = a.find(".modal-title");
                i.text("Detail"), Helper.blockElement($(t)), Helper.ajax(Helper.baseUrl("registrasi/load_detail"), "get", "html", {
                    id: e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    t.html(e), Helper.unblockElement($(t))
                })
            },

            payment: function(e) {
                var today = new Date();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
		  var userLevelMain = document.getElementById("userLevelMain").value;

                var xmlHttp;
                function srvTime(){
                    try {
                            //FF, Opera, Safari, Chrome
                            xmlHttp = new XMLHttpRequest();
                        }
                        catch (err1) 
                            {
                            //IE
                            try {
                                xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
                                }
                                catch (err2) 
                                {
                                    try {
                                        xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
                                    }
                                    catch (eerr3) 
                                    {
                                    //AJAX not supported, use CPU time.
                                    alert("AJAX not supported");
                                    }
                                }
                            }
                        xmlHttp.open('HEAD',window.location.href.toString(),false);
                        xmlHttp.setRequestHeader("Content-Type", "text/html");
                        xmlHttp.send('');
                        return xmlHttp.getResponseHeader("Date");
                        
                }

                var st = srvTime();
                var date = new Date(st);

                $.ajax({
                    url : Helper.baseUrl("payment/cek"),
                    method : "POST",
                    data : {id: e},
                    async : false,
                    dataType : 'json',
                    success: function(data){
                        if (data.cek != 0) {
                            alert("Terdapat Payment yang belum diverifikasi");
                        // } else {
                        //    if (date.getHours() >= 16 && userLevelMain == 0) {
                        //    alert("Tidak dapat melakukan Payment diatas Pukul 16:00 WITA");
                        } else {
                            var a = Helper.loadModal("lg"),
                                i = a.find(".modal-body"),
                                t = a.find(".modal-title");
                            t.text("Payment"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("payment/load_payment_form"), "get", "html", {
                                id:e
                            }).error(function(e) {
                                a.find(".close").click()
                            }).done(function(e) {
                                i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                                var a = null;
                                Helper.datePicker(a), Helper.unblockElement($(i));
                                Helper.datePicker(a), Helper.unblockElement($(i));
                                
                                const checkbox = document.getElementById('1');
                                checkbox.addEventListener('change', (event) => {
                                  if (event.target.checked) {
                                    document.getElementById('ps').value = 1;
                                  } else {
                                    document.getElementById('ps').value = 0;
                                  }
                                })
                            })                 
                            // }
                        }

                    }
                });
            },
            export: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Schedule');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('payment/load_export'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            export_tabungan: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Tabungan');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('payment/load_export_tabungan'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            export_excel: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Schedule');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('payment/load_export_excel'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            multi: function() {
            
                var modal = Helper.loadModal('lg');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Multi Payment');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('payment/load_multi'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            handleTransaction: function() {
                
                //DYNAMIC ROWS
                var count = 0;
                $("#add_row").click(function(){
                    count += 1;

                    $('#container').append('\
                        <tr class="baris form-create-barang" id="form-create-barang'+ count +'"">\
                        <td align="center"><input class="form-control" type="text" value="'+ count +'"></td>\
                        <td align="center">\
                        <select id="barang'+count+'" class="form-control barang" name="nm_lengkap[]" required></select>\
                        <td align="center"><input id="sisa'+count+'" class="form-control sisa money" name="sisa[]" type="text" readonly></td>\
                        <td align="center"><input id="catatan'+count+'" class="form-control catatan" name="catatan[]" type="text" required></td>\
                        <td align="center"><input id="jml'+count+'" name="jml_bayar[]" class="form-control jml money" type="text" placeholder="0" required></td>\
                        <td>\
                            <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>\
                            <input id="rows'+count+'" name="rows[]" value="'+count+'" type="hidden">\
                            <input id="id_pendaftaran'+count+'" name="id_pendaftaran[]" type="hidden">\
                            <input id="id_marketing'+count+'" name="id_marketing[]" type="hidden">\
                            <input id="kd_cabang'+count+'" name="kd_cabang[]" type="hidden">\
                            <input id="id_paket'+count+'" name="id_paket[]" type="hidden">\
                            <input id="id_jadwal'+count+'" name="id_jadwal[]" type="hidden">\
                            <input id="jenis'+count+'" name="jenis[]" type="hidden">\
                        </td>\
                        </tr>\
                        ');

                    // Helper.selectField('#barang'+count);
                    $('#barang'+count).select2({
                        placeholder: 'Masukkan Nama Jamaah',
                        ajax: {
                          url: Helper.baseUrl("payment/load_pendaftar"),
                          dataType: 'json',
                          delay: 250,
                          processResults: function (data) {
                            return {
                              results: data
                            };
                          },
                          cache: true
                        }
                    });


                    $(document).on('change', '#barang'+count, function() {
                        var id= $(this).find(":selected").val();
                        $.ajax({
                            url : Helper.baseUrl("payment/get_select_pendaftar"),
                            method : "POST",
                            data : {id: id},
                            async : true,
                            dataType : 'json',
                            success: function(data){
                                var sisa = data.total_biaya - data.telah_bayar;
                                document.getElementById('sisa'+count).value = sisa;
                                document.getElementById('id_pendaftaran'+count).value = data.id;
                                document.getElementById('id_marketing'+count).value = data.id_marketing;
                                document.getElementById('id_paket'+count).value = data.id_paket;
                                document.getElementById('id_jadwal'+count).value = data.id_jadwal;
                                document.getElementById('jenis'+count).value = data.jenis;
                                document.getElementById('kd_cabang'+count).value = data.kd_office;
                                $('#catatan'+count).focus();
                                 
                            }
                        });
                    });


                    $(document).on('keyup', '.jml', function() {
                        $(".money").autoNumeric("init", {
                            aSep: ",",
                            aDec: ".",
                            aSign: "",
                            wEmpty: "zero",
                            mDec: "0"
                        });                        

                        Page.hitungTotal(); 
                    });


                });



                $(document).on('click', '#hapus', function() {

                    $(this).parents(".baris").remove();
                    Page.hitungTotal();
                    $('#btn-submit-add-form').prop('disabled',false);

                });

            },

            hitungTotal: function()
            {
                var njumlah=0;  
            
                $(".jml").each(function(){
                    njumlah += parseFloat($(this).autoNumeric('get'), 10) || 0;    
                });

                $("#total-bayar").autoNumeric('set',njumlah); 

            },

            submitFormMulti: function(e, a) {
                                     
                            if (confirm('Anda Yakin ?')) {
                                var i = e.find(".submit");
                                var file_data = $('#path_bukti_bayar').prop('files')[0];
                                var form = $('form')[0];
                                var form_data = new FormData(form);
                                form_data.append('file', file_data);

                                if (Helper.validateForm(e, {
                                        jml_bayar: {
                                            required: !0,
                                            number: !0
                                        },
                                        tgl_bayar: {
                                            required: !0
                                        },
                                        catatan: {
                                            required: !0
                                        },
                                        path_bukti_bayar: {
                                            required: !0
                                        },
                                    }), e.valid()) {
                                    var t = Helper.baseUrl("payment/add_multi");
                                    a && (t = Helper.baseUrl("payment/edit_multi")), 
                                    Helper.blockElement(e.parent()), i.attr("disabled", !0); 

                                    $.ajax({
                                        url: t, // point to server-side PHP script
                                        dataType: 'json',  // what to expect back from the PHP script, if anything
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'POST',
                                        error: function(t) {
                                            swal(a.status.toString(), a.statusText, "error")
                                        }
                                    })

                                    .error(function(a){Helper.unblockElement(e.parent()), i.attr("disabled", !1)}).done(function(a) 
                                    {
                                        a.status ? (e.parent().parent().find(".close").click(), 
                                            swal(a.action, a.message, "success"), 
                                            grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), 
                                            Helper.unblockElement(e.parent()), swal({
                                            title: a.action,
                                            text: a.message,
                                            type: "error",
                                            html: !0
                                        }))
                                    })
                                } 
                            }
                        
            },

            // print: function(e) {
            //     window.location.href = Helper.baseUrl("payment/cetak/") + e;
            // }
            
        }
    }();