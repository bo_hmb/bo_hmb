var loadData = window.base_url + "schedule_movement/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "schedule_movement/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker()
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), 

                $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1
                }), $(document).on("click", ".btn-add", function() {
                    var e = $(this).data("id");
                    Page.add(e);
                }), $(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e);
                }), $(document).on("click", ".btn-download", function() {
                    var e = $(this).data("id");
                    Page.download(e);
                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1
                })
            },
            submitForm: function(e, a) {
                var i = e.find(".submit");
                var form = $('form')[0];
                var form_data = new FormData(form);
                var ins = document.getElementById('multiFiles').files.length;
                for (var x = 0; x < ins; x++) {
                    form_data.append("files["+x+"]", document.getElementById('multiFiles').files[x]);
                }
                    if (Helper.validateForm(e, {
                        file: {
                            required: !0,
                        }

                    }), e.valid()) {
                    var t = Helper.baseUrl("schedule_movement/add");
                    a && (t = Helper.baseUrl("schedule_movement/edit")), 
                    Helper.blockElement(e.parent()), i.attr("disabled", !0); 
                                    $.ajax({
                                        url: t, // point to server-side PHP script
                                        dataType: 'json',  // what to expect back from the PHP script, if anything
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'POST',
                                        error: function(t) {
                                            swal(a.status.toString(), a.statusText, "error")
                                        }
                                    })
                    .error(function(a){Helper.unblockElement(e.parent()), i.attr("disabled", !1)}).done(function(a) 
                                    {
                                        a.status ? (e.parent().parent().find(".close").click(), 
                                            swal(a.action, a.message, "success"), 
                                            grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), 
                                            Helper.unblockElement(e.parent()), swal({
                                            title: a.action,
                                            text: a.message,
                                            type: "error",
                                            html: !0
                                        }))
                                    })
                    
                }
            },
            add: function(e) {
                var a = Helper.loadModal("sm"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Add Itinerary"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("schedule_movement/load_add_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    var a = null;
                })
            },
            edit: function(e) {
                var a = Helper.loadModal("sm"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Itinerary"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("schedule_movement/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    var a = null;
                })

                // Helper.ajax(Helper.baseUrl("movement"), "get", "html", {
                //     id:e
                // });

                // Helper.baseUrl("movement");

                // window.location.href = Helper.baseUrl("movement/schedule/") + e;

                // window.base_url + "movement" + e;
            },

            download: function(e) {
                // Helper.ajax(Helper.baseUrl("movement"), "get", "html", {
                //     id:e
                // });

                // Helper.baseUrl("movement");

                window.location.href = Helper.baseUrl("schedule_movement/download/") + e;

                // window.base_url + "movement" + e;
            }

            
        }
    }();