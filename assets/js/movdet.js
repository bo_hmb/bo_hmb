var addBtn = $("#add-btn"),
    loadData = window.base_url + "movdet/load_data/" + idmovement,
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "movdet/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker()
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), 
                addBtn.click(function() {
                    Page.add()
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1
                }), $(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e)
                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1
                })
            },
            submitForm: function(e, a) {
                var i = e.find(".submit");
                if (Helper.validateForm(e, {

                        id_movement: {
                            required: !0
                        },
                        jam: {
                            required: !0
                        },
                        agenda: {
                            required: !0
                        }
                    }), e.valid()) {
                    var t = Helper.baseUrl("movdet/add");
                    a && (t = Helper.baseUrl("movdet/edit")), Helper.blockElement(e.parent()), i.attr("disabled", !0), Helper.ajax(t, "post", "json", Helper.serializeForm(e)).error(function(a) {
                        Helper.unblockElement(e.parent()), i.attr("disabled", !1)
                    }).done(function(a) {
                        a.status ? (e.parent().parent().find(".close").click(), swal(a.action, a.message, "success"), grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), Helper.unblockElement(e.parent()), swal({
                            title: a.action,
                            text: a.message,
                            type: "error",
                            html: !0
                        }))
                    })
                }
            },
            add: function() {
                var modal = Helper.loadModal('lg');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Masukkan Jam dan Agenda');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('movdet/load_add_form/'+idmovement), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".movedet"), "Pilih Jadwal");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });

            },
            edit: function(e) {
                var a = Helper.loadModal("lg"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Data Itinerary"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("movdet/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($("select")), Helper.unblockElement($(i))
                    var a = null;
                    Helper.datePicker(a), Helper.clockfaceTimePicker(), Helper.unblockElement($(i))
                })
            },

            handleTransaction: function() {
                //DYNAMIC ROWS
                var count = 0;
                $("#add_row").click(function(){
                    count += 1;

                    $('#container').append('\
                        <tr class="baris form-create-barang" id="form-create-barang'+ count +'"">\
                            <td align="center"><input id="item'+count+'" class="form-control name="item[]" type="text" value="'+count+'" readonly></td>\
                            <td align="center"><input type="text" id="clockface_2 jam'+count+'" value="00:00" class="form-control" name="jam[]"/>\
                            </td>\
                            <td align="center"><input id="agenda'+count+'" class="form-control agenda" name="agenda[]" type="text"></td>\
                            <td>\
                                <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>\
                                <input id="rows'+count+'" name="rows[]" value="'+count+'" type="hidden">\
                            </td>\
                        </tr>\
                        ');

                });



                $(document).on('click', '#hapus', function() {
                    $(this).parents(".baris").remove();
                    $('#btn-submit-add-form').prop('disabled',false);
                   
                });

            },
            
        }
    }();