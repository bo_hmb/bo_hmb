var addBtn = $("#add-btn"),
    loadData = window.base_url + "sale/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "sale/cancel_purchase",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker()
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), 
                addBtn.click(function() {
                    Page.add();
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null), !1
                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1
                }),$(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e);
                })
            },
            // Submit function
            submitForm: function(form, id) {
                if (confirm("Are you sure ?")) {
                    var submitButton = form.find('.submit');

                    // Validate the form with rules
                    Helper.validateForm(form, {
                         no_faktur: {
                                required: !0
                         },
                         nm_toko: {
                                required: !0
                         },
                         tgl_beli: {
                                required: !0
                         },
                         total_bayar: {
                                required: !0
                         },
                    });

                    // If form is valid run submit through ajax request
                    if (form.valid()) {
                        
                        var url = Helper.baseUrl('sale/add');

                        if (id) {
                            url = Helper.baseUrl('sale/edit');
                        }
                        
                        Helper.blockElement(form.parent());
                        submitButton.attr('disabled', true);

                        Helper.ajax(url, 'post', 'json', Helper.serializeForm(form))
                            
                        .error(function(err) {
                            Helper.unblockElement(form.parent());
                            submitButton.attr('disabled', false);
                        })

                        .done(function(data) {
                            if (data.status) {
                                form.parent().parent().find('.close').click();
                                swal(data.action, data.message, "success");
                                grid.getDataTable(tableID).ajax.reload();
                            } else {
                                submitButton.attr('disabled', false);
                                Helper.unblockElement(form.parent());
                                swal({title: data.action, text: data.message, type: "error", html: true});
                            }
                        });
                    }
                    else {
                    alert("You decided to not submit the form!");
                    }
                }
            },
            add: function() {
            
                var modal = Helper.loadModal('full');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Tambah Penjualan Barang');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('sale/load_add_form'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker(), Helper.selectField($(".jadwal"), "Pilih Jadwal");
                    Page.handleTransaction();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            edit: function(e) {
                var a = Helper.loadModal("full"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Data Penjualan"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("sale/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), 
                    Page.handleTransaction();

                    $(document).on('keyup', '.jml-edit', function() {
                        $(".money").autoNumeric("init", {
                            aSep: ",",
                            aDec: ".",
                            aSign: "",
                            wEmpty: "zero",
                            mDec: "0"
                        });
                        
                        var qty = $(this).autoNumeric('get');   
                        var sub_total = $(this).closest('tr').find('.sub_harga').autoNumeric('get');
                        var harga = $(this).closest('tr').find('.harga_beli').autoNumeric('get');                     

                        var jumlah = parseFloat(harga)*parseFloat(qty);
                        $(this).closest('tr').find('.sub_harga').val(jumlah);
                        $(this).closest('tr').find('.sub_harga').autoNumeric('set', jumlah);

                        Page.hitungTotal(); 
                    });

                    Helper.datePicker(), Helper.unblockElement($(i));
                })
            },
            // service
            handleTransaction: function() {
                
                //DYNAMIC ROWS
                var x = 0;
                $("#add_row").click(function(){
                    x += 1;

                    $('#container').append('\
                        <tr class="baris form-create-barang" id="form-create-barang'+ x +'"">\
                        <td align="center">\
                        <select id="barang'+x+'" class="form-control barang" name="nm_barang[]" required></select>\
                        <td align="center"><input id="harga_jual'+x+'" class="form-control harga_jual money" name="harga_jual[]" type="text" readonly></td>\
                        <td align="center"><input id="jml'+x+'" name="jml[]" class="form-control jml money" type="text" placeholder="0"></td>\
                        <td align="center"><input type="checkbox" id="free'+x+'" class="free" name="free[]" value="1" /></td>\
                        <td align="center"><input id="sub_harga'+x+'" class="form-control sub_harga money" name="sub_harga[]" type="text" value="0" readonly></td>\
                        <input id="cekstok'+x+'" name="cekstok[]" class="form-control cekstok money" type="hidden">\
                        <td>\
                            <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>\
                            <input id="rows'+x+'" name="rows[]" value="'+x+'" type="text">\
                            <input id="sts_free'+x+'" name="sts_free[]" class="sts_free" value="0" type="hidden">\
                        </td>\
                        </tr>\
                        ');

                    // Helper.selectField('#barang'+count);
                    $('#barang'+x).select2({
                        placeholder: '--- Masukkan Nama Barang ---',
                        ajax: {
                          url: Helper.baseUrl("sale/load_barang"),
                          dataType: 'json',
                          delay: 250,
                          processResults: function (data) {
                            return {
                              results: data
                            };
                          },
                          cache: true
                        }
                    });


                    $(document).on('change', '#barang'+x, function() {
                        var id= $(this).find(":selected").val();
                        $.ajax({
                            url : Helper.baseUrl("sale/get_select_barang"),
                            method : "POST",
                            data : {id: id},
                            async : true,
                            dataType : 'json',
                            success: function(data){
                                var hrg_jual = data.harga_jual;
                                document.getElementById('harga_jual'+x).value = hrg_jual;
                                var cekstok = data.stok;
                                document.getElementById('cekstok'+x).value = cekstok;
                                $('#jml'+x).focus();
                                 
                            }
                        });
                    });
                    
                    $(document).on('keyup', '.jml', function() {
                        $(".money").autoNumeric("init", {
                            aSep: ",",
                            aDec: ".",
                            aSign: "",
                            wEmpty: "zero",
                            mDec: "0"
                        });

                        var qty = $(this).autoNumeric('get');   
                        var sub_total = $(this).closest('tr').find('.sub_harga').autoNumeric('get');
                        var harga = $(this).closest('tr').find('.harga_jual').autoNumeric('get');                     
                        var jumlah = parseFloat(harga)*parseFloat(qty);
                        $(this).closest('tr').find('.sub_harga').val(jumlah);
                        $(this).closest('tr').find('.sub_harga').autoNumeric('set', jumlah);
                        
                        Page.hitungTotal();
                         
                    });
                    
                });
                
                $(document).on("click", ".free", function() {
                    $(".money").autoNumeric("init", {
                            aSep: ",",
                            aDec: ".",
                            aSign: "",
                            wEmpty: "zero",
                            mDec: "0"
                        });

                    if ($(this).prop('checked') == true) {
                        jmlh = 0;
                        $(this).closest('tr').find('.sub_harga').val(jmlh);
                        $(this).closest('tr').find('.sub_harga').autoNumeric('set', jmlh);
                        $(this).closest('tr').find('.sts_free').val(1);

                    }else { 
                        var qty = $(this).closest('tr').find('.jml').autoNumeric('get');   
                        var harga = $(this).closest('tr').find('.harga_jual').autoNumeric('get');                    
                        var jmlh = parseFloat(harga)*parseFloat(qty);
                        $(this).closest('tr').find('.sub_harga').val(jmlh);
                        $(this).closest('tr').find('.sub_harga').autoNumeric('set', jmlh);
                        $(this).closest('tr').find('.sts_free').val(0);


                    }
                    Page.hitungTotal();
                   
                });

                $(document).on('click', '#hapus', function() {
                    $(this).parents(".baris").remove();
                    $(".money").autoNumeric("init", {
                            aSep: ",",
                            aDec: ".",
                            aSign: "",
                            wEmpty: "zero",
                            mDec: "0"
                        });
                    $('#btn-submit-add-form').prop('disabled',false);
                    Page.hitungTotal();

                });

            },

            hitungTotal: function()
            {
                var njumlah=0;  
            
                $(".sub_harga").each(function(){
                    njumlah += parseFloat($(this).autoNumeric('get'), 10) || 0;    
                });

                $("#total-bayar").autoNumeric('set',njumlah); 

            }
            

            

        }
    }();


