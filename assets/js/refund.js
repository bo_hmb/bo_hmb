var addBtn = $("#add-btn"),
    loadData = window.base_url + "refund/load_data",
    tableID = $("#datatable_ajax"),
    grid = new Datatable,
    urlUpdateStatus = window.base_url + "refund/delete",
    Page = function() {
        return {
            init: function() {
                Page.main(), Helper.datePicker()
            },
            main: function() {
                Helper.tableAjax(grid, tableID, loadData, urlUpdateStatus, null), 

                addBtn.click(function() {
                    Page.add()
                }), $(document).on("submit", "#form-create", function() {
                    return Page.submitForm($("#form-create"), null, null), !1

                }), $(document).on("click", "#add-btn-refund", function() {
                    Page.add_refund()

                }), $(document).on("submit", "#form-edit", function() {
                    var a = $(this).find(".submit").val();
                    return Page.submitForm($("#form-edit"), a), !1

                }),$(document).on("click", ".btn-edit", function() {
                    var e = $(this).data("id");
                    Page.edit(e)

                }),$(document).on("click", ".btn-print", function() {
                    var e = $(this).data("id");
                    Page.print(e)
                }),$(document).on("click", ".btn-export", function() {
                    Page.export()
                })
            },
            submitForm: function(e, a) {
                if (confirm('Anda Yakin ?')) {
                    var i = e.find(".submit");
                    var file_data = $('#path_bukti_bayar').prop('files')[0];
                    var form = $('form')[0];
                    var form_data = new FormData(form);
                    form_data.append('file', file_data);
                    
                    if (Helper.validateForm(e, {
                            id_pendaftar: {
                                required: !0
                            },
                            biaya: {
                                required: !0
                            },
                            ket: {
                                required: !0
                            },
                            alasan: {
                                required: !0
                            },
                            biaya: {
                                required: !0
                            },
                            path_bukti_bayar: {
                                required: !0
                            },
                        }), e.valid()) {
                        var t = Helper.baseUrl("refund/add");
                        a && (t = Helper.baseUrl("refund/edit")), Helper.blockElement(e.parent()), i.attr("disabled", !0), 
                            $.ajax({
                                url: t, // point to server-side PHP script
                                dataType: 'json',  // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: form_data,
                                type: 'POST',
                                error: function(t) {
                                swal(a.status.toString(), a.statusText, "error")
                                }
                            })

                            .error(function(a){Helper.unblockElement(e.parent()), i.attr("disabled", !1)}).done(function(a) 
                            {
                                a.status ? (e.parent().parent().find(".close").click(), 
                                swal(a.action, a.message, "success"), 
                                grid.getDataTable(tableID).ajax.reload()) : (i.attr("disabled", !1), 
                                Helper.unblockElement(e.parent()), swal({
                                title: a.action,
                                text: a.message,
                                type: "error",
                                html: !0
                            }))
                        })
                } else {
                    alert("Data Belum Lengkap");
                }
                }
            },
            add: function() {

                var modal = Helper.loadModal('lg');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Add Data Reschedule');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('refund/load_add_form'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    var i = null;
                    Helper.datePicker(i);
                    Helper.selectField($(".jadwal"), "Pilih");
                    Helper.selectField($(".paket"), "Pilih paket");
                    Helper.selectField($(".pilih"), "Pilih");
                    Page.getpaket();
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            add_refund: function() {

                var modal = Helper.loadModal('lg');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Add Data Refund');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('refund/load_add_form_refund'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    var i = null;
                    Helper.datePicker(i);
                    Helper.selectField($(".jadwal"), "Pilih");
                    Helper.selectField($(".paket"), "Pilih paket");
                    Helper.selectField($(".pilih"), "Pilih");
                    Page.getpaket();
                    Helper.unblockElement($(modalBody));
                    
                });
            },
            edit: function(e) {
                var a = Helper.loadModal("lg"),
                    i = a.find(".modal-body"),
                    t = a.find(".modal-title");
                t.text("Edit Data refund"), Helper.blockElement($(i)), Helper.ajax(Helper.baseUrl("refund/load_edit_form"), "get", "html", {
                    id:e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    i.html(e), Helper.selectField($(".jenis_travel"), "Pilih jenis travel");
                    Helper.selectField($(".jadwal"), "Pilih");
                    Helper.selectField($(".paket"), "Pilih paket");
                    Helper.selectField($(".pilihbanyak"), "Pilih");
                    Helper.selectField($(".cabang"), "Pilih");
                    Helper.selectField($(".pilih"), "Pilih");
                    Page.getpaket();
                    var a = null;
                    Helper.datePicker(a), Helper.unblockElement($(i));
                })
            },
            detail: function(e) {
                var a = Helper.loadModal("lg"),
                    t = a.find(".modal-body"),
                    i = a.find(".modal-title");
                i.text("Detail"), Helper.blockElement($(t)), Helper.ajax(Helper.baseUrl("refund/load_detail"), "get", "html", {
                    id: e
                }).error(function(e) {
                    a.find(".close").click()
                }).done(function(e) {
                    t.html(e), Helper.unblockElement($(t))
                })
            },

            getpaket: function() {

                    $('#id_registrsi').change(function(){
                        var id=$(this).find(":selected").val();
                        $.ajax({
                            url : Helper.baseUrl("refund/get_paket_where"),
                            method : "POST",
                            data : {id: id},
                            async : false,
                            dataType : 'json',
                            success: function(data){
                                $(".paket_lama").val(data.nm_paket+' | '+data.nm_jadwal);
                                $(".h_").val('- '+data.h_min+' Hari');
                                var h_ = data.h_min,
                                    jenis = data.jenis_travel, 
                                    potongan = 0,
                                    jenis_refund = document.getElementById('jenis').value;


                                $.ajax({
                                    url : Helper.baseUrl("refund/cek_payment"),
                                    method : "POST",
                                    data : {id: id},
                                    async : false,
                                    dataType : 'json',
                                    success: function(data){
                                        var total_biaya = data.total_biaya,
                                            id_paket = data.id_paket,
                                            penalti = 0;
                                            refund = 0;

                                        $(".id_paket_lama").val(id_paket);
                                        $(".id_paket_baru").val(id_paket);
                                        $(".money").autoNumeric("init", {
                                            aSep: ",",
                                            aDec: ".",
                                            aSign: "",
                                            wEmpty: "zero",
                                            mDec: "0"
                                        });
                                        $(".total_biaya").autoNumeric("set", 0);
                                        $(".telah_bayar").autoNumeric("set", 0);
                                        $(".refund").autoNumeric("set", 0);
                                        $(".penalti").autoNumeric("set", 0);

                                        if (data.telah_bayar != null) { $(".telah_bayar").autoNumeric("set", data.telah_bayar); } else { $(".telah_bayar").val(0) }
                                        $(".total_biaya").autoNumeric("set", data.total_biaya);

                                        if (jenis_refund == 'Refund') {
                                            if (h_ >30 ) {
                                                if (jenis == 'HAJI KHUSUS KUOTA' || jenis == 'HAJI KHUSU NON KUOTA' || jenis == 'UMRAH' || jenis == 'TOUR/WISATA') {
                                                    penalti = 5000000;
                                                    refund = data.telah_bayar - penalti;
                                                    if (penalti > data.telah_bayar) { 
                                                        $(".penalti").autoNumeric("set", data.telah_bayar); 
                                                        $(".refund").autoNumeric("set", data.telah_bayar); 
                                                    } else { 
                                                        $(".penalti").autoNumeric("set",penalti); 
                                                        $(".refund").autoNumeric("set",refund); 
                                                    }
                                                }
                                            } else if (h_ <=30 && h_ >14) {
                                                if (jenis == 'HAJI KHUSUS KUOTA' || jenis == 'HAJI KHUSU NON KUOTA') {
                                                    penalti = (total_biaya/100)*50;
                                                    refund = data.telah_bayar - penalti;
                                                    if (penalti > data.telah_bayar) { 
                                                        $(".penalti").autoNumeric("set", data.telah_bayar); 
                                                        $(".refund").autoNumeric("set", data.telah_bayar); 
                                                    } else { 
                                                        $(".penalti").autoNumeric("set",penalti);
                                                        $(".refund").autoNumeric("set",refund); 
                                                    }
                                                }
                                                if (jenis == 'UMRAH') {
                                                    penalti = (total_biaya/100)*75;
                                                    refund = data.telah_bayar - penalti;
                                                    if (penalti > data.telah_bayar) { 
                                                        $(".penalti").autoNumeric("set", data.telah_bayar); 
                                                        $(".refund").autoNumeric("set", data.telah_bayar); 
                                                    } else { 
                                                        $(".penalti").autoNumeric("set",penalti);
                                                        $(".refund").autoNumeric("set",refund); 
                                                    }
                                                }
                                                if (jenis == 'TOUR/WISATA') {
                                                    penalti = (total_biaya/100)*50;
                                                    refund = data.telah_bayar - penalti;
                                                    if (penalti > data.telah_bayar) { 
                                                        $(".penalti").autoNumeric("set", data.telah_bayar); 
                                                        $(".refund").autoNumeric("set", data.telah_bayar); 
                                                    } else { 
                                                        $(".penalti").autoNumeric("set",penalti);
                                                        $(".refund").autoNumeric("set",refund); 
                                                    }
                                                }
                                            } else if (h_ <= 14) { 
                                                if (jenis == 'HAJI KHUSUS KUOTA' || jenis == 'HAJI KHUSU NON KUOTA' || jenis == 'UMRAH' || jenis == 'TOUR/WISATA') {
                                                    penalti = total_biaya;
                                                    refund = 0;
                                                    $(".refund").autoNumeric("set", refund); 
                                                    $(".penalti").autoNumeric("set", penalti);
                                                }
                                            }
                                        } else if (jenis_refund == 'Reschedule') {
                                            if (h_ >30 ) {
                                                penalti = 0;
                                                $(".penalti").autoNumeric("set", penalti);
                                            } else if (h_ <= 30  && h_ > 21) {
                                                penalti = (total_biaya/100)*10;
                                                $(".penalti").autoNumeric("set",penalti);
                                            }
                                            else if (h_ <= 21) {
                                                penalti = (total_biaya/100)*50;
                                                $(".penalti").autoNumeric("set",penalti);
                                            }                                                
                                        }

                                    }
                                    
                                });

                                
                                
// di bawa 1 blan, 10 %
// di bawah 3 minggu 50%


                            }
                        });

                    });

                    $('#paket').change(function(){
                        var id=$(this).find(":selected").val();
                        $.ajax({
                            url : Helper.baseUrl("refund/get_paket_baru"),
                            method : "POST",
                            data : {id: id},
                            async : false,
                            dataType : 'json',
                            success: function(data){
                                var paket_lama = document.getElementById('paket_lama').value;
                                $(".ket").val('Reschedule dari '+paket_lama+' ke '+data.nm_paket+' | '+data.nm_jadwal+'');

                            }
                        });
                    });
                    
            },
            export: function() {
            
                var modal = Helper.loadModal('small');
                var modalBody = modal.find('.modal-body');
                var modalTitle = modal.find('.modal-title');

                modalTitle.text('Export Based On Schedule');
                Helper.blockElement($(modalBody));
                Helper.ajax(Helper.baseUrl('payment/load_export'), 'get', 'html')
                
                .error(function(err) {
                    modal.find('.close').click();
                })

                .done(function(data) {
                    modalBody.html(data);
                    Helper.datePicker();
                    Helper.selectField($(".jadwal"), "Select Schedule");
                    Helper.unblockElement($(modalBody));
                    
                });
            },

            // print: function(e) {
            //     window.location.href = Helper.baseUrl("payment/cetak/") + e;
            // }
            
        }
    }();