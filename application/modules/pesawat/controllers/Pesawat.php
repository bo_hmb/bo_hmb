<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Pesawat extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Pesawat';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_pesawat');
            // $this->output->enable_profiler(TRUE);
        }

        public function index()
        {
            // Page components
            $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Pesawat';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function get_where($where) {
            $query = $this->M_pesawat->get_pesawat_where($where);
            return $query;
        }

    }
?>
