<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_pesawat extends CI_Model 
	{
		

		var $table = 'pesawat';
		
	    var $column_order = array('marketing.id','nik','nm_marketing','nm_cabang','level','marketing.active');
	    var $column_search = array('marketing.id','nik','nm_marketing','nm_cabang','level','marketing.active');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_select()
		{
			$results = array();
			$query = $this->db->query('SELECT marketing.id,marketing.nik,marketing.nm_marketing,
							marketing.kd_cabang,cabang.nm_cabang,marketing.id_level,
							level_marketing.level FROM marketing
							INNER JOIN cabang ON marketing.kd_cabang = cabang.kd_cabang
							INNER JOIN level_marketing ON marketing.id_level = level_marketing.id');
			return $query->result();	
		}

		public function get_where($where) {
			$this->db->select(' marketing.id,
								marketing.nik,
								marketing.nm_marketing,
								marketing.kd_cabang,
								cabang.nm_cabang,
								marketing.id_level,
								level_marketing.level,
								marketing.banefit,
								marketing.active ');
	    	$this->db->where($where);
	        $this->db->from($this->table);
	        $this->db->join($this->cabang, ''.$this->table.'.kd_cabang = '.$this->table.'.kd_cabang');
	    	$this->db->join($this->level_marketing, ''.$this->table.'.id_level = '.$this->level_marketing.'.id');
	        $query = $this->db->get();
	        return $query;

			// $table = $this->table;
			// $this->db->where($where);
			// $query=$this->db->get($table);
			// return $query;
		}

		public function get_pesawat_where($where) {
			$table = $this->table;
            $this->db->where($where);
            $query=$this->db->get($table);
            return $query;
        }

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->select(' marketing.id,
								marketing.nik,
								marketing.nm_marketing,
								marketing.kd_cabang,
								cabang.nm_cabang,
								marketing.id_level,
								level_marketing.level,
								marketing.banefit,
								marketing.active ');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        //join INNER JOIN level_marketing ON marketing.id_level = level_marketing.id');
	    	$this->db->join($this->cabang, ''.$this->cabang.'.kd_cabang = '.$this->table.'.kd_cabang');
	    	$this->db->join($this->level_marketing, ''.$this->level_marketing.'.id = '.$this->table.'.id_level');

	    	// $this->db->join($this->ind_regencies, ''.$this->ind_regencies.'.id_kota = '.$this->table.'.id_kota');


	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
