<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nik" placeholder="Masukkan NIK" value="<?php echo $main->nik?>">
                    <label for="form_control_1">NIK
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan NIK</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_marketing" placeholder="Masukkan Nama Marketing" value="<?php echo $main->nm_marketing?>">
                    <label for="form_control_1">Nama Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Marketing</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <div class="fg-line">
                    <label>Work Area</label>
                    <select id="kd_cabang" name="kd_cabang" class="form-control" data-placeholder="Pilih Office">
                        <option disabled selected></option>
                        <?php
                        foreach ($workarea as $row) { ?>

                        <option <?php echo ($row->kd_cabang === $main->kd_cabang) ? 'selected' : ''; ?> value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>
                        
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            </div>
            <div class="col-md-6">            
                <div class="form-group">
                <div class="fg-line">
                    <label>Level</label>
                    <select id="id_level" name="id_level" class="form-control" data-placeholder="Pilih Level Marketing">
                        <option disabled selected></option>
                        <?php
                        foreach ($level as $row) { ?>

                        <option <?php echo ($row->id === $main->id_level) ? 'selected' : ''; ?> value="<?php echo $row->id; ?>"><?php echo $row->level; ?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>

            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>