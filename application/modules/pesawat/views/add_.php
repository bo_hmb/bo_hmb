<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nik" placeholder="Masukkan NIK">
                    <label for="form_control_1">NIK
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan NIK</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_marketing" placeholder="Masukkan Nama Marketing">
                    <label for="form_control_1">Nama Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Marketing</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <div class="fg-line">
                    <label>Work Area</label>
                    <select id="kd_cabang" name="kd_cabang" class="form-control select" data-placeholder="Pilih Office">
                        <option disabled selected></option>
                        <?php
                        foreach ($workarea as $row) { ?>

                        <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>
                        
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                <div class="fg-line">
                    <label>Work Area</label>
                    <select id="id_level" name="id_level" class="form-control select" data-placeholder="Pilih Level Marketing">
                        <option disabled selected></option>
                        <?php
                        foreach ($level as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->level; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>

            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>