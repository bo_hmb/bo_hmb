            <form role="form" action="#" class="form-horizontal" id="form-create">
                <input type="text" class="hidden" name="id_movement" value="<?php echo $movement?>">
                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:10%">No</th>
                                    <th>Masukkan Jam</th>
                                    <th>Masukkan Agenda</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>                            
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i>Baris Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     

