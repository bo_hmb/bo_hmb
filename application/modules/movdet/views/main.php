<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Mainivest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Itinerary Detail</span>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Agenda Itinerary <?php  echo date('d-m-Y', strtotime($movement->tgl)) ?></span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            
                        </div>
                    </div>
                     <script>
                    idmovement = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="2%">No</th>
                                        <!-- <th width="10%">id movement</th> -->
                                        <th width="1%">Aksi</th>
                                        <th width="10%">Jam</th>
                                        <th width="10%">Agenda</th>
                                    </tr>
                                    <tr role="row" class="filter">

                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="id_movement"> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="jam"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="agenda"> </td>
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>