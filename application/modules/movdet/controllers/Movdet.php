<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Movdet extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Itinerary';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_movdet');
            // $this->load->model('M_movement');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function movement()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Agenda Itinerary';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['idmovement'] = $this->uri->segment(3);
            $this->data['movement'] = $this->M_movdet->get_movement(array('id' => $this->uri->segment(3)))->row();

            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
			// $id_movement = $this->input->post('id_movement',TRUE);
            $jam = $this->input->post('jam',TRUE);
            $agenda = $this->input->post('agenda',TRUE);
            
            
            $cols = array();
			// if (!empty($id_movement)) { $cols['id_movement'] = $id_movement; }
			if (!empty($jam)) { $cols['jam'] = $jam; }
			if (!empty($agenda)) { $cols['agenda'] = $agenda; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1' AND id_movement = '".$id."'";              
            } else {
             $where = "active = '1' AND id_movement = '".$id."'";              
            }


	        $list = $this->M_movdet->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_movdet->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';

                $records["data"][] = array(
                    $no,                    
                    //$r->id_movement,
                    $btn_action,
                    $r->jam,
                    $r->agenda,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
			$data['title'] = 'Tambah Data Movedet';
            $data['movement'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			// $this->validateInput();
            $this->ajaxRequest();

            $cek = count($this->input->post('jam'));

            for ($i=0; $i < $cek; $i++) { 
    			$data = array(
    				'id_movement' => $this->input->post('id_movement',TRUE),
                    'jam' => $this->input->post('jam',TRUE)[$i],
                    'agenda' => $this->input->post('agenda',TRUE)[$i],
                    'active' => '1',
    				);

    			$query = $this->M_movdet->_insert($data);
            }
            $query = 'true';

            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_movdet->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                // 'id_movement' => $this->input->post('id_movement',TRUE),
                'jam' => $this->input->post('jam',TRUE),
                'agenda' => $this->input->post('agenda',TRUE),
            );

            $query = $this->M_movdet->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_movdet->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            //id_movement,jam,agenda
            $this->form_validation->set_rules('id_movement', 'nm hotel', 'trim|required');
            $this->form_validation->set_rules('jam', 'kota lokasi', 'trim|required');
            $this->form_validation->set_rules('agenda', 'alamat', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
