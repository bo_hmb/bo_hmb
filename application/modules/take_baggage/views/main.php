<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Handling</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Take Baggage</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Take Baggage</span>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <!-- <th width="15%">Jenis Travel</th> -->
                                        <th width="15%">Paket</th>
                                        <th width="15%">No. Pendaftaran</th>
                                        <th width="15%">Tanggal Keberangkatan</th>
                                        <th width="10%">No. KTP</th>
                                        <th width="10%">Nama Sesuai Pasport</th>
                                        
                                        <th width="15%">Total Biaya</th>
                                        <th width="15%">Status</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <!-- <td>
                                            <select class="form-control form-filter input-sm"name="jenis_travel">
                                                 <option></option>
                                                <option value="UMRAH">UMRAH</option>
                                                <option value="HAJI KHUSUS KUOTA">HAJI KHUSUS KUOTA</option>
                                                <option value="HAJI KHUSUS NON KUOTA">HAJI KHUSUS NON KUOTA</option>
                                                <option value="TOUR/WISATA">TOUR/WISATA</option>                        
                                            </select>
                                        </td> -->
                                        <td>                                            
                                            <select class="form-control form-filter input-sm" name="nm_paket">
                                                <option value=""></option>                                                
                                                <?php 
                                                    $packet = Modules::run('packet/get_where', array('paket.active' => '1','paket.kd_cabang'=>$this->kode_cabang))->result();
                                                    foreach ($packet as $row) { ?> 
                                                    <option value="<?php echo $row->nm_paket; ?>"><?php echo $row->nm_paket; ?> | <?php echo $row->nm_jadwal; ?></option>    
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_pendaftaran"> </td>                                         
                                        <td><select name="tgl_keberangkatan" class="form-control form-filter input-sm">
                                                <option value=""></option>                                                
                                                <?php
                                                 $schedule= Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
                                                foreach ($schedule as $row) { ?>
                                                <option value="<?php echo $row->tgl_keberangkatan; ?>"><?php echo $x=date('d-m-Y', strtotime($row->tgl_keberangkatan)); ?></option>   
                                                <?php
                                                }
                                                ?>
                                            </select> 
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_ktp"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        
                                        <td></td>
                                        <td>
                                            <!-- <select name="status" class="form-control form-filter input-sm">
                                                <option value=""></option>
                                                <option value="Belum Lunas">Belum Lunas</option>
                                                <option value="Registrasi">Registrasi</option>
                                                <option value="Lunas">Lunas</option>
                                            </select>   -->
                                        </td>
                                        
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>