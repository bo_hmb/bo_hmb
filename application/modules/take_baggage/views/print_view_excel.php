<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=data_jamaah.xls");
?>

<html>
<head>
    <title>Excel</title>

    <style>
        table{
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{

        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<table border="1">
    <tr>
        <th>NO JAMAAH</th>
        <th>NIK (*)</th>
        <th>JENIS IDENTITAS (*)</th>
        <th>NAMA JAMAAH  (*)</th>
        <th>JENIS KELAMIN (*)</th>
        <th>TEMPAT LAHIR (*)</th>
        <th>TGL LAHIR (*)</th>
        <th>STATUS MENIKAH (*)</th>
        <th>NO TELP/HP (*)</th>
        <th>EMAIL</th>
        <th>PEKERJAAN</th>
        <th>PENDIDIKAN TERAKHIR</th>
        <th>NO PASPOR</th>
        <th>NAMA PASPOR</th>
        <th>TGL DIKELUARKA</th>
        <th>TGL HABIS</th>
        <th>KOTA PASPOR</th>
        <th>PILIHAN KAMAR (*)</th>
    </tr>
    <?php $no = 1;
    foreach ($main as $row) { ?>
        <tr style="border:0px solid #000;">
            <td><?php echo $no?></td>
            <td><?php echo $row->no_ktp ?></td>
            <td><?php echo 'KTP' ?></td>
            <td><?php echo $row->nm_lengkap ?></td>
            <td><?php echo $row->jk ?></td>
            <td><?php echo $row->tmp_lahir ?></td>
            <td><?php echo date('Y-m-d', strtotime($row->tgl_lahir))?></td>
            <td><?php echo $row->status_nikah2 ?></td>
            <td><?php echo $row->hp ?></td>
            <td><?php echo 'developer@sipatuh.id' ?></td>
            <td><?php echo $row->pekerjaan ?></td>
            <td><?php echo $row->pendidikan_terakhir ?></td>
            <td><?php echo $row->no_pasport ?></td>
            <td><?php echo $row->nm_lengkap ?></td>
            <td><?php echo date('Y-m-d', strtotime($row->create_pasport))?></td>
            <td><?php echo date('Y-m-d', strtotime($row->exp_pasport))?></td>
            <td><?php echo $row->kota_paspor ?></td>
            <td><?php echo $row->jenis_kamar2 ?></td>
        </tr>
    <?php $no++;  }?>
</table>

</body>
</html>