<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <input type="hidden" id="no_pendaftaran" name="no_pendaftaran" value="<?php echo $main->no_pendaftaran ?>">
        <!-- <span class="caption-subject font-dark bold uppercase">List Baggage</span> -->
        <br>
        <div>
            <div class="col-md-12">
                <div class="portlet light ">
                    <!-- STAT -->
                    <div class="row list-separated profile-stat">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="profile-stat-text">No. KTP : <?php echo $main->no_ktp ?> </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="profile-stat-text">Nama Lengkap : <?php echo $main->nm_lengkap ?></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:30%">Nama Barang</th>
                            <th>Harga</th>
                            <th style="width:10%">Qty</th>
                            <th>Free</th>
                            <th>Sub Harga</th>
                            <th>Take</th>
                        </tr>
                    </thead>
                    <tbody id="container">
                        
                            <?php 
                            foreach ($barang as $row) { ?>

                            <tr class="baris form-create-barang" id="form-create-barang" ">    
                                <td align="center">
                                    <input id="id_barang" name="id_barang[]" type="hidden" value="<?php echo $row->id_barang ?>">
                                    <select id="barang" class="form-control barang" name="nm_barang[]" readonly>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->nm_barang ?></option>
                                    </select>

                                </td>
                                <td align="center">
                                    <input id="harga_jual" class="form-control harga_jual money" name="harga_jual[]" type="text" value="<?php echo $row->harga_satuan ?>" readonly>
                                </td>
                                <td align="center">
                                    <input id="jml" name="jml[]" class="form-control jml money" type="text" placeholder="0" value="<?php echo $row->jml ?>" readonly>
                                </td>
                                <td align="center">
                                    <input type="checkbox" id="free[]" class="free" <?php if ($row->free==='1') echo "checked"?> value="1" disabled='disabled' name="free[]">
                                    <input type="hidden" id="sts_free[]" value="<?php echo $row->free ?>" name="sts_free[]">
                                </td>

                                <td align="center">
                                    <input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" readonly value="<?php echo $row->sub_harga ?>" readonly>
                                </td>
                            
                                <td align="center">
                                    <input type="checkbox" id="take[]" class="take" <?php if ($row->take==='1') echo "checked"?> <?php if ($row->take==='1') echo "disabled='disabled'"?> value="1" name="take[]">
                                </td>
                                <!-- <td style="border:0px;"> -->
                                    <?php 
                                    $barang = $this->M_barang_order->get_barang_where(array('id' => $row->id_barang))->row();
                                     ?>
                                    <input type="hidden" id="cekstok[]" class="cekstok" value="<?php echo $barang->stok ?>" name="cekstok[]">
                                    <input type="hidden" id="dis[]" class="dis" value="<?php if ($row->take==='1') echo "1"; else echo "0";?>" name="dis[]">
                                <!-- </td> -->
                                <input id="rows" name="rows[]" value="" type="hidden">
                            </td>
                                
                            <?php } ?>
                        </tr>
                    </tbody>
                    <tbody>
                        <!-- <tr>
                            <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                            <td align="center">
                                <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" readonly="" value="<?php echo $main->biaya_order_barang ?>">
                            </td>
                            <td></td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>" 
                    <?php 
                    if (array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Admin',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) echo ""; else echo "disabled=";
                    ?> ><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>

<script>
function generate() {
  document.getElementById('no_pendaftaran').value = "<?php echo $no_pendaftaran; ?>";
}
</script>