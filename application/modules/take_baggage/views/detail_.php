<div class="row">
	<div class="col-md-4">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?php echo base_url() ?>upload/<?php echo $main->path_foto; ?>" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
	</div>
        <div class="col-md-8">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">No. KTP : <?php echo strtoupper($main->no_ktp); ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">Nama Lengkap : <?php echo strtoupper($main->nm_lengkap); ?> </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                   <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">Tggl. Lahir : <?php echo date("d-m-Y",strtotime($main->tgl_lahir)); ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">Tggl. Pendaftaran : <?php echo date("d-m-Y",strtotime($main->tgl_pendaftaran)); ?> </div>
                    </div>
                </div>


            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <br>
        <div class="col-md-8">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">No. Pendaftaran : <?php echo $main->no_pendaftaran; ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">Jenis Travel : <?php echo $main->jenis_travel ?> </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="profile-stat-text">Nama Paket : <?php echo $main->nm_paket; ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">                        
                        <div class="profile-stat-text">Tggl. Berangkat : <?php echo date("d-m-Y",strtotime($main->tgl_keberangkatan)); ?> </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Biaya Mahram : Rp.<?php echo $main->biaya_mahram; ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Biaya Upgr. Hotel *4 : Rp.<?php echo $main->biaya_up_hotel_b4 ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Biaya Upgr. Kamar : Rp.<?php echo $main->biaya_upgrade_kamar ?> </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Biaya Progresif : Rp.<?php echo $main->biaya_progresif; ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Biaya Upgr. Hotel *5 : Rp.<?php echo $main->biaya_up_hotel_b5; ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text strong">Biaya Order Barang : Rp.<?php echo $main->biaya_order_barang; ?> </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Total Biaya : <?php echo $main->total_biaya; ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Telah Bayar : <?php echo $main->telah_bayar; ?> </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="profile-stat-text">Marketing : <?php echo $main->nm_marketing; ?> </div>
                    </div>
                </div>
            </div>
        </div>

</div>
        
   