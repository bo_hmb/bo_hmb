<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_barang_order extends CI_Model 
	{
		

		var $table = 'jual_barang';
		var $barang = 'barang';

	    var $column_order = array('id');
	    var $column_search = array('id');
	    var $order = array('id' => 'desc'); 

		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_barang_where($where) {
			$table = $this->barang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		 public function get_order_formulir($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT jual_barang.id,jual_barang.no_pendaftaran,barang.id AS id_barang,barang.nm_barang,
										jual_barang.harga_satuan,jual_barang.jml,jual_barang.free,jual_barang.sub_harga,jual_barang.take
										FROM jual_barang INNER JOIN barang ON jual_barang.id_barang = barang.id
										WHERE no_pendaftaran="'.$id.'"');
			return $query->result();	
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function delete_by_id($id)
		{
			$this->db->where('no_pendaftaran', $id);
			$this->db->delete($this->table);
		}

		function _insert_batch($result = array())
		{
			$table = $this->table;
			$total_array = count($result);
			 
			if($total_array != 0)
			{
				$this->db->insert_batch($table, $result);
			}
		}


		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
			

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	// $this->db->join($this->tablejoin1, ''.$this->tablejoin1.'.id = '.$this->table.'.id_barang');
	    	// $this->db->join($this->tablejoin2, ''.$this->tablejoin2.'.id = '.$this->table.'.id_beli_barang');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function take($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT no_pendaftaran,
										(SELECT count(id) FROM jual_barang WHERE take=1 AND no_pendaftaran="'.$id.'") AS sudah, 
										(SELECT count(id) FROM jual_barang WHERE take=0 AND no_pendaftaran="'.$id.'") AS belum
										FROM jual_barang 
										WHERE no_pendaftaran="'.$id.'"
										GROUP BY no_pendaftaran');
			return $query;	
		}
	}
