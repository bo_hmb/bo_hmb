<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_registrasi extends CI_Model 
	{
		

		var $table = 'pendaftar';
		var $formulir = 'formulir';
		var $paket = 'paket';
		var $jadwal = 'jadwal';
		var $marketing = 'marketing';


	    var $column_order = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $column_search = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $order = 'RIGHT(no_pendaftaran,3) DESC'; 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_detail($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pendaftar.id,
										pendaftar.id_formulir,
										pendaftar.no_pendaftaran,
										paket.jenis_travel,
										pendaftar.id_paket,
										pendaftar.potongan_paket,
										paket.nm_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										jadwal.lama_hari,
										formulir.no_ktp,
										formulir.nm_lengkap,
										formulir.tgl_lahir,
										TIMESTAMPDIFF(MONTH, tgl_lahir, NOW()) AS age,
										formulir.path_foto,
										pendaftar.tgl_pendaftaran,
										pendaftar.nm_mahram,
										pendaftar.hp_mahram,
										pendaftar.hub_mahram,
										pendaftar.keluarga_terdekat,
										pendaftar.hubungan,
										pendaftar.biaya_mahram,
										pendaftar.biaya_progresif,										
										pendaftar.jenis_kamar,
										pendaftar.jenis_kamar_madinah,
										pendaftar.biaya_upgrade_kamar,
										pendaftar.status_upgrade_kamar,
										pendaftar.biaya_upgrade_kamar_madinah,
										pendaftar.status_upgrade_kamar_madinah,
										pendaftar.biaya_order_barang,
										pendaftar.add_on,
										pendaftar.fc_ktp,
										pendaftar.fc_aktakelahiran,
										pendaftar.fc_bukunikah,
										pendaftar.fc_kk,
										pendaftar.buku_faksin_asli,
										pendaftar.pasport_asli,
										pendaftar.pas_photo,
										((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
										(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+
										(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah)+
										pendaftar.biaya_order_barang+pendaftar.add_on) AS	total_biaya,
										(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id) as telah_bayar,
										formulir.id_marketing,
										marketing.nm_marketing,
									
										pendaftar.hotel_request_makkah,
										pendaftar.hotel_request_madinah,
										pendaftar.biaya_up_hotel_makkah,
										pendaftar.status_up_hotel_makkah,
										pendaftar.biaya_up_hotel_madinah,
										pendaftar.status_up_hotel_madinah,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE pendaftar.id="'.$id.'"');
	        return $query->row();
		}

		public function get_export_excel() 
		{
			$results = array();
	        $query = $this->db->query(' SELECT
										formulir.no_ktp,
										formulir.nm_lengkap,
										formulir.jk,
										formulir.tmp_lahir,
										formulir.tgl_lahir,
										formulir.status_nikah,
										( case 
												WHEN formulir.status_nikah = "Menikah"	THEN "1"
												WHEN formulir.status_nikah = "Belum Menikah" THEN "2"
												WHEN formulir.status_nikah = "Janda/Duda"	THEN "3"
											END
										) AS status_nikah2,
										formulir.hp,
										formulir.pekerjaan,
										formulir.pendidikan_terakhir,
										formulir.no_pasport,
										formulir.create_pasport,
										formulir.exp_pasport,
										formulir.kota_paspor,
										pendaftar.jenis_kamar,								
										( case 
												WHEN pendaftar.jenis_kamar = "Double"	THEN "2"
												WHEN pendaftar.jenis_kamar = "Triple"	THEN "3"
												WHEN pendaftar.jenis_kamar = "Quad"	THEN "4"
												WHEN pendaftar.jenis_kamar = "Quint"	THEN "5"
											END
										) AS jenis_kamar2
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										');
	        return $query->result();
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	// (SELECT nm_hotel FROM hotel where id=pendaftar.id_up_hotel_b4) as hotel_b4,
						// 		pendaftar.id_up_hotel_b4,
						// 		pendaftar.biaya_up_hotel_b4,
						// 		(SELECT nm_hotel FROM hotel where id=pendaftar.id_up_hotel_b5) as hotel_b5,
						// 		pendaftar.id_up_hotel_b5,
						// 		pendaftar.biaya_up_hotel_b5,
	    	$this->db->select(' pendaftar.id,
								pendaftar.id_formulir,
								pendaftar.no_pendaftaran,
								pendaftar.kd_office,
								paket.jenis_travel,
								pendaftar.id_paket,
								paket.nm_paket,
								jadwal.nm_jadwal,
								jadwal.tgl_keberangkatan,
								formulir.no_ktp,
								formulir.nm_lengkap,
								formulir.tgl_lahir,
								pendaftar.tgl_pendaftaran,
								pendaftar.nm_mahram,
								pendaftar.hp_mahram,
								pendaftar.hub_mahram,
								pendaftar.keluarga_terdekat,
								pendaftar.hubungan,
								pendaftar.biaya_mahram,
								pendaftar.biaya_progresif,
								pendaftar.jenis_kamar,
								pendaftar.jenis_kamar_madinah,
								pendaftar.biaya_upgrade_kamar,
								pendaftar.biaya_order_barang,
								pendaftar.fc_ktp,
								pendaftar.fc_aktakelahiran,
								pendaftar.fc_bukunikah,
								pendaftar.fc_kk,
								pendaftar.buku_faksin_asli,
								pendaftar.pasport_asli,
								pendaftar.pas_photo,
								((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
										((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
										pendaftar.biaya_order_barang+pendaftar.add_on) AS total_biaya,
								IF((SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)<> null,
											(case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) = 0	THEN "Registrasi"
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on + (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Belum Lunas"
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+ (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Lunas"
												ELSE "Registrasi"
											END
										),
										( case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) = 0	THEN "Registrasi"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Belum Lunas"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Lunas"
												ELSE "Registrasi"
											END
										))	AS status,
								pendaftar.active ');
	    	$this->db->where($where);
	        $this->db->from($this->table);
	        // $this->db->order_by('');

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by($order);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->formulir, ''.$this->table.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->paket, ''.$this->table.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	    	//$this->db->join($this->marketing, ''.$this->formulir.'.id_marketing = '.$this->marketing.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {
	    	$this->db->where($where);
	        $this->db->from($this->table);
			$this->db->join('formulir','pendaftar.id_formulir=formulir.id');	        
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_formulir_where($where) {
			$table = $this->formulir;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_umur($id) {
			$results = array();
	        $query = $this->db->query('SELECT id,nm_lengkap,tgl_lahir, (YEAR(CURDATE())-YEAR(tgl_lahir)) 
	        						   AS age_yeard,TIMESTAMPDIFF(MONTH, tgl_lahir, NOW()) AS age
	        						   FROM formulir WHERE id='.$id.'');
	        return $query->row();
		}
	}
