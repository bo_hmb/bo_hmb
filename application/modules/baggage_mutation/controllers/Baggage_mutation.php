<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Baggage_mutation extends AdminController {

        public function __construct()
        {
            parent::__construct();

            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Barang Mutasi';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_baggage_mutation');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Barang Mutasi';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {

            $no_mutasi = $this->input->post('no_mutasi',TRUE);
            $tgl_mutasi = $this->input->post('tgl_mutasi',TRUE);
            $kd_office = $this->input->post('kd_office',TRUE);
            $kd_tujuan = $this->input->post('kd_tujuan',TRUE);
            $app = $this->input->post('app',TRUE);

			$cols = array();
			if (!empty($no_mutasi)) { $cols['no_mutasi'] = $no_mutasi; }
			if (!empty($tgl_mutasi)) { $cols['tgl_mutasi'] = $tgl_mutasi; }
			if (!empty($kd_office)) { $cols['kd_office'] = $kd_office; }
			if (!empty($kd_tujuan)) { $cols['kd_tujuan'] = $kd_tujuan; }
            if (!empty($app)) { $cols['app'] = $app; }  

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "app = 'pending'";
            } else {
             $where = "app = 'pending'";
            }


	        $list = $this->M_baggage_mutation->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_baggage_mutation->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">';
                
                if(array_key_exists('Super Admin', $this->userLevel)) {
                $btn_action .= '<button type="button" class="btn btn-xs blue btn-outline btn-mutation tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-truck"></i> Konfirmasi</button>
                    <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->no_mutasi.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i> Cancel</button>';
                }

                $btn_action .= '</div>';

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->no_mutasi,
                    $r->tgl_mutasi,
                    $r->kd_office,
                    $r->kd_tujuan,
                    $r->app,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        public function load_mutation_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_baggage_mutation->get_where(array('id' => $id))->row();
            $data['main_his'] = $this->M_baggage_mutation->get_where_his(array('no_mutasi' => $data['main']->no_mutasi))->result();
            $data['office'] = Modules::run('workarea/get_workarea');
            return response($this->load->view('approve_mutation', $data, TRUE), 'html');
        }

        public function mutation()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            
            $cek = count($this->input->post('nm_barang'));

            for ($i=0; $i < $cek; $i++) { 

                $where = array(
                    'kd_office' => $this->input->post('kd_tujuan')[$i],
                    'nm_barang' => $this->input->post('nm_barang')[$i],
                    );

                $main = Modules::run('baggage/get_where', $where)->row();

                $data = array(
                    'stok' => $this->input->post('stok',TRUE)[$i] + $main->stok,
                );

                $query = Modules::run('baggage/get_update_stok', $where, $data);


            }

            $no_mutasi = $this->input->post('no_mutasi');

            $where_mutasi = array('no_mutasi' => $no_mutasi);

            $data_mutasi = array(
            'app' => 'approve',
            'user_update' => $this->currentUser->id,
            );

            $query_mutasi = $this->M_baggage_mutation->_update($where_mutasi, $data_mutasi);

            

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function delete()
        {
            $no_mutasi = $this->input->post('id');

            $main_his = $this->M_baggage_mutation->get_where_his(array('no_mutasi' => $no_mutasi))->result();


            foreach ($main_his as $row) {
                $where = array(
                    'id' => $row->id_barang,
                    'kd_office' => $row->kd_office,
                    );

                $main = Modules::run('baggage/get_where', $where)->row();

                $datastok = array(
                    'stok' => $row->stok + $main->stok,
                );

                $querystok = Modules::run('baggage/get_update_stok', $where, $datastok);
            }


            $data = array(
                'app' => 'cancel',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_baggage_mutation->_update(array('no_mutasi' => $no_mutasi), $data);


            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('nm_barang', 'nm barang', 'trim|required');
            $this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
            $this->form_validation->set_rules('untuk_jk', 'untuk jk', 'trim|required');
            $this->form_validation->set_rules('stok', 'stok', 'trim|required|numeric');
            $this->form_validation->set_rules('harga_beli', 'harga beli', 'trim|required|numeric');
            $this->form_validation->set_rules('harga_jual', 'harga jual', 'trim|required|numeric');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function get_where($where)
        {
            $query = $this->M_baggage_mutation->get_where($where);
            return $query;
        }

        public function get_update_stok($where, $data)
        {
            $query = $this->M_baggage_mutation->_update($where, $data);
            return $query;
        }

        public function get_barang()
        {
            $query = $this->M_baggage_mutation->get_where(array('active' => '1' ))->result();
                foreach ($query as $row){
                   $result[] = array(
                  'nm_barang' => $row->nm_barang,
                  'harga_beli' => $row->harga_beli,
                  'harga_jual' => $row->harga_jual,
                );
            }

            echo json_encode($result);
        }

        public function load_barang()
        {   
            $json = [];


            $this->load->database();

            if(!empty($this->input->get("q"))){

                $this->db->like('nm_barang', $this->input->get("q"));
                $this->db->where('kd_office', $this->kode_cabang);
                 $this->db->where('active', '1');
                $query = $this->db->select('id,nm_barang as text')
                            ->limit(10)
                            ->get("barang");
                $json = $query->result();
            }

            
            echo json_encode($json);

        }

        public function get_select_barang()
        {
            $id = $this->input->post("id");
            $data = Modules::run('baggage_mutation/get_where', array('active' => '1', 'id' => $id))->row();
            echo json_encode($data);
        }

    }
?>
