            <form role="form" action="#" class="form-horizontal" id="form-mutasi">
                <div class="form-group">
                    <label class="col-md-2 control-label">Lokasi Tujuan</label>
                    <div class="col-md-6">
                        <input type="text" name="lokasi_tujuan" value="<?php echo $main->kd_tujuan; ?>" placeholder="No Mutasi" class="form-control" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">No Mutasi</label>
                    <div class="col-md-6">
                        <input type="text" name="no_mutasi" value="<?php echo $main->no_mutasi; ?>" placeholder="No Mutasi" class="form-control" readonly/> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Mutasi</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium">
                            <input type="text" name="tgl_mutasi" class="form-control" placeholder="Tanggal" value="<?php echo $main->tgl_mutasi ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nama Barang</th>
                                    <th>Asal</th>
                                    <th>Tujuan</th>
                                    <th>Jumlah Mutasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($main_his as $row) { ?>
                                <tr>
                                    <td><input class="form-control" type="text" name="nm_barang[]" value="<?php echo $row->nm_barang; ?>" readonly></td>
                                    <td><input class="form-control" type="text" name="kd_office[]" value="<?php echo $row->kd_office; ?>" readonly></td>
                                    <td><input class="form-control" type="text" name="kd_tujuan[]" value="<?php echo $row->kd_tujuan; ?>" readonly></td>
                                    <td><input class="form-control" type="text" name="stok[]" value="<?php echo $row->jmlh; ?>" readonly></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline blue submit"><i class="fa fa-save"></i> Konfirmasi</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     