            <form role="form" action="#" class="form-horizontal" id="form-create">
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Jamaah</label>
                    <div class="col-md-6">
                        <select id="nm_jamaah" name="no_pendaftaran" class="form-control jadwal" data-placeholder="Pilih Jamaah">
                            <option disabled selected></option>
                            <option value="0"></option>
                            <?php
                            foreach ($jamaah as $row) { ?>
                            <option value="<?php echo $row->no_pendaftaran; ?>"><?php echo $row->nm_ktp.' / '.$row->nm_lengkap.' / '.$row->nm_jadwal; ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Keberangkatan</label>
                    <div class="col-md-6">
                        <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                            <option disabled selected></option>
                            <option></option>
                            <?php
                            foreach ($jamaah as $row) { ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Penjualan</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="tgl_jual" class="form-control" placeholder="Tanggal" value="<?php echo date('Y-m-d') ?>">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:150px">Nama Barang</th>
                            <th style="width:150px">Harga</th>
                            <th style="width:80px">Qty</th>
                            <th style="width:50px">Free</th>
                            <th style="width:150px">Sub Harga</th>
                            <th style="width:100px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="container">                        
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                            <td align="center">
                                <input type="text" name="total_harga" class="form-control total-bayar tot-byr money" id="total-bayar" readonly="" value="0">
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
            </div>
        </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     