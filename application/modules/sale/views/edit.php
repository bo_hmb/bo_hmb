            <form role="form" action="#" class="form-horizontal" id="form-create">
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Jamaah</label>
                    <div class="col-md-6">
                        <input type="text" name="no_pendaftaran" class="form-control" value="<?php echo $jamaah->no_pendaftaran.' / '.$jamaah->nm_lengkap  ?>" readonly>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Keberangkatan</label>
                    <div class="col-md-6">
                        <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                            <option disabled selected></option>
                            <option></option>
                            <?php
                            foreach ($jamaah as $row) { ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div> -->
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Penjualan</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="tgl_jual" class="form-control" placeholder="Tanggal" value="<?php echo $main->tgl_jual ?>">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:150px">Nama Barang</th>
                            <th style="width:150px">Harga</th>
                            <th style="width:80px">Qty</th>
                            <th style="width:50px">Free</th>
                            <th style="width:150px">Sub Harga</th>
                            <th style="width:100px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="container">
                    <?php foreach ($detail as $row) { ?>    
                        <tr class="baris form-create-barang" id="form-create-barang">
                            <td align="center"><input id="barang" class="form-control barang" name="nm_barang[]" value="<?php echo $row->nm_barang ?>" readonly></td>
                            <td align="center"><input id="harga_jual" class="form-control harga_jual money" name="harga_jual[]" type="text" value="<?php echo $row->harga_satuan ?>" readonly></td>
                            <td align="center"><input id="jml" name="jml[]" class="form-control jml money" type="text" value="<?php echo $row->jml ?>"></td>
                            <td align="center"><input type="checkbox" id="free" class="free" name="free[]" <?php if ($row->free != "0") echo "checked"?>></td>
                            <td align="center"><input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" value="<?php echo $row->sub_harga ?>" readonly></td>
                            <input id="cekstok" name="cekstok[]" class="form-control cekstok money" type="hidden">
                            <td>
                                <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>
                                <input id="rows" name="rows[]" value="" type="text">
                                <input id="sts_free" name="sts_free[]" class="sts_free" value="<?php echo $row->free ?>" type="hidden">
                            </td>
                        </tr>
                    <?php } ?>                        
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                            <td align="center">
                                <input type="text" name="total_harga" class="form-control total-bayar tot-byr money" id="total-bayar" readonly="" value="<?php echo $main->total_harga ?>">
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
            </div>
        </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     