<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Sale extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");
			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Sale Baggage';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_barang_order');
            $this->load->model('M_barang_jual');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $this->kode_cabang = $this->currentUser->kode_cabang;
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            
            $this->data['pageTitle'] = 'Sale Baggage';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {

            $no_faktur = $this->input->post('no_faktur',TRUE);
            $tgl_jual = $this->input->post('tgl_jual',TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $total_harga = $this->input->post('total_harga',TRUE);
           
            $cols = array();
            if (!empty($no_faktur)) { $cols['no_faktur'] = $no_faktur; }
            if (!empty($tgl_jual)) { $cols['tgl_jual'] = $tgl_jual; }
            if (!empty($no_pendaftaran)) { $cols['jual_barang_master.no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($total_harga)) { $cols['total_harga'] = $total_harga; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "jual_barang_master.active = '1'";
            } else {
             $where = "jual_barang_master.active = '1'";
            }

            $list = $this->M_barang_jual->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_barang_jual->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->no_pendaftaran.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->no_pendaftaran.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';
                if ($r->nm_jadwal == 'Tabungan') $j="-"; else $j=$r->nm_jadwal;
                $records["data"][] = array(
                    $no,   
                    $btn_action,
                    $r->no_faktur,
                    $x=date('d-m-Y', strtotime($r->tgl_jual)),
                    $r->no_pendaftaran,
                    $r->nm_lengkap,
                    $j,
                    $t="Rp." . number_format($r->total_harga,0,',','.'),
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_barang_order->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
            $data['barang'] = Modules::run('baggage/get_where', array('active' => '1' ))->result();
            $data['jamaah'] = $this->M_barang_jual->get_tv_formulir(array('active' => '1' ))->result();
			return response($this->load->view('add', $data, TRUE), 'html');
		}

        public function load_barang()
        {   
            $json = [];
            $this->load->database();

            // if(!empty($this->input->get("q"))){

                $this->db->like('nm_barang', $this->input->get("q"));
                $query = $this->db->select('id,nm_barang as text')
                            ->where('kd_office = "'. $this->kode_cabang.'"')
                            ->limit(10)
                            ->get("barang");
                $json = $query->result();
            // }

            
            echo json_encode($json);

        }

        public function get_select_barang()
        {
            $id = $this->input->post("id");
            $data = Modules::run('baggage/get_where', array('active' => '1', 'id' => $id, 'kd_office' => $this->kode_cabang ))->row();
            echo json_encode($data);
        }

		public function add()
	    {
            $this->ajaxRequest();

			$cek = $this->M_barang_jual->get_where(array('MONTH(tgl_jual)' => date('m'),'YEAR(tgl_jual)' => date('Y'),'active'=>'1'))->num_rows();
            $faktur = 'EB-'.date('m').date("Y").sprintf('%03d',$cek+1);

			$data_order = array(
				'no_faktur' => $faktur,
                'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                'tgl_jual' => $this->input->post('tgl_jual',TRUE),
                'total_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_harga')),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_barang_jual->_insert($data_order);

            $cek = count($this->input->post('nm_barang'));

            for ($i=0; $i < $cek; $i++) { 
                $data_order_detail = array(
                    'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                    'id_barang' => $this->input->post('nm_barang',TRUE)[$i],
                    'harga_satuan' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga_jual',TRUE))[$i],
                    'jml' => $this->input->post('jml',TRUE)[$i],
                    'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                    'take' => '1',
                    'active' => '1',
                    'kd_office' => $this->kode_cabang,
                    );

                $query1 = $this->M_barang_order->_insert($data_order_detail);
                // update stok

                $where = array(
                'id' => $this->input->post('nm_barang',TRUE)[$i],
                );

                $data = array(
                'stok' => $this->input->post('cekstok',TRUE)[$i] - $this->input->post('jml',TRUE)[$i],
                );
                $querystok = Modules::run('baggage/get_update_stok', $where, $data);
            }

            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_barang_jual->get_where(array('no_pendaftaran' => $id))->row();
            $data['jamaah'] = $this->M_barang_jual->get_tv_formulir(array('no_pendaftaran' => $id))->row();
            $data['detail'] = $this->M_barang_order->get_where_det($id)->result();
            return response($this->load->view('edit', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            // $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data_order = array(
                // 'no_faktur' => $faktur,
                // 'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                'tgl_jual' => $this->input->post('tgl_jual',TRUE),
                'total_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_harga')),
                'active' => '1',
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_barang_jual->_update(array('id' => $id), $data_order);

            $cek = count($this->input->post('nm_barang'));

            for ($i=0; $i < $cek; $i++) { 
                $data_order_detail = array(
                    'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                    'id_barang' => $this->input->post('nm_barang',TRUE)[$i],
                    'harga_satuan' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga_jual',TRUE))[$i],
                    'jml' => $this->input->post('jml',TRUE)[$i],
                    'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                    'take' => '1',
                    'active' => '1',
                    'kd_office' => $this->kode_cabang,
                    );

                $query1 = $this->M_barang_order->_update(array('no_faktur' => $this->input->post('no_faktur',TRUE)), $data_order_detail);
                // update stok

                $where = array(
                'id' => $this->input->post('nm_barang',TRUE)[$i],
                );

                $data = array(
                'stok' => $this->input->post('jml',TRUE)[$i] + $this->input->post('cekstok',TRUE)[$i],
                );
                $querystok = Modules::run('baggage/get_update_stok', $where, $data);
            }

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function cancel_purchase()
        {
            $no_faktur = $this->input->post('id');

            $main_his = $this->M_barang_order->get_where(array('no_faktur' => $no_faktur))->result();


            foreach ($main_his as $row) {
                $where = array(
                    'id' => $row->id_barang,
                    'kd_office' => $row->kd_office,
                    );

                $main = Modules::run('baggage/get_where', $where)->row();

                
                $datastok = array(
                    'stok' => $main->stok - $row->jml,
                );
                $querystok = Modules::run('baggage/get_update_stok', $where, $datastok);
                
            }


            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_barang_jual->_update(array('no_faktur' => $no_faktur), $data);
            $query1 = $this->M_barang_order->_update(array('no_faktur' => $no_faktur), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil cancel');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal cancel');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_barang_order->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('no_faktur', 'no faktur', 'trim|required');
            $this->form_validation->set_rules('tgl_beli', 'tgl_beli', 'trim|required');
            $this->form_validation->set_rules('nm_toko', 'nama toko', 'trim|required');
            $this->form_validation->set_rules('total_bayar', 'total_bayar', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
