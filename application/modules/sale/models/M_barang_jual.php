<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_barang_jual extends CI_Model 
	{
		

		var $table = 'jual_barang_master';
		var $jual_barang = 'jual_barang';
		var $pendaftar = 'pendaftar';
		var $formulir = 'formulir';
		var $jadwal = 'jadwal';
		var $paket = 'paket';		
		var $tv_formulir = 'tv_formulir';

	    var $column_order = array('');
	    var $column_search = array('');
	    var $order = array('id' => 'desc'); 

		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_tv_formulir($where) {
			$table = $this->tv_formulir;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _insert_jual_barang($data) {
			$table = $this->jual_barang;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' jual_barang_master.id,
								jual_barang_master.no_faktur,
								jual_barang_master.no_pendaftaran,
								formulir.nm_lengkap,
								jual_barang_master.tgl_jual,
								jual_barang_master.total_harga,
								jadwal.nm_jadwal,
								jadwal.tgl_keberangkatan,
								pendaftar.kd_office');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	$this->db->join($this->pendaftar, ''.$this->table.'.no_pendaftaran = '.$this->pendaftar.'.no_pendaftaran');
	    	$this->db->join($this->formulir, ''.$this->pendaftar.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->paket, ''.$this->pendaftar.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
