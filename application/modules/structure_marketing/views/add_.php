<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">                
                <div class="form-group form-md-line-input">
                    <select class="form-control pilihbanyak" name="id_marketing">
                        <option></option>
                        <?php foreach ($marketing as $mkt) { ?>
                        <option value="<?php echo $mkt->id; ?>"><?php echo $mkt->nm_marketing; ?> (KTP : <?php echo $mkt->nik; ?>)</option>
                        <?php } ?>
                    </select>
                    <label>Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih marketing</span>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilihbanyak" name="id_atasan">
                        <option></option>
                        <?php foreach ($marketing as $mkt) { ?>
                        <option value="<?php echo $mkt->id; ?>"><?php echo $mkt->nm_marketing; ?> (KTP : <?php echo $mkt->nik; ?>)</option>
                        <?php } ?>
                    </select>
                    <label>Atasan Pertama Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Atasan Pertama Marketing</span>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">                
                <div class="form-group">
                    <label for="multi-append" class="control-label">Atasan - atasan Marketing</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_id_atasan[]" class="form-control pilihbanyak" multiple>
                            <?php
                            foreach ($marketing as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nik; ?> - <?php echo $row->nm_marketing; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>