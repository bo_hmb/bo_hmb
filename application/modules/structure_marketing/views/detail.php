    <link rel="stylesheet" href="<?php echo base_url() ?>assets/jOrgChart-master/example/css/jquery.jOrgChart.css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/jOrgChart-master/example/css/custom.css"/>
    <!-- jQuery includes -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
    
    <script src="<?php echo base_url() ?>assets/jOrgChart-master/jquery.jOrgChart.js"></script>
	
	<script>
    jQuery(document).ready(function() {
        $("#org").jOrgChart({
            chartElement : '#chart',
            dragAndDrop  : true
        });
    });
    </script>	
    
    <ul id="org" style="display:none">
    <!-- <li>
       Food
       <ul>
         <li id="beer">Beer</li>
         <li>Vegetables
           <ul>
             <li>Pumpkin</li>
             <li>
                <p>A link and paragraph is all we need.</p>
             </li>
           </ul>
         </li>
         <li class="fruit">Fruit
           <ul>
             <li>Apple
               <ul>
                 <li>Granny Smith</li>
               </ul>
             </li>
             <li>Berries
               <ul>
                 <li>Blueberry</li>
                 <li><img src="images/raspberry.jpg" alt="Raspberry"/></li>
                 <li>Cucumber</li>
               </ul>
             </li>
           </ul>
         </li>
         <li>Bread</li>
       </ul>
     </li> -->
    <?php foreach ($mainatasan as $row) { ?>
	
    <?php 
        echo '<li>';
            echo '<a href="#">'.$row->nm_marketing.'</a>';
        echo '</li>';

     ?>
	
	<?php } ?> 
   </ul>           
    
    <div id="chart" class="orgChart"></div>
    
    <script>
        jQuery(document).ready(function() {
            
            /* Custom jQuery for the example */
            $("#show-list").click(function(e){
                e.preventDefault();
                
                $('#list-html').toggle('fast', function(){
                    if($(this).is(':visible')){
                        $('#show-list').text('Hide underlying list.');
                        $(".topbar").fadeTo('fast',0.9);
                    }else{
                        $('#show-list').text('Show underlying list.');
                        $(".topbar").fadeTo('fast',1);                  
                    }
                });
            });
            
            $('#list-html').text($('#org').html());
            
            $("#org").bind("DOMSubtreeModified", function() {
                $('#list-html').text('');
                
                $('#list-html').text($('#org').html());
                
                prettyPrint();                
            });
        });
    </script>




