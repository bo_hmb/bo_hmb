<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Structure_marketing extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Structure Marketing';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_level_marketing');
            $this->kode_cabang = $this->currentUser->kode_cabang;
            
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                    $this->data['userGroup'] = $this->userLevel;
                }

                $this->userLevel = $userGroup;
            }

            // $this->output->enable_profiler(TRUE);
        }

        public function index()
        {
            // Page components

            $this->data['pageTitle'] = 'Structure Marketing';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        // public function get_hotel_where($where) {
        //     $query = $this->M_level_marketing->get_where($where);
        //     return $query;
        // }

        // public function get_hotel_where_in($where) {
        //     $query = $this->M_level_marketing->get_where_in($where);
        //     return $query;
        // }

        public function load_data()
        {
            $nm_marketing = $this->input->post('nm_marketing',TRUE);
            $level = $this->input->post('level',TRUE);
            $nama_atasan = $this->input->post('nama_atasan',TRUE);
            $level_atasan = $this->input->post('level_atasan',TRUE);
            $nm_cabang = $this->input->post('id_atasan',TRUE);
            
            $active = '1';

            $cols = array();
            if (!empty($nm_marketing)) { $cols['nm_marketing'] = $nm_marketing; }
            if (!empty($level)) { $cols['level'] = $level; }
            if (!empty($nama_atasan)) { $cols['nama_atasan'] = $nama_atasan; }
            if (!empty($level_atasan)) { $cols['level_atasan'] = $level_atasan; }
            if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1'";
            } else {
             $where = "active = '1' AND kd_cabang = '$this->kode_cabang'";
            }


            $list = $this->M_level_marketing->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_level_marketing->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-detail tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-search"></i></button>
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_marketing,
                    $r->level,
                    $r->nama_atasan,
                    $r->level_atasan,
                    $r->nm_cabang,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');
            $data['dataid'] = $id;
			$data['main'] = $this->M_level_marketing->get_where(array('id_marketing' => $id ))->row();
            $data['mainatasan'] = $this->db->query('SELECT
                                             tv_level_marketing.nm_marketing,
                                             tv_level_marketing.id_atasan,
                                             tv_level_marketing.id_marketing
                                             FROM
                                             tv_level_marketing
                                             WHERE
                                             tv_level_marketing.id_atasan = "'.$data['main']->id_atasan.'"')->result();
            // $this->checkParent($data['main']->id_atasan);

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

        public function checkParent($idParent){
         $resultQCP = $this->db->query('SELECT
                                             tv_level_marketing.nm_marketing,
                                             tv_level_marketing.id_atasan,
                                             tv_level_marketing.id_marketing
                                             FROM
                                             tv_level_marketing
                                             WHERE
                                             tv_level_marketing.id_atasan = "'.$idParent.'"')->result();
            
         if(count($resultQCP) > 0){
             echo '<ul>';
             foreach ($resultQCP as $data) {
                 echo '<li>';
                     echo '<a href="#">'.$data->nm_marketing.'</a>';
                     $this->checkParent($data->id_atasan); 
                 echo '</li>';
             }
             echo '</ul>';
         }
        }

        public function get($orderBy = 'name')
        {
            $query = $this->m_groups->get($orderBy);
            return $query;
        }

		public function load_add_form()
        {
            $data['title'] = 'Add Data Level Marketing';
            $data['marketing'] = $this->M_level_marketing->get_marketing(array('active' => '1'))->result();

            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function add()
        {

            $this->ajaxRequest();
            $this->validateInput();

            $arrmarketing = $this->input->post('id_id_atasan');
            foreach($arrmarketing as $val)
            {
                $marketingarr = @$marketingarr . $val. ",";
            }

            $data = array(
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                'id_atasan' => $this->input->post('id_atasan',TRUE),
                'id_id_atasan' => substr(trim($marketingarr), 0, -1),
                'active' => '1',
                'id_user' => $this->currentUser->id,
                );

            $query = $this->M_level_marketing->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
            
            return response($response, 'json');
        }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['atasan'] = Modules::run('marketing/get', array('id' => $id))->result();
            $data['main'] = $this->M_level_marketing->get_where(array('id' => $id))->row();
            $data['marketing'] = $this->M_level_marketing->get_marketing(array('active' => '1'))->result();            
            $data['lvl_mkt'] = $this->M_level_marketing->get_lvl_where(array('id' => $id ))->row();

            // $this->output->enable_profiler(TRUE);

            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $arrmarketing = $this->input->post('id_id_atasan');
            foreach($arrmarketing as $val)
            {
                $marketingarr = @$marketingarr . $val. ",";
            }

            $data = array(
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                'id_atasan' => $this->input->post('id_atasan',TRUE),
                'id_id_atasan' => substr(trim($marketingarr), 0, -1),
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_level_marketing->_update(array('id' => $id), $data);
            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_level_marketing->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules 
            $this->form_validation->set_rules('id_marketing', '', 'trim|required');
            $this->form_validation->set_rules('id_atasan', '', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
