<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_formulir extends CI_Model 
	{
		

		var $table = 'formulir';
		var $ind_regencies = 'ind_regencies';
		var $ind_provinces = 'ind_provinces';
		var $marketing = 'marketing';
		var $cabang = 'cabang';
		var $pendaftar = 'pendaftar';

	    var $column_order = array('formulir.id','no_ktp','nm_lengkap','tgl_lahir','jk','alamat','name_regencies','kd_pos','name_provinces','telp_rumah','hp','pekerjaan','tgl_formulir','no_pasport','exp_pasport','path_foto','nm_marketing');
	    var $column_search = array('formulir.id','no_ktp','nm_lengkap','tgl_lahir','jk','alamat','name_regencies','kd_pos','name_provinces','telp_rumah','hp','pekerjaan','tgl_formulir','no_pasport','exp_pasport','path_foto','nm_marketing');
	    var $order = array('formulir.id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' formulir.id,
								formulir.no_ktp,
								formulir.nm_ktp,
								formulir.nm_lengkap,
								formulir.tmp_lahir,
								formulir.tgl_lahir,
								formulir.jk,
								formulir.alamat,
								ind_regencies.name_regencies,
								formulir.kd_pos,
								ind_provinces.name_provinces,
								formulir.telp_rumah,
								formulir.hp,
								formulir.pekerjaan,
								formulir.status_nikah,
								formulir.tgl_formulir,
								formulir.no_pasport,
								formulir.exp_pasport,
								formulir.path_foto,
								marketing.nm_marketing,
								marketing.kd_cabang,
								pendaftar.stts,
								formulir.active');

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->ind_regencies, ''.$this->ind_regencies.'.id_kota = '.$this->table.'.id_kota');
	    	$this->db->join($this->ind_provinces, ''.$this->ind_provinces.'.id = '.$this->table.'.id_prov');
	    	$this->db->join($this->marketing, ''.$this->marketing.'.id = '.$this->table.'.id_marketing');
	    	$this->db->join($this->pendaftar, ''.$this->pendaftar.'.id_formulir = '.$this->table.'.id','left');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from('formulir');	       
	        // $this->db->from($this->table);
			$this->db->join('marketing','formulir.id_marketing=marketing.id');	        
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_work_area($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}
	}
