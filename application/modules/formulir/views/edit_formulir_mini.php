<form action="#" id="form-edit-mini" role="form">
    <div class="form-body">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $main->id ?>">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_lengkap" placeholder="Masukkan Nama Sesuai Paspor" value="<?php echo $main->nm_lengkap ?>">
                    <label for="form_control_1">Nama Sesuai Paspor
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Sesuai Paspor</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="jk">
                        <option <?php echo ("L" === $main->jk) ? 'selected' : ''; ?> value="L">Laki-Laki</option>
                        <option <?php echo ("P" === $main->jk) ? 'selected' : ''; ?> value="P">Perempuan</option>
                    </select>
                    <label>Jenis Kelamin
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih jenis kelamin</span>
                </div>
            </div>
        </div>
        <div class="row">    
             <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="tmp_lahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $main->tmp_lahir ?>">
                    <label for="form_control_1">Tempat Lahir
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Tempat Lahir</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                        <input type="text" name="tgl_lahir" class="form-control" value="<?php echo $main->tgl_lahir ?>">
                        <label class="control-label">Tanggal Lahir
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Lahir </span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_pasport" placeholder="Masukkan Nomor Paspor" value="<?php echo $main->no_pasport ?>">
                    <label>Nomor Paspor
                    </label>
                    <span class="help-block">Masukkan Nomor Paspor</span>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="create_pasport" class="form-control" value="<?php echo $main->create_pasport ?>">
                        <label class="control-label">Tanggal Pembuatan Paspor
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pembuatan Paspor </span>
                </div>
            </div>
        </div>
        <div class="row">
            
           <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kota_paspor" value="<?php echo $main->kota_paspor ?>">
                    <label>Kota Paspor
                    </label>
                    <span class="help-block">Masukkan Kota Paspor</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="exp_pasport" class="form-control" value="<?php echo $main->exp_pasport ?>">
                        <label class="control-label">Tanggal Expired Paspor
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Expired Paspor</span>
                </div>
            </div>
            <input type="hidden" class="form-control" name="path_foto" id="path_foto" placeholder="Masukkan Path foto" value="<?php echo $main->path_foto ?>">

        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>