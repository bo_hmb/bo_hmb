<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Customer Service</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Form</span>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Form</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            <div class="btn-group">
                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;"> Excel </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> PDF </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Print </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">                            
                            <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <!-- <th width="1%">Export</th> -->
                                        <th width="20%">No. KTP</th>
                                        <th width="50%">Nama Sesuai KTP</th>
                                        <th width="50%">Nama Sesuai Paspor</th>
                                        <th width="20%">Tanggal Lahir</th>
                                        <!-- <th>Jenis Kelamin</th>
                                        <th>Alamat</th> -->
                                        <th width="20%">Kota</th>
                                        <!-- <th>Kode Pos</th> -->
                                        <th width="20%">Provinsi</th>
                                        <!-- <th width="20%">Telpon Rumah</th> -->
                                        <th width="20%">No. HP</th>
                                        <!-- <th>Pekerjaan</th> -->
                                        <th width="20%">Tanggal Formulir</th>
                                        <!-- <th>No. Paspor</th>
                                        <th>Exp. Paspor</th>
                                        <th>Path Foto</th> -->
                                        <!-- <th width="200">Marketing</th> -->
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <!-- <td> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_ktp"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_ktp"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        <td><input type="date" class="form-control form-filter input-sm" name="tgl_lahir"> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="jk"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="alamat"> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="kota"/> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="kd_pos"/> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="prov"/> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="telp_rumah"/> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="hp"/> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="pekerjaan"/> </td> -->
                                        <td><input type="date" class="form-control form-filter input-sm" name="tgl_formulir"/> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="no_pasport"/> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="exp_pasport"/> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="path_foto"/> </td> -->
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="nm_marketing"/> </td> -->
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>