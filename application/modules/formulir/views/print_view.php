<div class="row">
	<div class="col-md-4">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?php echo base_url() ?>upload/<?php echo $main->path_foto; ?>" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
	</div>
        <div class="col-md-8">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Nama Lengkap : <?php echo strtoupper($main->nm_lengkap); ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Pekerjaan : <?php echo $main->pekerjaan; ?>  </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Kontak : <br>
                        No. Handphone :<?php echo strtoupper($main->hp); ?>
                        
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Telep. Rumah :<?php echo strtoupper($main->telp_rumah); ?> </div>
                    </div>
                </div>


            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <br>
        <div class="col-md-8">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Tanggal Lahir : <?php echo date("d-m-Y",strtotime($main->tgl_lahir)); ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Jenis Kelamin : <?php echo $main->jk ?> </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Alamat : <br><?php echo $main->alamat; ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">
                            Kota : <?php echo $kota->name_regencies ?> <br>
                            Provinsi : <?php echo $provinsi->name_provinces; ?>                            
                        </div>
                    </div>
                </div>

                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">No. pasport : <?php echo $main->no_pasport; ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Tgl. Exprired : <?php echo date("d-m-Y",strtotime($main->exp_pasport)); ?></div>
                    </div>
                </div>

            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <br>
        <div class="col-md-8">
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Marketing : <?php echo $marketing->nm_marketing; ?> </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="uppercase profile-stat-text">Tgl. Formulir : <?php echo $main->tgl_formulir ?> </div>
                    </div>
                </div>
            </div>
        </div>
</div>
        
   