<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_ktp" placeholder="Masukkan No. KTP">
                    <label for="form_control_1">No. KTP
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan No. KTP</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_ktp" placeholder="Masukkan Nama Sesuai KTP">
                    <label for="form_control_1">Nama Sesuai KTP
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Sesuai KTP</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_lengkap" placeholder="Masukkan Nama Sesuai Paspor">
                    <label for="form_control_1">Nama Sesuai Paspor
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Sesuai Paspor</span>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="jk">
                        <option value="P">Perempuan</option>
                        <option value="L">Laki-Laki</option>
                    </select>
                    <label>Jenis Kelamin
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Jenis Kelamin</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="tmp_lahir" placeholder="Masukkan Tempat Lahir">
                    <label for="form_control_1">Tempat Lahir
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Tempat Lahir</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="tgl_lahir" class="form-control" >
                        <label class="control-label">Tanggal Lahir
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Lahir </span>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="pendidikan_terakhir">
                        <option value="D4/S1">D4/S1</option>
                        <option value="Tidak Sekolah">Tidak Sekolah</option>                        
                        <option value="SD/MI">SD/MI</option>
                        <option value="SMP/MTs">SMP/MTs</option>
                        <option value="SMA/MA">SMA/MA</option>
                        <option value="D1">D1</option>
                        <option value="D2">D2</option>
                        <option value="D3">D3</option>
                        <option value="S2">S2</option>
                        <option value="S3">S3</option>
                    </select>
                    <label>Pendidikan Terakhir</label>
                    <span class="help-block">Silahkan Pilih Pendidikan Terakhir</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="pekerjaan">
                        <option value="PNS">PNS</option>
                        <option value="Peg. Swasta">Peg. Swasta</option>
                        <option value="Wirausaha">Wirausaha</option>
                        <option value="TNI / POLRI">TNI / POLRI</option>
                        <option value="Petani">Petani</option>
                        <option value="Nelayan">Nelayan</option>
                        <option value="Lainnya">Lainnya</option>
                        <option value="Tidak Bekerja">Tidak Bekerja</option>
                    </select>
                    <label>Pekerjaan</label>
                    <span class="help-block">Silahkan pilih Pekerjaan</span>
                </div>
            </div>
        
            

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="status_nikah">
                        <option value="Menikah">Menikah</option>                        
                        <option value="Belum Menikah">Belum Menikah</option>
                        <option value="Janda/Duda">Janda/Duda</option>
                    </select>
                    <label>Status Nikah<span class="required">*</span></label>
                    <span class="help-block">Silahkan Pilih Status Nikah</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat">
                    <label for="form_control_1">Alamat
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Alamat</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control provinsi" name="id_prov" id="prov">
                        <?php foreach ($provinsi as $prov) { ?>
                            <option 
                                <?php echo $provinsi_selected == $prov->id ? 'selected="selected"' : '' ?> 
                                <?php echo ($prov->id === $work_area->prov) ? 'selected' : ''; ?>
                                value="<?php echo $prov->id ?>"><?php echo $prov->name_provinces ?>
                            </option>
                        <?php } ?>
                    </select>
                    <label>Provinsi<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Provinsi</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <script>var idkota = '<?php echo $work_area->kota ?>';</script>
                    <select class="form-control kota" name="id_kota" id="kota">
                        <option value="0"></option>
                    </select>
                    <label>Kota
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kota</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kec" placeholder="Masukkan Kecamatan">
                    <label>Kecamatan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kecamatan</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kel" placeholder="Masukkan Kelurahan">
                    <label>Kelurahan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kelurahan</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_ayah" placeholder="Masukkan Nama Ayah">
                    <label>Nama Ayah <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Ayah</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kd_pos" placeholder="Masukkan Kode Pos">
                    <label>Kode Pos
                    </label>
                    <span class="help-block">Masukkan Nama Ayah</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="telp_rumah" placeholder="Masukkan Nomor Telepon Keluarga Terdekat">
                    <label>Nomor Telepon Keluarga Terdekat
                    <span class="required">*</span>
                    
                    </label>
                    <span class="help-block">Masukkan Nomor Telepon Keluarga Terdekat</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="hp" placeholder="Masukkan Nomor HP">
                    <label>Nomor HP <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nomor HP</span>
                </div>
            </div>
        </div>

        <!-- <h5><b>Data Paspor</b></h5> -->
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_pasport" placeholder="Masukkan Nomor Paspor">
                    <label>Nomor Paspor
                    </label>
                    <span class="help-block">Masukkan Nomor Paspor</span>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="create_pasport" class="form-control">
                        <label class="control-label">Tanggal Pembuatan Paspor
                          
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pembuatan Paspor </span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="exp_pasport" class="form-control" >
                        <label class="control-label">Tanggal Expired Paspor
                       
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Expired Paspor</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kota_paspor" placeholder="Masukkan Kota Paspor">
                    <label>Kota Paspor
                    </label>
                    <span class="help-block">Masukkan Kota Paspor</span>
                </div>
            </div>
        
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_foto" id="path_foto" placeholder="Masukkan Path foto">
                    <label>Path foto
                    </label>
                    <span class="help-block">Masukkan Path foto</span>
                </div>
            </div>

            <div class="col-md-4">
                 <div class="form-group form-md-line-input">
                    <select class="form-control marketing" name="id_marketing">
                        <option></option>
                        <option value="3">Non Agen</option>
                        <?php foreach ($marketing as $mkt) { ?>
                        <option value="<?php echo $mkt->id; ?>"><?php echo $mkt->nik; ?> - <?php echo $mkt->nm_marketing; ?></option>
                        <?php } ?>
                    </select>
                    <label>Referensi
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih Referensi</span>
                </div>
            </div>
        </div>
        
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>