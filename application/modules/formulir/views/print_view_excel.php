<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=tutorialweb-export.xls");
?>

<html>
<head>
    <title>Excel</title>
</head>
<body>
    <table border="1">
    <tr>
        <th>NO.</th>
        <th>NAMA LENGKAP</th>
    </tr>
    <?php $no = 1;
    foreach ($main as $row) { ?>
        <tr>
            <td><?php echo $no?></td>
            <td><?php echo $main->nm_lengkap ?></td>
        </tr>
    <?php $no++;  }?>
</table>

</body>
</html>