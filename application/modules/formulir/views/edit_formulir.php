<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $main->id ?>">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_ktp" placeholder="Masukkan No. KTP" value="<?php echo $main->no_ktp ?>">
                    <label for="form_control_1">No. KTP
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan No. KTP</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_ktp" placeholder="Masukkan Nama Sesuai KTP" value="<?php echo $main->nm_ktp ?>">
                    <label for="form_control_1">Nama Sesuai KTP
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Sesuai KTP</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_lengkap" placeholder="Masukkan Nama Sesuai Paspor" value="<?php echo $main->nm_lengkap ?>">
                    <!-- <?php //if(array_key_exists('Admin', $this->userLevel) or array_key_exists('CS', $this->userLevel)) echo "readonly" ?> -->
                    <label for="form_control_1">Nama Sesuai Paspor
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Sesuai Paspor</span>
                </div>
            </div>
            
        </div>
        <div class="row">    
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="jk">
                        <option <?php echo ("L" === $main->jk) ? 'selected' : ''; ?> value="L">Laki-Laki</option>
                        <option <?php echo ("P" === $main->jk) ? 'selected' : ''; ?> value="P">Perempuan</option>
                    </select>
                    <label>Jenis Kelamin
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih jenis kelamin</span>
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="tmp_lahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $main->tmp_lahir ?>">
                    <label for="form_control_1">Tempat Lahir
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Tempat Lahir</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                        <input type="text" name="tgl_lahir" class="form-control" value="<?php echo $main->tgl_lahir ?>">
                        <label class="control-label">Tanggal Lahir
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Lahir </span>
                </div>
            </div>
            
        </div>

        <!-- <h5><b>Data Alamat & Telepon</b></h5> -->
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="pendidikan_terakhir">
                        <option <?php echo ("-" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="-">Pendidikan</option>
                        <option <?php echo ("Tidak Sekolah" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="Tidak Sekolah">Tidak Sekolah</option>
                        <option <?php echo ("SD/MI" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="SD/MI">SD/MI</option>
                        <option <?php echo ("SMP/MTs" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="SMP/MTs">SMP/MTs</option>
                        <option <?php echo ("SMA/MA" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="SMA/MA">SMA/MA</option>
                        <option <?php echo ("D1" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="D1">D1</option>
                        <option <?php echo ("D2" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="D2">D2</option>
                        <option <?php echo ("D3" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="D3">D3</option>
                        <option <?php echo ("D4/S1" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="D4/S1">D4/S1</option>
                        <option <?php echo ("S1" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="S2">S2</option>
                        <option <?php echo ("S2" === $main->pendidikan_terakhir) ? 'selected' : ''; ?> value="S3">S3</option>
                    </select>
                    <label>Pendidikan Terakhir</label>
                    <span class="help-block">Silahkan pilih Pendidikan Terakhir</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="pekerjaan">
                        <option <?php echo ("-" === $main->pekerjaan) ? 'selected' : ''; ?> value="-">Pekerjaan</option>
                        <option <?php echo ("PNS" === $main->pekerjaan) ? 'selected' : ''; ?> value="PNS">PNS</option>
                        <option <?php echo ("Peg. Swasta" === $main->pekerjaan) ? 'selected' : ''; ?> value="Peg. Swasta">Peg. Swasta</option>
                        <option <?php echo ("Wirausaha" === $main->pekerjaan) ? 'selected' : ''; ?> value="Wirausaha">Wirausaha</option>
                        <option <?php echo ("TNI / POLRI" === $main->pekerjaan) ? 'selected' : ''; ?> value="TNI / POLRI">TNI / POLRI</option>
                        <option <?php echo ("Petani" === $main->pekerjaan) ? 'selected' : ''; ?> value="Petani">Petani</option>
                        <option <?php echo ("Nelayan" === $main->pekerjaan) ? 'selected' : ''; ?> value="Nelayan">Nelayan</option>
                        <option <?php echo ("Lainnya" === $main->pekerjaan) ? 'selected' : ''; ?> value="Lainnya">Lainnya</option>
                        <option <?php echo ("Tidak Bekerja" === $main->pekerjaan) ? 'selected' : ''; ?> value="Tidak Bekerja">Tidak Bekerja</option>
                    </select>
                    <label>Pekerjaan</label>
                    <span class="help-block">Silahkan pilih Pekerjaan</span>
                </div>
            </div>
        
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="status_nikah">
                        <option <?php echo ("-" === $main->status_nikah) ? 'selected' : ''; ?> value="-">Status Nikah</option>
                        <option <?php echo ("Menikah" === $main->status_nikah) ? 'selected' : ''; ?> value="Menikah">Menikah</option>                        
                        <option <?php echo ("Belum Menikah" === $main->status_nikah) ? 'selected' : ''; ?> value="Belum Menikah">Belum Menikah</option>
                        <option <?php echo ("Janda/Duda" === $main->status_nikah) ? 'selected' : ''; ?> value="Janda/Duda">Janda/Duda</option>
                    </select>
                    <label>Status Nikah
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Status Nikah</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat" value="<?php echo $main->alamat ?>">
                    <label for="form_control_1">Alamat
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Alamat</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control provinsi" name="id_prov" id="prov">
                        <option></option>
                        <?php foreach ($provinsi as $prov) { ?>
                            <option 
                                <?php echo $provinsi_selected == $prov->id ? 'selected="selected"' : '' ?> 
                                <?php echo ($prov->id === $main->id_prov) ? 'selected' : ''; ?>
                                value="<?php echo $prov->id ?>"><?php echo $prov->name_provinces ?>
                            </option>
                        <?php } ?>
                    </select>
                    <label>Provinsi<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Provinsi</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <script>var idkota = '<?php echo $main->id_kota ?>';</script>
                    <select class="form-control kota" name="id_kota" id="kota">
                        <option value="0"></option>
                    </select>
                    <label>Kota
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kota</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kec" value="<?php echo $main->kec ?>">
                    <label>Kecamatan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kecamatan</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kel" value="<?php echo $main->kel ?>">
                    <label>Kelurahan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Kelurahan</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_ayah" value="<?php echo $main->nm_ayah ?>">
                    <label>Nama Ayah <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Ayah</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kd_pos" placeholder="Masukkan Kode Pos" value="<?php echo $main->kd_pos ?>">
                    <label>Kode Pos
                    </label>
                    <span class="help-block">Masukkan Kode Pos</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="telp_rumah" placeholder="Masukkan Nomor Telepon Keluarga Terdekat" value="<?php echo $main->telp_rumah ?>">
                    <label>Nomor Telepon Keluarga Terdekat<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nomor Telepon Keluarga Terdekat</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="hp" placeholder="Masukkan Nomor HP" value="<?php echo $main->hp ?>">
                    <label>Nomor HP<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nomor HP</span>
                </div>
            </div>
        </div>


        <!-- <h5><b>Data Pendaftaran</b></h5> -->
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_pasport" placeholder="Masukkan Nomor Paspor" value="<?php echo $main->no_pasport ?>">
                    <label>Nomor Paspor
                    </label>
                    <span class="help-block">Masukkan Nomor Paspor</span>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="create_pasport" class="form-control" value="<?php echo $main->create_pasport ?>">
                        <label class="control-label">Tanggal Pembuatan Paspor
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pembuatan Paspor </span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" name="exp_pasport" class="form-control" value="<?php echo $main->exp_pasport ?>">
                        <label class="control-label">Tanggal Expired Paspor
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Expired Paspor</span>
                </div>
            </div>
        </div>
        <div class="row">
            
           <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="kota_paspor" value="<?php echo $main->kota_paspor ?>">
                    <label>Kota Paspor
                    </label>
                    <span class="help-block">Masukkan Kota Paspor</span>
                </div>
            </div>
        
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_foto" id="path_foto" placeholder="Masukkan Path foto" value="<?php echo $main->path_foto ?>">
                    <label>Path foto
                    </label>
                    <span class="help-block">Masukkan Path foto</span>
                </div>

                <div class="profile-userpic">
                    <img src="<?php echo base_url() ?>upload/<?php echo $main->path_foto; ?>" class="img-responsive" alt=""> 
                </div>
            </div>

            <div class="col-md-4">
                 <div class="form-group form-md-line-input">
                    <select class="form-control marketing" name="id_marketing">
                        <option></option>
                        <option value="3">Non Agen</option>
                        <?php foreach ($marketing as $mkt) { ?>
                            <option <?php echo ($mkt->id === $main->id_marketing) ? 'selected' : ''; ?> value="<?php echo $mkt->id; ?>"><?php echo $mkt->nik; ?> - <?php echo $mkt->nm_marketing; ?></option>
                        <?php } ?>
                    </select>
                    <label>Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih marketing</span>
                </div>
            </div>
        </div>
        
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>