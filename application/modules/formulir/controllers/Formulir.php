<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Formulir extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');
            // Module components            
            $this->data['module'] = 'Form';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_formulir');
            //$this->load->library('Mpdf');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                $this->akses_cabang = $this->currentUser->akses_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Form';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $no_ktp = $this->input->post('no_ktp',TRUE);
            $nm_ktp = $this->input->post('nm_ktp',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $tgl_lahir = $this->input->post('tgl_lahir',TRUE);
            $jk = $this->input->post('jk',TRUE);
            $alamat = $this->input->post('alamat',TRUE);
            $kota = $this->input->post('kota',TRUE);
            $kd_pos = $this->input->post('kd_pos',TRUE);
            $prov = $this->input->post('prov',TRUE);
            $telp_rumah = $this->input->post('telp_rumah',TRUE);
            $hp = $this->input->post('hp',TRUE);
            $pekerjaan = $this->input->post('pekerjaan',TRUE);
            $tgl_formulir = $this->input->post('tgl_formulir',TRUE);
            $no_pasport = $this->input->post('no_pasport',TRUE);
            $exp_pasport = $this->input->post('exp_pasport',TRUE);
            $path_foto = $this->input->post('path_foto',TRUE);
            $id_marketing = $this->input->post('id_marketing',TRUE);

			$cols = array();
            if (!empty($no_ktp)) { $cols['no_ktp'] = $no_ktp; }
			if (!empty($nm_ktp)) { $cols['nm_ktp'] = $nm_ktp; }
			if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }
			if (!empty($tgl_lahir)) { $cols['tgl_lahir'] = $tgl_lahir; }
			if (!empty($jk)) { $cols['jk'] = $jk; }
            if (!empty($alamat)) { $cols['alamat'] = $alamat; }
            if (!empty($kota)) { $cols['kota'] = $kota; }
            if (!empty($kd_pos)) { $cols['kd_pos'] = $kd_pos; }
            if (!empty($prov)) { $cols['prov'] = $prov; }
            if (!empty($telp_rumah)) { $cols['telp_rumah'] = $telp_rumah; }
            if (!empty($hp)) { $cols['hp'] = $hp; }
            if (!empty($pekerjaan)) { $cols['pekerjaan'] = $pekerjaan; }
            if (!empty($tgl_formulir)) { $cols['tgl_formulir'] = $tgl_formulir; }
            if (!empty($no_pasport)) { $cols['no_pasport'] = $no_pasport; }
            if (!empty($exp_pasport)) { $cols['exp_pasport'] = $exp_pasport; }
            if (!empty($id_marketing)) { $cols['id_marketing'] = $id_marketing; }

            if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
             $where = "formulir.active = '1'";
            } else { 
             $where = "formulir.active = '1' AND marketing.kd_cabang IN($this->akses_cabang)";
            }
            


	        $list = $this->M_formulir->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_formulir->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-detail tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-search"></i></button>';
                if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Admin Finance', $this->userLevel)) {               
                $btn_action .= '

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                                <button type="button" class="btn btn-xs btn-outline btn-edit-mini tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-file-powerpoint-o"></i></button>
                                
                            </div>';
                } else {
                    if ($r->stts == 0)
                    $btn_action .= '

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                                <button type="button" class="btn btn-xs btn-outline btn-edit-mini tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-file-powerpoint-o"></i></button>
                                
                            </div>';
                }
                $btn_print = '<div class="btn-group btn-group-xs btn-group-solid">
                                <a type="button" class="btn btn-xs btn-outline blue tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'formulir/print/'.$r->id.'" target="_blank"><i class="fa fa-print"></i></a>
                                <a type="button" class="btn btn-xs btn-outline red tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'formulir/cetak/'.$r->id.'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                                <a type="button" class="btn btn-xs btn-outline green tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'formulir/cetak_excel/'.$r->id.'" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                            </div>';

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    // $btn_print,
                    $r->no_ktp,
                    $r->nm_ktp,
                    $r->nm_lengkap,
                    $x=date('d-m-Y', strtotime($r->tgl_lahir)),
                    // $r->jk,
                    // $r->alamat,
                    $r->name_regencies,
                    // $r->kd_pos,
                    $r->name_provinces,
                    // $r->telp_rumah,
                    $r->hp,
                    // $r->pekerjaan,
                    $x=date('d-m-Y', strtotime($r->tgl_formulir)),
                    // $r->no_pasport,
                    // $r->exp_pasport,
                    // $r->path_foto,
                    // $r->nm_marketing,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        public function detail()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
            $data ['provinsi'] = Modules::run('master_indonesia/get_provinsi_where', array('id' => $data['main']->id_prov))->row();
            $data ['kota'] = Modules::run('master_indonesia/get_kota_where', array('id_kota' => $data['main']->id_kota))->row();

            $data ['marketing'] = Modules::run('marketing/get_marketing_where', array('id' => $data['main']->id_marketing))->row();
            
            return response($this->load->view('detail', $data, TRUE), 'html');
        }

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Form';
            $data = array(
            'provinsi' => Modules::run('master_indonesia/get_provinsi'),
            'kota' => Modules::run('master_indonesia/get_kota'),
            'provinsi_selected' => '',
            'work_area' => $this->M_formulir->get_work_area(array('kd_cabang' => $this->kode_cabang))->row(),
            'kota_selected' => '',
            );

            if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel)){
                $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();
            } else {
                $data['marketing'] = Modules::run('marketing/get_marketing_where','active = "1" AND kd_cabang IN('.$this->akses_cabang.')')->result();
            }
           
			return response($this->load->view('add_formulir', $data, TRUE), 'html');
		}

        public function get_kota()
        {
            $id_kota = $this->input->post('id');
            $data = Modules::run('master_indonesia/get_kota_where', array('province_id' => $id_kota))->result();
            echo json_encode($data);
        }

		public function add()
	    {
			// $this->validateInput();
            $this->ajaxRequest();
            
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                
                $data = array(
                'no_ktp' => $this->input->post('no_ktp',TRUE),
                'nm_ktp' => $this->input->post('nm_ktp',TRUE),
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'nm_ayah' => $this->input->post('nm_ayah',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'id_kota' => $this->input->post('id_kota',TRUE),
                'kd_pos' => $this->input->post('kd_pos',TRUE),
                'id_prov' => $this->input->post('id_prov',TRUE),
                'kec' => $this->input->post('kec',TRUE),
                'kel' => $this->input->post('kel',TRUE),
                'telp_rumah' => $this->input->post('telp_rumah',TRUE),
                'hp' => $this->input->post('hp',TRUE),
                'pekerjaan' => $this->input->post('pekerjaan',TRUE),
                'status_nikah' => $this->input->post('status_nikah',TRUE),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
                'tgl_formulir' => date('Y-m-d'),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                'path_foto' => $image,
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
                );
            } else {
                $data = array(
                'no_ktp' => $this->input->post('no_ktp',TRUE),
                'nm_ktp' => $this->input->post('nm_ktp',TRUE),
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'nm_ayah' => $this->input->post('nm_ayah',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'id_kota' => $this->input->post('id_kota',TRUE),
                'kd_pos' => $this->input->post('kd_pos',TRUE),
                'id_prov' => $this->input->post('id_prov',TRUE),
                'kec' => $this->input->post('kec',TRUE),
                'kel' => $this->input->post('kel',TRUE),
                'telp_rumah' => $this->input->post('telp_rumah',TRUE),
                'hp' => $this->input->post('hp',TRUE),
                'pekerjaan' => $this->input->post('pekerjaan',TRUE),
                'status_nikah' => $this->input->post('status_nikah',TRUE),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
                'tgl_formulir' => date('Y-m-d'),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
                );
            }

            $count = $this->M_formulir->get_where(array('no_ktp' => $this->input->post('no_ktp'),'active' => '1'))->num_rows();
            if ($count == 0) {
                $q = $this->M_formulir->_insert($data);
                $query='true';
            } else $query='false';
                // Check if query was success
                if ($query=='true') {
                    $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
                } else {
                    $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data duplikat ditemukan, gagal ditambahkan');
                }

            return response($response, 'json');
            

            
 
	    }

        public function tambah_file(){          

            //create rekapan kode
            $years = date('Y');
            $month = date('m');
            $kode = Modules::run('driver_spl_kode/get_where_spl', array('kdoffice' => $this->kdoffice, 'tahun' => $years, 'bulan' => $month))->row();
            $office = Modules::run('office/get_where', array('kode_cabang' => $this->kdoffice))->row();
            
            if(!empty($kode)):
                $kode = $kode->jumlah;
            else:
                $kode = 1;
            endif;

            $no_spl = 'SPL-'.$office->inisial_cabang.sprintf('%04d',$kode).date('m').date("Y");
            
            $config['upload_path'] = './upload/';
            $config['allowed_types'] = 'jpg|png|jpeg|pdf';
            $config['max_size'] = '1024';
            $config['remove_space'] = TRUE;
            $config['file_name'] = $no_spl;

            $this->load->library('upload',$config); // Load konfigurasi uploadnya
            if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
                print_r($this->upload->data());
            }else{
                // Jika gagal :
                $response = array('status' => 'failed', 'Action' => 'Failed', 'message' => $this->upload->display_errors());
                echo $this->upload->display_errors();
            }

            return response($response, 'json');
            
        }


        public function load_edit_form()
        {

            $id = $this->input->get('id');
            
            $data['provinsi'] = Modules::run('master_indonesia/get_provinsi');
            $data['kota'] = Modules::run('master_indonesia/get_kota');
            $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
            if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
                $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();
            } else { 
                $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1','marketing.kd_cabang' => $this->kode_cabang))->result();
            }
            $data['provinsi_selected'] = '';
            $data['kota_selected'] = '';
        
            return response($this->load->view('edit_formulir', $data, TRUE), 'html');
        }

        public function load_edit_mini_form()
        {

            $id = $this->input->get('id');
            
            // $data['provinsi'] = Modules::run('master_indonesia/get_provinsi');
            // $data['kota'] = Modules::run('master_indonesia/get_kota');
            $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
            // if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
            //     $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();
            // } else { 
            //     $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1','marketing.kd_cabang' => $this->kode_cabang))->result();
            // }
            // $data['provinsi_selected'] = '';
            // $data['kota_selected'] = '';
        
            return response($this->load->view('edit_formulir_mini', $data, TRUE), 'html');
        }


        public function edit()
        {
            $this->ajaxRequest();
           
            // Preparing the data before update
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            
            $id = $this->input->post('id');
            
            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                $data = array(
                'no_ktp' => $this->input->post('no_ktp',TRUE),
                'nm_ktp' => $this->input->post('nm_ktp',TRUE),
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'nm_ayah' => $this->input->post('nm_ayah',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'id_kota' => $this->input->post('id_kota',TRUE),
                'kd_pos' => $this->input->post('kd_pos',TRUE),
                'id_prov' => $this->input->post('id_prov',TRUE),
                'kec' => $this->input->post('kec',TRUE),
                'kel' => $this->input->post('kel',TRUE),
                'telp_rumah' => $this->input->post('telp_rumah',TRUE),
                'hp' => $this->input->post('hp',TRUE),
                'pekerjaan' => $this->input->post('pekerjaan',TRUE),
                'status_nikah' => $this->input->post('status_nikah',TRUE),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
                'tgl_formulir' => date('Y-m-d'),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                'path_foto' => $image,
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                'user_update' => $this->currentUser->id,
                );
            } else {
                $data = array(
                'no_ktp' => $this->input->post('no_ktp',TRUE),
                'nm_ktp' => $this->input->post('nm_ktp',TRUE),
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'nm_ayah' => $this->input->post('nm_ayah',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'id_kota' => $this->input->post('id_kota',TRUE),
                'kd_pos' => $this->input->post('kd_pos',TRUE),
                'id_prov' => $this->input->post('id_prov',TRUE),
                'kec' => $this->input->post('kec',TRUE),
                'kel' => $this->input->post('kel',TRUE),
                'telp_rumah' => $this->input->post('telp_rumah',TRUE),
                'hp' => $this->input->post('hp',TRUE),
                'pekerjaan' => $this->input->post('pekerjaan',TRUE),
                'status_nikah' => $this->input->post('status_nikah',TRUE),
                'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir',TRUE),
                'tgl_formulir' => date('Y-m-d'),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                'id_marketing' => $this->input->post('id_marketing',TRUE),
                );
            }

            $query = $this->M_formulir->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function edit_mini()
        {
            $this->ajaxRequest();
            
            $id = $this->input->post('id');
            
                $data = array(
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                );

            $query = $this->M_formulir->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_formulir->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
            $data ['provinsi'] = Modules::run('master_indonesia/get_provinsi_where', array('id' => $data['main']->id_prov))->row();
            $data ['kota'] = Modules::run('master_indonesia/get_kota_where', array('id_kota' => $data['main']->id_kota))->row();

            $data ['marketing'] = Modules::run('marketing/get_marketing_where', array('id' => $data['main']->id_marketing))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('formulir/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf(); 

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function cetak_excel($id=null)
        {
            $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
            $data ['provinsi'] = Modules::run('master_indonesia/get_provinsi_where', array('id' => $data['main']->id_prov))->row();
            $data ['kota'] = Modules::run('master_indonesia/get_kota_where', array('id_kota' => $data['main']->id_kota))->row();

            $data ['marketing'] = Modules::run('marketing/get_marketing_where', array('id' => $data['main']->id_marketing))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $this->load->view('formulir/print_view_excel', $data);
        }

        // public function print($id=null)
        // {
        //     $data['main'] = $this->M_formulir->get_where(array('id' => $id))->row();
        //     $data ['provinsi'] = Modules::run('master_indonesia/get_provinsi_where', array('id' => $data['main']->id_prov))->row();
        //     $data ['kota'] = Modules::run('master_indonesia/get_kota_where', array('id_kota' => $data['main']->id_kota))->row();

        //     $data ['marketing'] = Modules::run('marketing/get_marketing_where', array('id' => $data['main']->id_marketing))->row();

        //     // $html = $this->load->view('welcome_message', $data, true);
        //     $this->load->view('formulir/print_view_print', $data);
        // }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('no_ktp', 'no ktp', 'trim|required');
            $this->form_validation->set_rules('nm_lengkap', 'nm lengkap', 'trim|required');
            $this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
            $this->form_validation->set_rules('jk', 'jk', 'trim|required');
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
            $this->form_validation->set_rules('kota', 'kota', 'trim|required');
            $this->form_validation->set_rules('kd_pos', 'kd pos', 'trim|required');
            $this->form_validation->set_rules('prov', 'prov', 'trim|required');
            $this->form_validation->set_rules('telp_rumah', 'telp rumah', 'trim|required');
            $this->form_validation->set_rules('hp', 'hp', 'trim|required');
            $this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'trim|required');
            $this->form_validation->set_rules('tgl_formulir', 'tgl formulir', 'trim|required');
            $this->form_validation->set_rules('no_pasport', 'no pasport', 'trim|required');
            $this->form_validation->set_rules('exp_pasport', 'exp pasport', 'trim|required');
            // $this->form_validation->set_rules('path_foto', 'path foto', 'trim|required');
            $this->form_validation->set_rules('id_marketing', 'id marketing', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
