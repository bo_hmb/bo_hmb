<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_expenditure_handling extends CI_Model 
	{
		

		var $table = 'pengeluaran';
		var $pengeluaran_detail = 'pengeluaran_detail';
		var $jadwal = 'jadwal';

	    var $column_order = array('id','no_faktur','tgl_faktur','jenis','diterima','total_biaya');
	    var $column_search = array('id','no_faktur','tgl_faktur','jenis','diterima','total_biaya');
	    var $order = array('id' => 'desc'); 

		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_detail($where) {
			$table = $this->pengeluaran_detail;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_cetak($id) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.nm_jadwal,pengeluaran.no_faktur,pengeluaran.tgl_faktur,
										pengeluaran.jenis,pengeluaran.diterima,pengeluaran.total_biaya,
										pengeluaran.active FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE pengeluaran.id="'.$id.'"');
	        return $query->row();
		}

		public function get_by_schedule($id) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.nm_jadwal,pengeluaran.no_faktur,pengeluaran.tgl_faktur,
										pengeluaran.jenis,pengeluaran.diterima,pengeluaran.total_biaya,
										pengeluaran.active FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE pengeluaran.id_jadwal="'.$id.'"');
	        return $query->result();
		}

		public function get_by_date($start,$to) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.nm_jadwal,pengeluaran.no_faktur,pengeluaran.tgl_faktur,
										pengeluaran.jenis,pengeluaran.diterima,pengeluaran.total_biaya,
										pengeluaran.active FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE tgl_faktur BETWEEN "'.$start.'" AND "'.$to.'"');
	        return $query->result();
		}

		public function get_sum_schedule($id) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.id AS id_jadwal,jadwal.nm_jadwal,
										SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE pengeluaran.id_jadwal="'.$id.'"');
	        return $query->row();
		}

		public function get_sum_date($start,$to) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.id AS id_jadwal,jadwal.nm_jadwal,
										SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE tgl_faktur BETWEEN "'.$start.'" AND "'.$to.'"');
	        return $query->row();
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _insert_detail($data) {
			$table = $this->pengeluaran_detail;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function _update_detail($where, $data) {
			$table = $this->pengeluaran_detail;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' pengeluaran.id,
								jadwal.nm_jadwal,
								pengeluaran.no_faktur,
								pengeluaran.tgl_faktur,
								pengeluaran.jenis,
								pengeluaran.diterima,
								pengeluaran.total_biaya,
								pengeluaran.status,
								pengeluaran.active
	    					');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	$this->db->join($this->jadwal, ''.$this->table.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
