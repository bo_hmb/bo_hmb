            <form role="form" action="#" class="form-horizontal" id="form-edit">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">No Faktur</label>
                        <div class="col-md-3">
                            <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" value="<?php echo $main->no_faktur ?>" readonly/> 
                        </div>
                    
                        <label class="col-md-2 control-label">Jenis Spending</label>
                        <div class="col-md-3">
                            <select class="form-control sp" name="jenis">
                                <option value="Expenditure Handling"  <?php if ($main->jenis == 'Expenditure Handling' ) {echo "selected";}  ?> >Expenditure Handling</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal Faktur</label>
                        <div class="col-md-3">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="tgl_faktur" class="form-control" placeholder="Tanggal" value="<?php echo $main->tgl_faktur; ?>">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button" disabled>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <label class="col-md-2 control-label">Diterima Oleh</label>
                        <div class="col-md-3">
                            <input type="text" name="diterima" value="<?php echo $main->diterima; ?>" placeholder="Diterima Oleh" class="form-control" /> 
                        </div>
                    </div>
                </div>
                <div class="row">      
                        <div class="form-group">
                        <label class="col-md-2 control-label">Nama Jadwal</label>
                        <div class="col-md-3">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                                <!-- <option disabled selected></option> -->
                                <option value="0">Umum</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>" <?php if($main->id_jadwal === $row->id){echo "selected";} ?> ><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-3">
                            <select class="form-control sp" name="status">
                                <option value="0"  <?php if ($main->status == '0' ) {echo "selected";}  ?> >Disagree</option>
                                <option value="2"  <?php if ($main->status == '2' ) {echo "selected";}  ?> >Agree</option>
                                <option value="1"  <?php if ($main->status == '1' ) {echo "selected";}  ?> >Payment</option>
                            </select>
                        </div>
                        </div>
                </div>

                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Item</th>
                                    <th>Deskripsi</th>
                                    <th>Harga Satuan</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                                <?php foreach ($detail as $row) { ?>                                
                                <tr class="baris form-create-barang" id="form-create-barang">
                                    <input type="hidden" value="<?php echo $row->id ?>" name="iddetail[]">
                                    <td align="center">
                                        <input id="item" class="form-control item" name="item[]" type="text" value="<?php echo $row->item ?>"></td>
                                    <td align="center">
                                        <input id="deskripsi" class="form-control deskripsi" name="deskripsi[]" type="text" value="<?php echo $row->deskripsi ?>"></td>
                                    <td align="center">
                                        <input id="harga" class="form-control harga money" name="harga[]" type="text" value="<?php echo $row->harga ?>"></td>
                                    <td align="center">
                                        <input id="jml" name="jml[]" class="form-control jml-edit money" type="text" placeholder="0" value="<?php echo $row->jml ?>"></td>
                                    <td align="center">
                                        <input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" value="<?php echo $row->sub_harga ?>" readonly></td>
                                    <td>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga </strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total_biaya money" id="total_biaya" value="<?php echo $main->total_biaya ?>" readonly>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     