<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>DATA EXPENDITURE</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 10%"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h2 style="text-align: center; font-weight: bold;">DATA EXPENDITURE</h2>
        </th>
    </tr>
</table>



<!-- <p style="text-align: center;">Jl. Poros Makassar - Maros, Km. 3, Karoada, Turikale Kab. Maros</p> -->
<hr>
<!-- N Kop -->

<!-- <h3 style="text-align: center; font-weight: bold;">Hasil Diagnosa</h3> -->
<table>    
    <tr>            
        <td style="width: 150px"> 
            <h4>No. Faktur</h4>
        </td>
        <td style="width: 1px"> 
            <h4>:</h4>
        </td>
        <td style="width: 200px"> 
            <h4><?php echo $main->no_faktur;?></h4>
        </td>
             
        <td style="width: 150px"> 
            <h4>Relasi Pengeluaran</h4>
        </td>
        <td style="width: 1px"> 
            <h4>:</h4>
        </td>
        <td style="width: 200px"> 
            <h4><?php echo $main->nm_jadwal;?></h4>
        </td>
    </tr>

    <tr>            
        <td style="width: 150px"> 
            <h4>Tanggal Faktur</h4>
        </td>
        <td style="width: 1px"> 
            <h4>:</h4>
        </td>
        <td style="width: 200px"> 
            <h4><?php echo date('d-m-Y', strtotime($main->tgl_faktur));?></h4>
        </td>

         <td style="width: 150px"> 
            <h4>Jenis Expenditure</h4>
        </td>
        <td style="width: 1px"> 
            <h4>:</h4>
        </td>
        <td style="width: 200px"> 
            <h4><?php echo $main->jenis;?></h4>
        </td>
    </tr>

    <tr>            
        <td style="width: 150px"> 
            <h4>Diterima Oleh</h4>
        </td>
        <td style="width: 1px"> 
            <h4>:</h4>
        </td>
        <td style="width: 200px"> 
            <h4><?php echo $main->diterima;?></h4>
        </td>
    </tr>

</table>

<br>    
<table>    
    <tr>           
        <th style="width: 10px;">No</th>
        <th style="width: 150px;">Items</th>
        <th style="width: 250px;">Deskripsi</th>
        <th style="width: 100px;">Harga Satuan</th>
        <th style="width: 70px;">Jumlah</th>
        <th style="width: 100px;">Sub harga</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->item ?></td>
           <td style="border:1px solid #000;"><?php echo $row->deskripsi ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->harga,0,',','.') ?></td>
           <td style="border:1px solid #000;text-align: center;"><?php echo $row->jml ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->sub_harga,0,',','.') ?></td>
        </tr>   
    <?php $nomor++;}?> 
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td  style="border:1px solid #000;text-align: center;">Total</td>
        <td  style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->total_biaya,0,',','.');?></td>
    </tr>
</table>
<br>
<p style="text-align: right;">Makassar, <?php echo date('d-m-Y'); ?></p>

<br>
<br>
<br>
<table>
    <tr>
        <th style="border:0px solid #000; width: 182px">Dibuat Oleh</th>
        <th style="border:0px solid #000; width: 60px"></th>
        <th style="border:0px solid #000; width: 182px">Mengetahui</th>
        <th style="border:0px solid #000; width: 60px"></th>
        <th style="border:0px solid #000; width: 182px">Diterima Oleh</th>
    </tr>
</table>
<br>
<br>
<br>

<table>
    <tr>
        <th style="border:0px solid #000; width: 182px"><?php echo $this->currentUser->first_name; ?></th>
        <th style="border:0px solid #000; width: 60px"></th>
        <th style="border:0px solid #000; width: 182px">Yuliansyah Siregar</th>
        <th style="border:0px solid #000; width: 60px"></th>
        <th style="border:0px solid #000; width: 182px"><?php echo $main->diterima;?></th>
    </tr>
</table>

</body>
</html>