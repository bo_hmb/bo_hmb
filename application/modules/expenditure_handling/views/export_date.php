            <?php echo form_open('spending/export_date'); ?>
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Select Start Date</label>
                        <div class="col-md-4">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_star" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    
                        <label class="col-md-2 control-label">Select To Date</label>
                        <div class="col-md-3">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_to" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     