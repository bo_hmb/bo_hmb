            <form role="form" action="#" class="form-horizontal" id="form-create">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">No Faktur</label>
                        <div class="col-md-3">
                            <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" value="<?php echo $faktur ?>" readonly/> 
                        </div>
                    
                        <!-- <label class="col-md-2 control-label">Jenis Spending</label>
                        <div class="col-md-3">
                            <select class="form-control sp" name="jenis" readonly>
                                <option value="Expenditure Handling">Expenditure Handling</option>
                            </select>
                        </div> -->
                        <label class="col-md-2 control-label">Nama Jadwal</label>
                        <div class="col-md-3">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                                <option disabled selected></option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <input type="hidden" name="jenis" value="Expenditure Handling">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal Faktur</label>
                        <div class="col-md-3">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="tgl_faktur" class="form-control" placeholder="Tanggal" value="<?php echo(date('Y-m-d')) ?>" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button" disabled>
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <label class="col-md-2 control-label">Diterima Oleh</label>
                        <div class="col-md-3">
                            <input type="text" name="diterima" placeholder="Diterima Oleh" class="form-control" /> 
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Item</th>
                                    <th>Deskripsi</th>
                                    <th>Harga Satuan</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga </strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" value="0" readonly="">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i>Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     