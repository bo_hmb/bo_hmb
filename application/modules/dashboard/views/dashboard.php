                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        
                        <!-- END DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-directions font-green hide"></i>
                                            <span class="caption-subject bold font-dark uppercase "> Schedule Departure</span>
                                            <!-- <span class="caption-helper">Horizontal Timeline</span> -->
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                        <div class="cd-horizontal-timeline mt-timeline-horizontal" data-spacing="60">
                                            <div class="timeline">
                                                <div class="events-wrapper">
                                                    <div class="events">
                                                        <ol>
                                                           <?php foreach ($jadwal as $row) { ?>
                                                                <li>
                                                                    <a href="#0" data-date="<?php echo date("d/m/y", strtotime($row->tgl_keberangkatan)); ?>" class="border-after-red bg-after-red <?php if ($row->id == $row_jadwal->id) echo "selected" ?>"><?php echo date("d/m/y", strtotime($row->tgl_keberangkatan)); ?></a>
                                                                </li>
                                                            <?php  } ?>
                                                        </ol>
                                                        <span class="filling-line bg-red" aria-hidden="true"></span>
                                                    </div>
                                                    <!-- .events -->
                                                </div>
                                                <!-- .events-wrapper -->
                                                <ul class="cd-timeline-navigation mt-ht-nav-icon">
                                                    <li>
                                                        <a href="#0" class="prev inactive btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#0" class="next btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- .cd-timeline-navigation -->
                                            </div>
                                            <!-- .timeline -->
                                            <div class="events-content">
                                                <ol>
                                                    <?php foreach ($jadwal as $row) { ?>                                                      
                                                    <li data-date="<?php echo date("d/m/y", strtotime($row->tgl_keberangkatan)); ?>" class="<?php if ($row->id == $row_jadwal->id) echo "selected enter-left" ?>">
                                                        

                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="icon-directions font-green hide"></i>
                                                                <span class="caption-subject bold font-dark uppercase "> SEAT AVAILABLE : <?php echo $row->sisa_kursi; ?> SEAT </span>
                                                                <!-- <span class="caption-helper">Horizontal Timeline</span> -->
                                                            </div>
                                                            
                                                        </div>  
                                                        <div class="table-scrollable table-scrollable-borderless">
                                                            <br>
                                                            <table class="table table-hover ">
                                                                <thead>
                                                                    <tr class="uppercase">
                                                                        <th> Nama Paket</th>
                                                                        <th> Lama Hari </th>
                                                                        <th> Hotel Makkah </th>
                                                                        <th> Hotel Madinah </th>
                                                                        <th> Pesawat </th>
                                                                        <th> Harga Paket </th>
                                                                    </tr>
                                                                </thead>
                                                                <?php 
                                                                if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
                                                                    $paket=$this->M_dashboard->get_group_paket2(array('paket.active' => '1','paket.id_jadwal' => $row->id_jadwal,'paket.kd_cabang' => $this->kode_cabang))->result();
                                                                } else {
                                                                    $paket=$this->M_dashboard->get_group_paket2(array('paket.active' => '1','paket.id_jadwal' => $row->id_jadwal,'paket.kd_cabang' => $this->kode_cabang))->result();
                                                                }
                                                                    foreach ($paket as $list) { 
                                                                        $pesawat = $this->M_dashboard->pesawat($list->id_pesawat);
                                                                        $hotel_makkah = $this->M_dashboard->hotel($list->id_hotel_makkah);
                                                                        $hotel_madinah = $this->M_dashboard->hotel($list->id_hotel_madinah); 
                                                                    ?>

                                                                    <tr>
                                                                        <td><?php echo $list->nm_paket ?> <?php if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin', $this->userLevel)) echo $list->kd_cabang ?></td>
                                                                        <td><?php echo $list->lama_hari ?></td>
                                                                        <td><?php echo $hotel_makkah->hotel ?></td>
                                                                        <td><?php echo $hotel_madinah->hotel ?></td>
                                                                        <td><?php echo $pesawat->nm_pesawat ?></td>
                                                                        <td><?php echo $t="Rp." . number_format($list->hrg_paket,0,',','.') ?></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                            </table>
                                                        </div>
                                                        <br>
                                                    </li>
                                                    <?php  } ?>
                                                </ol>

                                            </div>
                                            <!-- .events-content -->
                                        </div>
                                    </div>
                                </div>
                            </div>                                                                    
                        </div>

                        <!-- //// -->
                        <div class="row">
                            <div class="col-lg-12 col-xs-12 col-sm-12">
                                <div class="portlet light portlet-fit bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-directions font-green hide"></i>
                                            <span class="caption-subject bold font-dark uppercase "> Packet</span>
                                            <!-- <span class="caption-helper">Horizontal Timeline</span> -->
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                        <div class="cd-horizontal-timeline mt-timeline-horizontal" data-spacing="60">
                                            <div class="timeline">
                                                <div class="events-wrapper">
                                                    <div class="events">
                                                        <ol>
                                                           <?php foreach ($paket_list as $row) { ?>
                                                                <li>
                                                                    <a href="#0" data-date="<?php echo date("d/m/y", strtotime($row->tgl_keberangkatan)); ?>" class="border-after-red bg-after-red <?php if ($row->id == $row_paket->id) echo "selected" ?>"><?php echo $row->lama_hari; ?> Hari</a>
                                                                </li> 
                                                            <?php  } ?>
                                                        </ol>
                                                        <span class="filling-line bg-red" aria-hidden="true"></span>
                                                    </div>
                                                    <!-- .events -->
                                                </div>
                                                <!-- .events-wrapper -->
                                                <ul class="cd-timeline-navigation mt-ht-nav-icon">
                                                    <li>
                                                        <a href="#0" class="prev inactive btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-left"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#0" class="next btn btn-outline red md-skip">
                                                            <i class="fa fa-chevron-right"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- .cd-timeline-navigation -->
                                            </div>
                                            <!-- .timeline -->
                                            
                                            <div class="events-content">
                                                <ol>
                                                    <?php foreach ($paket_list as $row) { ?>                                            
                                                    <li data-date="<?php echo date("d/m/y", strtotime($row->tgl_keberangkatan)); ?>" class="<?php if ($row->id == $row_paket->id) echo "selected enter-left" ?>">
                                                        

                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="icon-directions font-green hide"></i>
                                                                <!-- <span class="caption-subject bold font-dark uppercase "> SEAT AVAILABLE : <?php echo $row->sisa_kursi; ?> SEAT </span> -->
                                                                <!-- <span class="caption-helper">Horizontal Timeline</span> -->
                                                            </div>
                                                            
                                                        </div>  

                                                        <div class="table-scrollable table-scrollable-borderless">
                                                            <br>
                                                            <table class="table table-hover ">
                                                                <thead>
                                                                    <tr class="uppercase">
                                                                        <th> Nama Paket</th>
                                                                        <th> Tgl. Keberangkatan </th>
                                                                        <th> Hotel Makkah </th>
                                                                        <th> Hotel Madinah </th>
                                                                        <th> Pesawat </th>
                                                                        <th> Harga Paket </th>
                                                                    </tr>
                                                                </thead>

                                                                <?php 
                                                                if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
                                                                    $paket=$this->M_dashboard->get_group_paket2(array('paket.active' => '1','lama_hari' => $row->lama_hari))->result();
                                                                } else {
                                                                    $paket=$this->M_dashboard->get_group_paket2(array('paket.active' => '1','lama_hari' => $row->lama_hari,'paket.kd_cabang' => $this->kode_cabang))->result();
                                                                }
                                                                    foreach ($paket as $list) { 
                                                                        $pesawat = $this->M_dashboard->pesawat($list->id_pesawat);
                                                                        $hotel_makkah = $this->M_dashboard->hotel($list->id_hotel_makkah);
                                                                        $hotel_madinah = $this->M_dashboard->hotel($list->id_hotel_madinah); 
                                                                    ?>

                                                                    <tr>
                                                                        <td><?php echo $list->nm_paket ?><?php if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin', $this->userLevel)) echo $list->kd_cabang ?></td>
                                                                        <td><?php echo $x=date('d-m-Y', strtotime($list->tgl_keberangkatan)) ?></td>
                                                                        <!-- <td><?php echo $list->lama_hari ?></td> -->
                                                                        <td><?php echo $hotel_makkah->hotel ?></td>
                                                                        <td><?php echo $hotel_madinah->hotel ?></td>
                                                                        <td><?php echo $pesawat->nm_pesawat ?></td>
                                                                        <td><?php echo $t="Rp." . number_format($list->hrg_paket,0,',','.') ?></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                            </table>
                                                        </div>
                                                       
                                                        
                                                    </li>
                                                    <?php  } ?>
                                                </ol>

                                            </div>
                                            <!-- .events-content -->
                                        </div>
                                    </div>
                                </div>
                            </div>                                                                    
                        </div>
                        <!-- /// -->
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->