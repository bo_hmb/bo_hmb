<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Schedule_movement extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Schedule Itinerary';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_movement');
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Schedule Itinerary';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);
            $tgl_kepulangan = $this->input->post('tgl_kepulangan',TRUE); 

            $cols = array();
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($tgl_keberangkatan)) { $cols['tgl_keberangkatan'] = $tgl_keberangkatan; }
            if (!empty($tgl_kepulangan)) { $cols['tgl_kepulangan'] = $tgl_kepulangan; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1'";
            } else {
             $where = "active = '1'";
            }


            $list = $this->M_schedule->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_schedule->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;


                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">';
                if ($r->file == "") 
                $btn_action .='<button type="button" class="btn btn-xs blue btn-outline btn-add tooltips" data-container="body" data-placement="top" 
                                data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-plus"></i> Tambah &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>'; else {
                $btn_action .= '<button type="button" class="btn btn-xs red btn-outline btn-edit tooltips" data-container="body" data-placement="top" 
                                data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i> Ubah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                
                                <a href="'.base_url().'upload/'.$r->file.'" target="_blank"><button type="button" class="btn btn-xs yellow btn-outline tooltips" data-container="body" data-placement="top" 
                                data-original-title="Tooltip in top"><i class="fa fa-download"></i> Download</button></a>
                                ';
                }
                $btn_action .= '</div>';
                           
                // selisih hari data-id="'.$r->id.'"
                // $lama_hari = ((abs(strtotime($r->tgl_kepulangan) - strtotime($r->tgl_keberangkatan)))/(60*60*24));

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_jadwal,
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan)),
                    $x=date('d-m-Y', strtotime($r->tgl_kepulangan)),
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Hotel';
            $data['id'] = $this->input->get('id');
            $data['schedule'] = $this->M_schedule->get()->result();
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

            $this->ajaxRequest();
			// $this->validateInput();
            $imagePath = realpath(APPPATH . './upload/');

            $number_of_files_uploaded = count($_FILES['files']['name']);
            for ($i = 0; $i <  $number_of_files_uploaded; $i++) {
                    $_FILES['userfile']['name']     = $_FILES['files']['name'][$i];
                    $_FILES['userfile']['type']     = $_FILES['files']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['userfile']['error']    = $_FILES['files']['error'][$i];
                    $_FILES['userfile']['size']     = $_FILES['files']['size'][$i];
                    
                    $config['upload_path']   = './upload/';
                    $config['allowed_types'] = 'pdf|jpg|png|jpeg';
                    // $config['encrypt_name']  = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    $errCount = 0;//counting errrs
                    if (!$this->upload->do_upload())
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $carFile[] = array(
                            'errors'=> $error
                        );
                        // print_r($error);
                        $fileUpload[$i]='default.jpg';

                    }
                    else
                    {
                        $filename = $this->upload->data();
                        $carFile[] = array(
                            'fileName'=>$filename['file_name']
                        );
                        $fileUpload[$i]=$filename['file_name'];
                        // print_r($carFile);
                    }
            }
			$id    = $this->input->post('id');

            $data = array(
                'file' => $fileUpload[0],
                
            );

            $query = $this->M_schedule->_update(array('id' => $id), $data);

            $data_paket = array(
                'itin_url' => 'http://103.101.225.215/hmbserv/upload/'.$fileUpload[0],
                
            );
            $query = $this->M_schedule->_update_paket(array('id_jadwal' => $id, 'kd_cabang' => 'MKS'), $data_paket);
            $query2 = $this->M_schedule->_update_paket(array('id_jadwal' => $id, 'kd_cabang' => 'PNK'), $data_paket);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['id'] = $this->input->get('id');
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();
            // $this->validateInput();
            $imagePath = realpath(APPPATH . './upload/');

            $number_of_files_uploaded = count($_FILES['files']['name']);
            for ($i = 0; $i <  $number_of_files_uploaded; $i++) {
                    $_FILES['userfile']['name']     = $_FILES['files']['name'][$i];
                    $_FILES['userfile']['type']     = $_FILES['files']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['userfile']['error']    = $_FILES['files']['error'][$i];
                    $_FILES['userfile']['size']     = $_FILES['files']['size'][$i];
                    
                    $config['upload_path']   = './upload/';
                    $config['allowed_types'] = 'pdf|jpg|png|jpeg';
                    // $config['encrypt_name']  = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    $errCount = 0;//counting errrs
                    if (!$this->upload->do_upload())
                    {
                        $error = array('error' => $this->upload->display_errors());
                        $carFile[] = array(
                            'errors'=> $error
                        );
                        // print_r($error);
                        $fileUpload[$i]='default.jpg';

                    }
                    else
                    {
                        $filename = $this->upload->data();
                        $carFile[] = array(
                            'fileName'=>$filename['file_name']
                        );
                        $fileUpload[$i]=$filename['file_name'];
                        // print_r($carFile);
                    }
            }
            $id    = $this->input->post('id');

            $data = array(
                'file' => $fileUpload[0],
                
            );

            $query = $this->M_schedule->_update(array('id' => $id), $data);

            $data_paket = array(
                'itin_url' => 'http://103.101.225.215/hmbserv/upload/'.$fileUpload[0],
                
            );
            $query = $this->M_schedule->_update_paket(array('id_jadwal' => $id, 'kd_cabang' => 'MKS'), $data_paket);
            $query2 = $this->M_schedule->_update_paket(array('id_jadwal' => $id, 'kd_cabang' => 'PNK'), $data_paket);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_movement->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('id_jadwal', '', 'trim|required');
            $this->form_validation->set_rules('hari', '', 'trim|required');
            $this->form_validation->set_rules('tgl', 'tgl', 'trim|required');
            $this->form_validation->set_rules('lokasi', '', 'trim|required');
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_movement->get_where(array('id_jadwal' => $id))->result();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('schedule_movement/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf(['format' => 'Legal']);
            
            $mpdf->SetHeader('<img src="' . base_url() . 'assets/img/kop.png"/>');
            $mpdf->SetFooter('<p style="text-align: center;font-weight: bold;"><i>MELAYANI ANDA DENGAN SEPENUH HATI</i></p>');
            $mpdf->SetFooter('<img src="' . base_url() . 'assets/img/footerFix.png"/>');
            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function cetak_excel($id=null)
        {
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_movement->get_where(array('id_jadwal' => $id))->result();
            

            // $html = $this->load->view('welcome_message', $data, true);
            $this->load->view('schedule_movement/print_view_excel', $data);
        }

    }
?>
