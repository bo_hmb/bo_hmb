<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <center>
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <div class="fileinput fileinput-exists" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 180px; height: 100px;"> 
                            </div>

                            <div>
                                <span class="btn red btn-outline btn-file">
                                    <span class="fileinput-new"> Browse </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="files[]" id="multiFiles" multiple="multiple"/>
                                </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                        </center>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>

    