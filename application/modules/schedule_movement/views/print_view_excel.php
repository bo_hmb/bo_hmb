<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Itenerary.xls");
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>MOVEMENT</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;width: 70px;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="110" width="110"></th>
        <th style="border:0px solid #000;width: 450px;"> 
            <h3 style="text-align: center; font-weight: bold;"><?php echo $main->nm_jadwal ?></h3>
        </th>
    </tr>
</table>
<br>
<table>    

    <?php $nomor=1; foreach($master as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;width: 700px;background-color: #FFFF00"><?php echo $nomor ?>   &nbsp; &nbsp;
                                                                                    <?php echo $row->hari ?> &nbsp;  &nbsp; &nbsp;
                                                                                    <?php echo date('d-m-Y', strtotime($row->tgl)) ?> &nbsp; &nbsp; &nbsp;
                                                                                    <?php echo $row->lokasi ?> </td>
           
        </tr>   
        <?php 
            $detail = $this->M_movement->get_where_detail(array('id_movement' => $row->id))->result();
            foreach($detail as $list) { ?>
                <tr>
                    <td style="border:0px solid #000;">&nbsp; &nbsp; &nbsp; &nbsp; <?php echo $list->jam ?> &nbsp; &nbsp; &nbsp; <?php echo $list->agenda ?> </td>
                </tr>
        <?php } ?>
    <?php $nomor++; }?> 
    
</table>

<p>*Jadwa Dapat Berubah Sewaktu-waktu Tanpa Mengurangi Nilai Ibadah</p>
 
<br>
<p style="text-align: center;font-weight: bold;"><i>INGAT UMRAH INGAT ZNH</i></p>

<table>    
    <tr>           
        <th style="border:0px solid #000;width: 80px;"><img src="<?php echo base_url() ?>assets/img/footer.png" class="img-responsive" alt="" height="100" width="120"></th>
    </tr>
</table>
</body>
</html>