<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>MOVEMENT</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<h3 style="text-align: right; font-weight: bold;">UMRAH <?php echo $main->nm_jadwal ?></h3>
<br>
<table>    

    <?php $nomor=1; foreach($master as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;width: 700px;background-color: #FFFF00">
                <table>
                    <tr>
                        <td><?php echo $nomor ?>.</td>
                        <td style="width: 100px;"><?php echo $row->hari ?></td>
                        <td style="width: 100px;"><?php echo date('d M Y', strtotime($row->tgl)) ?></td>
                        <td><?php echo $row->lokasi ?></td>
                    </tr>
                    
                </table>
            </td>
           
        </tr>   
        <?php 
            $detail = $this->M_movement->get_where_detail(array('id_movement' => $row->id))->result();
            foreach($detail as $list) { ?>
                <tr>
                    <td style="border:0px solid #000;">
                        <table>
                            <tr>
                                <td style="width: 20px;"></td>
                                <td style="width: 100px;"><?php echo $list->jam ?></td>
                                <td style="width: 100px;"><?php echo $list->agenda ?></td>
                                
                            </tr>
                            
                        </table>
                    </td>
                </tr>
        <?php } ?>
    <?php $nomor++; }?> 
    
</table>

<p>*Jadwa Dapat Berubah Sewaktu-waktu Tanpa Mengurangi Nilai Ibadah</p>
 
<br>


</body>
</html>