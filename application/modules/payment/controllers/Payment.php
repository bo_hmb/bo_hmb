<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Payment extends AdminController {

        public function __construct()
        {
            parent::__construct();
            date_default_timezone_set('Asia/Makassar');
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");
            $this->bulan = date('m');
            $this->tahun = date('Y');
            $this->curdate = date('Y-m-d H:i:s');
            // $this->kode_cabang = $this->currentUser->kode_cabang;

            // Module components            
            $this->data['module'] = 'Payment';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_payment');
            $this->load->library('upload');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                // $this->userLevel = $userGroup;                
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;
            $this->data['pageTitle'] = 'Payment';
            $this->data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);
             
            // Render page
            $this->renderPage();
        }

        public function pendaftaran($id)
        {
            $this->data['pageTitle'] = 'Payment Detail';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);
            $this->data['pageJs'] = $this->load->view('assets/_pageJs_detail', $this->data, true);
            $this->data['content'] = $this->load->view('main_detail', $this->data, true);
           

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $jenis = $this->input->post('jenis',TRUE);
            $nm_marketing = $this->input->post('nm_marketing',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);
            $no_ktp = $this->input->post('no_ktp',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $status = $this->input->post('status',TRUE);

            $cols = array();
            if (!empty($no_pendaftaran)) { $cols['no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($nm_marketing)) { $cols['nm_marketing'] = $nm_marketing; }
            if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
            if (!empty($jenis)) { $cols['jenis'] = $jenis; }
            if (!empty($tgl_keberangkatan)) { $cols['id_jadwal'] = $tgl_keberangkatan; }
            if (!empty($no_ktp)) { $cols['no_ktp'] = $no_ktp; }
            if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }   
            if (!empty($status)) { $cols['status'] = $status; }   

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "pendaftar.active = '1'";              
            } else if(array_key_exists('Admin Finance', $this->userLevel)) {
             $where = "pendaftar.active = '1'";              
            } else if(array_key_exists('Finance', $this->userLevel)) {
             $where = "pendaftar.active = '1' AND kd_office = '$this->kode_cabang'";              
            }       


            $list = $this->M_payment->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_payment->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                $cek = $this->M_payment->get_where_payment(array('active'=>'1','id_pendaftaran'=>$r->id,'status_verifikasi'=>1))->num_rows();
                if ($cek >= 1 ) {
                    $btn_cetak='';
                } else {
                   $btn_cetak='none';
                }

                $cek2 = $this->M_payment->get_where_payment(array('active'=>'1','id_pendaftaran'=>$r->id,'status_verifikasi'=>0))->num_rows();
                if ($cek2 >= 1 ) {
                    $btn_detail='';
                } else {
                   $btn_detail='none';
                }

                if ($r->total_biaya == $r->telah_bayar ) {
                    $btn_payment='none';
                } else {
                   $btn_payment='';
                }

                
// <button type="button" class="btn btn-xs blue btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'" ><i class="fa fa-search"> Detail * <span>'.$cek2.'</span></i> </button>
                // <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'" ><i class="fa fa-search"> Detail * <span>'.$cek2.'</span></i> </button>
                if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) {
                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                

                                <a type="button" style="display:'.$btn_cetak.';" class="btn btn-xs btn-outline yellow tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'payment_detail/pendaftaran/'.$r->id_pendaftaran.'" target="_blank"><i class="fa fa-file-pdf-o"> Detail * <span>'.$cek2.'</span></i></a>   
                                
                                <a type="button" style="display:'.$btn_cetak.';" class="btn btn-xs btn-outline red tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'payment/cetak/'.$r->id.'" target="_blank"><i class="fa fa-file-pdf-o"> Print</i></a>                              

                                <button type="button" style="display:'.$btn_payment.';" class="btn btn-xs blue btn-outline btn-payment tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-money"> Payment</i></button>
                            </div>';
                } else {
                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">

                                <a type="button" style="display:'.$btn_cetak.';" class="btn btn-xs btn-outline yellow tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'payment_detail/pendaftaran/'.$r->id_pendaftaran.'" target="_blank"><i class="fa fa-file-pdf-o"> Detail * <span>'.$cek2.'</span></i></a>  
                                                                         
                                <a type="button" style="display:'.$btn_cetak.';" class="btn btn-xs btn-outline red tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'payment/cetak/'.$r->id.'" target="_blank"><i class="fa fa-file-pdf-o"> Print</i></a>                              

                                <button type="button" style="display:'.$btn_payment.';" class="btn btn-xs blue btn-outline btn-payment tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-money"> Payment</i></button>
                            </div>';
                }
                //$hotel_makkah = Modules::run('hotel/get_hotel_where', array('id in ($r->id_hotel_makkah)'))->row();
                if ($r->tgl_keberangkatan == "0000-00-00") $h_min = "Tabungan"; else
                $h_min = ((abs(strtotime($r->tgl_keberangkatan) - strtotime(date('Y-m-d'))))/(60*60*24));

                // if($r->status == 'Lunas')
                // {
                //     $active = $r->status;
                //     $label = 'success';
                // }else if($r->status == 'Registrasi'){
                //     $active = $r->status;
                //     $label = 'danger';
                // }else {
                //     $active = $r->status;
                //     $label = 'danger';
                // }

                if($h_min <= 33 )
                {
                    $active_h_min = '-'.$h_min;
                    $label_h_min = 'danger';
                }else {
                    $active_h_min = '-'.$h_min;
                    $label_h_min = 'info';
                }

                if ($r->tgl_keberangkatan == '0000-00-00') {
                    $x='Paket Tabungan';
                } else {
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan));
                }
                

                $records["data"][] = array(
                    $no,   
                    $btn_action,
                    $r->nm_marketing,
                    $r->nm_cabang,
                    $r->jenis,
                    $r->no_pendaftaran,
                    // $r->tgl_keberangkatan,
                    // $x=date('d-m-Y', strtotime($r->tgl_keberangkatan)),
                    $x,
                    // $r->no_ktp,
                    $x=$r->nm_lengkap.' - '.$r->age.' Thn',
                    // $r->tgl_lahir,
                    $t="Rp." . number_format($r->total_biaya,0,',','.'),
                    '<span class="label label-sm label-'.$label_h_min.'">'.$active_h_min.'</span>',
                    // $h_min,
                    $b="Rp." . number_format($r->telah_bayar,0,',','.'),
                    //$b="Rp." . number_format($r->telah_bayar,0,',','.'),
                    // '<span class="label label-sm label-'.$label.'">'.$active.'</span>',
                    //$hotel_makkah->nm_hotel,
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        public function load_detail()
        {

            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $tgl_pendaftaran = $this->input->post('tgl_pendaftaran',TRUE);
            $no_ktp = $this->input->post('no_ktp',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $tgl_lahir = $this->input->post('tgl_lahir',TRUE);
            $total_biaya = $this->input->post('total_biaya',TRUE);
            $status = $this->input->post('status',TRUE);

            $cols = array();
            if (!empty($nm_paket)) { $cols['nm_paket'] = $nm_paket; }
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($tgl_keberangkatan)) { $cols['tgl_keberangkatan'] = $tgl_keberangkatan; }
            if (!empty($pesawat)) { $cols['pesawat'] = $pesawat; }
 
            if(array_key_exists('admin', $this->userLevel)) {
             $where = "pendaftar.active = '1'";
            }            


            $list = $this->M_payment->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_payment->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;


                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-detail tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-search"> Detail</i></button>

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"> E d i t</i></button>


                                <button type="button" class="btn btn-xs red btn-outline btn-payment tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'">Payment</button>
                            </div>';
                //$hotel_makkah = Modules::run('hotel/get_hotel_where', array('id in ($r->id_hotel_makkah)'))->row();
                $h_min = '-'.((abs(strtotime($r->tgl_keberangkatan) - strtotime(date('Y-m-d'))))/(60*60*24));

                $records["data"][] = array(
                    $no,   
                    $r->no_pendaftaran,
                    $r->tgl_pendaftaran,                 
                    $r->no_ktp,
                    $r->nm_lengkap,
                    // $r->tgl_lahir,
                    $t="Rp." . number_format($r->total_biaya,0,',','.'),
                    $h_min,
                    $b="Rp." . number_format($r->telah_bayar,0,',','.'),
                    $r->status,
                    //$hotel_makkah->nm_hotel,
                    $btn_action,
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        public function load_add_form()
        {
            $data['title'] = 'Add Data Registrasi';
            $data['packet'] = Modules::run('packet/get_where', array('active' => '1','kd_cabang'=>$this->kode_cabang))->result();    
            // $data['workarea'] = $this->M_workarea->get()->result();     
            $data['pesawat'] = Modules::run('pesawat/get_where', array('pesawat.active' => '1'))->result();
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();

            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function add()
        {
            $this->ajaxRequest();
            // $this->validateInput();
            $jenis = $this->input->post('jenis');
            $id_paket = $this->input->post('id_paket');
            $jenis_travel = $this->input->post('jenis_travel');

            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;
            
            $cek = $this->M_payment->get_where_payment(array())->num_rows();
            $faktur = 'IC-'.date('m').date("Y").sprintf('%03d',$cek+1);

            
            if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel) or $this->input->post('kd_cabang') == 'PNK' or $this->input->post('kd_cabang') == 'SKD' or $this->input->post('kd_cabang') == 'STG') {
                $sts_verifikasi='1';
            // } else if ($jenis == 'Tabungan') {
            //    $sts_verifikasi='0';
            } else $sts_verifikasi='0';

            $this->db->trans_start();

            // $this->load->library('upload',$config);
            $this->upload->initialize($config);
            if($this->upload->do_upload("file")){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./upload/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '70%';
                // $config['width']= 600;
                // $config['height']= 400;
                $config['new_image']= './upload/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar=$gbr['file_name'];

                $data = array(
                    'no_faktur' => $faktur,
                    'jenis' => 'IC',
                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'path_bukti_bayar' => $gambar,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'active' => '1',
                    'status_verifikasi' => $sts_verifikasi,
                    'id_user' => $this->currentUser->id,
                    );
            } else {
                $data = array(
                    'no_faktur' => $faktur,
                    'jenis' => 'IC',
                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'active' => '1',
                    'status_verifikasi' => $sts_verifikasi,
                    'id_user' => $this->currentUser->id,
                    );
            }
            $query = $this->M_payment->_insert($data);

            $data_notif = array(
                    'user_id' => $this->currentUser->id,
                    'for_user' => '9',
                    'activity_type' => 'Payment '.$this->input->post('no_pendaftaran',TRUE),
                    'activity_uri_1' => 'payment',
                    'activity_time' => $this->curdate,
                    'deleted' => '1',
                    'kdoffice' => $this->kode_cabang,
                    'reading' => '1',
                    );

            $query_notif = $this->M_payment->_insert_notif($data_notif);
        //
if ($this->input->post('ps') == 1) {
    if ($jenis_travel == 'UMRAH') {
        if ($jenis == 'Tabungan') 
        {
            if ($id_paket == 1 && $jenis == 'Tabungan') 
            {                
                $benefit_DP_marketing = array(
                    'no_faktur' => $faktur,
                    'id_marketing' => $this->input->post('id_marketing',TRUE),
                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                    'jml_benefit' => '400000',
                    'catatan' => 'PS DP Tabungan',
                    'status' => '0',
                    'active' => '1',
                    'id_user' => $this->currentUser->id,
                    );
                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$this->input->post('id_marketing'),'jml_benefit'=>'400000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                if ($cek_beneftit != 1) $q_benefit_DP_marketing = $this->M_payment->_insert_benefit($benefit_DP_marketing);

                if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'KTG' && $this->input->post('kd_cabang') != 'STG' && $this->input->post('kd_cabang') != 'SKD' && $this->input->post('kd_cabang') != 'SKW' && $this->input->post('kd_cabang') != 'KPS' ) {  
                    // if ($this->input->post('kd_cabang') != 'JYP') {
                        //Insert benefit Bom
                        $benefit_DP_BOM = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => 24,  //id Bu Murni
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => '50000',
                                    'catatan' => 'PS BOM',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);
                        //Batas benefit Bom

                        $benefit_DP_pic_tabungan = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => 103,
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '35000',
                            'catatan' => 'PS PIC Tabungan',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'103','jml_benefit'=>'35000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_DP_pic_tabungan = $this->M_payment->_insert_benefit($benefit_DP_pic_tabungan);

                        $benefit_DP_developer = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => 210, //id H. Ahmad
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '35000',
                            'catatan' => 'PS Developer',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'210','jml_benefit'=>'35000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_DP_developer = $this->M_payment->_insert_benefit($benefit_DP_developer);

                        $benefit_Operasional = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => 'MKS',  //id operasional
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '50000',
                            'catatan' => 'PS Operasional',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'MKS','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_Operasional = $this->M_payment->_insert_benefit($benefit_Operasional);

                        // insert benefit cabang
                        if ($this->input->post('kd_cabang') != 'MKS') {                                            
                            $benefit_cabang = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => '100000',
                                'catatan' => 'PS Cabang DP Tabungan',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>'100000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);
                        }
                        //Insert atasan" Marketing
                        $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                        $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                        $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());
                    
                        for ($i=0; $i < $count_id_atasan; $i++) {   
                            if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                    $benefit = '150000';                            
                                    $benefit_id_id_atasan = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $id_atasan[$i]->id,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit,
                                        'catatan' => 'PS Agen 2 - DP Tabungan',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                            //if ($this->input->post('id_marketing') != 24) 
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                            }
                        }
                    // }
                } else {
                    // jika Kalimantan
                    // if ($this->input->post('kd_cabang') != 'JYP') {
                    //Insert benefit Bom
                    $benefit_DP_BOM = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => 104,  //id Rafli
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => '50000',
                                'catatan' => 'PS Tabungan BOM PNK',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                    if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);
                    //Batas benefit Bom

                    $benefit_Operasional = array(
                        'no_faktur' => $faktur,
                        'id_marketing' => 'PNK',  //id operasional
                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                        'jml_benefit' => '50000',
                        'catatan' => 'PS Operasional PNK',
                        'status' => '0',
                        'active' => '1',
                        'id_user' => $this->currentUser->id,
                        );
                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'PNK','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                    if ($cek_beneftit != 1) $q_benefit_Operasional = $this->M_payment->_insert_benefit($benefit_Operasional);
                    
                   
                    // insert benefit cabang
                    if ($this->input->post('kd_cabang') != 'PNK') {                
                        
                        $benefit_cabang = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => $this->input->post('kd_cabang',TRUE),
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '100000',
                            'catatan' => 'PS Cabang Kalimantan',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>'100000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);
                    }
                
                    //Insert atasan" Marketing
                    $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                    $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                    $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());
                
                    for ($i=0; $i < $count_id_atasan; $i++) {   
                        if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                $benefit = '150000';                            
                                $benefit_id_id_atasan = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $id_atasan[$i]->id,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit,
                                    'catatan' => 'PS Agen 2 - DP Tabungan',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                        //if ($this->input->post('id_marketing') != 24) 
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1','catatan' => 'PS Agen 2 - DP Tabungan'))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                        }
                    }
                }

            } else {
                //Pelunasan
                if ($this->input->post('jml_bayar') == $this->input->post('sisa2')) {
                    $benefit_lunas_marketing = array(
                        'no_faktur' => $faktur,
                        'id_marketing' => $this->input->post('id_marketing',TRUE),
                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                        'jml_benefit' => '600000',
                        'catatan' => 'PS - Pelunasan Tabungan',
                        'status' => '0',
                        'active' => '1',
                        'id_user' => $this->currentUser->id,
                        );
                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$this->input->post('id_marketing'),'jml_benefit'=>'600000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                    if ($cek_beneftit != 1) $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);
                    // benefet referensi cabang
                    if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'KTG' && $this->input->post('kd_cabang') != 'STG' && $this->input->post('kd_cabang') != 'SKD' && $this->input->post('kd_cabang') != 'SKW' && $this->input->post('kd_cabang') != 'KPS' ) { 
                        $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                        if ($row_cabang->referensi_cabang != 0) {
        
                            if ($row_cabang->jenis_cabang == 'Perwakilan') {
                                //Execute jika cabang perwakilan
                                // if ($this->input->post('kd_cabang') != 'JYP') {
                                        $benefit_DP_BOM = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => 24,  //id Bu Murni
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => '25000',
                                            'catatan' => 'PS BOM Cab. Perwakilan',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'25000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                        if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);

                                        $benefit_DP_pic_tabungan = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => 103,
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => '15000',
                                            'catatan' => 'PS PIC Tabungan',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'103','jml_benefit'=>'15000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                        if ($cek_beneftit != 1) $q_benefit_DP_pic_tabungan = $this->M_payment->_insert_benefit($benefit_DP_pic_tabungan);

                                        $benefit_DP_developer = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => 210, //id H. Ahmad
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => '50000',
                                            'catatan' => 'PS Developer',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'210','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                        if ($cek_beneftit != 1) $q_benefit_DP_developer = $this->M_payment->_insert_benefit($benefit_DP_developer);
                                        //Batas Execute jika cabang perwakilan

                                        $benefit_referensi_cabang=200000; 
                                // }
                                
                            } else $benefit_referensi_cabang=250000;
                            $benefit_lunas_ref_area = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $row_cabang->referensi_cabang,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit_referensi_cabang,
                                    'catatan' => 'PS Referensi Cabang '.$this->input->post('kd_cabang'),
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$row_cabang->referensi_cabang,'jml_benefit'=>$benefit_referensi_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_lunas_ref_area = $this->M_payment->_insert_benefit($benefit_lunas_ref_area);
                        }
                        // benefet referensi cabang
                        //Insert atasan" Marketing
                        $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                        $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                        $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());
                        for ($i=0; $i < $count_id_atasan; $i++) {   
                            if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                $ps_tambah_cabang = 0;
                                $benefit = '150000';                            
                                $benefit_id_id_atasan = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $id_atasan[$i]->id,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit,
                                    'catatan' => 'PS Agen 2 - Pelunasan Tabungan',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1','catatan' => 'PS Agen 2 - Pelunasan Tabungan'))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                            } else $ps_tambah_cabang = 300000;
                        }
                        // insert benefit cabang jika ada SPV
                        $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                        if ($row_cabang2->jenis_cabang == 'Perwakilan') $benefit_berdasarkan_cabang = 200000; else $benefit_berdasarkan_cabang = 600000;
                        if (($this->input->post('kd_cabang') == 'PLP') or ($this->input->post('kd_cabang') == 'PAL') or ($this->input->post('kd_cabang') == 'KLK')) {
                            $benefit_berdasarkan_cabang = 200000;
                            //Special Case H.Murni
                            $benefit_DP_BOM = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => 24,  //id Bu Murni
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => '200000',
                                        'catatan' => 'PS Spesial Case Palopo / KLK',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'200000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);
                            //Batas Special Case H.Murni
                        }
                        $benefit_fix_cabang = $ps_tambah_cabang + $benefit_berdasarkan_cabang;
                        if ($this->input->post('kd_cabang') != 'MKS') {                
                            $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_fix_cabang,
                                        'catatan' => 'PS Cabang',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);
                        }
                        // Batas insert benefit cabang 
                    } else {
                        // jika di kalimantan
                        $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                        if ($row_cabang->referensi_cabang != 0) {
                            // $cek_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')))->num_rows();
                            if ($row_cabang->jenis_cabang == 'Perwakilan') {
                                //Execute jika cabang perwakilan
                                if ($this->input->post('kd_cabang') != 'JYP') {
                                    $benefit_lunas_ref_area = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => $row_cabang->referensi_cabang,
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => '200000',
                                            'catatan' => 'PS Referensi Cabang '.$this->input->post('kd_cabang'),
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$row_cabang->referensi_cabang,'jml_benefit'=>'200000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_lunas_ref_area = $this->M_payment->_insert_benefit($benefit_lunas_ref_area);
                                } 
                            } 
                        }
                        // benefet referensi cabang
                        //Insert atasan" Marketing
                        $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                        $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                        $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());
                        for ($i=0; $i < $count_id_atasan; $i++) {   
                            if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                $ps_tambah_cabang = 0;
                                $ps_tambah_OPS = 0;
                                $benefit = '150000';                            
                                $benefit_id_id_atasan = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $id_atasan[$i]->id,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit,
                                    'catatan' => 'PS Agen 2 - Pelunasan Tabungan',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1','catatan' => 'PS Agen 2 - Pelunasan Tabungan'))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);

                                //Insert benefit Bom
                                if ($this->input->post('kd_cabang') == 'PNK') {
                                    $benefit_BOM = array(
                                                'no_faktur' => $faktur,
                                                'id_marketing' => 104,  //id Rafli
                                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                                'jml_benefit' => '50000',
                                                'catatan' => 'PS Pelunasan BOM PNK',
                                                'status' => '0',
                                                'active' => '1',
                                                'id_user' => $this->currentUser->id,
                                                );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_BOM = $this->M_payment->_insert_benefit($benefit_BOM);
                                }
                                //Batas benefit Bom

                            } else {
                                $ps_tambah_cabang = 300000;
                                $ps_tambah_OPS = 50000;
                                //Insert benefit Bom
                                if ($this->input->post('kd_cabang') == 'PNK') {
                                    $benefit_DP_BOM = array(
                                                'no_faktur' => $faktur,
                                                'id_marketing' => 104,  //id Rafli
                                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                                'jml_benefit' => '200000',
                                                'catatan' => 'PS Pelunasan BOM PNK',
                                                'status' => '0',
                                                'active' => '1',
                                                'id_user' => $this->currentUser->id,
                                                );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'104','jml_benefit'=>'200000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);

                                }
                                //Batas benefit Bom
                            }
                        }
                        
                        $benefit_fix_cabang = 100000 + $ps_tambah_cabang ;
                        if ($this->input->post('kd_cabang') != 'PNK') {                
                            $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_fix_cabang,
                                        'catatan' => 'PS Cabang Kalimantan',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);
                        // Batas insert benefit cabang 
                        } else {
                            $benefit_Operasional = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => 'PNK',  //id operasional
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => $PS_FIX_OPS = 50000 + $ps_tambah_OPS,
                                'catatan' => 'PS Operasional PNK',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                            $q_benefit_Operasional = $this->M_payment->_insert_benefit($benefit_Operasional);
                        }

                    } 
                    // btas jika di kalimantan
                    
                }
            }
        }

        if ($jenis == 'Reguler') 
        {            
            // Benefit
            if ($this->input->post('jml_bayar') == $this->input->post('total_biaya2'))  {
                //Insert LANGSUNG LUNAS
                
                //Insert benefit Lunas Markeing
                if ($this->input->post('id_marketing') != '3') {
                    $benefit_lunas_marketing = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => $this->input->post('id_marketing',TRUE),
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '1000000',
                            'catatan' => 'PS Pelunasan Regular',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$this->input->post('id_marketing'),'jml_benefit'=>'1000000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                    if ($cek_beneftit != 1) $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);
                }
                 
                // benefet referensi cabang
                $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                // $cek_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')))->num_rows();
                
                        if ($row_cabang->jenis_cabang == 'Perwakilan') {
                            $benefit_referensi_cabang=200000; 
                            $benefit_BOM=75000; 
                            $PS_dev=75000; 
                        } else {
                            $benefit_referensi_cabang=250000;
                            $benefit_BOM=50000; 
                            $PS_dev=50000; 
                        }

                        //Benefit referensi Area
                        if ($row_cabang->referensi_cabang != '0') {
                            $benefit_lunas_ref_area = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $row_cabang->referensi_cabang,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit_referensi_cabang,
                                    'catatan' => 'PS Referensi Cabang '.$this->input->post('kd_cabang'),
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$row_cabang->referensi_cabang,'jml_benefit'=>$benefit_referensi_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_lunas_ref_area = $this->M_payment->_insert_benefit($benefit_lunas_ref_area);
                        }
                        //Batas Benefit referensi Area
                if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'KTG' && $this->input->post('kd_cabang') != 'STG' && $this->input->post('kd_cabang') != 'SKD' && $this->input->post('kd_cabang') != 'SKW' && $this->input->post('kd_cabang') != 'KPS' ) { 
                    //Insert benefit Bom
                    if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'JYP') {
                                $benefit_DP_BOM = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => 24,  //id Bu Murni
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit_BOM,
                                    'catatan' => 'PS BOM',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>$benefit_BOM,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);

                                //Insert Developer
                                $benefit_DP_developer = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => 210, //id H. Ahmad
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $PS_dev,
                                        'catatan' => 'PS Developer',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'210','jml_benefit'=>$PS_dev,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_DP_developer = $this->M_payment->_insert_benefit($benefit_DP_developer);             
                                //batas Insert Developer  
                    }
                    //Batas benefit Bom
                
                

                    //// kurangi kursi
                    if ($sts_verifikasi=='1') {
                        $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran'));

                        $id_jadwal = $jadwal->id_jadwal;
                        $data_jadwal = array(
                            'sisa_kursi' => $jadwal->sisa_kursi - 1,
                            );
                        $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                    } 
                    /// batas kurangi kursi

                    //Insert atasan" Marketing
                    $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                    $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                    $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                    for ($i=0; $i < $count_id_atasan; $i++) {                           
                        if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                            $ps_tambah_cabang = 0;
                                $benefit = '300000';                            
                                $benefit_id_id_atasan = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $id_atasan[$i]->id,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit,
                                    'catatan' => 'PS Agen 2 - Pelunasan Regular',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                        } else $ps_tambah_cabang = 300000;
                    }
                    // batas Insert atasan" Marketing

                    // insert benefit cabang
                    $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                    if ($row_cabang2->jenis_cabang == 'Perwakilan') 
                        $benefit_berdasarkan_cabang = 200000; 
                            else 
                                $benefit_berdasarkan_cabang = 700000;

                    // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                    // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;
                    if (($this->input->post('kd_cabang') == 'PLP') or ($this->input->post('kd_cabang') == 'PAL') or ($this->input->post('kd_cabang') == 'KLK')) {                    
                        $benefit_berdasarkan_cabang = 200000;
                        //Special Case H.Murni
                        $benefit_DP_BOM = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => 24,  //id Bu Murni
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => '200000',
                                        'catatan' => 'PS Spesial Case Palopo / KLK',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'200000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);
                        //Batas Special Case H.Murni
                    }
                    $benefit_fix_cabang = $ps_tambah_cabang + $benefit_berdasarkan_cabang;            

                    if ($this->input->post('kd_cabang') != 'MKS') {                
                        $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_fix_cabang,
                                        'catatan' => 'PS Cabang',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);

                    }
                    // Batas insert benefit cabang    
                    //Insert benefit Lunas MKU
                    $idm = $this->input->post('id_marketing',TRUE);
                    if ($idm == '290' or $idm == '292' or $idm == '291' or $idm == '293' or $idm == '289' or $idm == '312' or $idm == '313' or $idm == '314' or $idm == '315' or $idm == '316') {
                        $benefit_MKU_lunas_marketing = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => '254', //id pak ikhwan
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => '50000',
                            'catatan' => 'PS MKU Pelunsan Regular',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'254','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_MKU_lunas_marketing = $this->M_payment->_insert_benefit($benefit_MKU_lunas_marketing);
                    }

                } else {
                    //jika kalimanta
                    //// kurangi kursi
                    if ($sts_verifikasi=='1') {
                        $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran'));

                        $id_jadwal = $jadwal->id_jadwal;
                        $data_jadwal = array(
                            'sisa_kursi' => $jadwal->sisa_kursi - 1,
                            );
                        $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                    } 
                    /// batas kurangi kursi

                    //Insert atasan" Marketing
                    $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                    $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                    $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                    for ($i=0; $i < $count_id_atasan; $i++) {                           
                        if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                            $ps_tambah_cabang = 0;
                            $ps_tambah_operasional = 0;
                            $ps_tambah_BOM = 0;
                                $benefit = '300000';                            
                                $benefit_id_id_atasan = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $id_atasan[$i]->id,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit,
                                    'catatan' => 'PS Agen 2 - Pelunasan Regular',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                        } else { $ps_tambah_cabang = 300000; $ps_tambah_BOM = 200000; $ps_tambah_operasional = 100000; }
                    }
                    // batas Insert atasan" Marketing

                    // insert benefit cabang
                    $benefit_berdasarkan_cabang = 200000; 

                    $benefit_fix_cabang = $ps_tambah_cabang + $benefit_berdasarkan_cabang;            

                    if ($this->input->post('kd_cabang') != 'PNK') {                
                        $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_fix_cabang,
                                        'catatan' => 'PS Cabang '.$this->input->post('kd_cabang'),
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);

                    }
                    // Batas insert benefit cabang 
                    $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                    if ($row_cabang->jenis_cabang == 'Perwakilan') 
                        $benefit_BOM = 100000;
                            else 
                                $benefit_BOM = 100000 + $ps_tambah_BOM;

                    if ($this->input->post('kd_cabang') != 'MKS' && $this->input->post('kd_cabang') != 'JYP') {
                                $benefit_DP_BOM = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => 104,  //id Rafli
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => $benefit_BOM,
                                    'catatan' => 'PS BOM PNK',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                         $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'104','jml_benefit'=>$benefit_BOM,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);        
                    }

                    $ps_operasional_pnk = 100000 + $ps_tambah_operasional;
                    if ($this->input->post('kd_cabang') == 'PNK') {  
                        $benefit_Operasional = array(
                            'no_faktur' => $faktur,
                            'id_marketing' => 'PNK',  //id operasional
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                            'jml_benefit' => $ps_operasional_pnk,
                            'catatan' => 'PS Operasional PNK',
                            'status' => '0',
                            'active' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'PNK','jml_benefit'=>$ps_operasional_pnk,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_Operasional = $this->M_payment->_insert_benefit($benefit_Operasional);
                    }              

                    //jika kalimantan
                }

            // BAAS Insert LANGSUNG LUNAS 
            } else {

                if ($this->input->post('jml_bayar') == $this->input->post('sisa2'))  {
                    //Insert PELUNASAN
                    if ($this->input->post('id_marketing') != '3') {
                        $benefit_lunas_marketing = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => $this->input->post('id_marketing',TRUE),
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => '1000000',
                                'catatan' => 'PS Pelunasan Regular',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                        $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$this->input->post('id_marketing'),'jml_benefit'=>'1000000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                        if ($cek_beneftit != 1) $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);
                    }
                     
                    // benefet referensi cabang
                    $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                    // $cek_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')))->num_rows();
                    
                            if ($row_cabang->jenis_cabang == 'Perwakilan') {
                                $benefit_referensi_cabang=200000; 
                                $benefit_BOM=75000; 
                                $PS_dev=75000; 
                            } else {
                                $benefit_referensi_cabang=250000;
                                $benefit_BOM=50000; 
                                $PS_dev=50000; 
                            }

                            //Benefit referensi Area
                            if ($row_cabang->referensi_cabang != '0') {
                                $benefit_lunas_ref_area = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $row_cabang->referensi_cabang,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_referensi_cabang,
                                        'catatan' => 'PS Referensi Cabang '.$this->input->post('kd_cabang'),
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$row_cabang->referensi_cabang,'jml_benefit'=>$benefit_referensi_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_lunas_ref_area = $this->M_payment->_insert_benefit($benefit_lunas_ref_area);
                            }
                            //Batas Benefit referensi Area
                    if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'KTG' && $this->input->post('kd_cabang') != 'STG' && $this->input->post('kd_cabang') != 'SKD' && $this->input->post('kd_cabang') != 'SKW' && $this->input->post('kd_cabang') != 'KPS' ) { 
                        //Insert benefit Bom
                        if ($this->input->post('kd_cabang') != 'PNK' && $this->input->post('kd_cabang') != 'JYP') {
                                    $benefit_DP_BOM = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => 24,  //id Bu Murni
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_BOM,
                                        'catatan' => 'PS BOM',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>$benefit_BOM,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);

                                    //Insert Developer
                                    $benefit_DP_developer = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => 210, //id H. Ahmad
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => $PS_dev,
                                            'catatan' => 'PS Developer',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'210','jml_benefit'=>$PS_dev,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                    if ($cek_beneftit != 1) $q_benefit_DP_developer = $this->M_payment->_insert_benefit($benefit_DP_developer);             
                                    //batas Insert Developer  
                        }
                        //Batas benefit Bom
                    
                    

                        //// kurangi kursi
                        if ($sts_verifikasi=='1') {
                            $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran'));

                            $id_jadwal = $jadwal->id_jadwal;
                            $data_jadwal = array(
                                'sisa_kursi' => $jadwal->sisa_kursi - 1,
                                );
                            $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                        } 
                        /// batas kurangi kursi

                        //Insert atasan" Marketing
                        $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                        $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                        $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                        for ($i=0; $i < $count_id_atasan; $i++) {                           
                            if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                $ps_tambah_cabang = 0;
                                    $benefit = '300000';                            
                                    $benefit_id_id_atasan = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $id_atasan[$i]->id,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit,
                                        'catatan' => 'PS Agen 2 - Pelunasan Regular',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                        if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                            } else $ps_tambah_cabang = 300000;
                        }
                        // batas Insert atasan" Marketing

                        // insert benefit cabang
                        $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')))->row();
                        if ($row_cabang2->jenis_cabang == 'Perwakilan') 
                            $benefit_berdasarkan_cabang = 200000; 
                                else 
                                    $benefit_berdasarkan_cabang = 700000;

                        // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                        // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;
                        if (($this->input->post('kd_cabang') == 'PLP') or ($this->input->post('kd_cabang') == 'PAL') or ($this->input->post('kd_cabang') == 'KLK')) {                    
                            $benefit_berdasarkan_cabang = 200000;
                            //Special Case H.Murni
                            $benefit_DP_BOM = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => 24,  //id Bu Murni
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => '200000',
                                            'catatan' => 'PS Spesial Case Palopo / KLK',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'24','jml_benefit'=>'200000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);
                            //Batas Special Case H.Murni
                        }
                        $benefit_fix_cabang = $ps_tambah_cabang + $benefit_berdasarkan_cabang;            

                        if ($this->input->post('kd_cabang') != 'MKS') {                
                            $benefit_cabang = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => $benefit_fix_cabang,
                                            'catatan' => 'PS Cabang',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);

                        }
                        // Batas insert benefit cabang    
                        //Insert benefit Lunas MKU
                        $idm = $this->input->post('id_marketing',TRUE);
                        if ($idm == '290' or $idm == '292' or $idm == '291' or $idm == '293' or $idm == '289' or $idm == '312' or $idm == '313' or $idm == '314' or $idm == '315' or $idm == '316') {
                            $benefit_MKU_lunas_marketing = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => '254', //id pak ikhwan
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => '50000',
                                'catatan' => 'PS MKU Pelunsan Regular',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'254','jml_benefit'=>'50000','id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_MKU_lunas_marketing = $this->M_payment->_insert_benefit($benefit_MKU_lunas_marketing);
                        }

                    } else {
                        //jika kalimanta
                        //// kurangi kursi
                        if ($sts_verifikasi=='1') {
                            $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran'));

                            $id_jadwal = $jadwal->id_jadwal;
                            $data_jadwal = array(
                                'sisa_kursi' => $jadwal->sisa_kursi - 1,
                                );
                            $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                        } 
                        /// batas kurangi kursi

                        //Insert atasan" Marketing
                        $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing'));
                        $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                        $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                        for ($i=0; $i < $count_id_atasan; $i++) {                           
                            if ($id_atasan[$i]->level !== 'Kacab' && $id_atasan[$i]->level !== 'Head HMB') {
                                $ps_tambah_cabang = 0;
                                $ps_tambah_operasional = 0;
                                $ps_tambah_BOM = 0;
                                    $benefit = '300000';                            
                                    $benefit_id_id_atasan = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $id_atasan[$i]->id,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit,
                                        'catatan' => 'PS Agen 2 - Pelunasan Regular',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>$id_atasan[$i]->id,'jml_benefit'=>$benefit,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                                if ($cek_beneftit != 1) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                            } else { $ps_tambah_cabang = 300000; $ps_tambah_BOM = 200000; $ps_tambah_operasional = 100000; }
                        }
                        // batas Insert atasan" Marketing

                        // insert benefit cabang
                        $benefit_berdasarkan_cabang = 200000; 

                        $benefit_fix_cabang = $ps_tambah_cabang + $benefit_berdasarkan_cabang;            

                        if ($this->input->post('kd_cabang') != 'PNK') {                
                            $benefit_cabang = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => $this->input->post('kd_cabang',TRUE),
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => $benefit_fix_cabang,
                                            'catatan' => 'PS Cabang '.$this->input->post('kd_cabang'),
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing' => $this->input->post('kd_cabang'),'jml_benefit'=>$benefit_fix_cabang,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_cabang = $this->M_payment->_insert_benefit($benefit_cabang);

                        }
                        // Batas insert benefit cabang  
                        $benefit_BOM = 100000 + $ps_tambah_BOM;
                        if ($this->input->post('kd_cabang') != 'MKS' && $this->input->post('kd_cabang') != 'JYP') {
                                    $benefit_DP_BOM = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => 104,  //id Rafli
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_BOM,
                                        'catatan' => 'PS BOM PNK',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                             $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'104','jml_benefit'=>$benefit_BOM,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_DP_BOM = $this->M_payment->_insert_benefit($benefit_DP_BOM);        
                        }

                        $ps_operasional_pnk = 100000 + $ps_tambah_operasional;
                        if ($this->input->post('kd_cabang') == 'PNK') {  
                            $benefit_Operasional = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => 'PNK',  //id operasional
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => $ps_operasional_pnk,
                                'catatan' => 'PS Operasional PNK',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                            $cek_beneftit = $this->M_payment->get_where_benefit(array('id_marketing'=>'PNK','jml_benefit'=>$ps_operasional_pnk,'id_pendaftaran' => $this->input->post('id_pendaftaran'),'active'=>'1' ))->num_rows();
                            if ($cek_beneftit != 1) $q_benefit_Operasional = $this->M_payment->_insert_benefit($benefit_Operasional);
                        }              

                        //jika kalimantan
                    }
                    
                       
                }
                
            }

        }  
    }
}   
            // Check if query was success
            $this->db->trans_complete();
            // Check if query was success
            if ($this->db->trans_status() === FALSE) {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            } else {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            }
            
            return response($response, 'json');
        }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_payment->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function load_payment_form()
        {

            $id = $this->input->get('id');
         if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) $data['userLevel'] = '1'; 
                else $data['userLevel'] = '0';
            $data['main'] = $this->M_payment->get_detail($id);
            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function load_export()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();    
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function load_export_excel()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();    
            return response($this->load->view('export_excel', $data, TRUE), 'html');
        }

        public function load_multi()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('multi', $data, TRUE), 'html');
        }

        public function load_pendaftar()
        {   
            $json = [];


            $this->load->database();

            // if(!empty($this->input->get("q"))){

                $this->db->like('nm_lengkap', $this->input->get("q"));

                if(array_key_exists('Super Admin', $this->userLevel)) {
                     $query = $this->db->select('id,nm_lengkap as text,total_biaya,telah_bayar,sisa,id_marketing,kd_office,id_paket,id_jadwal,jenis,nm_lengkap')
                                ->where('active = 1')
                                ->limit(10)
                                ->get("tv_payment");      
                } else if(array_key_exists('Admin Finance', $this->userLevel)) {
                     $query = $this->db->select('id,nm_lengkap as text,total_biaya,telah_bayar,sisa,id_marketing,kd_office,id_paket,id_jadwal,jenis,nm_lengkap')
                                ->where('active = 1')
                                ->limit(10)
                                ->get("tv_payment");       
                } else if(array_key_exists('Finance', $this->userLevel)) {
                     $query = $this->db->select('id,nm_lengkap as text,total_biaya,telah_bayar,sisa,id_marketing,kd_office,id_paket,id_jadwal,jenis,nm_lengkap')
                                ->where('active = 1 AND kd_office = "'.$this->kode_cabang.'"')
                                ->limit(10)
                                ->get("tv_payment");             
                }

                $json = $query->result();
            // }

            
            echo json_encode($json);

        }

        public function get_select_pendaftar()
        {
            $id = $this->input->post("id");
            $data = $this->M_payment->get_detail($id);
            echo json_encode($data);
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'nM_payment' => $this->input->post('nM_payment',TRUE),
                'kota_lokasi' => $this->input->post('kota_lokasi',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jrk_mkkh_masjidl' => $this->input->post('jrk_mkkh_masjidl',TRUE),
                'jrk_mdinh_msjidl' => $this->input->post('jrk_mdinh_msjidl',TRUE),
                'bintang' => $this->input->post('bintang',TRUE),
                'biaya_sewa_kamar' => $this->input->post('biaya_sewa_kamar',TRUE),
                'biaya_sewa_hotel' => $this->input->post('biaya_sewa_hotel',TRUE),
                'biaya_upgrade_hotel' => $this->input->post('biaya_upgrade_hotel',TRUE),
                'biaya_upgrade_kamar' => $this->input->post('biaya_upgrade_kamar',TRUE),
            );

            $query = $this->M_payment->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_payment->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules 
            $this->form_validation->set_rules('tgl_bayar', 'tgl_bayar', 'trim|required');
            $this->form_validation->set_rules('jml_bayar', 'jml_bayar', 'trim|required|numeric');
            // $this->form_validation->set_rules('path_bukti_bayar', 'path_bukti_bayar', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cek_first($id)
        {
            // $id = $this->input->post('id_pendaftaran');
            $data['cek_first'] = $this->M_payment->cek_payment(array('active'=>'1','id_pendaftaran'=>$id,'status_verifikasi'=>1))->num_rows();
            echo json_encode($data);
        }

        public function cek()
        {
            $id = $this->input->post('id');
            $data['cek'] = $this->M_payment->cek_payment(array('active'=>'1','id_pendaftaran'=>$id,'status_verifikasi'=>0))->num_rows();
            echo json_encode($data);
        }

        public function cetak($id=null)
        {

            //$cek =  $this->M_payment->get_where_payment(array('active'=>'1','id_pendaftaran'=>$id,'status_verifikasi'=>0))->num_rows();

            
                    $data = [];

                    $data['title'] = "Report";

                    //get data main
                    
                    $data['main'] = $this->M_payment->get_detail($id);
                    $data['umur'] = $this->M_payment->get_umur($data['main']->id_formulir);
                    $data['detail'] = $this->M_payment->get_payment_detail($id);
                    $data['detail_row'] = $this->M_payment->get_payment_detail_row($id);
                    $data['cabang'] = $this->M_payment->get_cabang(array('kd_cabang' => $this->kode_cabang))->row();


                    // $html = $this->load->view('welcome_message', $data, true);
                    $html = $this->load->view('payment/print_view', $data, TRUE);
                
                    //this the the PDF filename that user will get to download
                    $pdfFilePath = "report.pdf";

                    //mPDF versi 7
                    $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                     
                                    ]);
                    $mpdf->setFooter('{PAGENO} dari {nb}');
                    $mpdf->WriteHTML($html);
                    $mpdf->Output();    
              
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');
            $id_marketing = $this->input->post('id_marketing');
            $status = $this->input->post('status');
            //$data['main'] = $id;
            $limit = $this->M_payment->get_pendaftar(array('jadwal.id' => $id ))->num_rows(); 
            $data['main'] = $this->M_payment->get_jadwal_exp(array('id' => $id ))->row(); 

            if ($id == 0) $order = 'id_marketing ASC'; else $order='no_pendaftaran DESC';

            if ($id_marketing == 'All') $marketing = 'id_jadwal = '.$id.''; else $marketing='id_jadwal = '.$id.' AND id_marketing ="'.$id_marketing.'"';        

            if ($status == 'All' ) $where = $marketing; else $where=$marketing.' AND IF((SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)<> null,
                                            (case 
                                                WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)  = "Refund" THEN "Refund"                                                
                                                WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
                                                        (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) > 0  AND 
                                                        (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) < 
                                                        ((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
                                                        (pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
                                                        (pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
                                                        ((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
                                                        pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment + (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Belum Lunas"
                                                WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
                                                        (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) >= 
                                                        ((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
                                                        (pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
                                                        (pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
                                                        ((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
                                                        pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment+ (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Lunas"
                                                ELSE "Registrasi"
                                            END
                                        ),
                                        ( case 
                                                WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)  = "Refund" THEN "Refund"                                                
                                                WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) > 0 AND 
                                                        (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) < 
                                                        ((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
                                                        (pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
                                                        (pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
                                                        ((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
                                                        pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Belum Lunas"
                                                WHEN (SELECT COUNT(id) FROM pembayaran WHERE via_bank="Terhutang" AND id_pendaftaran=pendaftar.id >= 1) THEN "Terhutang"
                                                WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) >= 
                                                        ((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
                                                        (pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
                                                        (pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
                                                        ((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
                                                        pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Lunas"
                                                ELSE "Registrasi"
                                            END
                                        )) = "'.$status.'"';
            $data ['detail'] = $this->M_payment->get_export($where,$order,$limit)->result();  
            $whereFooter=$where;
            $data ['footer'] = $this->M_payment->get_sum_export2($id,$whereFooter)->row();
            $data ['totalx'] = $id;
            // ini_set("pcre.backtrack_limit", "5000000");
            if ($id == '0') {
                $this->load->view('payment/print_export', $data);
            } else {
                $html = $this->load->view('payment/print_export', $data, TRUE);
                $pdfFilePath = "GM.pdf";
                $mpdf = new \Mpdf\Mpdf([
                                            'mode' => 'utf-8',
                                            'format' => 'Legal-L',
                                            'orientation' => 'L'
                                        ]);

                $mpdf->setFooter('{PAGENO} dari {nb}');
                $mpdf->WriteHTML($html);
                $mpdf->Output();       
            }
        }

        public function load_export_tabungan()
        {
            $data['title'] = '';
            $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();    
            $data['cabang'] =$this->M_payment->get_cabang(array('active' => '1'))->result(); 
            return response($this->load->view('export_tabungan', $data, TRUE), 'html');
        }

        public function export_tabungan()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id_cabang = $this->input->post('id_cabang');
            $id_marketing = $this->input->post('id_marketing');
            $status = $this->input->post('status');
            //$data['main'] = $id;
            if ($id_cabang == 'All') $cabang = ''; else $cabang=' AND kd_office = "'.$id_cabang.'"';
            $data['cabang'] =  $id_cabang;       
            if ($id_marketing == 'All') {
                $marketing = '';
                $data['marketing'] = 'All';
            } else {
                $marketing=' AND id_marketing ="'.$id_marketing.'"';  
                $x=Modules::run('marketing/get_marketing_where', array('id' => $id_marketing))->row();    
                $data['marketing'] = $x->nm_marketing;
            }
            if ($status == 'All' ) $statusFx=''; else if ($status == 'Belum Berangkat') $statusFx=' AND id_paket = "1"'; else $statusFx=' AND id_paket <> 1';
            $data['status'] =  $status;       

            $data['detail'] = $this->M_payment->get_export_tabungan($cabang.$marketing.$statusFx)->result();  


            $html = $this->load->view('payment/print_export_tabungan', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_excel()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');
            $id_marketing = $this->input->post('id_marketing');
            $status = $this->input->post('status');
            //$data['main'] = $id;
            $limit = $this->M_payment->get_pendaftar(array('jadwal.id' => $id ))->num_rows(); 
            $data['main'] = $this->M_payment->get_jadwal_exp(array('id' => $id ))->row(); 

            if ($id == 0) $order = 'id_marketing ASC'; else $order='no_pendaftaran DESC';

            if ($id_marketing == 'All') $marketing = 'id_jadwal = '.$id.''; else $marketing='id_jadwal = '.$id.' AND id_marketing ="'.$id_marketing.'"';        

            if ($status == 'All' ) $where = $marketing; else $where=$marketing.' AND status = "'.$status.'"';

            $data ['detail'] = $this->M_payment->get_export($where,$order,$limit)->result();  

            $whereFooter=$marketing;
            $data ['footer'] = $this->M_payment->get_sum_export2($id,$whereFooter)->row();
            $data ['totalx'] = $id;
            $this->load->view('payment/print_export_excel', $data);


        }

        public function add_multi()
        {
            // $this->validateInput();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;            

            if(array_key_exists('Admin Finance', $this->userLevel)) {
                $sts_verifikasi='1';
            } else {
               $sts_verifikasi='1';
            }

            $cek = count($this->input->post('nm_lengkap'));

            for ($i=0; $i < $cek; $i++) { 

                $x = $this->M_payment->get_where_payment(array('active'=>'1'))->num_rows();
                $faktur = 'IC-'.date('m').date("Y").sprintf('%03d',$x+1);
            
                    $this->upload->initialize($config);
                    if($this->upload->do_upload("file")){
                        $gbr = $this->upload->data();
                        //Compress Image
                        $config['image_library']='gd2';
                        $config['source_image']='./upload/'.$gbr['file_name'];
                        $config['create_thumb']= FALSE;
                        $config['maintain_ratio']= FALSE;
                        $config['quality']= '70%';
                        // $config['width']= 600;
                        // $config['height']= 400;
                        $config['new_image']= './upload/'.$gbr['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();
                        $gambar=$gbr['file_name'];

                        $data = array(
                            'no_faktur' => $faktur,
                            'jenis' => 'IC',
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                            'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                            'jml_bayar' => preg_replace("/[^0-9\.]/", "",$this->input->post('jml_bayar',TRUE))[$i],
                            'path_bukti_bayar' => $gambar,
                            'catatan' => $this->input->post('catatan',TRUE)[$i],
                            'tunai' => 'Transfer',
                            'via_bank' => $this->input->post('via_bank',TRUE),
                            'active' => '1',
                            'status_verifikasi' => $sts_verifikasi,
                            'id_user' => $this->currentUser->id,
                            );
                    } else {
                        $data = array(
                            'no_faktur' => $faktur,
                            'jenis' => 'IC',
                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                            'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                            'jml_bayar' => preg_replace("/[^0-9\.]/", "",$this->input->post('jml_bayar',TRUE))[$i],
                            'catatan' => $this->input->post('catatan',TRUE)[$i],
                            'tunai' => 'Transfer',
                            'via_bank' => $this->input->post('via_bank',TRUE),
                            'active' => '1',
                            'status_verifikasi' => $sts_verifikasi,
                            'id_user' => $this->currentUser->id,
                            );
                    }
                    $query = $this->M_payment->_insert($data);
                    //
                    $jenis = $this->input->post('jenis')[$i];
                    $id_paket = $this->input->post('id_paket')[$i];

                    if ($jenis == 'Tabungan') 
                    {
                        if ($id_paket == 1 && $jenis == 'Tabungan') 
                        {                
                            $benefit_lunas_marketing = array(
                                'no_faktur' => $faktur,
                                'id_marketing' => $this->input->post('id_marketing',TRUE)[$i],
                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                'jml_benefit' => '400000',
                                'status' => '0',
                                'active' => '1',
                                'id_user' => $this->currentUser->id,
                                );
                            $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);

                            // insert benefit cabang
                            if ($this->input->post('kd_cabang')[$i] != 'MKS') {                
                                $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                                if ($row_cabang2->jenis_cabang == 'Mandiri') {
                                    $benefit_berdasarkan_cabang = 150000;
                                } else {
                                    $benefit_berdasarkan_cabang = 150000;
                                }                
                                // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                                // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;

                                $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_berdasarkan_cabang,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $q_benefit_cabang = $this->M_payment->_insert_benefit_cabang($benefit_cabang);
                            }
                            // Batas insert benefit cabang
                            
                                //Insert atasan" Marketing
                                $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing')[$i]);
                                $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                                $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                                //print_r($id_atasan);
                                //echo 'ini'.$id_atasan->id[1];

                                for ($i=0; $i < $count_id_atasan; $i++) {   

                                    if ($id_atasan[$i]->level == 'Marketing') {
                                            $benefit = '150000';
                                        } else {
                                            $benefit = '150000';
                                        }

                                    $benefit_id_id_atasan = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $id_atasan[$i]->id,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    if ($this->input->post('id_marketing')[$i] != 24) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);

                                    // $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                                }

                        } else {
                            //Pelunasan
                            $dx=preg_replace("/[^0-9\.]/", "",$this->input->post('jml_bayar'))[$i];
                            if ($dx == $this->input->post('sisa')[$i])  {
                                    $benefit_lunas_marketing = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $this->input->post('id_marketing',TRUE),
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE),
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => '600000',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);
                                // benefet referensi cabang
                                $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                                //$cek_cabang = count($this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang'),'referensi_cabang' => $row_cabang->referensi_cabang))->result());
                                if ($row_cabang->referensi_cabang != 0) {
                                    $benefit_referensi = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $row_cabang->referensi_cabang,
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE)[$i],
                                    'jml_benefit' => '250000',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => 'x', //SUPAYA TIDAK TERHITUNG DALAM 1.500.000
                                    );
                                $q_benefit_referensi = $this->M_payment->_insert_benefit($benefit_referensi);
                                }

                                // insert benefit cabang
                                $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                                if ($row_cabang2->jenis_cabang == 'Mandiri') {
                                    $benefit_berdasarkan_cabang = 550000;
                                } else {
                                    $benefit_berdasarkan_cabang = 550000;
                                }                
                                // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                                // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;

                                $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_berdasarkan_cabang,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $q_benefit_cabang = $this->M_payment->_insert_benefit_cabang($benefit_cabang);
                                // Batas insert benefit cabang 
                                //Insert atasan" Marketing
                                $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing')[$i]);
                                $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                                $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                                //print_r($id_atasan);
                                //echo 'ini'.$id_atasan->id[1];

                                for ($i=0; $i < $count_id_atasan; $i++) {   

                                    if ($id_atasan[$i]->level == 'Marketing') {
                                            $benefit = '150000';
                                        } else {
                                            $benefit = '150000';
                                        }

                                    $benefit_id_id_atasan = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $id_atasan[$i]->id,
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    if ($this->input->post('id_marketing')[$i] != 24) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);

                                    // $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                                } 

                                //Insert benefit Bom
                                if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'MJF' or $this->input->post('kd_cabang')[$i] == 'KDI' or 
                                    $this->input->post('kd_cabang')[$i] == 'BLG' or $this->input->post('kd_cabang')[$i] == 'BNE' or $this->input->post('kd_cabang')[$i] == 'BKB' or 
                                    $this->input->post('kd_cabang')[$i] == 'KOU' or $this->input->post('kd_cabang')[$i] == 'SFR' or $this->input->post('kd_cabang')[$i] == 'PKP' or $this->input->post('kd_cabang')[$i] == 'PNR') 
                                {
                                        $cek_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->num_rows();
                                        if ($cek_benefit_bom == 0) {                    
                                                $benefit_lunas_bom = array(
                                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                                    'id_marketing' => '24', //id Bu Murni
                                                    'id_jadwal' => $this->input->post('id_jadwal',TRUE)[$i],
                                                    'jml_benefit' => '50000',
                                                    'status' => '0',
                                                    'active' => '1',
                                                    'id_user' => $this->currentUser->id,
                                                    );
                                                $q_benefit_bom = $this->M_payment->_insert_benefit_bom($benefit_lunas_bom);            
                                        } else {
                                                $row_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->row();
                                                $benefit_lunas_bom = array(
                                                    'id_pendaftaran' => $row_benefit_bom->id_pendaftaran.','.$this->input->post('id_pendaftaran',TRUE)[$i],
                                                    'jml_benefit' => $row_benefit_bom->jml_benefit + 50000,                        
                                                    'user_update' => $this->currentUser->id,
                                                    );
                                                $q_benefit_bom = $this->M_payment->_update_benefit_bom(array('id' => $row_benefit_bom->id), $benefit_lunas_bom);                     
                                        }
                                }
                                //Batas benefit Bom
                            }
                        }

                    }

                    if ($jenis == 'Reguler') 
                    {            
                        // Benefit
                        $dx=preg_replace("/[^0-9\.]/", "",$this->input->post('jml_bayar'))[$i];
                        if ($dx == $this->input->post('total_biaya')[$i])  {
                            //Insert LANGSUNG LUNAS
                            if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'KOU') 
                            {
                                //Insert benefit Lunas Markeing
                                $benefit_lunas_marketing = array(
                                    'no_faktur' => $faktur,
                                    'id_marketing' => $this->input->post('id_marketing',TRUE)[$i],
                                    'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                    'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                    'jml_benefit' => '1000000',
                                    'status' => '0',
                                    'active' => '1',
                                    'id_user' => $this->currentUser->id,
                                    );
                                $q_benefit_lunas_marketing = $this->M_payment->_insert_benefit($benefit_lunas_marketing);                
                            }

                            //Insert benefit Bom
                            if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'MJF' or $this->input->post('kd_cabang')[$i] == 'KDI' or 
                                $this->input->post('kd_cabang')[$i] == 'BLG' or $this->input->post('kd_cabang')[$i] == 'BNE' or $this->input->post('kd_cabang')[$i] == 'BKB' or 
                                $this->input->post('kd_cabang')[$i] == 'KOU' or $this->input->post('kd_cabang')[$i] == 'SFR' or $this->input->post('kd_cabang')[$i] == 'PNR' or $this->input->post('kd_cabang')[$i] == 'PKP' ) 
                            {
                                $cek_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->num_rows();
                                if ($cek_benefit_bom == 0) {                    
                                        $benefit_lunas_bom = array(
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                            'id_marketing' => '24', //id Bu Murni
                                            'id_jadwal' => $this->input->post('id_jadwal',TRUE)[$i],
                                            'jml_benefit' => '50000',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                        $q_benefit_bom = $this->M_payment->_insert_benefit_bom($benefit_lunas_bom);            
                                } else {
                                        $row_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->row();
                                        $benefit_lunas_bom = array(
                                            'id_pendaftaran' => $row_benefit_bom->id_pendaftaran.','.$this->input->post('id_pendaftaran',TRUE)[$i],
                                            'jml_benefit' => $row_benefit_bom->jml_benefit + 50000,                        
                                            'user_update' => $this->currentUser->id,
                                            );
                                        $q_benefit_bom = $this->M_payment->_update_benefit_bom(array('id' => $row_benefit_bom->id), $benefit_lunas_bom);                     
                                }
                            }
                            //Batas benefit Bom

                            // benefet referensi cabang
                            $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                            if ($row_cabang->referensi_cabang != 0) {
                                $cek_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->num_rows();
                                if ($cek_benefit_ref_area == 0) {                    
                                    //Insert benefit ref_area
                                    $benefit_lunas_ref_area = array(
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                            'id_marketing' => $row_cabang->referensi_cabang,
                                            'id_jadwal' => $this->input->post('id_jadwal',TRUE)[$i],
                                            'jml_benefit' => '250000',
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                    $q_benefit_ref_area = $this->M_payment->_insert_benefit_ref_area($benefit_lunas_ref_area);            
                                } else {
                                    $row_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->row();
                                    $benefit_lunas_ref_area = array(
                                            'id_pendaftaran' => $row_benefit_ref_area->id_pendaftaran.','.$this->input->post('id_pendaftaran',TRUE)[$i],
                                            'jml_benefit' => $row_benefit_ref_area->jml_benefit + 250000,                        
                                            'user_update' => $this->currentUser->id,
                                            );
                                    $q_benefit_ref_area = $this->M_payment->_update_benefit_ref_area(array('id' => $row_benefit_ref_area->id), $benefit_lunas_ref_area);                     
                                }
                            }
                            // benefet referensi cabang

                            //// kurangi kursi
                            if ($sts_verifikasi=='1') {
                                $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran')[$i]);

                                $id_jadwal = $jadwal->id_jadwal;
                                $data_jadwal = array(
                                    'sisa_kursi' => $jadwal->sisa_kursi - 1,
                                    );
                                $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                            } 
                            /// batas kurangi kursi

                            //Insert atasan" Marketing
                            $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing')[$i]);
                            $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                            $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                            //print_r($id_atasan);
                            //echo 'ini'.$id_atasan->id[1];
                            if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'KOU') 
                            {
                                for ($j=0; $j < $count_id_atasan; $j++) {   

                                    if ($id_atasan[$j]->level == 'Marketing') {
                                        $benefit = '300000';
                                        $benefit_id_id_atasan = array(
                                            'no_faktur' => $faktur,
                                            'id_marketing' => $id_atasan[$j]->id,
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$j],
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => $benefit,
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                        if ($this->input->post('id_marketing')[$i] != 24) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);

                                        // $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                                    }
                                }
                            }
                            // batas Insert atasan" Marketing
                            // insert benefit cabang
                            $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                            if ($row_cabang2->jenis_cabang == 'Mandiri') {
                                $benefit_berdasarkan_cabang = 2000000;
                            } else {
                                $benefit_berdasarkan_cabang = 1500000;
                            }                
                            // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                            // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;
                            if ($this->input->post('kd_cabang')[$i] != 'MKS') {                
                                $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_berdasarkan_cabang,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $q_benefit_cabang = $this->M_payment->_insert_benefit_cabang($benefit_cabang);
                            }
                            // Batas insert benefit cabang                

                        // BAAS Insert LANGSUNG LUNAS 
                        } else {
                            if ($this->input->post('kd_cabang')[$i] != 'MKS') {
                            $count_status_ada_cabang = count($this->M_payment->get_status_ada_cabang($this->input->post('id_pendaftaran')[$i]));
                                if ($count_status_ada_cabang==0) { 
                                    $benefit_cabang = array(
                                            'no_faktur' => $faktur,
                                            'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                                            'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                            'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                            'jml_benefit' => 500000,
                                            'status' => '0',
                                            'active' => '1',
                                            'id_user' => $this->currentUser->id,
                                            );
                                    $q_benefit_cabang = $this->M_payment->_insert_benefit_cabang($benefit_cabang);
                                } 
                            } 

                            $count_status_ada = count($this->M_payment->get_status_ada($this->input->post('id_pendaftaran')[$i]));
                            if ($count_status_ada==0) {
                                if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'KOU') {
                                    //Insert benefit DP Marketing
                                    $benefit_dp_marketing = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('id_marketing',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => '300000',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    $q_benefit_dp_marketing = $this->M_payment->_insert_benefit($benefit_dp_marketing);
                                }                    
                                //// kurangi kursi
                                if ($sts_verifikasi=='1') {
                                    $jadwal=$this->M_payment->get_jadwal($this->input->post('id_pendaftaran')[$i]);

                                    $id_jadwal = $jadwal->id_jadwal;
                                    $data_jadwal = array(
                                        'sisa_kursi' => $jadwal->sisa_kursi - 1,
                                        );
                                    $query_jadwal = $this->M_payment->_update_jadwal(array('id' => $id_jadwal), $data_jadwal);
                                } 
                                /// batas kurangi kursi
                            }
                            $dx=preg_replace("/[^0-9\.]/", "",$this->input->post('jml_bayar'))[$i];
                            if ($dx == $this->input->post('sisa')[$i])  {
                                if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'KOU') 
                                {
                                    //Insert benefit lunas Marketing
                                    $benefit_pelunasan_marketing = array(
                                        'no_faktur' => $faktur,
                                        'id_marketing' => $this->input->post('id_marketing',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => '700000',
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                    $q_benefit_pelunasan_marketing = $this->M_payment->_insert_benefit($benefit_pelunasan_marketing);
                                
                                    //Insert atasan" Marketing
                                    $get_id_id_atasan = $this->M_payment->get_marketing($this->input->post('id_marketing')[$i]);
                                    $id_atasan = $this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result();
                                    $count_id_atasan = count($this->M_payment->get_benefit($get_id_id_atasan->id_id_atasan)->result());

                                    //print_r($id_atasan);
                                    //echo 'ini'.$id_atasan->id[1];

                                    for ($j=0; $j < $count_id_atasan; $j++) {   

                                        if ($id_atasan[$j]->level == 'Marketing') {
                                            $benefit = '300000';
                                            $benefit_id_id_atasan = array(
                                                'no_faktur' => $faktur,
                                                'id_marketing' => $id_atasan[$j]->id,
                                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$j],
                                                'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                                'jml_benefit' => $benefit,
                                                'status' => '0',
                                                'active' => '1',
                                                'id_user' => $this->currentUser->id,
                                                );
                                            if ($this->input->post('id_marketing')[$i] != 24) $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);

                                            // $q_benefit_id_id_atasan = $this->M_payment->_insert_benefit($benefit_id_id_atasan);
                                        }
                                    }
                                }

                                //Insert benefit Bom
                                if ($this->input->post('kd_cabang')[$i] == 'MKS' or $this->input->post('kd_cabang')[$i] == 'MJF' or $this->input->post('kd_cabang')[$i] == 'KDI' or 
                                    $this->input->post('kd_cabang')[$i] == 'BLG' or $this->input->post('kd_cabang')[$i] == 'BNE' or $this->input->post('kd_cabang')[$i] == 'BKB' or 
                                    $this->input->post('kd_cabang')[$i] == 'KOU' or $this->input->post('kd_cabang')[$i] == 'SFR' or $this->input->post('kd_cabang')[$i] == 'PNR' or $this->input->post('kd_cabang')[$i] == 'PKP' ) 
                                {
                                    $cek_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->num_rows();
                                    if ($cek_benefit_bom == 0) {                    
                                            $benefit_lunas_bom = array(
                                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                                'id_marketing' => '24', //id Bu Murni
                                                'id_jadwal' => $this->input->post('id_jadwal',TRUE)[$i],
                                                'jml_benefit' => '50000',
                                                'status' => '0',
                                                'active' => '1',
                                                'id_user' => $this->currentUser->id,
                                                );
                                            $q_benefit_bom = $this->M_payment->_insert_benefit_bom($benefit_lunas_bom);            
                                    } else {
                                            $row_benefit_bom = $this->M_payment->get_benefit_bom(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->row();
                                            $benefit_lunas_bom = array(
                                                'id_pendaftaran' => $row_benefit_bom->id_pendaftaran.','.$this->input->post('id_pendaftaran',TRUE)[$i],
                                                'jml_benefit' => $row_benefit_bom->jml_benefit + 50000,                        
                                                'user_update' => $this->currentUser->id,
                                                );
                                            $q_benefit_bom = $this->M_payment->_update_benefit_bom(array('id' => $row_benefit_bom->id), $benefit_lunas_bom);                     
                                    }
                                }
                                //Batas benefit Bom

                                // benefet referensi cabang
                                $row_cabang = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                                if ($row_cabang->referensi_cabang != 0) {
                                    $cek_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->num_rows();
                                    if ($cek_benefit_ref_area == 0) {                    
                                        //Insert benefit ref_area
                                        $benefit_lunas_ref_area = array(
                                                'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                                'id_marketing' => $row_cabang->referensi_cabang,
                                                'id_jadwal' => $this->input->post('id_jadwal',TRUE)[$i],
                                                'jml_benefit' => '250000',
                                                'status' => '0',
                                                'active' => '1',
                                                'id_user' => $this->currentUser->id,
                                                );
                                        $q_benefit_ref_area = $this->M_payment->_insert_benefit_ref_area($benefit_lunas_ref_area);            
                                    } else {
                                        $row_benefit_ref_area = $this->M_payment->get_benefit_ref_area(array('status' => '0','id_jadwal' => $this->input->post('id_jadwal')[$i]))->row();
                                        $benefit_lunas_ref_area = array(
                                                'id_pendaftaran' => $row_benefit_ref_area->id_pendaftaran.','.$this->input->post('id_pendaftaran',TRUE)[$i],
                                                'jml_benefit' => $row_benefit_ref_area->jml_benefit + 250000,                        
                                                'user_update' => $this->currentUser->id,
                                                );
                                        $q_benefit_ref_area = $this->M_payment->_update_benefit_ref_area(array('id' => $row_benefit_ref_area->id), $benefit_lunas_ref_area);                     
                                    }
                                }
                                // benefet referensi cabang
                                // benefet referensi cabang
                                // insert benefit cabang
                                $row_cabang2 = $this->M_payment->get_cabang(array('kd_cabang'=>$this->input->post('kd_cabang')[$i]))->row();
                                if ($row_cabang2->jenis_cabang == 'Mandiri') {
                                    $benefit_berdasarkan_cabang = 1500000;
                                } else {
                                    $benefit_berdasarkan_cabang = 1000000;
                                }                
                                // $sumbenefit = $this->M_payment->get_sum_benefit($this->input->post('id_pendaftaran'))->row();
                                // $tam_benefit = $benefit_berdasarkan_cabang - $sumbenefit->jml_benefit;
                                if ($this->input->post('kd_cabang')[$i] != 'MKS') {
                                $benefit_cabang = array(
                                        'no_faktur' => $faktur,
                                        'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                                        'id_pendaftaran' => $this->input->post('id_pendaftaran',TRUE)[$i],
                                        'tgl_payment' => $this->input->post('tgl_bayar',TRUE),
                                        'jml_benefit' => $benefit_berdasarkan_cabang,
                                        'status' => '0',
                                        'active' => '1',
                                        'id_user' => $this->currentUser->id,
                                        );
                                $q_benefit_cabang = $this->M_payment->_insert_benefit_cabang($benefit_cabang);
                                }
                                // Batas insert benefit cabang                
                            }
                            // batas Insert atasan" Marketing
                        }

                    } 
            }   
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
            
            return response($response, 'json');
        }

    }
?>

