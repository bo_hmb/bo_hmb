<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_payment extends CI_Model 
	{
		

		var $table = 'pendaftar';
		var $table2 = 'pembayaran';
		var $formulir = 'formulir';
		var $paket = 'paket';
		var $jadwal = 'jadwal';
		var $benefit_marketing = 'benefit_marketing';
		var $benefit_cabang = 'benefit_cabang';
		var $benefit_bom = 'benefit_bom';
		var $benefit_ref_area = 'benefit_ref_area';
		var $cabang = 'cabang';
		var $marketing = 'marketing';
		var $tv_payment = 'tv_payment';
		var $kota = 'ind_regencies';

	    var $column_order = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $column_search = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_export($where,$order,$limit) {
			$results = array();
			$query = $this->db->query(' SELECT
										pendaftar.no_pendaftaran,
										paket.jenis_travel,
										paket.nm_paket,
										paket.hrg_paket,
										pendaftar.potongan_paket,
										(paket.hrg_paket-pendaftar.potongan_paket) AS hrg_paket2,
										formulir.nm_lengkap,
										(
											( YEAR ( now( ) ) - YEAR ( formulir.tgl_lahir ) ) - (
										CASE
											
											WHEN (
											( ( MONTH ( formulir.tgl_lahir ) * 100 ) + dayofmonth( formulir.tgl_lahir ) ) > ( ( MONTH ( now( ) ) * 100 ) + dayofmonth( now( ) ) ) 
											) THEN
												1 ELSE 0 
											END 
											) 
											) AS age,
										pendaftar.biaya_mahram,
										pendaftar.biaya_progresif,
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah) AS biaya_up_hotel_makkah,
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah) AS biaya_up_hotel_madinah,
										(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar) AS biaya_upgrade_kamar,
										(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah) AS biaya_upgrade_kamar_madinah,
										pendaftar.biaya_order_barang,
										pendaftar.add_on,
										pendaftar.overpayment,
										((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
										((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
										pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) AS total_biaya,
										IF((SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1) = "Refund",
											((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) -
											(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)),
											((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1))) as telah_bayar,
											
										IF((SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1) = "Refund",
											((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
											(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
											(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
											((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
											pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) - (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) + 
											(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1),
											((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
											(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
											(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
											((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
											pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) - (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1)) as sisa,
										marketing.nm_marketing,
										marketing.no_telp AS no_telp_marketing,
										IF((SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)<> null,
											(case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"												
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment + (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Belum Lunas"
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment+ (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Lunas"
												ELSE "Registrasi"
											END
										),
										( case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"												
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Belum Lunas"
												WHEN (SELECT COUNT(id) FROM pembayaran WHERE via_bank="Terhutang" AND id_pendaftaran=pendaftar.id >= 1) THEN "Terhutang"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Lunas"
												ELSE "Registrasi"
											END
										))	AS status,
										pendaftar.jenis,
										cabang.nm_cabang,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id										
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE '.$where.' AND pendaftar.active=1 order by '.$order.' LIMIT 0,'.$limit.'');
			return $query;
		}

		public function get_export_tabungan($where) {
			$results = array();
			$query = $this->db->query(' SELECT * FROM tv_payment WHERE jenis = "Tabungan" AND active=1 '.$where.' order by kd_office ASC');
			return $query;
		}

		public function get_jadwal_exp($where) {
			$table = $this->jadwal;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_jadwal($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.id_pendaftaran,
										pendaftar.no_pendaftaran,
										pendaftar.id_paket,
										paket.nm_paket,
										paket.id_jadwal,
										jadwal.nm_jadwal,
										jadwal.sisa_kursi
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE id_pendaftaran="'.$id.'"
									');
			return $query->row();	
		}

		public function _update_jadwal($where, $data) {
			$table = $this->jadwal;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function get_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$this->db->order_by('nm_cabang','ASC');
			$query=$this->db->get($table);
			return $query;
		}

		public function get_kota_where($where) {
            $table = $this->kota;
            $this->db->where($where);
            $query=$this->db->get($table);
            return $query;
        }

		public function get_where_payment($where) {
			$table = $this->table2;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_benefit($where) {
			$table = $this->benefit_marketing;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_detail($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pendaftar.id,
										pendaftar.id_formulir,
										pendaftar.kd_office,
										pendaftar.id AS id_pendaftaran,
										pendaftar.no_pendaftaran,
										paket.jenis_travel,
										pendaftar.id_paket,
										paket.nm_paket,
										paket.hrg_paket,
										pendaftar.potongan_paket,
										(paket.hrg_paket-pendaftar.potongan_paket) AS hrg_paket2,
										jadwal.id as id_jadwal,
										jadwal.tgl_keberangkatan,
										jadwal.batas_pelunasan,
										formulir.no_ktp,
										formulir.nm_lengkap,
										formulir.tgl_lahir,
										(
											( YEAR ( now( ) ) - YEAR ( formulir.tgl_lahir ) ) - (
										CASE
											
											WHEN (
											( ( MONTH ( formulir.tgl_lahir ) * 100 ) + dayofmonth( formulir.tgl_lahir ) ) > ( ( MONTH ( now( ) ) * 100 ) + dayofmonth( now( ) ) ) 
											) THEN
												1 ELSE 0 
											END 
											) 
											) AS age,
										formulir.path_foto,
										pendaftar.tgl_pendaftaran,
										pendaftar.nm_mahram,
										pendaftar.hp_mahram,
										pendaftar.hub_mahram,
										pendaftar.keluarga_terdekat,
										pendaftar.hubungan,
										pendaftar.biaya_mahram,
										pendaftar.biaya_progresif,
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah) AS biaya_up_hotel_makkah,
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah) AS biaya_up_hotel_madinah,
										pendaftar.jenis_kamar,
										(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar) AS biaya_upgrade_kamar,
										(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah) AS biaya_upgrade_kamar_madinah,
										pendaftar.biaya_order_barang,
										pendaftar.add_on,
										pendaftar.overpayment,
										pendaftar.fc_ktp,
										pendaftar.fc_aktakelahiran,
										pendaftar.fc_bukunikah,
										pendaftar.buku_faksin_asli,
										pendaftar.pasport_asli,
										pendaftar.pas_photo,
										((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
										((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
										pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) AS total_biaya,
										IF((SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1) = "Refund",
											((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) -
											(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)),
											((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1))) as telah_bayar,
											
										IF((SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1) = "Refund",
											((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
											(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
											(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
											((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
											pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) - (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) + 
											(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1),
											((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
											(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
											(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
											((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
											pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) - (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1)) as sisa,
										
										
										formulir.id_marketing,
										marketing.nm_marketing,
										marketing.no_telp AS no_telp_marketing,
										IF((SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)<> null,
											(case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"												
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment + (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Belum Lunas"
												WHEN ((SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) - 
														(SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment+ (SELECT biaya FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)) THEN "Lunas"
												ELSE "Registrasi"
											END
										),
										( case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"												
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Belum Lunas"
												WHEN (SELECT COUNT(id) FROM pembayaran WHERE via_bank="Terhutang" AND id_pendaftaran=pendaftar.id >= 1) THEN "Terhutang"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1  AND pembayaran.active=1) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
														pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) THEN "Lunas"
												ELSE "Registrasi"
											END
										))	AS status,
										pendaftar.jenis,
										cabang.nm_cabang,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id										
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE pendaftar.id="'.$id.'"');
	        return $query->row();
		}

		public function get_payment_detail($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.id_pendaftaran,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.tunai,
										pembayaran.status_verifikasi,
										pembayaran.active
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										WHERE pembayaran.id_pendaftaran="'.$id.'" AND pembayaran.active=1 AND pembayaran.status_verifikasi=1 
										ORDER BY pembayaran.tgl_bayar ASC');
	        return $query->result();
		}

		public function get_payment_detail_row($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.id_pendaftaran,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.tunai,
										pembayaran.status_verifikasi,
										pembayaran.active
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										WHERE pembayaran.id_pendaftaran="'.$id.'" AND pembayaran.active=1 AND pembayaran.status_verifikasi=1
										ORDER BY pembayaran.id DESC');
	        return $query->row();
		}

		public function _insert($data) {
			$table2 = $this->table2;
			$insert = $this->db->insert($table2, $data);
			return $insert;
		}

		public function _insert_benefit($data) {
			$table3 = $this->benefit_marketing;
			$insert = $this->db->insert($table3, $data);
			return $insert;
		}

		public function get_benefit_bom($where) {
			$table = $this->benefit_bom;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert_benefit_bom($data) {
			$table5 = $this->benefit_bom;
			$insert = $this->db->insert($table5, $data);
			return $insert;
		}

		public function _update_benefit_bom($where, $data) {
			$table6 = $this->benefit_bom;
			$this->db->where($where);
			$update = $this->db->update($table6, $data);
			return $update;
		}

		public function get_benefit_ref_area($where) {
			$table = $this->benefit_ref_area;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert_benefit_ref_area($data) {
			$table5 = $this->benefit_ref_area;
			$insert = $this->db->insert($table5, $data);
			return $insert;
		}

		public function _update_benefit_ref_area($where, $data) {
			$table6 = $this->benefit_ref_area;
			$this->db->where($where);
			$update = $this->db->update($table6, $data);
			return $update;
		}

		public function _insert_benefit_cabang($data) {
			$table4 = $this->benefit_cabang;
			$insert = $this->db->insert($table4, $data);
			return $insert;
		}


		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    
	    	$this->db->select(		   'pendaftar.id,
										pendaftar.id_formulir,
										pendaftar.kd_office,
										pendaftar.id AS id_pendaftaran,
										pendaftar.no_pendaftaran,
										paket.jenis_travel,
										pendaftar.id_paket,
										paket.nm_paket,
										paket.hrg_paket,	
										jadwal.id AS id_jadwal,										
										jadwal.tgl_keberangkatan,										
										formulir.nm_lengkap,		
										(
											( YEAR ( now( ) ) - YEAR ( formulir.tgl_lahir ) ) - (
										CASE
											
											WHEN (
											( ( MONTH ( formulir.tgl_lahir ) * 100 ) + dayofmonth( formulir.tgl_lahir ) ) > ( ( MONTH ( now( ) ) * 100 ) + dayofmonth( now( ) ) ) 
											) THEN
												1 ELSE 0 
											END 
											) 
											) AS age,								
										((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
										(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
										(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
										((pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah))+
										pendaftar.biaya_order_barang+pendaftar.add_on+pendaftar.overpayment) AS total_biaya,
										(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1 AND pembayaran.active=1) as telah_bayar,
										formulir.id_marketing,
										marketing.nm_marketing,
										marketing.no_telp AS no_telp_marketing,										
										pendaftar.jenis,
										cabang.nm_cabang,
										pendaftar.active


	    	');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        $l = ($_POST['length']) ? $_POST['length'] : 20 ;
	        if($l != -1)
	        $this->db->limit($_POST['length'], $_POST['start']); 
	    	// else
	     //    $this->db->limit(20,0);
	    	// join table
	    	$this->db->join($this->formulir, ''.$this->table.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->paket, ''.$this->table.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	    	$this->db->join($this->marketing, ''.$this->formulir.'.id_marketing = '.$this->marketing.'.id');
	    	$this->db->join($this->cabang, ''.$this->table.'.kd_office = '.$this->cabang.'.kd_cabang');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_marketing($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT marketing_level.id_id_atasan from marketing_level WHERE marketing_level.id_marketing='.$id.'');
			return $query->row();	
		}

		public function get_benefit($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT marketing.id,marketing.nik,marketing.nm_marketing,marketing.id_level,
										level_marketing.level FROM marketing INNER JOIN level_marketing ON 
										marketing.id_level = level_marketing.id WHERE marketing.id IN ('.$id.') ORDER BY marketing.id ASC');
			return $query;	
		}

		public function get_status_ada($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT * FROM benefit_marketing WHERE id_pendaftaran='.$id.'');
			return $query->result();	
		}

		public function get_status_ada_cabang($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT * FROM benefit_cabang WHERE id_pendaftaran='.$id.'');
			return $query->result();	
		}

		public function get_sum_benefit($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT SUM(jml_benefit) AS jml_benefit FROM benefit_marketing WHERE id_pendaftaran='.$id.' AND NOT id_user="x"');
			return $query;	
		}

		public function get_sum_export($where)
		{
			$results = array();
			$query = $this->db->query(' SELECT 
										SUM(hrg_paket) AS hrg_paket,SUM(biaya_mahram) AS biaya_mahram, 
										SUM(biaya_progresif) AS biaya_progresif,SUM(biaya_order_barang) AS biaya_order_barang,
										SUM(biaya_upgrade_kamar) AS biaya_upgrade_kamar,SUM(biaya_up_hotel_makkah+biaya_up_hotel_madinah) AS biaya_up_hotel,SUM(add_on) AS add_on, 
										SUM(total_biaya) AS total_biaya, SUM(sisa) AS sisa
										FROM tv_payment');
	    	$this->db->where($where);
			return $query;	
		}

		public function get_sum_export2($id,$where)
		{
			$results = array();
			$query = $this->db->query(' SELECT SUM(hrg_paket) AS hrg_paket,SUM(potongan_paket) AS potongan_paket,SUM(biaya_mahram) AS biaya_mahram, 
										SUM(biaya_progresif) AS biaya_progresif,SUM(biaya_order_barang) AS biaya_order_barang,
										SUM(biaya_upgrade_kamar) AS biaya_upgrade_kamar,SUM(biaya_up_hotel_makkah+biaya_up_hotel_madinah) AS biaya_up_hotel,SUM(add_on) AS add_on,SUM(overpayment) AS overpayment,
										SUM(total_biaya) AS total_biaya,SUM(telah_bayar) AS telah_bayar, SUM(sisa) AS sisa
										FROM tv_payment WHERE active="1" AND '.$where.'');
			return $query;
		}

		public function cek_payment($where)
		{
			$table = $this->table2;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_umur($id) {
			$results = array();
	        $query = $this->db->query('SELECT id,nm_lengkap,tgl_lahir, (YEAR(CURDATE())-YEAR(tgl_lahir)) 
	        						   AS age_yeard,TIMESTAMPDIFF(MONTH, tgl_lahir, NOW()) AS age
	        						   FROM formulir WHERE id='.$id.'');
	        return $query->row();
		}

		public function _insert_notif($data) {
			$table = $this->table;
			$insert = $this->db->insert('notification', $data);
			return $insert;
		}

		public function get_pendaftar($where)
		{
			$this->db->where($where);
	    	$this->db->join('paket', 'pendaftar.id_paket = paket.id');
	    	$this->db->join('jadwal', 'paket.id_jadwal = jadwal.id');
			$query=$this->db->get('pendaftar');
			return $query;
		}
		
	}
