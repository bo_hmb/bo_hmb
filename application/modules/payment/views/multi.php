            <form role="form" action="#" class="form-horizontal" id="form-multi">
                
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal Transfer</label>
                        <div class="col-md-2">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="tgl_bayar" class="form-control" placeholder="Tanggal" value="<?php echo(date('Y-m-d')) ?>" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-md-1">
                        </div>
                        <label class="col-md-2 control-label">Pilih Bank</label>
                        <div class="col-md-3">
                            <select class="form-control jeniskelamin " id="via_bank" name="via_bank">
                                <option value="Mandiri">Mandiri</option>
                                <option value="BRI">BRI</option>
                                <option value="BNI Syariah">BNI Syariah</option>
                                <option value="BSM">BSM</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                    <label class="col-md-3 control-label" style="color:red">Bukti Bayar(Max.1.9MB)</label>
                        <div class="col-md-5">
                            <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                        </div>
                    </div>
                </div>


                

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:10%">No</th>
                                    <th style="width:30%">Nama Lengkap</th>
                                    <th>Sisa Bayar</th>
                                    <th>Catatan</th>
                                    <th>Jumlah Bayar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" value="0" readonly="">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     