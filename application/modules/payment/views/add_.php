
<form action="#" id="form-create" role="form">
    <div class="form-body">
        <span class="caption-subject font-dark bold uppercase">Data Pendaftar</span>
        <input type="text" class="hidden" name="id_pendaftaran" id="id_pendaftar" value="<?php echo $main->id ?>">
        <input type="text" class="hidden" name="id_marketing" value="<?php echo $main->id_marketing ?>">
        <input type="text" class="hidden" name="kd_cabang" value="<?php echo $main->kd_office ?>">
        <input type="text" class="hidden" name="id_paket" id="id_paket" value="<?php echo $main->id_paket ?>">
        <input type="text" class="hidden" name="id_jadwal" id="id_jadwal" value="<?php echo $main->id_jadwal ?>">
        <input type="text" class="hidden" name="jenis" id="jenis" value="<?php echo $main->jenis ?>">
        <input type="text" class="hidden" name="jenis_travel" id="jenis" value="<?php echo $main->jenis_travel ?>">
        <input type="hidden" name="userLevel" id="userLevel" value="<?php echo $userLevel ?>">

        <div class="row">
            <br>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" name="no_pendaftaran" class="form-control" readonly placeholder="Nomor Pendaftaran" value="<?php echo $main->no_pendaftaran ?>">
                    <label for="form_control_1">Nomor Pendaftaran
                    </label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group" data-date-format="yyyy-mm-dd">
                        <input type="text" name="tgl_pendaftaran" class="form-control" readonly value="<?php echo $main->tgl_pendaftaran ?>">
                        <label class="control-label">Tanggal Pendaftaran
                        </label>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly placeholder="Nama Lengkap" value="<?php echo $main->nm_lengkap ?>">
                    <label for="form_control_1">Nama Lengkap
                    </label>
                    <span class="help-block">Nama Lengkap</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->no_ktp ?>">
                    <label for="form_control_1">NIK
                    </label>
                </div>
            </div>
        </div>
        <span class="caption-subject font-dark bold uppercase">Biaya - biaya</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_mahram,0,',','.')?>">
                    <label for="form_control_1">Biaya Mahram
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_progresif,0,',','.')?>">
                    <label for="form_control_1">Biaya Progresif
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_order_barang,0,',','.')?>">
                    <label for="form_control_1">Biaya Order Barang
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_up_hotel_makkah,0,',','.')?>">
                    <label for="form_control_1">Biaya Upgrade Hotel Makkah
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_up_hotel_madinah,0,',','.')?>">
                    <label for="form_control_1">Biaya Upgrade Hotel Madinah
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->biaya_upgrade_kamar,0,',','.') ?>">
                    <label for="form_control_1">Biaya Upgrade Kamar
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->hrg_paket,0,',','.') ?>">
                    <label for="form_control_1">Harga Paket
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->add_on,0,',','.') ?>">
                    <label for="form_control_1">Add On
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo "Rp." . number_format($main->overpayment,0,',','.') ?>">
                    <label for="form_control_1">Overpayment
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="total_biaya" readonly value="<?php echo "Rp." . number_format($main->total_biaya,0,',','.') ?>">
                    <input type="text" class="hidden" name="total_biaya2" readonly value="<?php echo $main->total_biaya ?>">

                    <label for="form_control_1">Total Biaya
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="telah_bayar" readonly value="<?php echo "Rp." . number_format($main->telah_bayar,0,',','.') ?>">
                    <label for="form_control_1">Telah Bayar
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <?php 
                    if ($main->sisa != null) {
                        $sisa=$main->sisa;
                        } else {
                        $sisa=$main->total_biaya;
                    } ?>
                    <input type="text" class="form-control" name="sisa" readonly value="<?php echo "Rp." . number_format($sisa,0,',','.') ?>">
                    <input type="text" class="hidden" name="sisa2" id="sisa2" readonly value="<?php echo $sisa ?>">
                    <label for="form_control_1">Sisa Bayar
                    </label>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Masukkan Data Pembayaran</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                        <input type="text" name="tgl_bayar" class="form-control" readonly value="<?php echo date('Y-m-d') ?>">
                        <label class="control-label">Tanggal Pembayaran
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pembayaran </span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jml_bayar" id="jml_bayar" placeholder="Masukkan Jumlah Bayar">
                    <label for="form_control_1">Jumlah Bayar
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Jumlah Bayar</span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin catatan" id="catatan" name="catatan">
                        <option value="DP">DP</option>
                        <option value="Pembayaran Ke-II">Pembayaran Ke-II</option>
                        <option value="Pembayaran Ke-III">Pembayaran Ke-III</option>
                        <option value="Pembayaran Ke-IV">Pembayaran Ke-IV</option>
                        <option value="Pembayaran Ke-V">Pembayaran Ke-V</option>
                        <option value="Pelunasan">Pelunasan</option>
                    </select>
                    <!-- <input type="text" class="form-control" name="catatan" placeholder="Masukkan Catatan"> -->
                    <label for="form_control_1">Catatan                        
                    </label>
                    <span class="help-block">Pilih Catatan</span>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group form-md-line-input">
                    <label class="mt-checkbox mt-checkbox-outline" for="1">                        
                        <input onclick="return <?php if (array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) echo ""; else echo "false;"; ?>" checked id="1" type="checkbox" name="cb_ps" value="1" class="input-small cb_ps" />PS
                        <span></span>
                    </label>
                    <input type="hidden" class="ps" name="ps" id="ps" value="1">
                </div>        
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin tunai" id="tunai" name="tunai">
                        <option value="Transfer">Transfer</option>
                        <?php if ($this->kode_cabang == "MKS") { ?>
                        <option value="Tunai">Tunai</option>
                        <?php } ?>
                    </select>
                    <label>Tunai / Transfer
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Tunai / Transfer</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                   <select class="form-control jeniskelamin " id="via_bank" name="via_bank">
                        <option value="Mandiri">Mandiri</option>
                        <option value="BRI">BRI</option>
                        <option value="BNI Syariah">BNI Syariah</option>
                        <option value="BSM">BSM</option>
                        <?php if ($this->kode_cabang == "MKS") { ?>
                        <option value="Terhutang">Terhutang</option>
                        <?php } ?>
                    </select>
                    <label>Via Bank
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Pilih Via Bank</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                    <label style="color:red">Bukti Bayar (Max. 1.9 MB)
                    </label>
                    <span class="help-block">Masukkan Bukti Bayar (Max. 1.9 MB)</span>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>
