<?php

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=ManifestPembayaran.xls");
?>

<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>MANIFEST PEMBAYARAN</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h2 style="text-align: center; font-weight: bold;">MANIFEST PEMBAYARAN</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h3 style="text-align: center;"><?php echo $main->nm_jadwal; ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 50px;">No. Pendaftaran</th>
        <th style="width: 150px;">Nama Lengkap</th>
        <th style="width: 105px;">By. Paket</th>
        <th style="width: 1px;">Potongan</th>
        <th style="width: 80px;">By. Mahram</th>
        <th style="width: 100px;">By. Visa</th>
        <th style="width: 100px;">By. Order Barang</th>
        <th style="width: 100px;">By. Upgrade Kamar</th>
        <th style="width: 100px;">By. Upgrade Hotel</th>
        <th style="width: 100px;">By. Add On</th>
        <th style="width: 105px;">Total Biaya</th>
        <th style="width: 105px;">Telah Bayar</th>
        <th style="width: 100px;">Sisa Bayar</th>
        <th style="width: 70px;">Rekomendasi</th>
        <!-- <th style="width: 70px;">No. HP Rekomendasi</th> -->
        <th style="width: 60px;">Status</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { 
        if  ($row->sisa != null) {
            $sisa=$row->sisa;
            } else {
            $sisa=$row->total_biaya;
            }
        if  ($row->telah_bayar != null) {
            $telah_bayar=$row->telah_bayar;
            } else {
            $telah_bayar=0;
            }

            if ($row->status == 'Registrasi') {
            $warna = 'background-color: #ff0000';
            } else 
            if ($row->status == 'Refund') {
            $warna = 'background-color: #990000';
            } else 
            if ($row->status == 'Belum Lunas') {
               $warna = 'background-color: #ffff00';
            } else {
               $warna = 'background-color: #39e600';
            }
        
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_pendaftaran ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->hrg_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->potongan_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_mahram,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_progresif,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_order_barang,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_upgrade_kamar + $row->biaya_upgrade_kamar_madinah,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format(($row->biaya_up_hotel_makkah + $row->biaya_up_hotel_madinah),0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->add_on,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->total_biaya,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($telah_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($sisa,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?>/<?php echo $row->no_telp_marketing ?></td>  
           <td style="border:1px solid #000;<?php echo $warna; ?>"><?php echo $row->status ?></td>      
        </tr>   
    <?php $nomor++;}?> 
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"></td>
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->hrg_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->potongan_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_mahram,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_progresif,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_order_barang,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_upgrade_kamar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_up_hotel,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->add_on,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->total_biaya,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->telah_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->sisa,0,',','.') ?></td>
           <td style="border:1px solid #000;"></td>  
           <td style="border:1px solid #000;"></td>      
        </tr> 
</table>
</body>
</html>