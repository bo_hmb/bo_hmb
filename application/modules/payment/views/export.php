            <form role="form" method="POST" action="<?php echo base_url(); ?>payment/export" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Schedule</label>
                        <div class="col-md-8">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Select Schedule">
                                <option disabled selected></option>
                                <option></option>
                                <option value="0">Tabungan</option>                                                
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Agen</label>
                        <div class="col-md-8">
                            <select id="id_marketing" name="id_marketing" class="form-control jadwal" data-placeholder="Select Agen">
                                <option disabled selected></option>
                                <option></option>
                                <option value="All">All</option>                                                
                                <?php
                                foreach ($marketing as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->kd_cabang.' - '.$row->nm_marketing ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Status</label>
                        <div class="col-md-8">
                            <select id="status" name="status" class="form-control jadwal" data-placeholder="Select Status">
                                <option disabled selected></option>
                                <option></option>
                                <option value="Registrasi">Registrasi</option>                                                
                                <option value="Belum Lunas">Belum Lunas</option>                                                
                                <option value="Lunas">Lunas</option>                                                
                                <option value="Terhutang">Terhutang</option>                                                
                                <option value="All">All</option>                                                
                            </select>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     