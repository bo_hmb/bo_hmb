<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Income</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Income</span>
                            <input id="userLevelMain" type="hidden" name="userLevelMain" value="<?php 
                            if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel) or $this->currentUser->id == "67" or $this->currentUser->id == "116" or $this->currentUser->id == "118" or $this->currentUser->id == "91") echo "1"; 
                                else echo "0";
                             ?>">
                        </div>
                        <div class="actions">
                            <!-- <button id="btn-multi" class="btn sbold green btn-multi"> Multi Payment
                                <i class="fa fa-plus"></i>
                            </button> -->
                            <div class="btn-group">
                                <!-- <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown"> -->

                                <a class="btn red btn-outline btn-circle " href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export" href="javascript:;"> PDF </a>
                                    </li>
                                    <li>
                                        <a class="btn-export-excel" href="javascript:;"> Excel </a>
                                    </li>
                                    <li>
                                        <a class="btn-export-tabungan" href="javascript:;"> Tabungan </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="table-container">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="10%">Aksi</th>
                                        <th width="10%">Nama Agen</th>
                                        <th width="10%">Nama Cabang</th>
                                        <th width="10%">Jenis</th>
                                        <th width="15%">No. Pendaftaran</th>
                                        <th width="1%">Tggl. Keberangkatan</th>
                                        <!-- <th width="10%">No. KTP</th> -->
                                        <th width="10%">Nama Lengkap</th>
                                        <th width="15%">Total Biaya</th>
                                        <th width="15%">H-</th>
                                        <th width="15%">Telah Bayar</th>
                                        <!-- <th width="15%">Status</th> -->
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <td>
                                        <?php 
                                            if(array_key_exists('Super Admin', $this->userLevel)) {
                                            $marketing = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();           
                                            } else if(array_key_exists('Admin Finance', $this->userLevel)) {
                                             $marketing = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();           
                                            } else if(array_key_exists('Admin CS', $this->userLevel)) {
                                             $marketing = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();

                                            } else {
                                             $marketing = Modules::run('marketing/get_marketing_where', array('active' => '1','kd_cabang' => "$this->kode_cabang"))->result();           
                                            } 
                                         ?>
                                            <select name="nm_marketing" class="form-control form-filter input-sm pilih">
                                                <option disabled selected></option>                                                
                                                <?php foreach ($marketing as $mkt) { ?>
                                                    <option value="<?php echo $mkt->nm_marketing; ?>"><?php echo $mkt->nm_marketing; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="nm_cabang" class="form-control form-filter input-sm pilih">
                                                <option></option>
                                                <?php
                                                    $cabang= $this->M_payment->get_cabang(array('active' => '1'))->result();
                                                    foreach ($cabang as $row) { ?>
                                                    <option value="<?php echo $row->nm_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="jenis" class="form-control form-filter input-sm pilih">
                                                <option disabled selected></option>
                                                    <option value="Reguler">Reguler</option>   
                                                    <option value="Tabungan">Tabungan</option>   
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_pendaftaran"> </td>
                                        <td>
                                            <select name="tgl_keberangkatan" class="form-control form-filter input-sm pilih">
                                                <option disabled selected></option>                                                
                                                <!-- <option value="0000-00-00">Tabungan</option>                                                 -->
                                                <?php
                                                foreach ($schedule as $row) { ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                                <?php
                                                }
                                                ?>
                                            </select>
                                         </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="no_ktp"> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <!-- <td>
                                            <select name="status" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="Belum Lunas">Belum Lunas</option>
                                                <option value="Registrasi">Registrasi</option>
                                                <option value="Lunas">Lunas</option>
                                            </select> 
                                        </td> -->
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>