<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="./assets/img/logo.png" /> 
  
<title>MANIFEST PEMBAYARAN</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="./assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h2 style="text-align: center; font-weight: bold;">MANIFEST PEMBAYARAN</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h3 style="text-align: center;"><?php echo $main->nm_jadwal; ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 40px;">No. Pendaftaran</th>
        <th style="width: 150px;">Nama Lengkap</th>
        <th style="width: 105px;">By. Paket</th>
        <th style="width: 1px;">Potongan</th>
        <th style="width: 80px;">By. Mahram</th>
        <th style="width: 100px;">By. Asuransi KSA Arab</th>
        <th style="width: 100px;">By. Order Barang</th>
        <th style="width: 100px;">By. Upgrade Kamar</th>
        <th style="width: 100px;">By. Upgrade Hotel</th>
        <th style="width: 100px;">By. Add On</th>
        <th style="width: 100px;">Over Payment</th>
        <th style="width: 105px;">Total Biaya</th>
        <th style="width: 105px;">Telah Bayar</th>
        <th style="width: 100px;">Sisa Bayar</th>
        <th style="width: 70px;">Rekomendasi</th>
        <!-- <th style="width: 70px;">No. HP Rekomendasi</th> -->
        <th style="width: 60px;">Status</th>
    </tr>

    <?php $nomor=1; $tot_sisa = 0; foreach($detail as $row) { 
        if  ($row->sisa != null) {
            $tot_sisa += $row->sisa;
            $sisa=$row->sisa;
            } else {
            $sisa=$row->total_biaya;
            $tot_sisa += $row->total_biaya;
            }
        if  ($row->telah_bayar != null) {
            $telah_bayar=$row->telah_bayar;
            } else {
            $telah_bayar=0;
            }

            if ($row->status == 'Registrasi') {
            $warna = 'background-color: #ff0000';
            } else 
            if ($row->status == 'Refund') {
            $warna = 'background-color: #990000';
            } else 
            if ($row->status == 'Belum Lunas') {
               $warna = 'background-color: #ffff00';
            } else 
            if ($row->status == 'Terhutang') {
               $warna = 'background-color: #2980b9';
            } else {
               $warna = 'background-color: #39e600';
            }

            if ($row->jenis == 'Tabungan') $warna_jenis = 'background-color: #2584DE'; else $warna_jenis = '';
        
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;<?php echo $warna_jenis; ?>"><?php echo $row->no_pendaftaran ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->hrg_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->potongan_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_mahram,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_progresif,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_order_barang,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->biaya_upgrade_kamar + $row->biaya_upgrade_kamar_madinah,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format(($row->biaya_up_hotel_makkah + $row->biaya_up_hotel_madinah),0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->add_on,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->overpayment,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->total_biaya,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($telah_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($sisa,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?>/<?php echo $row->no_telp_marketing ?></td>  
           <td style="border:1px solid #000;<?php echo $warna; ?>"><?php echo $row->status ?></td>      
        </tr>   
    <?php $nomor++;}?> 
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"></td>
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->hrg_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->potongan_paket,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_mahram,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_progresif,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_order_barang,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_upgrade_kamar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->biaya_up_hotel,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->add_on,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->overpayment,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->total_biaya,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($footer->telah_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($tot_sisa,0,',','.') ?></td>
           <td style="border:1px solid #000;"></td>  
           <td style="border:1px solid #000;"></td>      
        </tr> 
</table>
</body>
</html>