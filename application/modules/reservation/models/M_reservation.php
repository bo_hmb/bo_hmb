<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_reservation extends CI_Model 
	{
		

		var $table = 'reservasi';
		var $group_movement = 'group_movement';
		var $kota = 'ind_regencies';
		var $cabang = 'cabang';
		var $hotel = 'hotel';
		
	    var $column_order = array('');
	    var $column_search = array('id');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' reservasi.id,
								reservasi.id_group_movement,
								group_movement.tour_code,
								reservasi.id_hotel,
								hotel.kota_lokasi,
								reservasi.type_room,
								reservasi.check_in,
								reservasi.check_out,
								reservasi.night,
								reservasi.`double`,
								reservasi.triple,
								reservasi.quad,
								reservasi.quint,
								reservasi.other,
								reservasi.meals_plan,
								reservasi.remarks,
								reservasi.create_at,
								reservasi.active ');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	$this->db->join($this->group_movement, ''.$this->table.'.id_group_movement = '.$this->group_movement.'.id');
	    	$this->db->join($this->hotel, ''.$this->table.'.id_hotel = '.$this->hotel.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {
	    	$table = $this->table;
	    	$this->db->where($where);
	        $this->db->from($table);	       
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_group_movement() {
			$this->db->order_by('id');
			$query=$this->db->get('group_movement');
			return $query;
		}

		public function hotel($id)
		{
			$results = array();
			$query = $this->db->query('SELECT GROUP_CONCAT(nm_hotel) AS hotel,kota_lokasi FROM hotel WHERE id in('.$id.')');
			return $query->row();
		}

		public function get_kota_where($where) {
            $table = $this->kota;
            $this->db->where($where);
            $query=$this->db->get($table);
            return $query;
        }

        public function get_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_cetak($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT
										reservasi.id,
										reservasi.id_group_movement,
										group_movement.tour_code,
										reservasi.id_hotel,
										reservasi.type_room,
										reservasi.check_in,
										reservasi.check_out,
										reservasi.night,
										reservasi.double,
										reservasi.triple,
										reservasi.quad,
										reservasi.quint,
										reservasi.other,
										reservasi.meals_plan,
										reservasi.remarks,
										reservasi.notes,
										reservasi.active
										FROM
										reservasi
										INNER JOIN group_movement ON reservasi.id_group_movement = group_movement.id
										WHERE reservasi.id='.$id.'');
			return $query;
		}
	}
