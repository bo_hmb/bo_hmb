<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>Reservation</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<?php $hotel = $this->M_reservation->hotel($master->id_hotel); ?>
<?php $kota = $this->M_reservation->get_kota_where(array('id_kota' => $cabang->kota))->row(); ?> 

<table>    
    <tr>           
        <th style="border:0px solid #000;width: 170px;"><img src="<?php echo base_url() ?>assets/img/kop_hotel.png" class="img-responsive" alt="" height="100" width="160"></th>
        <th style="border:0px solid #000;width: 800px;"> 
            <h2 style="font-weight: bold;">HOTEL RESERVATION</h2><br>
            <h2 style="text-align: left; font-weight: bold;"><?php echo $hotel->hotel ?> <?php echo $master->type_room; ?></h2>
            <h2 style="text-align: left; font-weight: bold;"><?php echo $hotel->kota_lokasi ?></h2>
        </th>
    </tr>
</table>
<br>
<table>    
    <tr>           
        <!-- <th style="width: 20px;">No</th> -->
        <th style="width: 190px;">Group Code</th>
        <th style="width: 150px;">Check In</th>
        <th style="width: 150px;">Check Out</th>
        <th style="width: 50px;">NTS</th>
        <th style="width: 50px;">Double</th>
        <th style="width: 50px;">Triple</th>
        <th style="width: 50px;">Quad</th>
        <th style="width: 50px;">Quint</th>
        <th style="width: 50px;">Other</th>
        <th style="width: 50px;">Tot. Room</th>
        <th style="width: 110px;">Meals Plan</th>
        <th style="width: 110px;">Remarks</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->tour_code ?></td>
           <td style="border:1px solid #000;"><?php echo $row->check_in ?></td>
           <td style="border:1px solid #000;"><?php echo $row->check_out ?></td>
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->night ?></td>
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->double ?></td>
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->triple ?></td>
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->quad ?></td>  
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->quint ?></td>  
           <td style="text-align: center;border:1px solid #000;"><?php echo $row->other ?></td>  
           <td style="text-align: center;border:1px solid #000;"><?php echo $x=$row->double+$row->triple+$row->quad+$row->quint+$row->other ?></td>  
           <td style="border:1px solid #000;"><?php echo $row->meals_plan ?></td>  
           <td style="border:1px solid #000;"><?php echo $row->remarks ?></td>  
        </tr>   
    <?php $nomor++;}?> 
</table>
 
<br>
<p style="text-align: left;font-weight: bold;color: red"><i>Notes: Please prepare the room keys 2 hour before the group check in</i></p>
<p style="text-align: left;font-weight: bold;color: blue"><i><?php echo $row->notes; ?></i></p>

<p style="text-align: left;"><?php echo $kota->name_regencies; ?>, <?php echo date('d-m-Y'); ?></p>

<table>
    <tr>
        <td>
            <p style="font-weight: bold;">Best Regards</p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="font-weight: bold;">Hotel Reservation and Transportation Department</p><br>
        </td>
    </tr>
    <tr>
        <td>
            <br><br><br><p style="font-weight: bold;text-decoration: underline; ">Andi Widiasari Maruddani</p>
            <p style="font-weight: bold;">+62 821 9183 8004</p>
            <p style="font-weight: bold;">+62 813 4200 6348</p>

        </td>
    </tr>
    <tr>
        <td>
            <br><br><p style="font-weight: bold;text-decoration: underline;color: blue; ">Email : hotelreservation@hmb.co.id</p>
        </td>
    </tr>
</table> 
</body>
</html>