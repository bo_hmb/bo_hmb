<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Mainivest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Movement Group Detail</span>
                </li>
                <li>
                    <span>Reservation</span>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Reservation Group Movement</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            
                        </div>
                    </div>
                     <script>
                    id_group = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <th width="50%">Tour Code</th>
                                        <th width="500%">Hotel</th>
                                        <th width="5%">Type Room</th>
                                        <th width="5%">Check In</th>
                                        <th width="5%">Check Out</th>
                                        <th width="5%">Night</th>
                                        <th width="5%">Double</th>
                                        <th width="5%">Triple</th>
                                        <th width="5%">Quad</th>
                                        <th width="5%">Quint</th>
                                        <th width="5%">Other</th>
                                        <th width="5%">Meals Plan</th>
                                        <th width="5%">Remarks</th>
                                        <th width="5%">Create At</th>
                                    </tr>
                                    
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>