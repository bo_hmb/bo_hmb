<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <input type="text" class="hidden" name="id_group_movement" value="<?php echo $group_movement?>">
                <div class="col-md-4">
                    <div class="form-group ">
                    <label for="form_control_1">Hotel <span class="required">*</span></label>
                        <div class="input-group select2-bootstrap-append">
                            <select id="multi-append" name="id_hotel[]" class="form-control pilihbanyak" multiple>
                                <option></option>
                                <?php
                                    foreach ($hotel as $row) { ?>
                                    <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <div class="input-group date form_datetime form_datetime bs-datetime">
                            <input type="text" size="16" class="form-control" name="check_in" placeholder="Masukkan Check In" data-date-format="yyyy-mm-dd hh:mm:ss">
                            <label class="control-label">Check In <span class="required">*</span>
                            </label>
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <div class="input-group date form_datetime form_datetime bs-datetime">
                            <input type="text" size="16" class="form-control" name="check_out" placeholder="Input Check Out" data-date-format="yyyy-mm-dd hh:mm:ss">
                            <label class="control-label">Check Out <span class="required">*</span>
                            </label>
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                </div>            
            </div>                    
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="night" placeholder="Input Night">
                    <label for="form_control_1">Night
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Night</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="meals_plan" placeholder="Input Meals Plan">
                    <label for="form_control_1">Meals Plan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Meals Plan</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="remarks" placeholder="Input Remarks">
                    <label for="form_control_1">Remarks
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Remarks</span>
                </div>
            </div>  
        </div>   

        <span class="caption-subject font-dark bold uppercase">Rooms Type</span>
        <br>
        <div class="row">
            <br>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilihbanyak" name="type_room">
                        <option value=" ">-</option>
                        <option value="Standard Rooms">Standard Rooms</option>
                        <option value="Suites Rooms">Suites Rooms</option>
                    </select>
                    <label>Type Room
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Type Room</span>
                </div>
            </div>
      
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="double" placeholder="Input Double" value="0">
                        <label for="form_control_1">Double
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Double</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="triple" placeholder="Input Triple" value="0">
                        <label for="form_control_1">Triple
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Triple</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="quad" placeholder="Input Quad" value="0">
                        <label for="form_control_1">Quad
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Quad</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="quint" placeholder="Input Quint" value="0">
                        <label for="form_control_1">Quint
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Quint</span>
                </div>
            </div>  
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="other" placeholder="Input Other" value="0">
                        <label for="form_control_1">Other
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Other</span>
                </div>
            </div>                 
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="notes" placeholder="Input Notes">
                        <label for="form_control_1">Notes
                        </label>
                        <span class="help-block">Input Notes</span>
                </div>
            </div>
        </div>

    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>