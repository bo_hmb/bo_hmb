<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <!-- <input type="text" class="hidden" name="id_group_movement" value="<?php echo $main->id_group_movement?>"> -->
                <div class="col-md-4">
                    <div class="form-group ">
                    <label for="form_control_1">Hotel <span class="required">*</span></label>
                        <div class="input-group select2-bootstrap-append">
                            <select id="multi-append" name="id_hotel[]" class="form-control pilihbanyak" multiple>
                                <option></option>
                                 <?php
                                    foreach ($hotel as $htl) {
                                    $current = $this->db->query("SELECT id,nm_hotel FROM hotel WHERE id IN ($main->id_hotel)")->result();
                                    $gID=$htl->id;
                                      $checked = "";
                                      foreach($current as $row) {
                                          if ($gID == $row->id) {
                                              $checked= "selected";
                                          break;
                                          }
                                      } 
                                ?>
                                    <option <?php echo $checked;?> value="<?php echo $htl->id; ?>"><?php echo $htl->nm_hotel; ?></option>
                                <?php }?>   
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <div class="input-group date form_datetime form_datetime bs-datetime">
                            <input type="text" size="16" class="form-control" name="check_in" placeholder="Masukkan Check In" data-date-format="yyyy-mm-dd hh:mm:ss" value="<?php echo $main->check_in?>">
                            <label class="control-label">Check In <span class="required">*</span>
                            </label>
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                </div> 
                <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <div class="input-group date form_datetime form_datetime bs-datetime">
                            <input type="text" size="16" class="form-control" name="check_out" placeholder="Input Check Out" data-date-format="yyyy-mm-dd hh:mm:ss" value="<?php echo $main->check_out?>">
                            <label class="control-label">Check Out <span class="required">*</span>
                            </label>
                                <span class="input-group-addon">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                </div>            
            </div>                    
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="night" placeholder="Input Night" value="<?php echo $main->night?>">
                    <label for="form_control_1">Night
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Night</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="meals_plan" placeholder="Input Meals Plan" value="<?php echo $main->meals_plan?>">
                    <label for="form_control_1">Meals Plan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Meals Plan</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="remarks" placeholder="Input Remarks" value="<?php echo $main->remarks?>">
                    <label for="form_control_1">Remarks
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Remarks</span>
                </div>
            </div>  
        </div>   

        <span class="caption-subject font-dark bold uppercase">Rooms Type</span>

        <div class="row">
            <br>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilihbanyak" name="type_room">
                        <option <?php echo (" " === $main->type_room) ? 'selected' : ''; ?> value=" ">-</option>
                        <option <?php echo ("Standard Rooms" === $main->type_room) ? 'selected' : ''; ?> value="Standard Rooms">Standard Rooms</option>
                        <option <?php echo ("Suites Rooms" === $main->type_room) ? 'selected' : ''; ?> value="Suites Rooms">Suites Rooms</option>
                    </select>
                    <label>Type Room
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Type Room</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="double" placeholder="Input Double" value="<?php echo $main->double?>">
                        <label for="form_control_1">Double
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Double</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="triple" placeholder="Input Triple" value="<?php echo $main->triple?>">
                        <label for="form_control_1">Triple
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Triple</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="quad" placeholder="Input Quad" value="<?php echo $main->quad?>">
                        <label for="form_control_1">Quad
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Quad</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="quint" placeholder="Input Quint" value="<?php echo $main->quint?>">
                        <label for="form_control_1">Quint
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Quint</span>
                </div>
            </div>   
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="other" placeholder="Input Other" value="<?php echo $main->other?>">
                        <label for="form_control_1">Other
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Input Other</span>
                </div>
            </div>                
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="notes" placeholder="Input Notes" value="<?php echo $main->notes?>">
                        <label for="form_control_1">Notes
                        </label>
                        <span class="help-block">Input Notes</span>
                </div>
            </div>
        </div>
    </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>