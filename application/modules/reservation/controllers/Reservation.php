<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Reservation extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Reservation';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_reservation');
            // $this->load->model('M_movement');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function group_movement()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = '';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['idmovement'] = $this->uri->segment(3);
            $this->data['group_movement'] = $this->M_reservation->get_group_movement(array('id' => $this->uri->segment(3)))->row();
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            $dat = $this->input->post('dat',TRUE);
            $cols = array();
			if (!empty($dat)) { $cols['dat'] = $dat; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "reservasi.active = '1' AND reservasi.id_group_movement = '".$id."'";             
            } else {
             $where = "reservasi.active = '1' AND reservasi.id_group_movement = '".$id."'";             
            }


	        $list = $this->M_reservation->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_reservation->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                                <a type="button" class="btn btn-xs btn-outline blue tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'reservation/cetak/'.$r->id.'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                            </div>';
                $hotel = $this->M_reservation->hotel($r->id_hotel);

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->tour_code,
                    $hotel->hotel,
                    $r->type_room,
                    $r->check_in,
                    $r->check_out,
                    $r->night,
                    $r->double,
                    $r->triple,
                    $r->quad,
                    $r->quint,
                    $r->other,
                    $r->meals_plan,
                    $r->remarks,
                    $r->create_at,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
			$data['title'] = 'Tambah Data Movedet';
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            $data['group_movement'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {
			// $this->validateInput();
            $this->ajaxRequest();

            if (count($this->input->post('id_hotel')) > 0) {
                $arrhotel = $this->input->post('id_hotel');
                foreach($arrhotel as $val)
                {
                    $hotelarr = @$hotelarr . $val. ",";
                }
            } else {
                $hotelarr ='';
            }

			$data = array(
				'id_group_movement' => $this->input->post('id_group_movement',TRUE),
                'id_hotel' => substr(trim($hotelarr), 0, -1),
                'check_in' => $this->input->post('check_in',TRUE),
                'type_room' => $this->input->post('type_room',TRUE),
                'check_out' => $this->input->post('check_out',TRUE),
                'night' => $this->input->post('night',TRUE),
                'double' => $this->input->post('double',TRUE),
                'triple' => $this->input->post('triple',TRUE),
                'quad' => $this->input->post('quad',TRUE),
                'quint' => $this->input->post('quint',TRUE),
                'other' => $this->input->post('other',TRUE),
                'meals_plan' => $this->input->post('meals_plan',TRUE),
                'remarks' => $this->input->post('remarks',TRUE),
                'notes' => $this->input->post('notes',TRUE),
                'id_user' => $this->currentUser->id,
                'active' => '1',
				);

			$query = $this->M_reservation->_insert($data);

            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            $data['main'] = $this->M_reservation->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            if (count($this->input->post('id_hotel')) > 0) {
                $arrhotel = $this->input->post('id_hotel');
                foreach($arrhotel as $val)
                {
                    $hotelarr = @$hotelarr . $val. ",";
                }
            } else {
                $hotelarr ='';
            }

            $data = array(
                'id_hotel' => substr(trim($hotelarr), 0, -1),
                'check_in' => $this->input->post('check_in',TRUE),
                'type_room' => $this->input->post('type_room',TRUE),
                'check_out' => $this->input->post('check_out',TRUE),
                'night' => $this->input->post('night',TRUE),
                'double' => $this->input->post('double',TRUE),
                'triple' => $this->input->post('triple',TRUE),
                'quad' => $this->input->post('quad',TRUE),
                'quint' => $this->input->post('quint',TRUE),
                'other' => $this->input->post('other',TRUE),
                'meals_plan' => $this->input->post('meals_plan',TRUE),
                'remarks' => $this->input->post('remarks',TRUE),
                'notes' => $this->input->post('notes',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_reservation->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_reservation->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            //id_movement,jam,agenda
            $this->form_validation->set_rules('id_movement', 'nm hotel', 'trim|required');
            $this->form_validation->set_rules('jam', 'kota lokasi', 'trim|required');
            $this->form_validation->set_rules('agenda', 'alamat', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            // $data['main'] = $this->M_reservation->get_where(array('id' => $id))->row();
            $data['master'] = $this->M_reservation->get_where_cetak($id)->row();
            $data['detail'] = $this->M_reservation->get_where_cetak($id)->result();
            $data['cabang'] = $this->M_reservation->get_cabang(array('kd_cabang' => $this->kode_cabang))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('reservation/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'A4-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>
