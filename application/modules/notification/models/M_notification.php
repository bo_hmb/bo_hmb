<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_notification extends CI_Model 
	{
		

		var $table = 'notification';

	    var $column_order = array('activity_id','user_id','activity_type','activity_uri_1','activity_uri_2','activity_time','activity_ip_address','activity_browser','deleted','position','kdoffice');
	    var $column_search = array('activity_id','user_id','activity_type','activity_uri_1','activity_uri_2','activity_time','activity_ip_address','activity_browser','deleted','position','kdoffice');
	    var $order = array('activity_id' => 'desc'); 

		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$table = $this->table;
			$this->db->order_by($order);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	}
