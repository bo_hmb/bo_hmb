<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Notification extends AdminController {

        public function __construct()
        {
            parent::__construct();

            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Notification';
            
            
            $this->load->model('M_notification');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            
        }

        public function get()
        {
            $query = $this->M_notification->get();
            return $query;

        }

        public function update($where, $data)
        {
            $query = $this->M_notification->_update($where, $data);
            return $query;

        }

        public function insert($data)
        {
            $query = $this->M_notification->_insert($data);
            return $query;

        }

    }
?>
