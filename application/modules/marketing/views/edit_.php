<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nik" placeholder="ID Marketing Otomatis" value="<?php echo $main->nik?>" readonly>
                    <label for="form_control_1">ID Marketing Otomatis
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">ID Marketing Otomatis</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_marketing" placeholder="Masukkan Nama Marketing" value="<?php echo $main->nm_marketing?>">
                    <label for="form_control_1">Nama Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Marketing</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_telp" placeholder="Masukkan No. telepon" value="<?php echo $main->no_telp?>">
                    <label for="form_control_1">No. telepon
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan No. telepon</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="rekening" placeholder="Masukkan Rekening Marketing" value="<?php echo $main->rekening?>">
                    <label for="form_control_1">Rekening Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Rekening Marketing</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                <div class="fg-line">
                    <label>Work Area</label>
                    <select id="kd_cabang" name="kd_cabang" class="form-control" data-placeholder="Pilih Office">
                        <option disabled selected></option>
                        <?php
                        foreach ($workarea as $row) { ?>

                        <option <?php echo ($row->kd_cabang === $main->kd_cabang) ? 'selected' : ''; ?> value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>
                        
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            </div>
            <div class="col-md-4">            
                <div class="form-group">
                <div class="fg-line">
                    <label>Level</label>
                    <select id="id_level" name="id_level" class="form-control" data-placeholder="Pilih Level Marketing">
                        <option disabled selected></option>
                        <?php
                        foreach ($level as $row) { ?>

                        <option <?php echo ($row->id === $main->id_level) ? 'selected' : ''; ?> value="<?php echo $row->id; ?>"><?php echo $row->level; ?></option>

                        <?php
                        }
                        ?>
                    </select>
                </div>

            </div>
            <div class="col-md-4">
                 <div class="form-group form-md-line-input">
                    <select class="form-control pilihbanyak" name="id_atasan">
                        <?php foreach ($marketing as $mkt) { ?>
                            <option <?php echo ($mkt->id === $main->id_atasan) ? 'selected' : ''; ?> value="<?php echo $mkt->id; ?>"><?php echo $mkt->nm_marketing; ?></option>
                        <?php } ?>
                    </select>
                    <label>Referensi Marketing
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Referensi</span>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>