<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Marketing extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Marketing';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_marketing');
            $this->load->model('M_level');
            $this->load->model('M_workarea');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Marketing';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function get_marketing_where($where) {
            $query = $this->M_marketing->get_marketing_where($where);
            return $query;
        }


        public function load_data()
        {
            $nik = $this->input->post('nik',TRUE);
            $nm_marketing = $this->input->post('nm_marketing',TRUE);
            $no_telp = $this->input->post('no_telp',TRUE);
            $rekening = $this->input->post('rekening',TRUE);
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $level = $this->input->post('level',TRUE);

			$cols = array();
			if (!empty($nik)) { $cols['nik'] = $nik; }
            if (!empty($nm_marketing)) { $cols['nm_marketing'] = $nm_marketing; }
            if (!empty($no_telp)) { $cols['no_telp'] = $no_telp; }
			if (!empty($rekening)) { $cols['rekening'] = $rekening; }
			if (!empty($kd_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
			if (!empty($id_level)) { $cols['level'] = $level; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "marketing.active = '1'";
            } else {
             $where = "marketing.active = '1' AND marketing.kd_cabang = '$this->kode_cabang'";
            }


	        $list = $this->M_marketing->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_marketing->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nik,
                    $r->nm_marketing,
                    $r->no_telp,
                    $r->rekening,
                    $r->nm_cabang,
                    $r->level,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Add Data Marketing';
            $data['level'] = $this->M_level->get()->result();
            $data['workarea'] = $this->M_workarea->get()->result();
            $data['marketing'] = $this->M_marketing->get_marketing_where(array('active' => '1'))->result();
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

            $this->ajaxRequest();

            $cek = $this->M_marketing->get_marketing_where(array('kd_cabang'=>$this->input->post('kd_cabang')))->num_rows();
            $id_marketing = $this->input->post('kd_cabang').'-'.sprintf('%04d',$cek+1);		

			$data = array(
				'nik' => $id_marketing,
                'nm_marketing' => $this->input->post('nm_marketing',TRUE),
                'kd_cabang' => $this->input->post('kd_cabang',TRUE),
                'id_level' => $this->input->post('id_level',TRUE),
                'no_telp' => $this->input->post('no_telp',TRUE),
                'rekening' => $this->input->post('rekening',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_marketing->_insert($data);
            $id_marketing = $this->db->insert_id();

            $dataLevel = array(
                'id_marketing' => $id_marketing,
                'id_atasan' => $this->input->post('id_atasan',TRUE),
                'id_id_atasan' => $this->input->post('id_atasan',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
                );

            $queryLevel = $this->M_marketing->_insert_level($dataLevel);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_marketing->get_where(array('marketing.id' => $id))->row();
            $data['marketing'] = $this->M_marketing->get_marketing_where(array('active' => '1'))->result();
            $data['level'] = $this->M_level->get()->result();
            $data['workarea'] = $this->M_workarea->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                // 'nik' => $this->input->post('nik',TRUE),
                'nm_marketing' => $this->input->post('nm_marketing',TRUE),
                'kd_cabang' => $this->input->post('kd_cabang',TRUE),
                'id_level' => $this->input->post('id_level',TRUE),
                'no_telp' => $this->input->post('no_telp',TRUE),
                'rekening' => $this->input->post('rekening',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_marketing->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_marketing->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('nik', 'nik', 'trim|required');
            $this->form_validation->set_rules('nm_marketing', 'nm marketing', 'trim|required');
            $this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
            $this->form_validation->set_rules('rekening', 'rekeningg', 'trim|required');
            $this->form_validation->set_rules('kd_cabang', 'kd cabang', 'trim|required');
            $this->form_validation->set_rules('id_level', 'id tingkatan', 'trim|required');
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function get($where) {
            $query=$this->M_marketing->get($where);
            return $query;
        }

    }
?>
