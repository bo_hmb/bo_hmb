<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_level extends CI_Model 
	{
		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$this->db->order_by('id');
			$query=$this->db->get('level_marketing');
			return $query;
		}
		
	}
