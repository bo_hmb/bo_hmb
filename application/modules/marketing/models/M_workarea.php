<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_workarea extends CI_Model 
	{
		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$this->db->order_by('kd_cabang');
			$query=$this->db->get('cabang');
			return $query;
		}
		
	}
