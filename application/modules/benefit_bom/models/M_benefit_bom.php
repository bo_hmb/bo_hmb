<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_benefit_bom extends CI_Model 
	{
		

		var $table = 'benefit_bom';
		var $marketing = 'marketing';
		var $pengeluaran = 'pengeluaran';
		var $jadwal = 'jadwal';
					

	    var $column_order = array('id');
	    var $column_search = array('id');
	    var $order = array('benefit_bom.id' => 'DESC'); 


		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}


		public function get_where_2($where) {
			$table2 = $this->pengeluaran;
			$this->db->where($where);
			$query=$this->db->get($table2);
			return $query;
		}

		public function get_detail($id) {	        
			$results = array();
	        $query = $this->db->query(' SELECT
										benefit_bom.id,
										benefit_bom.id_marketing,
										marketing.nik,
										marketing.nm_marketing,
										marketing.rekening,
										benefit_bom.jml_benefit,
										benefit_bom.`status`,
										benefit_bom.active,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan
										FROM
										benefit_bom
										INNER JOIN marketing ON benefit_bom.id_marketing = marketing.id
										INNER JOIN jadwal ON benefit_bom.id_jadwal = jadwal.id
										WHERE benefit_bom.id='.$id.'
										');
	        return $query->row();
		}

		public function _insert($data) {
			$table2 = $this->table2;
			$insert = $this->db->insert($table2, $data);
			return $insert;
		}


		public function _insert_pengeluaran($data) {
			$table4 = $this->pengeluaran;
			$insert = $this->db->insert($table4, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' benefit_bom.id,
								benefit_bom.id_marketing,
								marketing.nik,
								marketing.nm_marketing,
								marketing.rekening,
								benefit_bom.jml_benefit,
								benefit_bom.`status`,
								jadwal.nm_jadwal,
								jadwal.tgl_keberangkatan,
								benefit_bom.active
								');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->marketing, ''.$this->table.'.id_marketing = '.$this->marketing.'.id');
	    	$this->db->join($this->jadwal, ''.$this->table.'.id_jadwal = '.$this->jadwal.'.id');
	    	
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	//$this->db->where($where);
	        $this->db->from($this->table);
			// $this->db->join('pembayaran','benefit_marketing.no_faktur=pembayaran.no_faktur');	        	        
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }



		public function get_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_month($where) {
			$query = $this->db->query('SELECT MONTH(tgl_keberangkatan) AS tahun FROM jadwal WHERE active=1 GROUP BY MONTH(tgl_keberangkatan)');
			return $query->result();
		}

		public function get_year() {
			$query = $this->db->query('SELECT YEAR(tgl_keberangkatan) AS tahun FROM jadwal WHERE active=1 GROUP BY YEAR(tgl_keberangkatan)');
			return $query->result();
		}

		public function get_export($MONTH, $YEAR) {
			$query = $this->db->query(' SELECT
										benefit_marketing.id AS id,
										cabang.kd_cabang AS kd_cabang,
										cabang.nm_cabang AS nm_cabang,
										benefit_marketing.id_marketing AS id_marketing,
										marketing.nik AS nik,
										marketing.nm_marketing AS nm_marketing,
										MONTH ( jadwal.tgl_keberangkatan ) AS keberangkatan_bulan,
										Sum(benefit_marketing.jml_benefit) AS benefit,
										marketing.rekening AS rekening,
										benefit_marketing.status AS status
										FROM
										(((((benefit_marketing
										JOIN marketing ON ((benefit_marketing.id_marketing = marketing.id)))
										JOIN pendaftar ON ((benefit_marketing.id_pendaftaran = pendaftar.id)))
										JOIN paket ON ((pendaftar.id_paket = paket.id)))
										JOIN jadwal ON ((paket.id_jadwal = jadwal.id)))
										JOIN cabang ON ((marketing.kd_cabang = cabang.kd_cabang)))
										WHERE
										((benefit_marketing.id_user = "x") AND
										(benefit_marketing.active = 1) AND
										MONTH(jadwal.tgl_keberangkatan) = "'.$MONTH.'" AND YEAR(jadwal.tgl_keberangkatan) = "'.$YEAR.'")
										GROUP BY
										MONTH ( jadwal.tgl_keberangkatan )
										ORDER BY
										nm_cabang ASC');
			return $query;
		}

		public function get_sum_all_export($id_jadwal) {
			$query = $this->db->query(' SELECT
										cabang.kd_cabang,cabang.nm_cabang,
										(SELECT SUM(jml_benefit) FROM benefit_cabang WHERE benefit_cabang.active="1" AND benefit_cabang.status="0" AND pembayaran.status_verifikasi="1" AND kd_cabang = cabang.kd_cabang) AS benefit_belum_lunas,
										(SELECT SUM(jml_benefit) FROM benefit_cabang WHERE benefit_cabang.active="1" AND benefit_cabang.status="1" AND kd_cabang = cabang.kd_cabang) AS benefit_lunas
										FROM
										benefit_cabang
										INNER JOIN cabang ON benefit_cabang.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_cabang.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_cabang.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id										
										WHERE jadwal.id = "'.$id_jadwal.'"
										GROUP BY cabang.kd_cabang');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_export($id_jadwal,$kd_cabang) {
			$query = $this->db->query(' SELECT
										cabang.kd_cabang,cabang.nm_cabang,
										(SELECT SUM(jml_benefit) FROM benefit_cabang WHERE benefit_cabang.active="1" AND benefit_cabang.status="0" AND pembayaran.status_verifikasi="1" AND kd_cabang = "'.$kd_cabang.'") AS benefit_belum_lunas,
										(SELECT SUM(jml_benefit) FROM benefit_cabang WHERE benefit_cabang.active="1" AND benefit_cabang.status="1" AND kd_cabang = "'.$kd_cabang.'") AS benefit_lunas
										FROM
										benefit_cabang
										INNER JOIN cabang ON benefit_cabang.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_cabang.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_cabang.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE jadwal.id = "'.$id_jadwal.'" AND cabang.kd_cabang = "'.$kd_cabang.'"
										GROUP BY cabang.kd_cabang');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}
	}
