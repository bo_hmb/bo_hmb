            <form role="form" method="POST" action="<?php echo base_url(); ?>benefit_ref_area/export" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Month</label>
                        <div class="col-md-8">
                            <select class="form-control jadwal" name="bulan" data-placeholder="Select Month">
                                <option disabled selected></option>
                                <!-- <option></option>                                 -->
                                <option value="1">Januari</option>   
                                <option value="2">Februari</option>   
                                <option value="3">Maret</option>   
                                <option value="4">April</option>   
                                <option value="5">Mei</option>   
                                <option value="6">Juni</option>   
                                <option value="7">Juli</option>   
                                <option value="8">Agustus</option>   
                                <option value="9">September</option>   
                                <option value="10">Oktober</option>   
                                <option value="11">November</option>   
                                <option value="12">Desember</option>   
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Year</label>
                        <div class="col-md-8">
                            <select id="tahun" name="tahun" class="form-control jadwal" data-placeholder="Select Year">
                                <option disabled selected></option>
                                <?php
                                foreach ($YEAR as $row) { ?>
                                <option value="<?php echo $row->tahun; ?>"><?php echo $row->tahun; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     