<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Benefit_bom extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Benefit BOM';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_benefit_bom');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Benefit BOM';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $nm_marketing = $this->input->post('nm_marketing',TRUE);
            $benefit = $this->input->post('benefit',TRUE);            
            $rekening = $this->input->post('rekening',TRUE);
            $status = $this->input->post('status',TRUE);

			$cols = array();
            if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
			if (!empty($nm_marketing)) { $cols['nm_marketing'] = $nm_marketing; }
            if (!empty($benefit)) { $cols['benefit'] = $benefit; }            
            if (!empty($rekening)) { $cols['rekening'] = $rekening; }
			if ($status != null) { $cols['status'] = $status; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "benefit_bom.active = '1'";
            } else {
             $where = "benefit_bom.active = '1'";
            }          


	        $list = $this->M_benefit_bom->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_benefit_bom->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-payment tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-money"> Payment</i></button>
                            </div>';
                if($r->status == '1')
                {
                    $status = 'Lunas';
                    $label = 'success';
                }else{
                    $status = 'Belum Lunas';
                    $label = 'danger';
                }
                
                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    $r->nik,
                    $r->nm_marketing,
                    $r->nm_jadwal,
                    $t="Rp." . number_format($r->jml_benefit,0,',','.'),
                    $r->rekening,                                 
                    '<span class="label label-sm label-'.$label.'">'.$status.'</span>'
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Add Data Registrasi';

			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {
            $this->ajaxRequest();
			//$this->validateInput();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
    			$data = array(
                    'id_jadwal' => $this->input->post('id_jadwal',TRUE),
    				'no_faktur' => $this->input->post('no_faktur',TRUE),
                    'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                    'id_benefit_marketing' => $this->input->post('id_benefit',TRUE),
                    'pph' => $this->input->post('pph',TRUE),
                    'harga_pph' => $this->input->post('harga_pph',TRUE),
                    'total_biaya' => $this->input->post('total_bayar',TRUE),
                    'path_bukti_bayar' => $image,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'jenis' => 'Benefit BOM',
                    'active' => '1',
                    'id_user' => $this->currentUser->id,
    				);
            } else {
                $data = array(
                    'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                    'no_faktur' => $this->input->post('no_faktur',TRUE),
                    'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                    'id_benefit_marketing' => $this->input->post('id_benefit',TRUE),
                    'pph' => $this->input->post('pph',TRUE),
                    'harga_pph' => $this->input->post('harga_pph',TRUE),
                    'total_biaya' => $this->input->post('total_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'jenis' => 'Benefit BOM',
                    'active' => '1',
                    'id_user' => $this->currentUser->id,
                    );
            }
            //$query = $this->M_benefit_bom->_insert($data);
            $query = $this->M_benefit_bom->_insert_pengeluaran($data);

            $id = $this->input->post('id_benefit');
            $data2 = array(
                'status' => '1',
                );
            $query2 = $this->M_benefit_bom->_update(array('id' => $id), $data2);
            
            
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_benefit_bom->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function load_payment_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_benefit_bom->get_detail($id);
            $cek = $this->M_benefit_bom->get_where_2(array('MONTH(tgl_faktur)' => date('m'),'YEAR(tgl_faktur)' => date('Y'),'active'=>'1'))->num_rows();
            $data['faktur'] = 'BB-'.date('m').date("Y").sprintf('%03d',$cek+1);
            $data['id'] = $id;
            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'nMbBenefit_bom' => $this->input->post('nMbBenefit_bom',TRUE),
                'kota_lokasi' => $this->input->post('kota_lokasi',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jrk_mkkh_masjidl' => $this->input->post('jrk_mkkh_masjidl',TRUE),
                'jrk_mdinh_msjidl' => $this->input->post('jrk_mdinh_msjidl',TRUE),
                'bintang' => $this->input->post('bintang',TRUE),
                'biaya_sewa_kamar' => $this->input->post('biaya_sewa_kamar',TRUE),
                'biaya_sewa_hotel' => $this->input->post('biaya_sewa_hotel',TRUE),
                'biaya_upgrade_hotel' => $this->input->post('biaya_upgrade_hotel',TRUE),
                'biaya_upgrade_kamar' => $this->input->post('biaya_upgrade_kamar',TRUE),
            );

            $query = $this->M_benefit_bom->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_benefit_bom->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('tgl_faktur', 'tgl_faktur', 'trim|required');
            $this->form_validation->set_rules('total_bayar', 'total_bayar', 'trim|required|numeric');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function load_export()
        {
            $data['title'] = '';
            // $data['MONTH'] = $this->M_benefit_bom->get_month(array('active' => '1'))->result();     
            $data['YEAR'] = $this->M_benefit_bom->get_year();   
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $bulan = $this->input->post('bulan');
            $tahun = $this->input->post('tahun');
            $data ['bulan'] = $this->input->post('bulan');
            $data ['tahun'] = $this->input->post('tahun');
            // $kd_cabang = $this->input->post('kd_cabang');
            //$data['main'] = $id;
            //$data['main'] = $this->M_benefit_bom->get_jadwal_exp(array('id' => $id ))->row(); 
            $data ['detail'] = $this->M_benefit_bom->get_export($bulan,$tahun)->result();                
            
            //$data ['total'] = $this->M_benefit_marketing->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('Bebefit_bom/print_export', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                        
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');
            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>
