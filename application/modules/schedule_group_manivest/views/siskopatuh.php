<?php

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Manifest_siskopatuh.xls");
?>

<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>GROUP MANIVEST</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        
        border:1px solid #000;  
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 1px;
        font-size: 13px;
              
        vertical-align: top;
          
        }

    </style>
</head>
<body>
<!-- kop -->
<h2 style="text-align: center; font-weight: bold;">MANIFEST SISKOPATUH</h2>
<table>    
    <tr>           
        	<th style="text-align: center;" width="31">NO.</th>
			<th style="text-align: center;" width="181">NAMA JAMAAH</th>
			<th style="text-align: center;" width="57">JENIS KELAMIN</th>
			<th style="text-align: center;" width="101">NAMA AYAH</th>
			<th style="text-align: center;" width="62">JENIS IDENTITAS</th>
			<th style="text-align: center;" width="121">NO. IDENTITAS</th>
			<th style="text-align: center;" width="110">TEMPAT LAHIR</th>
			<th style="text-align: center;" width="90">TANGGAL LAHIR</th>
			<th style="text-align: center;" width="207">ALAMAT</th>
			<th style="text-align: center;" width="131">PROVINSI</th>
			<th style="text-align: center;" width="108">KABUPATEN / KOTA</th>
			<th style="text-align: center;" width="117">KECAMATAN</th>
			<th style="text-align: center;" width="107">KELURAHAN</th>
			<th style="text-align: center;" width="93">NO. TELPON</th>
			<th style="text-align: center;" width="104">NO. HANDPHONE</th>
			<th style="text-align: center;" width="128">KEWARGANEGARAAN</th>
			<th style="text-align: center;" width="79">STATUS PERNIKAHAN</th>
			<th style="text-align: center;" width="120">JENIS PENDIDIKAN</th>
			<th style="text-align: center;" width="118">JENIS PEKERJAAN</th>
			<th style="text-align: center;" width="76">NO. PASPOR</th>
			<th style="text-align: center;" width="82">DATE OF ISSUE</th>
			<th style="text-align: center;" width="83">DATE OF EXPIRED</th>
			<th style="text-align: center;" width="76">ISSUING OFFICE</th>
			<th style="text-align: center;" width="84">KETERANGAN</th>
    </tr>
    <?php $nomor=1; foreach($detail as $row) {?>

        <?php if ($row->no_pasport == "") $no_pasport="-"; else $no_pasport=$row->no_pasport; ?>

        <?php if ($row->id_marketing == "3") $sts="TOUR LEADER"; else $sts='JAMAAH'; ?>

        <?php if ($row->exp_pasport == "0000-00-00") $exp_pas="-"; else $exp_pas=date('d/n/Y', strtotime($row->exp_pasport)); ?>
        
        <?php $create_pas="-"; if ($row->create_pasport == "0000-00-00") $exp_pas="-"; else $create_pas=date('d/n/Y', strtotime($row->create_pasport)); ?>

        <tr>                                                
           <td style="text-align: center;"><?php echo $nomor ?></td>
           <td style="text-transform: uppercase; "><?php echo strtoupper($row->nm_lengkap) ?></td>
           <td style="text-align: center;"><?php echo $row->jk ?></td>
           <td style=""><?php echo $row->nm_ayah ?></td>
           <td style="text-align: center;"><?php echo $row->jenis_identitas ?></td>
           <td style="">'<?php echo $row->no_ktp ?></td>
           <td style="text-transform: uppercase; "><?php echo strtoupper($row->tmp_lahir) ?></td>
           <td style=""><?php echo $x="'".date('d/n/Y', strtotime($row->tgl_lahir))?></td>
           <td style=""><?php echo $row->alamat ?></td>
           <td style=""><?php echo $row->prov ?></td>
           <td style=""><?php echo $row->kab ?></td>
           <td style=""><?php echo $row->kec ?></td>
           <td style=""><?php echo $row->kel ?></td>
           <td style=""><?php echo $row->telp_rumah ?></td>
           <td style=""><?php echo $row->hp ?></td>
           <td style="text-align: center;"><?php echo $row->kewarganegaraan ?></td>
           <td style=""><?php echo $row->status_nikah ?></td>
           <td style="text-align: center;"><?php echo $row->pendidikan_terakhir ?></td>
           <td style=""><?php echo $row->pekerjaan ?></td>
           <td style=""><?php echo $no_pasport ?></td>
           <td style=""><?php echo $x="'".$create_pas?></td>
           <td style=""><?php echo $x="'".$exp_pas?></td>
           <td style="text-transform: uppercase; "><?php echo strtoupper($row->kota_paspor) ?></td>
           <td style="text-align: center;"><?php echo $sts ?></td>
        </tr>   
    <?php $nomor++;}?>            
    
      

</table>

</body>
</html>