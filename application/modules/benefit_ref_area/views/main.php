<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Benefit Referensi Area</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Benefit Referensi Area</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <!-- <a class="btn red btn-outline btn-circle btn-export" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a> -->
                                <!-- <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;"> Excel </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> PDF </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Print </a>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                   <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="10%">Aksi</th>
                                        <th width="10%">Cabang</th>
                                        <th width="25%">Nama Marketing</th>
                                        <th width="10%">Tgl. Keberangkatan</th>
                                        <th width="10%">Benefit</th>
                                        <th width="25%">Rekening</th>
                                        <th width="25%">Status</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nik"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_marketing"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="benefit"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_jadwal"> </td>

                                        <td><input type="text" class="form-control form-filter input-sm" name="rekening"> </td>

                                        <td>
                                            <select class="form-control form-filter input-sm" name="status">
                                                <option value="0">Belum Lunas</option>
                                                <option value="1">Lunas</option>
                                            </select> 
                                        </td>
                                        
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>