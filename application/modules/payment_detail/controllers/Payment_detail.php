<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Payment_detail extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Payment Detail';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            $this->load->model('M_Payment_detail');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function pendaftaran()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Payment Detail';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['id_data'] = $this->uri->segment(3);
            $this->data['pendaftaran'] = $this->M_Payment_detail->get_where_pendaftar(array('id' => $this->uri->segment(3)))->row();
            $this->data['content'] = $this->load->view('main', $this->data, true);
            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            $tgl_bayar = $this->input->post('tgl_bayar',TRUE);
            $jml_bayar = $this->input->post('jml_bayar',TRUE);
            $catatan = $this->input->post('catatan',TRUE);
            $status_verifikasi = $this->input->post('status_verifikasi',TRUE);

			$cols = array();
			//if (!empty($id_jadwal)) { $cols['id_jadwal'] = $id_jadwal; }
			if (!empty($hari)) { $cols['hari'] = $hari; }
			if (!empty($jml_bayar)) { $cols['jml_bayar'] = $jml_bayar; }
            if (!empty($catatan)) { $cols['catatan'] = $catatan; }
			if (!empty($status_verifikasi)) { $cols['status_verifikasi'] = $catatan; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "pembayaran.active = '1' AND pembayaran.id_pendaftaran = '".$id."'";
              
            } else {
             $where = "pembayaran.active = '1' AND pembayaran.id_pendaftaran = '".$id."'";
              
            }


	        $list = $this->M_Payment_detail->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_Payment_detail->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                if($r->status_verifikasi == '1')
                {
                    $active = 'Terverifikasi';
                    $label = 'success';
                }else{
                    $active = 'Belum Terverifikasi';
                    $label = 'danger';
                }


                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">';
                if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) {

                $btn_action .= '
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>

                                

                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>';
                }
                $btn_action .= '
                            </div>';

                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    $r->no_faktur,
                    $tgl=date('d-m-Y', strtotime($r->tgl_bayar)),
                    $t="Rp." . number_format($r->jml_bayar,0,',','.'),
                    $r->catatan,
                    $path_bukti_bayar='<img src="'.base_url().'upload/'.$r->path_bukti_bayar.'" class="img-responsive" />',
                    '<span class="label label-sm label-'.$label.'">'.$active.'</span>',
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
			$data['title'] = 'Tambah Data Hotel';
            $data['jadwal'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

			$data = array(
                
				'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'hari' => date('l', strtotime($this->input->post('tgl'))),
                'tgl' => $this->input->post('tgl',TRUE),
                'lokasi' => $this->input->post('lokasi',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_Payment_detail->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_Payment_detail->get_where(array('id' => $id))->row();
            // $data['schedule'] = $this->M_schedule->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // $this->ajaxRequest();
            // $this->validateInput();
            // $config['upload_path']="./upload";
            // $config['allowed_types']='gif|jpg|png|jpeg';
            // $config['encrypt_name'] = TRUE;

            // // Preparing the data before update
            // $id    = $this->input->post('id');
            
            // $this->upload->initialize($config);
            // if($this->upload->do_upload("file")){
            //     $gbr = $this->upload->data();
            //     //Compress Image
            //     $config['image_library']='gd2';
            //     $config['source_image']='./upload/'.$gbr['file_name'];
            //     $config['create_thumb']= FALSE;
            //     $config['maintain_ratio']= FALSE;
            //     $config['quality']= '50%';
            //     $config['width']= 600;
            //     $config['height']= 400;
            //     $config['new_image']= './upload/'.$gbr['file_name'];
            //     $this->load->library('image_lib', $config);
            //     $this->image_lib->resize();

            //     $gambar=$gbr['file_name'];
            //     $data = array(
            //         'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
            //         'jml_bayar' => $this->input->post('jml_bayar',TRUE),
            //         'path_bukti_bayar' => $image,
            //         'catatan' => $this->input->post('catatan',TRUE),
            //         'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
            //         'tunai' => $this->input->post('tunai',TRUE),
            //     );
            //  } else {
            //  $data = array(
            //         'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
            //         'jml_bayar' => $this->input->post('jml_bayar',TRUE),
            //         'catatan' => $this->input->post('catatan',TRUE),
            //         'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
            //         'tunai' => $this->input->post('tunai',TRUE),                    
            //     );
            // }   
            $this->ajaxRequest();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            // Preparing the data before update
            $id    = $this->input->post('id');
            $this->load->library('upload',$config);
            
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                $data = array(
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'path_bukti_bayar' => $image,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'user_update' => $this->currentUser->id,
                );
             } else {
             $data = array(
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),   
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'user_update' => $this->currentUser->id,                 
                );
            }   
            $query = $this->M_Payment_detail->_update(array('id' => $id), $data);

            //// kurangi kursi
            if ($this->input->post('status_verifikasi')=='1') {
                $jadwal=$this->M_Payment_detail->get_jadwal($this->input->post('id_pendaftaran'));

                $id = $jadwal->id_jadwal;
                $data = array(
                    'sisa_kursi' => $jadwal->sisa_kursi - 1,
                    );
                $query = $this->M_Payment_detail->_update_jadwal(array('id' => $id), $data);

            } 
            /// batas kurangi kursi


            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_Payment_detail->_update(array('id' => $id), $data);
            $qu_select = $this->M_Payment_detail->get_where(array('id' => $id))->row();
            $query_del = $this->M_Payment_detail->delete_by_id($qu_select->no_faktur);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            // $this->form_validation->set_rules('id_jadwal', '', 'trim|required');
            // $this->form_validation->set_rules('hari', '', 'trim|required');
            $this->form_validation->set_rules('tgl_bayar', 'tgl_bayar', 'trim|required');
            $this->form_validation->set_rules('jml_bayar', 'jml_bayar', 'trim|required');
            $this->form_validation->set_rules('status_verifikasi', 'status_verifikasi', 'trim|required');
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_Payment_detail->get_cetak($id);
            $data['detail'] = $this->M_Payment_detail->get_detail(array('active' => '1','no_faktur' => $data['main']->no_faktur))->result();


            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('spending/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf(); 

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>
