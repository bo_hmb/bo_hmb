<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_Payment_detail extends CI_Model 
	{

		var $table = 'pembayaran';
		var $pendaftar = 'pendaftar';
		var $formulir = 'formulir';
		var $jadwal = 'jadwal';

	    var $column_order = array('id','tgl_bayar','jml_bayar','catatan','status_verifikasi');
	    var $column_search = array('id','tgl_bayar','jml_bayar','catatan','status_verifikasi');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function delete_by_id($id)
		{
			$this->db->where('no_faktur', $id);
			$this->db->delete('benefit_marketing');
		}

		public function get_where_pendaftar($where) {
			$table = $this->pendaftar;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function get_jadwal($id)
		{
			$results = array();
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.id_pendaftaran,
										pendaftar.no_pendaftaran,
										pendaftar.id_paket,
										paket.nm_paket,
										paket.id_jadwal,
										jadwal.nm_jadwal,
										jadwal.sisa_kursi
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE id_pendaftaran="'.$id.'"
									');
			return $query->row();	
		}

		public function _update_jadwal($where, $data) {
			$table = $this->jadwal;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	   private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' pembayaran.id,
								pembayaran.id_pendaftaran,
								pembayaran.no_faktur,
								pendaftar.no_pendaftaran,
								pendaftar.id_formulir,
								formulir.nm_lengkap,
								pembayaran.tgl_bayar,
								pembayaran.jml_bayar,
								pembayaran.path_bukti_bayar,
								pembayaran.catatan,
								pembayaran.status_verifikasi,
								pembayaran.active');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->pendaftar, ''.$this->table.'.id_pendaftaran = '.$this->pendaftar.'.id');
	    	$this->db->join($this->formulir, ''.$this->pendaftar.'.id_formulir = '.$this->formulir.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
