<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_marketing_bagans extends CI_Model 
	{
		

		var $table = 'tv_level_marketing';

		var $master_table ='marketing_level';
		var $marketing ='marketing';

	    var $column_order = array('id','id_marketing','id_atasan');
	    var $column_search = array('id','id_marketing','id_atasan');
	    var $order = array('id' => 'ASC'); 



		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$table = $this->table;
			$query=$this->db->get($table);
			return $query;
		}

		public function get_marketing_cabang($where)	
		{
			$query = $this->db->query("SELECT
						marketing_level.id AS id,
						marketing_level.id_marketing AS id_marketing,
						marketing.nm_marketing AS nm_marketing,
						level_marketing.level AS level,
						marketing_level.id_atasan AS id_atasan,
						(
							SELECT
								marketing.nm_marketing
							FROM
								marketing
							WHERE
								(
									marketing.id = marketing_level.id_atasan
								)
						) AS nama_atasan,
						(
							SELECT
								level_marketing.level
							FROM
								level_marketing
							WHERE
								(
									level_marketing.id = (
										SELECT
											marketing.id_level
										FROM
											marketing
										WHERE
											(
												marketing.id = marketing_level.id_atasan
											)
									)
								)
						) AS level_atasan,
						marketing.kd_cabang AS kode_cabang,
						cabang.nm_cabang AS nm_cabang,
						(
							SELECT
								count(formulir.id)
							FROM
								formulir
							WHERE
								(
									formulir.id_marketing = CONVERT (
										marketing_level.id_marketing USING utf8
									)
								)
						) AS total_jamaah,
						marketing_level.active AS active
					FROM
						(
							(
								(
									marketing_level
									JOIN marketing ON (
										(
											marketing_level.id_marketing = marketing.id
										)
									)
								)
								JOIN level_marketing ON (
									(
										marketing.id_level = level_marketing.id
									)
								)
							)
							JOIN cabang ON (
								(
									marketing.kd_cabang = cabang.kd_cabang
								)
							)
						)
					WHERE
						(marketing_level.active = 1) ");
					// -- AND marketing.kd_cabang = '".$where."'");
			return $query;
		}

		public function get_lvl($order_by) {
			$table = $this->master_table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_lvl_where($where) {
			$table = $this->master_table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_marketing($where) {
			$table = $this->marketing;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->master_table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->master_table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}