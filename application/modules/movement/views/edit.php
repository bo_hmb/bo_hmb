<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <input type="text" class="hidden" name="id_jadwal" value="<?php echo $main->id_jadwal?>">
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl" name="tgl" class="form-control" readonly value="<?php echo $main->tgl?>">
                        <label class="control-label">Tanggal
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal </span>
                </div>
            </div>

            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="lokasi" placeholder="Masukkan lokasi" value="<?php echo $main->lokasi?>">
                    <label for="form_control_1">Lokasi
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Lokasi</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>