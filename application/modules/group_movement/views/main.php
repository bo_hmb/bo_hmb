<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>schedule_group_movement">Mainifest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>schedule_group_movement">Group Movement</a>
                    <i class="fa fa-circle"></i>
                    
                </li> 
                <li>
                    <a href="#"><?php echo $jadwal->nm_jadwal ?></a>
                </li> 
                
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Group Movement <?php echo $jadwal->nm_jadwal ?></span>
                        </div>
                    
                           <div class="actions">
                                <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        
                        
                    </div>
                    <script>
                    id_jadwal = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <!-- <th>Id Jadwal</th> -->
                                        <th  width="15%">Tour Code</th>
                                        <th  width="15%">Arrival Date</th>
                                        <th  width="15%">Departure Date</th>
                                        <th  width="15%">Carrier</th>
                                        <th  width="15%">Person In Charge</th>
                                        <th  width="15%">Contact No</th>
                                        <th  width="15%">Email Addres</th>
                                        <th width="15%">Hotel Madinah</th>
                                        <th width="15%">Hotel Mekkah</th>
                                        <th width="15%">Land Transport</th>
                                        <th width="15%">Total Passengers</th>
                                        <th width="15%">Guide Request</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>