<form action="#" id="form-create" role="form">
    <!-- <div class="form-body"> -->
        <div class="row"><br>
            <input type="text" class="hidden" name="id_jadwal" value="<?php echo $id_jadwal?>">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="tour_code" placeholder="Masukkan Tour Code">
                    <label for="form_control_1">Tour Code
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Tour Code</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group date form_datetime form_datetime bs-datetime">
                        <input type="text" size="16" class="form-control pilih" name="arrival_date" placeholder="Masukkan Arrival Date">
                        <label class="control-label">Arrival Date <span class="required">*</span>
                        </label>
                            <span class="input-group-addon">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>   
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group date form_datetime form_datetime bs-datetime">
                        <input type="text" size="16" class="form-control" name="departure_date" placeholder="Masukkan Departure Date">
                        <label class="control-label">Departure Date <span class="required">*</span>
                        </label>
                            <span class="input-group-addon">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>  
        </div> 

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="carrier" placeholder="Masukkan Carrier">
                    <label for="form_control_1">Carrier</label>
                    <span class="help-block">Masukkan Carrier</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="person_in_charge" placeholder="Masukkan Person In Charge">
                    <label for="form_control_1">Person In Charge</label>
                    <span class="help-block">Masukkan Person In Charge</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="contact_no" placeholder="Masukkan Contac No.">
                    <label for="form_control_1">Contac No.</label>
                    <span class="help-block">Masukkan Contac No.</span>
                </div>
            </div> 
        </div> 

        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="email_addres" placeholder="Masukkan Email Address">
                    <label for="form_control_1">Email Address</label>
                    <span class="help-block">Masukkan Email Address</span>
                </div>
            </div> 
        </div> 

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label for="multi-append" class="control-label">Hotel Madinah</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel_madinah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div> 

            <div class="col-md-6">
                <div class="form-group">
                <label for="multi-append" class="control-label">Hotel Makkkah</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel_makkah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
            

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="land_transport" placeholder="Masukkan Land Transport">
                    <label for="form_control_1">Land Transport</label>
                    <span class="help-block">Masukkan Land Transport</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="total_passengers" placeholder="Masukkan Total Passenger">
                    <label for="form_control_1">Total Passenger</label>
                    <span class="help-block">Masukkan Total Passenger</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="guide_request" placeholder="Masukkan Guide Request">
                    <label for="form_control_1">Guide Request</label>
                    <span class="help-block">Masukkan Guide Request</span>
                </div>
            </div>            
        </div>    
    <!-- </div> -->
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>
