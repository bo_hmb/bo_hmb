<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Group_movement extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Group Movement';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            $this->load->model('M_group_movement');
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function schedule()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Group Movement';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['id_data'] = $this->uri->segment(3);
            $this->data['jadwal'] = $this->M_schedule->get_where(array('id' => $this->uri->segment(3)))->row();
            $this->data['cek'] = count($this->M_group_movement->get_where(array('id_jadwal' => $this->uri->segment(3)))->result());
            $this->data['content'] = $this->load->view('main', $this->data, true);
            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            
            $tour_code = $this->input->post('tour_code',TRUE);
            $arrival_date = $this->input->post('arrival_date',TRUE);
            $departure_date = $this->input->post('departure_date',TRUE);
            $carrier = $this->input->post('carrier',TRUE);
            $person_in_charge = $this->input->post('person_in_charge',TRUE);
            $contact_no = $this->input->post('contact_no',TRUE);
            $email_addres = $this->input->post('email_addres',TRUE);
            $hotel_madinah = $this->input->post('hotel_madinah',TRUE);
            $hotel_mekkah = $this->input->post('hotel_mekkah',TRUE);
            $land_transport = $this->input->post('land_transport',TRUE);
            $total_passengers = $this->input->post('total_passengers',TRUE);
            $guide_request = $this->input->post('guide_request',TRUE);

			$cols = array();
			//if (!empty($id_jadwal)) { $cols['id_jadwal'] = $id_jadwal; }
			if (!empty($tour_code)) { $cols['tour_code'] = $tour_code; }
			if (!empty($arrival_date)) { $cols['arrival_date'] = $arrival_date; }
			if (!empty($departure_date)) { $cols['departure_date'] = $departure_date; }
            if (!empty($carrier)) { $cols['carrier'] = $carrier; }
            if (!empty($person_in_charge)) { $cols['person_in_charge'] = $person_in_charge; }
            if (!empty($contact_no)) { $cols['contact_no'] = $contact_no; }
            if (!empty($email_addres)) { $cols['email_addres'] = $email_addres; }
            if (!empty($hotel_madinah)) { $cols['hotel_madinah'] = $hotel_madinah; }
            if (!empty($hotel_mekkah)) { $cols['hotel_mekkah'] = $hotel_mekkah; }
            if (!empty($land_transport)) { $cols['land_transport'] = $land_transport; }
            if (!empty($total_passengers)) { $cols['total_passengers'] = $total_passengers; }
            if (!empty($guide_request)) { $cols['guide_request'] = $guide_request; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
              $where = "group_movement.active = '1' AND group_movement.id_jadwal = '".$id."'";
            } else {
              $where = "group_movement.active = '1' AND group_movement.id_jadwal = '".$id."'";
            }

	        $list = $this->M_group_movement->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_group_movement->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-agenda tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-search"></i></button>

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>

                                <button type="button" class="btn btn-xs blue btn-outline btn-reservation tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-hotel"></i></button>
                            </div>';
                $hotel_makkah = $this->M_group_movement->hotel($r->id_hotel_makkah);
                $hotel_madinah = $this->M_group_movement->hotel($r->id_hotel_madinah);

                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    // $r->nm_jadwal,
                    $r->tour_code,
                    $r->arrival_date,
                    $r->departure_date,
                    $r->carrier,
                    $r->person_in_charge,
                    $r->contact_no,
                    $r->email_addres,
                    $hotel_madinah->hotel,
                    $hotel_makkah->hotel,
                    $r->land_transport,
                    $r->total_passengers,
                    $r->guide_request,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
            $data['id_jadwal'] = $id;
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();

			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

            $arrhotel_makkah = $this->input->post('id_hotel_makkah');
            foreach($arrhotel_makkah as $val)
            {
                $hotel_makkaharr = @$hotel_makkaharr . $val. ",";
            }

            $arrhotel_madinah = $this->input->post('id_hotel_madinah');
            foreach($arrhotel_madinah as $val)
            {
                $hotel_madinaharr = @$hotel_madinaharr . $val. ",";
            }

			$data = array(
				'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'tour_code' => $this->input->post('tour_code',TRUE),
                'arrival_date' => $this->input->post('arrival_date',TRUE),
                'departure_date' => $this->input->post('departure_date',TRUE),
                'carrier' => $this->input->post('carrier',TRUE),
                'person_in_charge' => $this->input->post('person_in_charge',TRUE),
                'contact_no' => $this->input->post('contact_no',TRUE),
                'email_addres' => $this->input->post('email_addres',TRUE),
                'id_hotel_makkah' => substr(trim($hotel_makkaharr), 0, -1),
                'id_hotel_madinah' => substr(trim($hotel_madinaharr), 0, -1),
                'land_transport' => $this->input->post('land_transport',TRUE),
                'total_passengers' => $this->input->post('total_passengers',TRUE),
                'guide_request' => $this->input->post('guide_request',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_group_movement->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_group_movement->get_where(array('id' => $id))->row();
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();
            // Validate the submitted data
            
            $id    = $this->input->post('id');
            
            $arrhotel_makkah = $this->input->post('id_hotel_makkah');
            foreach($arrhotel_makkah as $val)
            {
                $hotel_makkaharr = @$hotel_makkaharr . $val. ",";
            }

            $arrhotel_madinah = $this->input->post('id_hotel_madinah');
            foreach($arrhotel_madinah as $val)
            {
                $hotel_madinaharr = @$hotel_madinaharr . $val. ",";
            }

            $data = array(
                'tour_code' => $this->input->post('tour_code',TRUE),
                'arrival_date' => $this->input->post('arrival_date',TRUE),
                'departure_date' => $this->input->post('departure_date',TRUE),
                'carrier' => $this->input->post('carrier',TRUE),
                'person_in_charge' => $this->input->post('person_in_charge',TRUE),
                'contact_no' => $this->input->post('contact_no',TRUE),
                'email_addres' => $this->input->post('email_addres',TRUE),
                'id_hotel_makkah' => substr(trim($hotel_makkaharr), 0, -1),
                'id_hotel_madinah' => substr(trim($hotel_madinaharr), 0, -1),
                'land_transport' => $this->input->post('land_transport',TRUE),
                'total_passengers' => $this->input->post('total_passengers',TRUE),
                'guide_request' => $this->input->post('guide_request',TRUE),
                'active' => '1',
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_group_movement->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_group_movement->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            $this->form_validation->set_rules('tour_code', 'tour code', 'trim|required');
            $this->form_validation->set_rules('arrival_date', 'arrival date', 'trim|required');
            $this->form_validation->set_rules('departure_date', 'departure date', 'trim|required');
            $this->form_validation->set_rules('carrier', 'carrier', 'trim|required');
            $this->form_validation->set_rules('email_addres', 'email addres', 'trim|required');
            $this->form_validation->set_rules('land_transport', 'land transport', 'trim|required');
            $this->form_validation->set_rules('total_passengers', 'total passengers', 'trim|required');
            $this->form_validation->set_rules('guide_request', 'guide request', 'trim|required');
           
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
