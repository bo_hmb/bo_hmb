<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_group_movement extends CI_Model 
	{
		//id_jadwal,hari,tgl,lokasi

		var $table = 'group_movement';
		var $jadwal = 'jadwal';

	    var $column_order = array('id','id_jadwal','hari','tgl','lokasi');
	    var $column_search = array('id','id_jadwal','hari','tgl','lokasi');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->select(' group_movement.id,
								group_movement.id_jadwal,
								jadwal.nm_jadwal,
								group_movement.tour_code,
								group_movement.arrival_date,
								group_movement.departure_date,
								group_movement.carrier,
								group_movement.person_in_charge,
								group_movement.contact_no,
								group_movement.email_addres,
								group_movement.id_hotel_madinah,
								group_movement.id_hotel_makkah,
								group_movement.land_transport,
								group_movement.total_passengers,
								group_movement.guide_request,
								group_movement.active 
	    						');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	$this->db->join($this->jadwal, ''.$this->table.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function hotel($id)
		{
			$results = array();
			$query = $this->db->query('SELECT GROUP_CONCAT(nm_hotel) AS hotel FROM hotel WHERE id in('.$id.')');
			return $query->row();
		}
	}
