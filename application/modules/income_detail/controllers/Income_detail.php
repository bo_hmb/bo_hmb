<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Income_detail extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

            $this->bulan = date('m');
            $this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Income Detail';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_income_detail');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Hotel';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);


            // Render page
            $this->renderPage();
        }        

        public function load_data()
        {            
            $no_faktur = $this->input->post('no_faktur',TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $tgl_bayar = $this->input->post('tgl_bayar',TRUE);
            $catatan = $this->input->post('catatan',TRUE);
            $tunai = $this->input->post('tunai',TRUE);
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $jenis = $this->input->post('jenis',TRUE);
            $status_verifikasi = $this->input->post('status_verifikasi',TRUE);

            $cols = array();
            //if (!empty($id_jadwal)) { $cols['id_jadwal'] = $id_jadwal; }
            if (!empty($no_faktur)) { $cols['no_faktur'] = $no_faktur; }
            if (!empty($no_pendaftaran)) { $cols['no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($tgl_bayar)) { $cols['tgl_bayar'] = $tgl_bayar; }
            if (!empty($catatan)) { $cols['catatan'] = $catatan; }
            if (!empty($tunai)) { $cols['tunai'] = $tunai; }
            if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
            if (!empty($jenis)) { $cols['pembayaran.jenis'] = $jenis; }
            if ($status_verifikasi != null) { $cols['status_verifikasi'] = $status_verifikasi; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "pembayaran.active = '1'";              
            } else {
             $where = "pembayaran.active = '1'";              
            }


            $list = $this->M_income_detail->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_income_detail->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                 if($r->status_verifikasi == '1')
                {
                    $active = 'Terverifikasi';
                    $label = 'success';
                }else{
                    $active = 'Belum Terverifikasi';
                    $label = 'danger';
                }


                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>

                            </div>';

                if ($r->jenis == 'IC') {
                   $x='Payment';
                } else {
                    $x='Other';
                }

                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    $r->nm_cabang,
                    $r->no_faktur,
                    $r->nm_lengkap,
                    $r->nm_jadwal,
                    $tgl=date('d-m-Y', strtotime($r->tgl_bayar)),
                    $r->tunai,
                    $t="Rp." . number_format($r->jml_bayar,0,',','.'),
                    $r->catatan,
                    $path_bukti_bayar='<img src="'.base_url().'upload/'.$r->path_bukti_bayar.'" class="img-responsive" />',
                    // $r->no_telp
                    '<span class="label label-sm label-'.$label.'">'.$active.'</span>',
                    $x,

                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        

        public function add()
        {
            $this->ajaxRequest();
            // $this->validateInput();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;
            
            $cek = $this->M_income_detail->get_where_payment(array('active'=>'1'))->num_rows();
            $faktur = 'IO-'.date('m').date("Y").sprintf('%03d',$cek+1);

            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                $data = array(
                    'no_faktur' => $faktur,
                    'jenis' => 'IO',
                    'id_pendaftaran' => $this->input->post('jenis_income',TRUE),
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'path_bukti_bayar' => $image,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'tunai' =>  $this->input->post('tunai',TRUE),
                    'active' => '1',
                    'status_verifikasi' => '1',
                    'id_user' => $this->currentUser->id,
                    );
            } else {
                $data = array(
                    'no_faktur' => $faktur,
                    'jenis' => 'IO',
                    'id_pendaftaran' => $this->input->post('jenis_income',TRUE),
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'tunai' =>  $this->input->post('tunai',TRUE),
                    'active' => '1',
                    'status_verifikasi' => '1',
                    'id_user' => $this->currentUser->id,
                    );
            }
            $query = $this->M_income_detail->_insert($data);

            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
            
            return response($response, 'json');
        }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_income_detail->get_where(array('id' => $id))->row();
            // $data['schedule'] = $this->M_schedule->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            $this->ajaxRequest();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            // Preparing the data before update
            $id    = $this->input->post('id');
            $this->load->library('upload',$config);
            
            if ($this->input->post('jenis') == 'IC') $id_pendaftaran = $this->input->post('id_pendaftaran'); 
                else 
                    $id_pendaftaran = $this->input->post('jenis_income'); 

            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                $data = array(
                    'id_pendaftaran' => $id_pendaftaran,
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'path_bukti_bayar' => $image,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'user_update' => $this->currentUser->id,
                );
             } else {
             $data = array(
                    'id_pendaftaran' => $id_pendaftaran,
                    'tgl_bayar' => $this->input->post('tgl_bayar',TRUE),
                    'jml_bayar' => $this->input->post('jml_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'status_verifikasi' => $this->input->post('status_verifikasi',TRUE),
                    'tunai' => $this->input->post('tunai',TRUE),   
                    'via_bank' => $this->input->post('via_bank',TRUE),
                    'user_update' => $this->currentUser->id,                 
                );
            }   
            $query = $this->M_income_detail->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_income_detail->_update(array('id' => $id), $data);
            $qu_select = $this->M_income_detail->get_where(array('id' => $id))->row();
            $query_del = $this->M_income_detail->delete_by_id($qu_select->no_faktur);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'Berhasil dihapus');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal dihapus');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function load_export()
        {
            $data['title'] = '';
            // $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['cabang'] = $this->M_income_detail->get_cabang(array('active' => '1'))->result();   
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function load_export_date()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['cabang'] = $this->M_income_detail->get_cabang(array('active' => '1'))->result(); 
            $data['marketing'] = Modules::run('marketing/get_marketing_where', array('active' => '1'))->result();
            return response($this->load->view('export_by_month', $data, TRUE), 'html');
        }

        public function load_verify()
        {
            $data['title'] = '';
            // $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['cabang'] = $this->M_income_detail->get_cabang(array('active' => '1'))->result();   
            return response($this->load->view('verify', $data, TRUE), 'html');
        }

        public function load_unverify()
        {
            $data['title'] = '';
            // $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['cabang'] = $this->M_income_detail->get_cabang(array('active' => '1'))->result();   
            return response($this->load->view('un_verify', $data, TRUE), 'html');
        }

        public function verify()
        {

            $tgl = $this->input->post('tgl_bayar');
            $kd_cabang = $this->input->post('kd_cabang');
            
            if ($kd_cabang == '0') {
                $list = $this->M_income_detail->get_payment_verify2($tgl)->result();                       
            } else {                                          
                $list = $this->M_income_detail->get_payment_verify($tgl,$kd_cabang)->result();   
            }   

            foreach ($list as $r) {
                $id = $r->id;
                $data = array(
                    'status_verifikasi' => 1,                 
                );
                $query = $this->M_income_detail->_update(array('id' => $id), $data);
            }

            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            } 
            
        }

        public function unverify()
        {
            $tgl = $this->input->post('tgl_bayar');
            $kd_cabang = $this->input->post('kd_cabang');

            if ($kd_cabang == '0') {
                $list = $this->M_income_detail->get_payment_verify2($tgl)->result();                       
            } else {                                          
                $list = $this->M_income_detail->get_payment_verify($tgl,$kd_cabang)->result();   
            }
            
            foreach ($list as $r) {
                $id = $r->id;
                $data = array(
                    'status_verifikasi' => 0,                 
                );
                $query = $this->M_income_detail->_update(array('id' => $id), $data);
            }

            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            } 

            // echo "<script type='text/javascript'>
            //     alert('Success');
            //     </script>       
            //     ";           
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $tgl_bayar1 = $this->input->post('tgl_bayar1');
            $tgl_bayar2 = $this->input->post('tgl_bayar2');
            $kd_cabang = $this->input->post('kd_cabang');
            //$data['main'] = $id;
            $data['tgl_bayar1'] = $tgl_bayar1;
            $data['tgl_bayar2'] = $tgl_bayar2;

            if ($kd_cabang == '0') {
                $data ['detail'] = $this->M_income_detail->get_export('pembayaran.status_verifikasi = "1" AND pembayaran.tgl_bayar BETWEEN "'.$tgl_bayar1.'" AND "'.$tgl_bayar2.'" ')->result();                
                $data ['sum'] = $this->M_income_detail->get_sum_export('pembayaran.status_verifikasi = "1" AND pembayaran.tgl_bayar BETWEEN "'.$tgl_bayar1.'" AND "'.$tgl_bayar2.'" ')->result();                
            } else {
                $data ['detail'] = $this->M_income_detail->get_export('pembayaran.status_verifikasi = "1" AND pendaftar.kd_office="'.$kd_cabang.'" AND pembayaran.tgl_bayar BETWEEN "'.$tgl_bayar1.'" AND "'.$tgl_bayar2.'"')->result();
                $data ['sum'] = $this->M_income_detail->get_sum_export('pembayaran.status_verifikasi = "1" AND pendaftar.kd_office="'.$kd_cabang.'" AND  pembayaran.tgl_bayar BETWEEN "'.$tgl_bayar1.'" AND "'.$tgl_bayar2.'" ')->result();                                            
            }
            
            //$data ['total'] = $this->M_benefit_marketing->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('income_detail/print_export', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                        'orientation' => 'L'
                                        
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_month()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $bulan = $this->input->post('bulan');
            $tahun = $this->input->post('tahun');
            $via_bank = $this->input->post('via_bank');
            $id_jadwal = $this->input->post('id_jadwal');
            $kd_cabang = $this->input->post('kd_cabang');
            $id_marketing = $this->input->post('id_marketing');
            //$data['main'] = $id;
            // $data['main'] = $tgl_bayar;

            if ($bulan == 'All') $FilterBulan = ''; else $FilterBulan='MONTH(tgl_bayar) = "'.$bulan.'" AND';
            if ($tahun == 'All') $Filtertahun = ''; else $Filtertahun='YEAR(tgl_bayar) = "'.$tahun.'" AND';
            if ($via_bank == 'All') $Filtervia_bank = ''; else $Filtervia_bank='via_bank = "'.$via_bank.'" AND';
            if ($id_jadwal == 'All') $Filterid_jadwal = ''; else $Filterid_jadwal='id_jadwal = "'.$id_jadwal.'" AND';
            if ($kd_cabang == 'All') $Filterkd_cabang = ''; else $Filterkd_cabang='pendaftar.kd_office = "'.$kd_cabang.'" AND';
            if ($id_marketing == 'All') $Filterid_marketing = ''; else $Filterid_marketing='formulir.id_marketing = "'.$id_marketing.'" AND';

            $whereFilter = 'pembayaran.status_verifikasi = "1" AND '.$FilterBulan.' '.$Filtertahun.' '.$Filtervia_bank.' '.$Filterid_jadwal.' '.$Filterkd_cabang.' '.$Filterid_marketing;

            $data ['detail'] = $this->M_income_detail->get_export_month($whereFilter)->result();                
            $data ['sum'] = $this->M_income_detail->get_sum_export_month($whereFilter)->result();   
            $data ['sum_by_schedule'] = $this->M_income_detail->get_sum_by_scheedule($whereFilter)->result();                
            
            //$data ['total'] = $this->M_benefit_marketing->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('income_detail/print_export_month', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                        
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function load_add_form()
        {
            $data['main'] = '$this->M_payment->get_detail()';
            return response($this->load->view('add_', $data, TRUE), 'html');
        }

    }
?>
