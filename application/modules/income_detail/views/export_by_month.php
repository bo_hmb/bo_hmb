            <form role="form" method="POST" action="<?php echo base_url(); ?>income_detail/export_month" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export">  -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-5">
                            <select id="kd_cabang" name="bulan" class="form-control jadwal" data-placeholder="Select Month">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="1">Januari</option>                                
                                <option value="2">Februari</option>                                
                                <option value="3">Maret</option>                                
                                <option value="4">April</option>                                
                                <option value="5">Mei</option>                                
                                <option value="6">Juni</option>                                
                                <option value="7">Juli</option>                                
                                <option value="8">Agustus</option>                                
                                <option value="9">September</option>                                
                                <option value="10">Oktober</option>                                
                                <option value="11">November</option>                                
                                <option value="12">Desember</option>                                
                            </select>
                        </div>
                   
                        <!-- <label class="col-md-2 control-label">Year</label> -->
                        <div class="col-md-5">
                            <select id="kd_cabang" name="tahun" class="form-control jadwal" data-placeholder="Select Year">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="2018">2018</option>                                
                                <option value="2019">2019</option>                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>    
                        <!-- <label class="col-md-2 control-label">Bank</label> -->
                        <div class="col-md-5">
                            <select id="kd_cabang" name="via_bank" class="form-control jadwal" data-placeholder="Select Bank">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="Mandiri">Mandiri</option>
                                <option value="BRI">BRI</option>
                                <option value="BNI Syariah">BNI Syariah</option>                              
                                <option value="BSM">BSM</option>
                                <option value="Terhutang">Terhutang</option>                              
                            </select>
                        </div>

                        <div class="col-md-5">
                            <select id="kd_cabang" name="id_jadwal" class="form-control jadwal" data-placeholder="Tgl. Keberangkatan">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="0">Tabungan</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>    
                        <!-- <label class="col-md-2 control-label">Bank</label> -->
                        <div class="col-md-5">
                            <select class="form-control jadwal" name="kd_cabang">
                                <option></option>
                                <option value="All">All</option>
                                <?php foreach ($cabang as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>
                                <?php } ?>
                            </select>                             
                            </select>
                        </div>

                        <div class="col-md-5">
                            <select class="form-control jadwal" name="id_marketing">
                                <option></option>
                                <option value="All">All</option>
                                <?php foreach ($marketing as $mkt) { ?>
                                <option value="<?php echo $mkt->id; ?>"><?php echo $mkt->nik; ?> - <?php echo $mkt->nm_marketing; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     