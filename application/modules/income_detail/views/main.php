<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Income Detail</a>
                </li> 
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Income Detail</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            <!-- <div class="btn-group">
                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs "> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </div> -->
                            <div class="btn-group">
                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-share"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export"> Export by Payment Date </a>
                                    </li>
                                    <li>
                                        <a class="btn-export-month"> Export by Schedule </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn green btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <span class="hidden-xs"> Verify </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-verify"> Verify </a>
                                    </li>
                                    <li>
                                        <a class="btn-unverify"> Un Verify </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="5%">Aksi</th>
                                        <th width="10%">Nama Cabang</th>
                                        <th width="10%">No. Faktur</th>
                                        <th width="10%">Nama Jamaah</th>
                                        <th width="10%">Income</th>
                                        <th width="1%">Tanggal Bayar</th>
                                        <th width="5%">Tunai/Transfer</th>
                                        <th width="15%">Jumlah Bayar</th>
                                        <th width="15%">Catatan</th>
                                        <th width="15%">Path Foto</th>
                                        <th width="15%">Status</th>
                                        <th width="15%">Jenis</th>
                                        <!-- <th width="15%">No.Telp. Cabang</th> -->
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td></td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        
                                        <td>
                                            <select name="nm_cabang" class="form-control form-filter input-sm">
                                                <option></option>
                                                <?php
                                                    $cabang= $this->M_income_detail->get_cabang(array('active' => '1'))->result();
                                                    foreach ($cabang as $row) { ?>
                                                    <option value="<?php echo $row->nm_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                                <?php }?>
                                            </select>
                                        </td>

                                        <td><input type="text" class="form-control form-filter input-sm" name="no_faktur"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"></td>
                                        <td>
                                            <select name="nm_jadwal" class="form-control form-filter input-sm pilih">
                                                <option disabled selected></option>                                                
                                                <!-- <option value="0000-00-00">Tabungan</option>                                                 -->
                                                <?php
            $schedule = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    

                                                foreach ($schedule as $row) { ?>
                                                <option value="<?php echo $row->nm_jadwal; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <!-- <input type="text" class="form-control form-filter input-sm" name="nm_jadwal"> -->
                                        </td>
                                        <td><input type="date" class="form-control form-filter input-sm" name="tgl_bayar"></td>
                                        <td>
                                            <select name="tunai" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="Tunai">Tunai</option>
                                                <option value="Transfer">Transfer</option>
                                            </select> 
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td> 
                                        <td>
                                           <select name="status_verifikasi" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="0">Belum</option>
                                                <option value="1">Terverifikasi</option>
                                            </select>  
                                        </td>                                       
                                        <td>
                                            <select name="jenis" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="IC">Payment</option>
                                                <option value="IO">Other</option>
                                            </select>
                                        </td>
                                        
                                        
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>