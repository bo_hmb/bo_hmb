<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <input type="text" class="hidden" name="id" value="<?php echo $main->id?>">
            <input type="text" class="hidden" name="id_pendaftaran" value="<?php echo $main->id_pendaftaran?>">
            <input type="hidden" name="jenis" value="<?php echo $main->jenis?>">
            
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" id="tgl_bayar" name="tgl_bayar" class="form-control" readonly value="<?php echo $main->tgl_bayar?>">
                        <label class="control-label">Tanggal Bayar
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Bayar</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jml_bayar" placeholder="Jumlah Bayar" value="<?php echo $main->jml_bayar?>">
                    <label for="form_control_1">Jumlah Bayar
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Jumlah Bayar</span>
                </div>
            </div>            
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="tunai">
                        <option <?php echo ("Transfer" === $main->tunai) ? 'selected' : ''; ?> value="Transfer">Transfer</option>
                        <option <?php echo ("Tunai" === $main->tunai) ? 'selected' : ''; ?> value="Tunai">Tunai</option>
                    </select>
                    <label>Tunai / Transfer
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Tunai / Transfer</span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                   <select class="form-control jeniskelamin " id="via_bank" name="via_bank">
                        <option <?php echo ("Mandiri" === $main->via_bank) ? 'selected' : ''; ?> value="Mandiri">Mandiri</option>
                        <option <?php echo ("BRI" === $main->via_bank) ? 'selected' : ''; ?> value="BRI">BRI</option>
                        <option <?php echo ("BNI Syariah" === $main->via_bank) ? 'selected' : ''; ?> value="BNI Syariah">BNI Syariah</option>
                        <option <?php echo ("BSM" === $main->via_bank) ? 'selected' : ''; ?> value="BSM">BSM</option>
                        <option <?php echo ("Terhutang" === $main->via_bank) ? 'selected' : ''; ?> value="Terhutang">Terhutang</option>
                    </select>
                    <label>Via Bank
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Pilih Via Bank</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilih" name="jenis_income" <?php if ($main->jenis == 'IC') echo "disabled"; ?>>
                        <option disabled selected></option>
                        <option <?php echo ("0" === $main->id_pendaftaran) ? 'selected' : ''; ?> value="0">Umum</option>
                        <option <?php echo ("3" === $main->id_pendaftaran) ? 'selected' : ''; ?> value="3">Tiket</option>
                        <option <?php echo ("4" === $main->id_pendaftaran) ? 'selected' : ''; ?> value="4">Visa</option>
                        <option <?php echo ("5" === $main->id_pendaftaran) ? 'selected' : ''; ?> value="5">Handling</option>
                        <option <?php echo ("6" === $main->id_pendaftaran) ? 'selected' : ''; ?> value="6">Hotel</option>
                    </select>
                    <label>Jenis Income
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Jenis Income</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <!-- <input type="text" class="form-control" name="catatan" placeholder="Masukkan catatan" value="<?php echo $main->catatan?>"> -->
                    <select class="form-control jeniskelamin catatan" id="catatan" name="catatan">
                        <option <?php echo ("DP" === $main->catatan) ? 'selected' : ''; ?> value="DP">DP</option>
                        <option <?php echo ("Pembayaran Ke-II" === $main->catatan) ? 'selected' : ''; ?> value="Pembayaran Ke-II">Pembayaran Ke-II</option>
                        <option <?php echo ("Pembayaran Ke-III" === $main->catatan) ? 'selected' : ''; ?> value="Pembayaran Ke-III">Pembayaran Ke-III</option>
                        <option <?php echo ("Pembayaran Ke-IV" === $main->catatan) ? 'selected' : ''; ?> value="Pembayaran Ke-IV">Pembayaran Ke-IV</option>
                        <option <?php echo ("Pembayaran Ke-V" === $main->catatan) ? 'selected' : ''; ?> value="Pembayaran Ke-V">Pembayaran Ke-V</option>
                        <option <?php echo ("Pelunasan" === $main->catatan) ? 'selected' : ''; ?> value="Pelunasan">Pelunasan</option>
                    </select>
                    <label for="form_control_1">Catatan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Catatan</span>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                    <label>Bukti Bayar
                    </label>
                    <span class="help-block">Masukkan Bukti Bayar</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="status_verifikasi">
                        <option <?php echo ("0" === $main->status_verifikasi) ? 'selected' : ''; ?> value="0">Belum Terverifikasi</option>
                        <option <?php echo ("1" === $main->status_verifikasi) ? 'selected' : ''; ?> value="1">Terverifikasi</option>
                    </select>
                    <label for="form_control_1">Status Verifikasi</label>
                    <span class="help-block">Masukkan Status Verifikasi</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>