<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                        <input type="text" id="tgl_bayar" name="tgl_bayar" class="form-control" readonly value="<?php echo date('Y-m-d') ?>">
                        <label class="control-label">Tanggal Bayar
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Bayar</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jml_bayar" placeholder="Masukkan Jumlah Income">
                    <label for="form_control_1">Jumlah Income
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Jumlah Income</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilih" name="tunai">
                        <option value="Tunai">Tunai</option>
                        <option value="Transfer">Transfer</option>
                    </select>
                    <label>Tunai / Transfer
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Tunai / Transfer</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <select class="form-control pilih" name="jenis_income">
                        <option value="0">Umum</option>
                        <option value="3">Tiket</option>
                        <option value="4">Visa</option>
                        <option value="5">Handling</option>
                        <option value="6">Hotel</option>
                    </select>
                    <label>Jenis Income
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Jenis Income</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                    <label style="color:red">Bukti Bayar (Max. 1.9 MB)
                    </label>
                    <span class="help-block">Masukkan Bukti Bayar</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                   <select class="form-control jeniskelamin " id="via_bank" name="via_bank">
                        <option value="Mandiri">Mandiri</option>
                        <option value="BRI">BRI</option>
                        <option value="BNI Syariah">BNI Syariah</option>
                        <!-- <option value="Terhutang">Terhutang</option> -->
                    </select>
                    <label>Via Bank
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Pilih Via Bank</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="catatan" placeholder="Masukkan catatan"">
                    <label for="form_control_1">Catatan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Catatan</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>