            <form role="form" method="POST" action="<?php echo base_url(); ?>income_detail/export" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Date</label>
                        <div class="col-md-8">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                                <input type="text" name="tgl_bayar1" class="form-control" value="<?php echo date('Y-m-d') ?>">                               
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Date</label>
                        <div class="col-md-8">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                                <input type="text" name="tgl_bayar2" class="form-control" value="<?php echo date('Y-m-d') ?>">                               
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Work Area</label>
                        <div class="col-md-8">
                            <select id="kd_cabang" name="kd_cabang" class="form-control jadwal" data-placeholder="Select Work Area">
                                <option disabled selected></option>
                                <option value="0">All</option>
                                <?php
                                foreach ($cabang as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     