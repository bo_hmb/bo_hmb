            <!-- <form role="form" method="POST" action="<?php echo base_url(); ?>income_detail/export" class="form-horizontal"> -->
            
            <form role="form" action="#" class="form-horizontal" id="form-unverify">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Date</label>
                        <div class="col-md-8">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                                <input type="text" name="tgl_bayar" class="form-control" value="<?php echo date('Y-m-d') ?>">                               
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Work Area</label>
                        <div class="col-md-8">
                            <select id="kd_cabang" name="kd_cabang" class="form-control jadwal" data-placeholder="Select Work Area">
                                <option disabled selected></option>
                                <option value="0">All</option>
                                <?php
                                foreach ($cabang as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-file-pdf-o"></i> UnVerify</button>
                                
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     