<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>Data Income Month</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>   
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 140px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h2 style="text-align: center; font-weight: bold;">DATA INCOME DETAIL</h2>
        </th>
    </tr>

    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 50PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <!-- <h3 style="text-align: center;"><?php echo $x=date('d-m-Y', strtotime($main)) ; ?></h3> -->
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width:20px;">No</th>
        <th style="width:80px;">Bank</th>
        <th style="width:90px;">Cabang</th>
        <th style="width:90px;">No. Faktur</th>
        <th style="width:240px;">Nama Jamaah</th>
        <th style="width:80px;">Tanggal Bayar</th>
        <th style="width:110px;">Jumlah Bayar</th>
        <th style="width:250px;">Catatan</th>
        <th style="width:150px;">Nama Agen</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { ?>
        <?php 
        if ($row->tgl_keberangkatan == '') {
            $tgl='';
        } else if ($row->tgl_keberangkatan == '0000-00-00') {
            $tgl='Tabungan';
        } else {
            $tgl= date('d M Y', strtotime($row->tgl_keberangkatan));
        }

         ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->via_bank ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_faktur ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_bayar)) ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->jml_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo $row->catatan ?> <?php echo $tgl ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>
        </tr>   
    <?php $nomor++;}?> 

</table>

<br>
<h3 style="font-weight: bold;">Rekapitulasi</h3>            
<table>    
    <tr>          
        <!-- <th style="width: 20px;">No</th>         -->
        <th style="width: 150px;">Bank</th>
        <th style="width: 150px;">Total Pembayaran</th>
    </tr>

    <?php $total=0; $nomor=1; foreach($sum as $row) { 
            ?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->via_bank ?></td>     
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->count_jml_bayar,0,',','.') ?></td>      
        </tr>   
    <?php $total = $total + $row->count_jml_bayar ; $nomor++;}?> 
        <tr>
            <td style="border:1px solid #000;">Total</td>
            <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($total,0,',','.') ?></td> 
        </tr>
        

</table>
<br>
<table>    
    <tr>          
        <!-- <th style="width: 20px;">No</th>         -->
        <th style="width: 150px;">Nama Jadwal</th>
        <th style="width: 150px;">Total Pembayaran</th>
    </tr>

    <?php $tot=0; $nomor=1; foreach($sum_by_schedule as $row) { 
            ?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->nm_jadwal ?></td>     
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->count_jml_bayar,0,',','.') ?></td>      
        </tr>   
    <?php $tot=$tot+$row->count_jml_bayar; $nomor++;}?> 
        <tr>
            <td style="border:1px solid #000;">Total</td>
            <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($tot,0,',','.') ?></td> 
        </tr>

</table>
</body>
</html>