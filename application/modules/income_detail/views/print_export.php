<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>Data Income Detail</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>   
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 140px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h2 style="text-align: center; font-weight: bold;">DATA INCOME DETAIL</h2>
        </th>
    </tr>

    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 50PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h3 style="text-align: center;"><?php echo $x=date('d-m-Y', strtotime($tgl_bayar1)) ; ?> - <?php echo $x=date('d-m-Y', strtotime($tgl_bayar2)) ; ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width:20px;">No</th>
        <th style="width:100px;">Nama Cabang</th>
        <th style="width:90px;">No. Faktur</th>
        <th style="width:240px;">Nama Jamaah</th>
        <th style="width:80px;">Tanggal Bayar</th>
        <th style="width:20px;">Tunai/Transfer</th>
        <th style="width:110px;">Jumlah Bayar</th>
        <th style="width:220px;">Catatan</th>
        <th style="width:150px;">Nama Agen</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_faktur ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_bayar)) ?></td>
           <td style="border:1px solid #000;"><?php echo $row->tunai ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->jml_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo $row->catatan ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>
        </tr>   
    <?php $nomor++;}?> 

</table>

<br>
<h3 style="font-weight: bold;">Rekapitulasi</h3>            
<table>    
    <tr>          
        <!-- <th style="width: 20px;">No</th>         -->
        <th style="width: 150px;">Nama Cabang</th>
        <th style="width: 100px;">Tunai/Transfer</th>
        <th style="width: 200px;">Total Pembayaran</th>
    </tr>

    <?php $nomor=1; foreach($sum as $row) { 
            ?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->tunai ?></td>      
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->total,0,',','.') ?></td>      
        </tr>   
    <?php $nomor++;}?> 

</table>
</body>
</html>