<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_income_detail extends CI_Model 
	{
		
		var $table = 'pembayaran';
		var $pendaftar = 'pendaftar';
		var $formulir = 'formulir';
		var $jadwal = 'jadwal';
		var $cabang = 'cabang';
		var $paket = 'paket';
		

	    var $column_order = array('id','tgl_bayar','jml_bayar','catatan','status_verifikasi');
	    var $column_search = array('id','tgl_bayar','jml_bayar','catatan','status_verifikasi');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}
		
		public function get_where_payment($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in_madinah($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$this->db->where('jenis_hotel', 'Madinah');
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in_makkah($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$this->db->where('jenis_hotel', 'Makkah');
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' pembayaran.id,
								pembayaran.no_faktur,
								pembayaran.jenis,
								pembayaran.id_pendaftaran,
								pendaftar.kd_office,
								pendaftar.id_formulir,
								pendaftar.no_pendaftaran,
								jadwal.nm_jadwal,
								formulir.nm_lengkap,
								pembayaran.tgl_bayar,
								pembayaran.jml_bayar,
								pembayaran.path_bukti_bayar,
								pembayaran.catatan,
								pembayaran.status_verifikasi,
								pembayaran.tunai,
								cabang.nm_cabang,
								cabang.no_telp,
								pembayaran.active');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->pendaftar, ''.$this->table.'.id_pendaftaran = '.$this->pendaftar.'.id');
	    	$this->db->join($this->paket, ''.$this->pendaftar.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	    	$this->db->join($this->formulir, ''.$this->pendaftar.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->cabang, ''.$this->pendaftar.'.kd_office = '.$this->cabang.'.kd_cabang');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
			$this->db->join('pendaftar','pembayaran.id_pendaftaran=pendaftar.id');	        

	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_jadwal_exp($where) {
			$table = $this->jadwal;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_export($where) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.id_pendaftaran,
										pendaftar.kd_office,
										pendaftar.id_formulir,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.tunai,
										cabang.nm_cabang,
										cabang.no_telp,
										pembayaran.active,
										formulir.id_marketing,
										marketing.nm_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE pembayaran.active=1 AND '.$where.'
										ORDER by pembayaran.no_faktur ASC');

			return $query;
		}

		public function get_sum_export($where) {
			$query = $this->db->query(' SELECT cabang.nm_cabang,
										tunai,SUM(jml_bayar) AS total
										FROM pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE pembayaran.active=1 AND '.$where.'
										GROUP BY kd_office,tunai
										');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		

		public function get_payment_verify($tgl,$kd_cabang) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.jenis,
										pembayaran.id_pendaftaran,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.status_verifikasi,
										pembayaran.tunai,
										pembayaran.active,
										pendaftar.kd_office
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										WHERE pembayaran.tgl_bayar="'.$tgl.'" AND pendaftar.kd_office="'.$kd_cabang.'" AND pembayaran.active=1 AND pembayaran.jenis="IC" 
										');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_payment_verify2($tgl) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.jenis,
										pembayaran.id_pendaftaran,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.status_verifikasi,
										pembayaran.tunai,
										pembayaran.active,
										pendaftar.kd_office
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										WHERE pembayaran.tgl_bayar="'.$tgl.'" AND pembayaran.active=1 AND pembayaran.jenis="IC" 
										');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_export_month($where) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.jenis,
										pembayaran.id_pendaftaran,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.status_verifikasi,
										pembayaran.tunai,
										pembayaran.via_bank,
										pembayaran.active,
										pendaftar.kd_office,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										formulir.id_marketing,
										cabang.nm_cabang,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										marketing.nm_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE
										'.$where.' pembayaran.active="1"
										ORDER BY tgl_bayar ASC
										');

			return $query;
		}

		public function get_export_month2($bulan,$tahun,$bank) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.jenis,
										pembayaran.id_pendaftaran,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.status_verifikasi,
										pembayaran.tunai,
										pembayaran.via_bank,
										pembayaran.active,
										pendaftar.kd_office,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										cabang.nm_cabang,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE
										MONTH(tgl_bayar) = "'.$bulan.'" AND
										YEAR(tgl_bayar) = "'.$tahun.'" AND
										via_bank = "'.$bank.'" AND pembayaran.active="1"
										ORDER BY tgl_bayar ASC
										');

			return $query;
		}

		public function get_sum_export_month($where) {
			$query = $this->db->query(' SELECT
										pembayaran.via_bank,
										Sum(pembayaran.jml_bayar) AS count_jml_bayar,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										pendaftar.kd_office,
										cabang.nm_cabang,
										pendaftar.id_formulir,
										formulir.id_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE
										'.$where.' pembayaran.active="1"
										GROUP BY via_bank
										');

			return $query;
		}

		public function get_sum_export_month2($bulan,$tahun,$bank) {
			$query = $this->db->query(' SELECT
										pembayaran.via_bank,
										Sum(pembayaran.jml_bayar) AS count_jml_bayar,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										pendaftar.kd_office,
										cabang.nm_cabang,
										pendaftar.id_formulir,
										formulir.id_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE
										MONTH(tgl_bayar) = "'.$bulan.'" AND
										YEAR(tgl_bayar) = "'.$tahun.'" AND
										via_bank = "'.$bank.'" AND pembayaran.active="1"
										ORDER BY tgl_bayar ASC
										');

			return $query;
		}

		public function get_sum_by_scheedule($where) {
			$query = $this->db->query(' SELECT										
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										SUM(jml_bayar) as count_jml_bayar
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE
										'.$where.' pembayaran.active="1"
										GROUP BY id_jadwal
										');

			return $query;
		}

		public function delete_by_id($id)
		{
			$this->db->where('no_faktur', $id);
			$this->db->delete('benefit_marketing');
		}

	}
