<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <!-- <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_jadwal" placeholder="Masukkan Nama Jadwal">
                    <label for="form_control_1">Nama Jadwal
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Jadwal</span>
                </div>
            </div> -->
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl_keberangkatan" name="tgl_keberangkatan" class="form-control" readonly>
                        <label class="control-label">Tanggal keberangkatan
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Keberangkatan </span>
                </div>
            </div>
            <div class="col-md-4">
                <!-- <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" name="tgl_kepulangan" class="form-control" readonly>
                        <label class="control-label">Tanggal Kepulangan
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <span class="help-block"> Masukkan Tanggal Kepulangan </span> -->

                    <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="lama_hari" placeholder="Masukkan Lama Hari">
                        <label for="form_control_1">Lama Hari
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Masukkan Lama Hari</span>
                    </div>
                <!-- </div> -->
            </div>
            <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="jumlah_kursi" placeholder="Masukkan Jumlah Kursi">
                        <label for="form_control_1">Jumlah Kursi
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Masukkan Jumlah Kursi</span>
                    </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>