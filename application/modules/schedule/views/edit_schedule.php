<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl_keberangkatan" name="tgl_keberangkatan" class="form-control" readonly value="<?php echo $main->tgl_keberangkatan ?>">
                        <label class="control-label">Tanggal Keberangkatan
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Keberangkatan </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                        <input type="text" class="form-control" name="lama_hari" placeholder="Masukkan Lama Hari" value="<?php echo $main->lama_hari ?>">
                        <label for="form_control_1">Lama Hari
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Masukkan Lama Hari</span>
                    </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jumlah_kursi" placeholder="Masukkan Jumlah Kursi" value="<?php echo $main->jumlah_kursi ?>">
                        <label for="form_control_1">Jumlah Kursi
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Masukkan Jumlah Kursi</span>
                </div>
            </div>
            <div class="col-md-2">
                 <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="sisa_kursi" placeholder="Masukkan Sisa Kursi" value="<?php echo $main->sisa_kursi ?>">
                        <label for="form_control_1">Sisa Kursi
                            <span class="required">*</span>
                        </label>
                        <span class="help-block">Masukkan Sisa Kursi</span>
                  </div>
                    
            </div>
            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>