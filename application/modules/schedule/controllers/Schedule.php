<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Schedule extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Schedule';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Schedule';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function get_schedule_where($where) {
            $query = $this->M_schedule->get_where($where);
            return $query;
        }

        public function load_data()
        {
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);
            $tgl_kepulangan = $this->input->post('tgl_kepulangan',TRUE); 
            $jumlah_kursi = $this->input->post('jumlah_kursi',TRUE); 
            $sisa_kursi = $this->input->post('sisa_kursi',TRUE); 
            $lama_hari = $this->input->post('lama_hari',TRUE);            
            $active = '1';
            $id_user = $this->currentUser->id;

			$cols = array();
			if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
			if (!empty($tgl_keberangkatan)) { $cols['tgl_keberangkatan'] = $tgl_keberangkatan; }
            if (!empty($tgl_kepulangan)) { $cols['tgl_kepulangan'] = $tgl_kepulangan; }
            if (!empty($jumlah_kursi)) { $cols['jumlah_kursi'] = $jumlah_kursi; }
            if (!empty($sisa_kursi)) { $cols['sisa_kursi'] = $sisa_kursi; }
			if (!empty($lama_hari)) { $cols['lama_hari'] = $lama_hari; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1'";              
            } else {
             $where = "active = '1'";              
            } 


	        $list = $this->M_schedule->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_schedule->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';
                            
                // selisih hari
                // $lama_hari = ((abs(strtotime($r->tgl_kepulangan) - strtotime($r->tgl_keberangkatan)))/(60*60*24));

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_jadwal,
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan)),
                    $x=date('d-m-Y', strtotime($r->tgl_kepulangan)),
                    $r->lama_hari." hari",
                    $r->jumlah_kursi,
                    $r->sisa_kursi,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->M_schedule->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Schedule';
			return response($this->load->view('add_schedule', $data, TRUE), 'html');
		}

		public function add()
	    {

            $this->ajaxRequest();
			// $this->validateInput();
            $kurang = $this->input->post('lama_hari') - 1;
			$data = array(
                'nm_jadwal' => date('d M Y', strtotime($this->input->post('tgl_keberangkatan'))). ' - '.$this->input->post('lama_hari').' Hari', //$this->input->post('nm_jadwal',TRUE),
				'tgl_keberangkatan' => $this->input->post('tgl_keberangkatan',TRUE),
                'tgl_kepulangan' => date('Y-m-d', strtotime('+'.$kurang.' days', strtotime($this->input->post('tgl_keberangkatan',TRUE)))),// $this->input->post('tgl_kepulangan',TRUE),        
                'lama_hari' => $this->input->post('lama_hari',TRUE),//((abs(strtotime($this->input->post('tgl_kepulangan',TRUE)) - strtotime($this->input->post('tgl_keberangkatan',TRUE))))/(60*60*24)), 
                'batas_pelunasan' => date('Y-m-d', strtotime('-30 days', strtotime($this->input->post('tgl_keberangkatan',TRUE)))),//date('yyyy-mm-dd', strtotime('-30 days',strtotime($this->input->post('tgl_keberangkatan',TRUE)))),      
                'jumlah_kursi' => $this->input->post('jumlah_kursi',TRUE),        
                'sisa_kursi' => $this->input->post('jumlah_kursi',TRUE),        
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_schedule->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_schedule', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');
            $kurang = $this->input->post('lama_hari') - 1;
            
            $data = array(
                'nm_jadwal' => date('d M Y', strtotime($this->input->post('tgl_keberangkatan'))). ' - '.$this->input->post('lama_hari').' Hari', //$this->input->post('nm_jadwal',TRUE),
                'tgl_keberangkatan' => $this->input->post('tgl_keberangkatan',TRUE),
                'tgl_kepulangan' => date('Y-m-d', strtotime('+'.$kurang.' days', strtotime($this->input->post('tgl_keberangkatan',TRUE)))),// $this->input->post('tgl_kepulangan',TRUE),        
                'lama_hari' => $this->input->post('lama_hari',TRUE),//((abs(strtotime($this->input->post('tgl_kepulangan',TRUE)) - strtotime($this->input->post('tgl_keberangkatan',TRUE))))/(60*60*24)), 
                'batas_pelunasan' => date('Y-m-d', strtotime('-30 days', strtotime($this->input->post('tgl_keberangkatan',TRUE)))),//date('yyyy-mm-dd', strtotime('-30 days',strtotime($this->input->post('tgl_keberangkatan',TRUE)))),      
                'jumlah_kursi' => $this->input->post('jumlah_kursi',TRUE),        
                'sisa_kursi' => $this->input->post('jumlah_kursi',TRUE),  
                'user_update' => $this->currentUser->id,                     
            );

            $query = $this->M_schedule->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_schedule->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('nm_jadwal', 'nm jadwal', 'trim|required');
            $this->form_validation->set_rules('tgl_keberangkatan', 'tgl keberangkatan', 'trim|required');
            $this->form_validation->set_rules('tgl_kepulangan', 'tgl kepulangan', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
