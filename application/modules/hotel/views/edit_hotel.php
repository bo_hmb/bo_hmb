<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_hotel" placeholder="Masukkan Nama Hotel" value="<?php echo $main->nm_hotel; ?>"> 
                    <label for="form_control_1">Nama Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Hotel</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control jenis" name="jenis_hotel">
                        <option <?php echo ('Umum' === $main->jenis_hotel) ? 'selected' : ''; ?> value="Umum">Umum</option>
                        <option <?php echo ('Makkah' === $main->jenis_hotel) ? 'selected' : ''; ?> value="Makkah">Makkah</option>
                        <option <?php echo ('Madinah' === $main->jenis_hotel) ? 'selected' : ''; ?> value="Madinah">Madinah</option>
                    </select>
                    <label>Jenis Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih jenis Hotel</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat Hotel" value="<?php echo $main->alamat; ?>">
                    <label for="form_control_1">Alamat
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Alamat Hotel</span>
                </div>
            </div>
             <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="bintang" placeholder="Masukkan Bintang Hotel" value="<?php echo $main->bintang; ?>">
                    <label>Bintang Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Bintang Hotel</span>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jrk_mdinh_msjidl" placeholder="Masukkan Jarak Hotel Madinah ke Masjid Nabawi" value="<?php echo $main->jrk_mdinh_msjidl; ?>">
                    <label for="form_control_1">Jarak Hotel Madinah ke Masjid Nabawi
                    </label>
                    <span class="help-block">Masukkan Jarak Hotel Madinah ke Masjid Nabawi</span>
                </div>
            </div>
           <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jrk_mkkh_masjidl" placeholder="Masukkan Jarak Hotel Makkah ke Masjidil Haram" value="<?php echo $main->jrk_mkkh_masjidl; ?>">
                    <label for="form_control_1">Jarak Hotel Makkah ke Masjidil Haram
                    </label>
                    <span class="help-block">Masukkan Jarak Hotel Makkah ke Masjidil Haram</span>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_sewa_kamar" placeholder="Masukkan Biaya Sewa Kamar" value="<?php echo $main->biaya_sewa_kamar; ?>">
                    <label>Biaya Sewa Kamar<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Sewa Kamar</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_sewa_hotel" placeholder="Masukkan Biaya Sewa Hotel" value="<?php echo $main->biaya_sewa_hotel; ?>">
                    <label>Biaya Sewa Hotel<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Sewa Hotel</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_upgrade_hotel" placeholder="Masukkan Biaya Upgrade Hotel" value="<?php echo $main->biaya_upgrade_hotel; ?>">
                    <label>Biaya Upgrade Hotel<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_upgrade_kamar" placeholder="Masukkan Biaya Upgrade Kamar" value="<?php echo $main->biaya_upgrade_kamar; ?>">
                    <label>Biaya Upgrade Kamar<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Kamar</span>
                </div>
            </div>
        </div> -->
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>