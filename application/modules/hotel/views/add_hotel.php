<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_hotel" placeholder="Masukkan Nama Hotel">
                    <label for="form_control_1">Nama Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Hotel</span>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group form-md-line-input">
                    <select class="form-control jenis" name="jenis_hotel">
                        <option value="Umum">Umum</option>
                        <option value="Makkah">Makkah</option>
                        <option value="Madinah">Madinah</option>
                    </select>
                    <label>Jenis Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih jenis Hotel</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat Hotel">
                    <label for="form_control_1">Alamat
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Alamat Hotel</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="bintang" placeholder="Masukkan Bintang Hotel">
                    <label>Bintang Hotel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Bintang Hotel</span>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jrk_mdinh_msjidl" placeholder="Masukkan Jarak Hotel Madinah ke Masjid Nabawi">
                    <label for="form_control_1">Jarak Hotel Madinah ke Masjid Nabawi
                    </label>
                    <span class="help-block">Masukkan Jarak Hotel Madinah ke Masjid Nabawi</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="jrk_mkkh_masjidl" placeholder="Masukkan Jarak Hotel Makkah ke Masjidil Haram">
                    <label for="form_control_1">Jarak Hotel Makkah ke Masjidil Haram
                    </label>
                    <span class="help-block">Masukkan Jarak Hotel Makkah ke Masjidil Haram</span>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_sewa_kamar" placeholder="Masukkan Biaya Sewa Kamar">
                    <label>Biaya Sewa Kamar<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Sewa Kamar</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" id="biaya_sewa_hotel" class="form-control" name="biaya_sewa_hotel" placeholder="Masukkan Biaya Sewa Hotel">
                    <label>Biaya Sewa Hotel<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Sewa Hotel</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_upgrade_hotel" placeholder="Masukkan Biaya Upgrade Hotel">
                    <label>Biaya Upgrade Hotel<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_upgrade_kamar" placeholder="Masukkan Biaya Upgrade Kamar">
                    <label>Biaya Upgrade Kamar<span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Kamar</span>
                </div>
            </div>
        </div> -->
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>