<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Hotel extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Hotel';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_hotel');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Hotel';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function get_hotel_where($where) {
            $query = $this->M_hotel->get_where($where);
            return $query;
        }

        public function get_hotel_where_in($where) {
            $query = $this->M_hotel->get_where_in($where);
            return $query;
        }

        public function get_hotel_where_in_madinah($where) {
            $query = $this->M_hotel->get_where_in_madinah($where);
            return $query;
        }

        public function get_hotel_where_in_makkah($where) {
            $query = $this->M_hotel->get_where_in_makkah($where);
            return $query;
        }

        public function load_data()
        {
            $nm_hotel = $this->input->post('nm_hotel',TRUE);
            $jenis_hotel = $this->input->post('jenis_hotel',TRUE);
            $alamat = $this->input->post('alamat',TRUE);
            $jrk_mkkh_masjidl = $this->input->post('jrk_mkkh_masjidl',TRUE);
            $jrk_mdinh_msjidl = $this->input->post('jrk_mdinh_msjidl',TRUE);
            $bintang = $this->input->post('bintang',TRUE);
            // $biaya_sewa_kamar = $this->input->post('biaya_sewa_kamar',TRUE);
            // $biaya_sewa_hotel = $this->input->post('biaya_sewa_hotel',TRUE);
            // $biaya_upgrade_hotel = $this->input->post('biaya_upgrade_hotel',TRUE);
            // $biaya_upgrade_kamar = $this->input->post('biaya_upgrade_kamar',TRUE);
            // $active = '1';
            // $id_user = $this->currentUser->id;

			$cols = array();
			if (!empty($nm_hotel)) { $cols['nm_hotel'] = $nm_hotel; }
			if (!empty($jenis_hotel)) { $cols['jenis_hotel'] = $jenis_hotel; }
			if (!empty($alamat)) { $cols['alamat'] = $alamat; }
			if (!empty($jrk_mkkh_masjidl)) { $cols['jrk_mkkh_masjidl'] = $jrk_mkkh_masjidl; }
            if (!empty($jrk_mdinh_msjidl)) { $cols['jrk_mdinh_msjidl'] = $jrk_mdinh_msjidl; }
            if (!empty($bintang)) { $cols['bintang'] = $bintang; }
            // if (!empty($biaya_sewa_kamar)) { $cols['biaya_sewa_kamar'] = $biaya_sewa_kamar; }
            // if (!empty($biaya_sewa_hotel)) { $cols['biaya_sewa_hotel'] = $biaya_sewa_hotel; }
            // if (!empty($biaya_upgrade_hotel)) { $cols['biaya_upgrade_hotel'] = $biaya_upgrade_hotel; }
            // if (!empty($biaya_upgrade_kamar)) { $cols['biaya_upgrade_kamar'] = $biaya_upgrade_kamar; }
            // if (!empty($active)) { $cols['active'] = $active; }
            // if (!empty($id_user)) { $cols['id_user'] = $id_user; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1'";
            } else {
             $where = "active = '1'";
            }


	        $list = $this->M_hotel->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_hotel->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_hotel,
                    $r->jenis_hotel,
                    $r->alamat,
                    $r->jrk_mkkh_masjidl,
                    $r->jrk_mdinh_msjidl,
                    $r->bintang,
                    // $x="Rp." . number_format($r->biaya_sewa_kamar,0,',','.'),
                    // $y="Rp." . number_format($r->biaya_sewa_hotel,0,',','.'),
                    // $z="Rp." . number_format($r->biaya_upgrade_hotel,0,',','.'),
                    // $v="Rp." . number_format($r->biaya_upgrade_kamar,0,',','.'),
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Add Data Hotel';
			return response($this->load->view('add_hotel', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

			$data = array(
				'nm_hotel' => $this->input->post('nm_hotel',TRUE),
                'jenis_hotel' => $this->input->post('jenis_hotel',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jrk_mkkh_masjidl' => $this->input->post('jrk_mkkh_masjidl',TRUE),
                'jrk_mdinh_msjidl' => $this->input->post('jrk_mdinh_msjidl',TRUE),
                'bintang' => $this->input->post('bintang',TRUE),
                'biaya_sewa_kamar' => $this->input->post('biaya_sewa_kamar',TRUE),
                'biaya_sewa_hotel' => $this->input->post('biaya_sewa_hotel',TRUE),
                'biaya_upgrade_hotel' => $this->input->post('biaya_upgrade_hotel',TRUE),
                'biaya_upgrade_kamar' => $this->input->post('biaya_upgrade_kamar',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_hotel->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_hotel->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_hotel', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'nm_hotel' => $this->input->post('nm_hotel',TRUE),
                'jenis_hotel' => $this->input->post('jenis_hotel',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jrk_mkkh_masjidl' => $this->input->post('jrk_mkkh_masjidl',TRUE),
                'jrk_mdinh_msjidl' => $this->input->post('jrk_mdinh_msjidl',TRUE),
                'bintang' => $this->input->post('bintang',TRUE),
                'biaya_sewa_kamar' => $this->input->post('biaya_sewa_kamar',TRUE),
                'biaya_sewa_hotel' => $this->input->post('biaya_sewa_hotel',TRUE),
                'biaya_upgrade_hotel' => $this->input->post('biaya_upgrade_hotel',TRUE),
                'biaya_upgrade_kamar' => $this->input->post('biaya_upgrade_kamar',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_hotel->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_hotel->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('nm_hotel', 'nm hotel', 'trim|required');
            $this->form_validation->set_rules('jenis_hotel', 'jenis hotel', 'trim|required');
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
            $this->form_validation->set_rules('jrk_mkkh_masjidl', 'jrk mkkh masjidl', 'trim|required');
            $this->form_validation->set_rules('jrk_mdinh_msjidl', 'jrk mdinh msjidl', 'trim|required');
            $this->form_validation->set_rules('bintang', 'bintang', 'trim|required');
            // $this->form_validation->set_rules('biaya_sewa_kamar', 'biaya sewa kamar', 'trim|required|numeric');
            // $this->form_validation->set_rules('biaya_sewa_hotel', 'biaya sewa hotel', 'trim|required|numeric');
            // $this->form_validation->set_rules('biaya_upgrade_hotel', 'biaya upgrade hotel', 'trim|required|numeric');
            // $this->form_validation->set_rules('biaya_upgrade_kamar', 'biaya upgrade kamar', 'trim|required|numeric');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
