<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_hotel extends CI_Model 
	{
		

		var $table = 'hotel';

	    var $column_order = array('id','nm_hotel','jenis_hotel','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $column_search = array('id','nm_hotel','jenis_hotel','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $order = array('jenis_hotel' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}


		public function get_where_in($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in_madinah($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$this->db->where('jenis_hotel', 'Madinah');
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in_makkah($where) {
			$table = $this->table;
			$this->db->where_in('id', $where);
			$this->db->where('jenis_hotel', 'Makkah');
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
