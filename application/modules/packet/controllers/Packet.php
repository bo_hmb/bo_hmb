<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Packet extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Packet';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_packet');
            $this->load->model('M_workarea');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function get_where($where) {
            $query = $this->M_packet->get_where($where);
            return $query;
        }

        public function get_where_in($where) {
            $query = $this->M_packet->get_where_in($where);
            return $query;
        }

        public function index()
        {
            // Page components

            $this->data['pageTitle'] = 'Packet';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
            
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $nm_paket = $this->input->post('nm_paket',TRUE);
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);


			$cols = array();
            if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
			if (!empty($nm_paket)) { $cols['nm_paket'] = $nm_paket; }
			if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
			if (!empty($tgl_keberangkatan)) { $cols['tgl_keberangkatan'] = $tgl_keberangkatan; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
              $where = "paket.active = '1'";
            } else {
              $where = "paket.active = '1' AND paket.kd_cabang = '$this->kode_cabang'";
            }      


	        $list = $this->M_packet->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_packet->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';
                $pesawat = $this->M_packet->pesawat($r->id_pesawat);
                $hotel_makkah = $this->M_packet->hotel($r->id_hotel_makkah);
                $hotel_madinah = $this->M_packet->hotel($r->id_hotel_madinah);
                $hotel_umum = $this->M_packet->hotel($r->id_hotel_umum);
                $hotel_b4 = $this->M_packet->hotel($r->id_up_hotel_b4);
                $hotel_b5 = $this->M_packet->hotel($r->id_up_hotel_b5);


                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_cabang,
                    $r->nm_paket,
                    $r->nm_jadwal,
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan)),
                    $pesawat->nm_pesawat,
                    $hotel_makkah->hotel,
                    $hotel_madinah->hotel,
                    $hotel_umum->hotel,
                    $t="Rp." . number_format($r->hrg_paket,0,',','.'),
                    $hotel_b4->hotel,
                    $t="Rp." . number_format($r->biaya_up_hotel_b4,0,',','.'),
                    $hotel_b5->hotel,
                    $t="Rp." . number_format($r->biaya_up_hotel_b5,0,',','.'),
                    // $t="Rp." . number_format($r->biaya_up_double,0,',','.'),
                    // $t="Rp." . number_format($r->biaya_up_triple,0,',','.'),
                    // $t="Rp." . number_format($r->biaya_up_quad,0,',','.'),
                    // $t="Rp." . number_format($r->biaya_up_quint,0,',','.'),
                    
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Add Data Packet';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['workarea'] = $this->M_workarea->get()->result();     
            $data['pesawat'] = Modules::run('pesawat/get_where', array('pesawat.active' => '1'))->result();
            // $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            $data['hotel_mkh'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotel_mdnh'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Madinah'))->result();
            $data['hotel_umum'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Umum'))->result();
            $data['hotelb4'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '4','hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotelb5'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '5','hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotelb4_madinah'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '4','hotel.jenis_hotel' => 'Madinah'))->result();
            $data['hotelb5_madinah'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '5','hotel.jenis_hotel' => 'Madinah'))->result();

			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->ajaxRequest();

            if (count($this->input->post('id_pesawat')) > 0) {
                $arrpesawat = $this->input->post('id_pesawat');
                foreach($arrpesawat as $val)
                {
                    $pesawatarr = @$pesawatarr . $val. ",";
                }
            } else {
                $pesawatarr ='';
            }

            if (count($this->input->post('id_hotel_makkah')) > 0) {
                $arrhotel_makkah = $this->input->post('id_hotel_makkah');
                foreach($arrhotel_makkah as $val)
                {
                    $hotel_makkaharr = @$hotel_makkaharr . $val. ",";
                }
            } else {
                $hotel_makkaharr ='';
            }

            if (count($this->input->post('id_hotel_madinah')) > 0) {
                $arrhotel_madinah = $this->input->post('id_hotel_madinah');
                foreach($arrhotel_madinah as $val)
                {
                    $hotel_madinaharr = @$hotel_madinaharr . $val. ",";
                }
            } else {
                $hotel_madinaharr ='';
            }

            if (count($this->input->post('id_hotel_umum')) > 0) {
                $arrhotel_umum = $this->input->post('id_hotel_umum');
                foreach($arrhotel_umum as $val)
                {
                    $hotel_umumarr = @$hotel_umumarr . $val. ",";
                }
            } else {
                $hotel_umumarr ='';
            }
            
            
            if (count($this->input->post('id_up_hotel_b4')) > 0) {
                $arrup_hotel_b4 = $this->input->post('id_up_hotel_b4');
                foreach($arrup_hotel_b4 as $val)
                {
                    $up_hotel_b4arr = @$up_hotel_b4arr . $val. ",";
                }
            } else {
                $up_hotel_b4arr ='';
            }

            if (count($this->input->post('id_up_hotel_b5')) > 0) {
                $arrup_hotel_b5 = $this->input->post('id_up_hotel_b5');
                foreach($arrup_hotel_b5 as $val)
                {
                    $up_hotel_b5arr = @$up_hotel_b5arr . $val. ",";
                }
            } else {
                $up_hotel_b5arr ='';
            }

            if (count($this->input->post('id_up_hotel_b4_madinah')) > 0) {
                $arrup_hotel_b4_madinah = $this->input->post('id_up_hotel_b4_madinah');
                foreach($arrup_hotel_b4_madinah as $val)
                {
                    $up_hotel_b4arr_madinah = @$up_hotel_b4arr_madinah . $val. ",";
                }
            } else {
                $up_hotel_b4arr_madinah ='';
            }

            if (count($this->input->post('id_up_hotel_b5_madinah')) > 0) {
                $arrup_hotel_b5_madinah = $this->input->post('id_up_hotel_b5_madinah');
                foreach($arrup_hotel_b5_madinah as $val)
                {
                    $up_hotel_b5arr_madinah = @$up_hotel_b5arr_madinah . $val. ",";
                }
            } else {
                $up_hotel_b5arr_madinah ='';
            }

           $cek = count($this->input->post('kd_cabang'));

            for ($i=0; $i < $cek; $i++) {           
			 $data = array(
				'jenis_travel' => $this->input->post('jenis_travel',TRUE),
                'nm_paket' => $this->input->post('nm_paket',TRUE),
                'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'id_pesawat' => substr(trim($pesawatarr), 0, -1),
                'id_hotel_makkah' => substr(trim($hotel_makkaharr), 0, -1),
                'id_hotel_madinah' => substr(trim($hotel_madinaharr), 0, -1),
                'id_hotel_umum' => substr(trim($hotel_umumarr), 0, -1),
                'hrg_paket' => $this->input->post('hrg_paket',TRUE),
                'id_up_hotel_b4' => substr(trim($up_hotel_b4arr), 0, -1),
                'biaya_up_hotel_b4' => $this->input->post('biaya_up_hotel_b4',TRUE),
                'id_up_hotel_b5' => substr(trim($up_hotel_b5arr), 0, -1),
                'biaya_up_hotel_b5' => $this->input->post('biaya_up_hotel_b5',TRUE),
                'id_up_hotel_b4_madinah' => substr(trim($up_hotel_b4arr_madinah), 0, -1),
                'biaya_up_hotel_b4_madinah' => $this->input->post('biaya_up_hotel_b4_madinah',TRUE),
                'id_up_hotel_b5_madinah' => substr(trim($up_hotel_b5arr_madinah), 0, -1),
                'biaya_up_hotel_b5_madinah' => $this->input->post('biaya_up_hotel_b5_madinah',TRUE),
                // 'biaya_up_double' => $this->input->post('biaya_up_double',TRUE),
                // 'biaya_up_triple' => $this->input->post('biaya_up_triple',TRUE),
                // 'biaya_up_quad' => $this->input->post('biaya_up_quad',TRUE),
                // 'biaya_up_quint' => $this->input->post('biaya_up_quint',TRUE),
                'kd_cabang' => $this->input->post('kd_cabang',TRUE)[$i],
                'use' => '1',
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_packet->_insert($data);
            }
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {
            $id = $this->input->get('id');
            $data['main'] = $this->M_packet->get_where(array('paket.id' => $id))->row();
            $paket=$this->M_packet->get_where(array('paket.id' => $id))->row();
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['workarea'] = $this->M_workarea->get()->result();     
            $data['pesawat'] = Modules::run('pesawat/get_where', array('pesawat.active' => '1'))->result();
            // $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            $data['hotel_mkh'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotel_mdnh'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Madinah'))->result();
            $data['hotel_umum'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.jenis_hotel' => 'Umum'))->result();
            $data['hotelb4'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '4','hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotelb5'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '5','hotel.jenis_hotel' => 'Makkah'))->result();
            $data['hotelb4_madinah'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '4','hotel.jenis_hotel' => 'Madinah'))->result();
            $data['hotelb5_madinah'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1', 'hotel.bintang' => '5','hotel.jenis_hotel' => 'Madinah'))->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $arrpesawat = $this->input->post('id_pesawat');
            $c_arrpesawat = count($this->input->post('id_pesawat'));
            if ($c_arrpesawat===0) {
                $pesawatarr='';
            } else {
                foreach($arrpesawat as $val)
                {
                    $pesawatarr = @$pesawatarr . $val. ",";
                }
            }
            
            $arrhotel_makkah = $this->input->post('id_hotel_makkah');
            $c_arrhotel_makkah = count($this->input->post('id_hotel_makkah'));
            if ($c_arrhotel_makkah===0) {
                $hotel_makkaharr='';
            } else {
                foreach($arrhotel_makkah as $val)
                {
                    $hotel_makkaharr = @$hotel_makkaharr . $val. ",";
                }
            }

            $arrhotel_madinah = $this->input->post('id_hotel_madinah');
            $c_arrhotel_madinah = count($this->input->post('id_hotel_madinah'));
            if ($c_arrhotel_madinah===0) {
                $hotel_madinaharr='';
            } else {
                foreach($arrhotel_madinah as $val)
                {
                    $hotel_madinaharr = @$hotel_madinaharr . $val. ",";
                }
            }

            $arrhotel_umum = $this->input->post('id_hotel_umum');
            $c_arrhotel_umum = count($this->input->post('id_hotel_umum'));
            if ($c_arrhotel_umum===0) {
                $hotel_umumarr='';
            } else {
                foreach($arrhotel_umum as $val)
                {
                    $hotel_umumarr = @$hotel_umumarr . $val. ",";
                }
            }

            $arrup_hotel_b4 = $this->input->post('id_up_hotel_b4');
            $c_arrup_hotel_b4 = count($this->input->post('id_up_hotel_b4'));
            if ($c_arrup_hotel_b4===0) {
                $up_hotel_b4arr='';
            } else {
                foreach($arrup_hotel_b4 as $val)
                {
                    $up_hotel_b4arr = @$up_hotel_b4arr . $val. ",";
                }
            }

            $arrup_hotel_b5 = $this->input->post('id_up_hotel_b5');
            $c_arrup_hotel_b5 = count($this->input->post('id_up_hotel_b5'));
            if ($c_arrup_hotel_b5===0) {
                $up_hotel_b5arr='';
            } else {
                foreach($arrup_hotel_b5 as $val)
                {
                    $up_hotel_b5arr = @$up_hotel_b5arr . $val. ",";
                }
            }

            if (count($this->input->post('id_up_hotel_b4_madinah')) > 0) {
                $arrup_hotel_b4_madinah = $this->input->post('id_up_hotel_b4_madinah');
                foreach($arrup_hotel_b4_madinah as $val)
                {
                    $up_hotel_b4arr_madinah = @$up_hotel_b4arr_madinah . $val. ",";
                }
            } else {
                $up_hotel_b4arr_madinah ='';
            }

            if (count($this->input->post('id_up_hotel_b5_madinah')) > 0) {
                $arrup_hotel_b5_madinah = $this->input->post('id_up_hotel_b5_madinah');
                foreach($arrup_hotel_b5_madinah as $val)
                {
                    $up_hotel_b5arr_madinah = @$up_hotel_b5arr_madinah . $val. ",";
                }
            } else {
                $up_hotel_b5arr_madinah ='';
            }

            $data = array(
                'jenis_travel' => $this->input->post('jenis_travel',TRUE),
                'nm_paket' => $this->input->post('nm_paket',TRUE),
                'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'id_pesawat' => substr(trim($pesawatarr), 0, -1),
                'id_hotel_makkah' => substr(trim($hotel_makkaharr), 0, -1),
                'id_hotel_madinah' => substr(trim($hotel_madinaharr), 0, -1),
                'id_hotel_umum' => substr(trim($hotel_umumarr), 0, -1),
                'hrg_paket' => $this->input->post('hrg_paket',TRUE),
                'id_up_hotel_b4' => substr(trim($up_hotel_b4arr), 0, -1),
                'biaya_up_hotel_b4' => $this->input->post('biaya_up_hotel_b4',TRUE),
                'id_up_hotel_b5' => substr(trim($up_hotel_b5arr), 0, -1),
                'biaya_up_hotel_b5' => $this->input->post('biaya_up_hotel_b5',TRUE),
                'id_up_hotel_b4_madinah' => substr(trim($up_hotel_b4arr_madinah), 0, -1),
                'biaya_up_hotel_b4_madinah' => $this->input->post('biaya_up_hotel_b4_madinah',TRUE),
                'id_up_hotel_b5_madinah' => substr(trim($up_hotel_b5arr_madinah), 0, -1),
                'biaya_up_hotel_b5_madinah' => $this->input->post('biaya_up_hotel_b5_madinah',TRUE),
                // 'biaya_up_double' => $this->input->post('biaya_up_double',TRUE),
                // 'biaya_up_triple' => $this->input->post('biaya_up_triple',TRUE),
                // 'biaya_up_quad' => $this->input->post('biaya_up_quad',TRUE),
                // 'biaya_up_quint' => $this->input->post('biaya_up_quint',TRUE),
                'kd_cabang' => $this->input->post('kd_cabang',TRUE),
                'active' => '1',
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_packet->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_packet->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('jenis_travel', 'jenis travel', 'trim|required');
            

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
