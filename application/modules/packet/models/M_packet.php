<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_packet extends CI_Model 
	{
		

		var $table = 'paket';
		var $pesawat = 'pesawat';
		var $cabang = 'cabang';
		var $jadwal = 'jadwal';


	    var $column_order = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $column_search = array('id','nm_hotel','kota_lokasi','alamat','jarak_mekkah_masjidl','jarak_madinah_masjidl','bintang','biaya_sewa_kamar','biaya_sewa_hotel','biaya_upgrade_hotel','biaya_upgrade_kamar');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		// public function get_where($where) {
		// 	$table = $this->table;
		// 	$this->db->where($where);
  //  			$this->db->join('jadwal','jadwal.id=paket.id_jadwal');
		// 	$query=$this->db->get($table);
		// 	return $query;
		// }

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$this->db->select(' paket.id AS id,
								paket.jenis_travel AS jenis_travel,
								paket.id_jadwal AS id_jadwal,
								paket.nm_paket AS nm_paket,
								paket.id_pesawat AS id_pesawat,
								paket.id_perlengkapan AS id_perlengkapan,
								paket.id_perlengkapan_p AS id_perlengkapan_p,
								paket.id_hotel_makkah AS id_hotel_makkah,
								paket.id_hotel_madinah AS id_hotel_madinah,
								paket.id_hotel_umum AS id_hotel_umum,
								paket.hrg_paket AS hrg_paket,
								paket.id_hotel_b3 AS id_hotel_b3,
								paket.id_up_hotel_b4 AS id_up_hotel_b4,
								paket.biaya_up_hotel_b4 AS biaya_up_hotel_b4,
								paket.id_up_hotel_b5 AS id_up_hotel_b5,
								paket.biaya_up_hotel_b5 AS biaya_up_hotel_b5,

								paket.id_up_hotel_b4_madinah AS id_up_hotel_b4_madinah,
								paket.biaya_up_hotel_b4_madinah AS biaya_up_hotel_b4_madinah,
								paket.id_up_hotel_b5_madinah AS id_up_hotel_b5_madinah,
								paket.biaya_up_hotel_b5_madinah AS biaya_up_hotel_b5_madinah,
								
								paket.biaya_up_double AS biaya_up_double,
								paket.biaya_up_triple AS biaya_up_triple,
								paket.biaya_up_quad AS biaya_up_quad,
								paket.biaya_up_quint AS biaya_up_quint,
								paket.kd_cabang AS kd_cabang,
								paket.active AS active,
								paket.id_user AS id_user,
								paket.create_at AS create_at,
								paket.update_at AS update_at,
								jadwal.nm_jadwal AS nm_jadwal,
								jadwal.tgl_keberangkatan AS tgl_keberangkatan,
								datediff(tgl_keberangkatan, CURDATE()) as h_min,
								jadwal.lama_hari');
			$this->db->order_by('jadwal.tgl_keberangkatan');
			$this->db->join('jadwal','jadwal.id=paket.id_jadwal');
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_in($where) {
			$table = $this->table;
			$this->db->where_in($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function hotel($id)
		{
			$results = array();
			$query = $this->db->query('SELECT GROUP_CONCAT(nm_hotel) AS hotel FROM hotel WHERE id in('.$id.')');
			return $query->row();
		}

		public function pesawat($id)
		{
			$results = array();
			$query = $this->db->query('SELECT GROUP_CONCAT(nama_pesawat) AS nm_pesawat FROM pesawat WHERE id in('.$id.')');
			return $query->row();
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' paket.id,
								paket.nm_paket,
								paket.id_jadwal,
								jadwal.nm_jadwal,
								jadwal.tgl_keberangkatan,
								paket.id_pesawat,
								paket.id_hotel_makkah,
								paket.id_hotel_madinah,
								paket.id_hotel_umum,
								paket.hrg_paket,
								paket.id_up_hotel_b4,
								paket.biaya_up_hotel_b4,
								paket.id_up_hotel_b5,
								paket.biaya_up_hotel_b5,
								paket.biaya_up_double,
								paket.biaya_up_triple,
								paket.biaya_up_quad,
								paket.biaya_up_quint,
								paket.kd_cabang,
								cabang.nm_cabang,
								paket.active');

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	// $this->db->join($this->pesawat, ''.$this->pesawat.'.id = '.$this->table.'.id_pesawat');
	    	$this->db->join($this->jadwal, ''.$this->jadwal.'.id = '.$this->table.'.id_jadwal');
	    	$this->db->join($this->cabang, ''.$this->cabang.'.kd_cabang = '.$this->table.'.kd_cabang');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
