<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <label for="multi-append" class="control-label">Work Area</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="kd_cabang[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                                <?php
                                foreach ($workarea as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>
                                
                                <?php
                                }
                                ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">            
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control" name="jenis_travel">
                        <option value="-">Pilih Jenis Travel </option>
                        <option value="UMRAH">UMRAH</option>
                        <option value="HAJI KHUSUS KUOTA">HAJI KHUSUS KUOTA</option>
                        <option value="HAJI KHUSUS NON KUOTA">HAJI KHUSUS NON KUOTA</option>
                        <option value="TOUR/WISATA">TOUR/WISATA</option>                        
                    </select>
                    <label>Jenis Travel
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan Pilih Jenis Travel</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_paket" placeholder="Masukkan Nama Paket">
                    <label for="form_control_1">Nama Paket
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Paket</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <div class="fg-line">
                    <label>Nama Jadwal</label>
                    <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                        <option disabled selected></option>
                        <option></option>
                        <?php
                        foreach ($schedule as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                        <?php
                        }
                        ?>
                    </select>
                </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="multi-append" class="control-label">Pesawat</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_pesawat[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($pesawat as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nama_pesawat; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Hotel Makkkah Default</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel_makkah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel_mkh as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label for="multi-append" class="control-label">Hotel Madinah Default</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel_madinah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel_mdnh as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Hotel Umum</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel_umum[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel_umum as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="hrg_paket" placeholder="Masukkan Harga Paket">
                    <label for="form_control_1">Harga Paket
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Harga Paket</span>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Upgrade Hotel Makkah</span>

        <div class="row">
            <br>
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Upgrade Hotel *4</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_up_hotel_b4[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotelb4 as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->jenis_hotel; ?> - <?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_hotel_b4" placeholder="Masukkan Biaya Upgrade Hotel *4">
                    <label for="form_control_1">Biaya Upgrade Hotel *4
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *4</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Upgrade Hotel *5</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_up_hotel_b5[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotelb5 as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->jenis_hotel; ?> - <?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_hotel_b5" placeholder="Masukkan Biaya Upgrade Hotel *5">
                    <label for="form_control_1">Biaya Upgrade Hotel *5
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *5</span>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Upgrade Hotel Madinah</span>

        <div class="row">
            <br>
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Upgrade Hotel *4</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_up_hotel_b4_madinah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotelb4_madinah as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->jenis_hotel; ?> - <?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_hotel_b4_madinah" placeholder="Masukkan Biaya Upgrade Hotel *4">
                    <label for="form_control_1">Biaya Upgrade Hotel *4
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *4</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                <label for="multi-append" class="control-label">Upgrade Hotel *5</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_up_hotel_b5_madinah[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotelb5_madinah as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->jenis_hotel; ?> - <?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_hotel_b5_madinah" placeholder="Masukkan Biaya Upgrade Hotel *5">
                    <label for="form_control_1">Biaya Upgrade Hotel *5
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *5</span>
                </div>
            </div>
        </div>

        <!-- <span class="caption-subject font-dark bold uppercase">Upgrade Kamar</span>

        <div class="row">
            <br>
            <div class="col-md-6">                
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_double" placeholder="Masukkan Biaya Upgr. Kamar Double">
                    <label for="form_control_1">Biaya Upgr. Kamar Double
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgr. Kamar Double</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_triple" placeholder="Masukkan Biaya Upgr. Kamar Triple">
                    <label for="form_control_1">Biaya Upgr. Kamar Triple
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgr. Kamar Triple</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_quad" placeholder="Masukkan Biaya Upgr. Kamar Quad">
                    <label for="form_control_1">Biaya Upgr. Kamar Quad
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgr. Kamar Quad</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="biaya_up_quint" placeholder="Masukkan Biaya Upgr. Kamar Quint">
                    <label for="form_control_1">Biaya Upgr. Kamar Quint
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgr. Kamar Quint</span>
                </div>
            </div>
        </div> -->

    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>