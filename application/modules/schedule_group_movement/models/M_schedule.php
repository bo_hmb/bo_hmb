<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_schedule extends CI_Model 
	{

		var $table = 'jadwal';
		var $group_movement_detail = 'group_movement_detail';
		var $group_movement = 'group_movement';
		var $order = array('tgl_keberangkatan' => 'ASC'); 
		
		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$this->db->order_by('id');
			$query=$this->db->get('jadwal');
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}



		private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
		
		public function get_where_group_movement($where) {
			$table = $this->group_movement;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_group_movement_det($where) {
			$table = $this->group_movement_detail;
			$this->db->where($where);
			$this->db->order_by('dat','ASC');
			$query=$this->db->get($table);
			return $query;
		}

		public function hotel($id)
		{
			$results = array();
			$query = $this->db->query('SELECT GROUP_CONCAT(nm_hotel) AS hotel FROM hotel WHERE id in('.$id.')');
			return $query->row();
		}
	}
