<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Group_Movement.xls");
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>GROUP MOVEMENT</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 300PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h1 style="text-align: center; font-weight: bold;">GROUP MOVEMENT</h1>
        </th>
    </tr>
</table>
<br>
<table>
    <tr>
       <td >
        <table>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Tour Code</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->tour_code;?></h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Hotel Makkah</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $hotel_makkah->hotel;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Arrival Date</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo date('d-m-Y  H:m:d', strtotime($master->arrival_date));?></h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Hotel Madinah</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $hotel_madinah->hotel;;?></h5>
                </td>
                
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Derpature Date</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->departure_date;?></h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Land Transport</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->land_transport;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Carrier</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->carrier;?></h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Total Passengger</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->total_passengers;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Person In Charge</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->person_in_charge;?></h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Guide Request</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->guide_request;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Contac No.</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->contact_no;?></h5>
                </td>
                
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Email Address</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->email_addres;?></h5>
                </td>
                
            </tr>
        </table>
      </th>
    </tr>
</table>
<!-- N Kop -->
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 70px;">Date</th>
        <th style="width: 100px;">Route</th>
        <th style="width: 200px;">Program</th>
        <th style="width: 90px;">Time</th>
        <th style="width: 130px;">Transportation</th>
        <th style="width: 280px;">Hotel Name</th>
        <th style="width: 150px;">Check In</th>
        <th style="width: 150px;">Check Out</th>
        <th style="width: 150px;">Meals Program</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { 
        ?>

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->dat ?></td>
           <td style="border:1px solid #000;"><?php echo $row->route ?></td>
           <td style="border:1px solid #000;"><?php echo $row->program ?></td>
           <td style="border:1px solid #000;"><?php echo $row->tim?></td>
           <td style="border:1px solid #000;"><?php echo $row->transportation ?></td>
           <?php $hotel = $this->M_schedule->hotel($row->id_hotel); ?>
           <td style="border:1px solid #000;"><?php echo $hotel->hotel ?></td>
           <td style="border:1px solid #000;"><?php echo $row->check_in ?></td>
           <td style="border:1px solid #000;"><?php echo $row->check_out ?></td>
           <td style="border:1px solid #000;"><?php echo $row->meals_program ?></td>  
           <!-- <td style="border:1px solid #000;"><?php echo date('d-m-Y', strtotime($row->exp_pasport))?></td> -->
           <!-- <td style="border:1px solid #000;"></td>       -->
        </tr>   
    <?php $nomor++;}?> 
</table>

</body>
</html>