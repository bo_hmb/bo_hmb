<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Benefit_marketing extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

            $this->bulan = date('m');
            $this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Benefit Marketing';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_benefit_marketing');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Benefit Marketing';
            $this->data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();             
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $no_faktur = $this->input->post('no_faktur',TRUE);
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $nm_marketing = $this->input->post('nm_marketing',TRUE);
            $rekening = $this->input->post('rekening',TRUE);
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $tgl_payment = $this->input->post('tgl_payment',TRUE);
            $jml_benefit = $this->input->post('jml_benefit',TRUE);
            $status = $this->input->post('status',TRUE);

            $cols = array();
            if (!empty($no_faktur)) { $cols['benefit_marketing.no_faktur'] = $no_faktur; }
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($nm_marketing)) { $cols['nm_marketing'] = $nm_marketing; }
            if (!empty($rekening)) { $cols['rekening'] = $rekening; }
            if (!empty($no_pendaftaran)) { $cols['no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }
            if (!empty($tgl_payment)) { $cols['tgl_payment'] = $tgl_payment; }
            if (!empty($jml_benefit)) { $cols['jml_benefit'] = $jml_benefit; }
            if ($status != null) { $cols['benefit_marketing.status'] = $status; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "benefit_marketing.active = '1' AND pembayaran.status_verifikasi = '1' AND benefit_marketing.id_user <> 'x' AND pembayaran.active = '1'";
            } else {
             $where = "benefit_marketing.active = '1' AND pembayaran.status_verifikasi = '1' AND benefit_marketing.id_user <> 'x' AND pembayaran.active = '1'";
            }          


            $list = $this->M_benefit_marketing->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_benefit_marketing->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

          //<button type="button" class="btn btn-xs yellow btn-outline btn-payment tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-money"> Payment</i></button>
                                
                if (array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel))

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"> Edit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"> Delete &nbsp;&nbsp;&nbsp;&nbsp;</i></button>

                            </div>'; 
                else 
                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                               </div>';

                $cek_num_row=$this->M_benefit_marketing->get_where_cabang(array('kd_cabang' => $r->id_marketing ))->num_rows();
                if ($cek_num_row >= 1 ) {
                    $query_find_cabang=$this->M_benefit_marketing->get_where_cabang(array('kd_cabang' => $r->id_marketing ))->row();
                    $nm=$query_find_cabang->nm_cabang ; 
                    // var_dump($query_find);
                    } else{
                    $query_find=$this->M_benefit_marketing->get_where_marketing(array('id' => $r->id_marketing))->row();
                    $nm=$query_find->nm_marketing; 
                        // $str = (string)$r->id_marketing;
                        // $kdcabang = "$r->id_marketing";
                        // var_dump($query_find_cabang);
                        // print_r($query_find_cabang);
                    }

                if($r->status == '1')
                {
                    $status = 'Lunas';
                    $label = 'success';
                    $checkbox = '<span class="label label-sm label-'.$label.'">'.$status.'</span>';
                }else{
                    $checkbox = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="checkbox[]" type="checkbox" class="checkboxes" value="'.$r->id.'"/><span></span></label>';
                    $status = 'Belum Lunas';
                    $label = 'danger';
                }
                
                if ($r->tgl_keberangkatan == '0000-00-00') {
                    $x='Tabungan';
                } else {
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan));
                }


                $records["data"][] = array(
                    $no, 
                     $btn_action,
                    $r->no_faktur,
                    $r->nm_jadwal,
                    // $r->kd_cabang.' - '.$r->nm_marketing,
                    $nm,
                    // $r->rekening,                 
                    $r->nm_lengkap,
                    $x=date('d-m-Y', strtotime($r->tgl_payment)),
                    $t="Rp." . number_format($r->jml_benefit,0,',','.'),
                    $r->catatan,
                    $checkbox,
                    // '<span class="label label-sm label-'.$label.'">'.$status.'</span>'
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

    function update_multiple()
        {
            // db transaction 
            $this->db->trans_begin();

            // prospect detail
            $totdata = $this->input->post('checkbox');
            if($totdata != null) {
                // $this->load->model('prospect/m_prospect_detail');
                foreach ($totdata as $key => $value) {
                    $data = array(
                            'status' => '1',
                            'user_update' => $this->currentUser->id,
                    );

                    $query = $this->M_benefit_marketing->_update(array('id' => $this->input->post('checkbox')[$key]), $data);
                }
            }else { $queryDetail = TRUE; }

            // db transaction
            // $this->db->trans_complete();

            // Check if query was success
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                // generate an error... or use the log_message() function to log your error
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }else{
                $this->db->trans_commit();
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Update successfully');
            }

            return response($response, 'json');

        }

        public function load_detail()
        {

            $id = $this->input->get('id');

            $data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

            return response($this->load->view('detail', $data, TRUE), 'html');
        }

        public function load_add_form()
        {
            $data['title'] = 'Add Data Registrasi';

            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function add()
        {
            $this->ajaxRequest();
            $this->validateInput();
            $config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            
            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 
                $data = array(
                    'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                    'no_faktur' => $this->input->post('no_faktur',TRUE),
                    'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                    'id_benefit_marketing' => $this->input->post('id_benefit',TRUE),
                    'pph' => $this->input->post('pph',TRUE),
                    'harga_pph' => $this->input->post('harga_pph',TRUE),
                    'total_biaya' => $this->input->post('total_bayar',TRUE),
                    'path_bukti_bayar' => $image,
                    'catatan' => $this->input->post('catatan',TRUE),
                    'jenis' => 'Benefit Marketing',
                    'img_url' => 'http://103.101.225.215/android/agen/default_agen.jpg',
                    'active' => '1',
                    'id_user' => $this->currentUser->id,
                    );
            } else {
                $data = array(
                    'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                    'no_faktur' => $this->input->post('no_faktur',TRUE),
                    'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                    'id_benefit_marketing' => $this->input->post('id_benefit',TRUE),
                    'pph' => $this->input->post('pph',TRUE),
                    'harga_pph' => $this->input->post('harga_pph',TRUE),
                    'total_biaya' => $this->input->post('total_bayar',TRUE),
                    'catatan' => $this->input->post('catatan',TRUE),
                    'jenis' => 'Benefit Marketing',
                    'img_url' => 'http://103.101.225.215/android/agen/default_agen.jpg',
                    'active' => '1',
                    'id_user' => $this->currentUser->id,
                    );
            }
            //$query = $this->M_benefit_marketing->_insert($data);
            $query = $this->M_benefit_marketing->_insert_pengeluaran($data);


            $id = $this->input->post('id_benefit');
            $data2 = array(
                'status' => '1',
                );
            $query2 = $this->M_benefit_marketing->_update(array('id' => $id), $data2);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
            
            return response($response, 'json');
        }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            // $data['main'] = $this->M_benefit_marketing->get_where(array('id' => $id))->row();
            $data['main'] = $this->M_benefit_marketing->get_detail($id);
            $cek_num_row=$this->M_benefit_marketing->get_where_cabang(array('kd_cabang' => $data['main']->id_marketing ))->num_rows();
            if ($cek_num_row >= 1 ) {
                $query_find_cabang=$this->M_benefit_marketing->get_where_cabang(array('kd_cabang' => $data['main']->id_marketing ))->row();
                $data['nm']=$query_find_cabang->nm_cabang ;
                $data['rekening']=$query_find_cabang->rekening ; 
            } else{
                $query_find=$this->M_benefit_marketing->get_where_marketing(array('id' => $data['main']->id_marketing))->row();
                $data['nm']=$query_find->nm_marketing;
                $data['rekening']=$query_find->rekening; 
                    
            }
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function load_payment_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_benefit_marketing->get_detail($id);
            $cek = $this->M_benefit_marketing->get_where_2(array('MONTH(tgl_faktur)' => date('m'),'YEAR(tgl_faktur)' => date('Y'),'active'=>'1'))->num_rows();
            $data['faktur'] = 'BM-'.date('m').date("Y").sprintf('%03d',$cek+1);
            $data['id'] = $id;
            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                    'jml_benefit' => $this->input->post('jml_benefit',TRUE),
                    'user_update' => $this->currentUser->id,
            );

            $query = $this->M_benefit_marketing->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_benefit_marketing->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules 
            $this->form_validation->set_rules('jml_benefit', 'jml_benefit', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function load_export()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['agen'] = $this->M_benefit_marketing->get_agen_exp(array('active' => '1'))->result();   
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');
            $id_marketing = $this->input->post('id_marketing');
            $star = $this->input->post('star_payment');
            $until = $this->input->post('until_payment');
            $status_lunas = $this->input->post('status_lunas');
            //$data['main'] = $id;
            $data['main'] = $this->M_benefit_marketing->get_jadwal_exp(array('id' => $id ))->row(); 
            if ($status_lunas == '0') {
                if ($id_marketing == '0') {
                    $data ['detail'] = $this->M_benefit_marketing->get_export('WHERE jadwal.id="'.$id.'" AND benefit_marketing.`status`="0"')->result();                
                    $data ['sum'] = $this->M_benefit_marketing->get_sum_all_export($id,$status_lunas)->result();                
                } else {
                    $data ['detail'] = $this->M_benefit_marketing->get_export('WHERE jadwal.id="'.$id.'" AND benefit_marketing.id_marketing="'.$id_marketing.'" AND benefit_marketing.`status`="0" ')->result();
                    $data ['sum'] = $this->M_benefit_marketing->get_sum_export($id,$id_marketing,$status_lunas)->result();                                            
                }
                $html = $this->load->view('benefit_marketing/print_export', $data, TRUE);
            } else {
                if ($id_marketing == '0') {
                    $data ['detail'] = $this->M_benefit_marketing->get_export('WHERE jadwal.id="'.$id.'" AND benefit_marketing.`status`="1"')->result();                
                    $data ['sum'] = $this->M_benefit_marketing->get_sum_all_export($id,$status_lunas)->result();                
                } else {
                    $data ['detail'] = $this->M_benefit_marketing->get_export('WHERE jadwal.id="'.$id.'" AND benefit_marketing.id_marketing="'.$id_marketing.'" AND benefit_marketing.`status`="1" ')->result();
                    $data ['sum'] = $this->M_benefit_marketing->get_sum_export($id,$id_marketing,$status_lunas)->result();                                            
                }
                $html = $this->load->view('benefit_marketing/print_export_lunas', $data, TRUE);
            }
            
            //$data ['total'] = $this->M_benefit_marketing->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                        
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');
            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function load_export_between()
        {
            $data['title'] = '';
            return response($this->load->view('export_between', $data, TRUE), 'html');
        }

        public function export_between()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $star = $this->input->post('star_payment');
            $until = $this->input->post('until_payment');
            $jns_payent = $this->input->post('jns_payent');
            $data['star'] = $this->input->post('star_payment');
            $data['until'] = $this->input->post('until_payment');
            if ($jns_payent == '0') {
                $jenis_payment = 'pendaftar.jenis="Tabungan" AND';
                $jenis_payment2 = 'jenis="Tabungan" AND';
            } else if ($jns_payent == '1') {
                $jenis_payment = 'pendaftar.jenis="Reguler" AND';
                $jenis_payment2 = 'jenis="Reguler" AND';
            } else {
                $jenis_payment = '';
                $jenis_payment2 = '';
            }
            //$data['main'] = $id;
            // $data['main'] = $this->M_benefit_marketing->get_jadwal_exp(array('id' => $id ))->row(); 
            $data['detail'] = $this->M_benefit_marketing->get_export_between($star,$until,$jenis_payment)->result();                
            $data['sum'] = $this->M_benefit_marketing->get_sum_between($star,$until,$jenis_payment2)->result();                
            
            
            //$data ['total'] = $this->M_benefit_marketing->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('benefit_marketing/print_export_between', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                        
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');
            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }


    }
?>
