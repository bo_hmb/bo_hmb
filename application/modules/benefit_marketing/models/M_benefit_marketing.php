<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_benefit_marketing extends CI_Model 
	{
		

		var $table = 'benefit_marketing';
		var $table2 = 'pengeluaran_benefit_marketing';
		var $marketing = 'marketing';
		var $cabang = 'cabang';
		var $pendaftar = 'pendaftar';
		var $formulir = 'formulir';
		var $pengeluaran = 'pengeluaran';
		var $pembayaran = 'pembayaran';
		var $paket = 'paket';
		var $jadwal = 'jadwal';
		
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_marketing($where) {
			$table = $this->marketing;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_2($where) {
			$table2 = $this->pengeluaran;
			$this->db->where($where);
			$query=$this->db->get($table2);
			return $query;
		}

		public function get_detail($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										benefit_marketing.id AS id,
										benefit_marketing.id_marketing AS id_marketing,
										benefit_marketing.no_faktur AS no_faktur,
										benefit_marketing.id_pendaftaran AS id_pendaftaran,
										benefit_marketing.tgl_payment AS tgl_payment,
										benefit_marketing.jml_benefit AS jml_benefit,
										pendaftar.no_pendaftaran AS no_pendaftaran,
										formulir.nm_lengkap AS nm_lengkap,
										benefit_marketing.status AS status,
										benefit_marketing.active AS active,
										benefit_marketing.catatan AS catatan,
										paket.nm_paket AS nm_paket,
										jadwal.id AS id_jadwal,
										jadwal.nm_jadwal AS nm_jadwal,
										jadwal.tgl_keberangkatan AS tgl_keberangkatan,
										benefit_marketing.active
										FROM
										benefit_marketing
										INNER JOIN pendaftar ON benefit_marketing.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE benefit_marketing.id="'.$id.'"');
	        return $query->row();
		}

		public function _insert($data) {
			$table2 = $this->table2;
			$insert = $this->db->insert($table2, $data);
			return $insert;
		}

		public function _insert_benefit($data) {
			$table3 = $this->benefit_marketing;
			$insert = $this->db->insert($table3, $data);
			return $insert;
		}

		public function _insert_pengeluaran($data) {
			$table4 = $this->pengeluaran;
			$insert = $this->db->insert($table4, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' 
								benefit_marketing.id AS id,
								benefit_marketing.id_marketing AS id_marketing,
								benefit_marketing.no_faktur AS no_faktur,
								benefit_marketing.id_pendaftaran AS id_pendaftaran,
								benefit_marketing.tgl_payment AS tgl_payment,
								benefit_marketing.jml_benefit AS jml_benefit,
								pendaftar.no_pendaftaran AS no_pendaftaran,
								formulir.nm_lengkap AS nm_lengkap,
								benefit_marketing.status AS status,
								benefit_marketing.active AS active,
								benefit_marketing.catatan AS catatan,
								pembayaran.status_verifikasi AS status_verifikasi,
								paket.nm_paket AS nm_paket,
								jadwal.id AS id_jadwal,
								jadwal.nm_jadwal AS nm_jadwal,
								jadwal.tgl_keberangkatan AS tgl_keberangkatan
								');
	    	$this->db->where($where);
	        $this->db->from($this->table);
	        $this->db->group_by('benefit_marketing.id');

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	// $this->db->join($this->marketing, ''.$this->table.'.id_marketing = '.$this->marketing.'.id');
	    	// $this->db->join($this->cabang, ''.$this->marketing.'.kd_cabang = '.$this->cabang.'.kd_cabang');
	    	$this->db->join($this->pendaftar, ''.$this->table.'.id_pendaftaran = '.$this->pendaftar.'.id');
	    	$this->db->join($this->formulir, ''.$this->pendaftar.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->pembayaran, ''.$this->table.'.no_faktur = '.$this->pembayaran.'.no_faktur');
	    	$this->db->join($this->paket, ''.$this->pendaftar.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {
	    	$this->db->where($where);
	        $this->db->from('benefit_marketing');
			$this->db->join('pembayaran','benefit_marketing.no_faktur=pembayaran.no_faktur');	        
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_export($where) {
			$query = $this->db->query(' SELECT
										benefit_marketing.id,
										benefit_marketing.id_marketing,
										benefit_marketing.no_faktur,
										benefit_marketing.id_pendaftaran,
										benefit_marketing.tgl_payment,
										benefit_marketing.jml_benefit,
										pendaftar.no_pendaftaran,
										pendaftar.jenis,
										formulir.nm_lengkap,
										benefit_marketing.`status`,
										benefit_marketing.active,
										benefit_marketing.catatan,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.id AS id_jadwal,
										jadwal.tgl_keberangkatan
										FROM
										benefit_marketing
										INNER JOIN pendaftar ON benefit_marketing.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_marketing.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id '.$where.' 
										AND benefit_marketing.active="1" AND pembayaran.status_verifikasi="1" AND benefit_marketing.id_user <> "x" AND pembayaran.active = "1"
										GROUP BY benefit_marketing.id
										ORDER by benefit_marketing.id ASC');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_jadwal_exp($where) {
			$table = $this->jadwal;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_agen_exp($where) {
			$table = $this->marketing;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_sum_all_export($id_jadwal,$status) {
			$query = $this->db->query(' SELECT id_marketing,sum(jml_benefit) AS benefit_belum_lunas,catatan FROM tv_benefit_marketing WHERE id_jadwal="'.$id_jadwal.'" 
										AND status="'.$status.'" GROUP BY id_marketing,catatan');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_export($id_jadwal,$id_marketing,$status) {
			$query = $this->db->query(' SELECT id_marketing,sum(jml_benefit) AS benefit_belum_lunas,catatan FROM tv_benefit_marketing WHERE id_jadwal="'.$id_jadwal.'" 
										AND id_marketing = "'.$id_marketing.'" AND status="'.$status.'" GROUP BY id_marketing,catatan');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_export_between($star,$until,$jenis_payment) {
			$query = $this->db->query(' SELECT
										benefit_marketing.id,
										benefit_marketing.id_marketing,
										benefit_marketing.no_faktur,
										benefit_marketing.id_pendaftaran,
										benefit_marketing.tgl_payment,
										benefit_marketing.jml_benefit,
										pendaftar.no_pendaftaran,
										pendaftar.jenis,
										formulir.nm_lengkap,
										benefit_marketing.`status`,
										benefit_marketing.active,
										benefit_marketing.catatan,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.id AS id_jadwal,
										jadwal.tgl_keberangkatan
										FROM
										benefit_marketing
										INNER JOIN pendaftar ON benefit_marketing.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_marketing.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id 
										WHERE '.$jenis_payment.'
										benefit_marketing.active="1" AND pembayaran.status_verifikasi="1" AND benefit_marketing.id_user <> "x" AND benefit_marketing.id_marketing <> 3 AND pembayaran.active = "1" AND
										pembayaran.tgl_bayar BETWEEN "'.$star.'" AND "'.$until.'"
										GROUP BY benefit_marketing.id
										ORDER by benefit_marketing.id ASC');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_schedule() {
			$results = array();
	        $query = $this->db->query(' SELECT
										jadwal.id,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										jadwal.lama_hari,
										jadwal.active,
										paket.nm_paket
										FROM
										jadwal
										INNER JOIN paket ON jadwal.id = paket.id_jadwal
										WHERE
										jadwal.active = 1 AND paket.kd_cabang="MKS"');
	        return $query;
		}

		public function get_export_between_id_jadwal($star,$until,$id_jadwal) {
			$query = $this->db->query(' SELECT
										benefit_marketing.id,
										benefit_marketing.id_marketing,
										benefit_marketing.no_faktur,
										marketing.nik,
										marketing.nm_marketing,
										marketing.rekening,
										marketing.kd_cabang,
										cabang.nm_cabang,
										benefit_marketing.id_pendaftaran,
										benefit_marketing.tgl_payment,
										benefit_marketing.jml_benefit,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										benefit_marketing.`status`,
										benefit_marketing.active,
										benefit_marketing.catatan,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.id AS id_jadwal,
										jadwal.tgl_keberangkatan
										FROM
										benefit_marketing
										INNER JOIN marketing ON benefit_marketing.id_marketing = marketing.id
										INNER JOIN cabang ON marketing.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_marketing.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_marketing.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id 
										WHERE jadwal.id_jadwal="'.$id_jadwal.'" AND benefit_marketing.active="1" AND pembayaran.status_verifikasi="1" AND benefit_marketing.id_user <> "x" AND benefit_marketing.id_marketing <> 3 AND pembayaran.active = "1" AND
										pembayaran.tgl_bayar BETWEEN "'.$star.'" AND "'.$until.'"
										GROUP BY benefit_marketing.id
										ORDER by benefit_marketing.id ASC');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_between($star,$until,$jenis_payment) {
			$query = $this->db->query(' SELECT id_marketing,sum(jml_benefit) AS benefit_belum_lunas FROM tv_benefit_marketing WHERE '.$jenis_payment.' status="0" AND id_marketing <> 3 AND tgl_payment BETWEEN "'.$star.'" AND "'.$until.'" GROUP BY id_marketing');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}
	}
