<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Benefit Marketing</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Benefit Marketing</span>
                        </div>
                        <div class="actions">
                            <div class="btn-group">
                                <a class="btn red btn-outline btn-circle " href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export" href="javascript:;"> Base On Schedule </a>
                                    </li>
                                    <li>
                                        <a class="btn-between" href="javascript:;"> Between </a>
                                    </li>                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">  
                            <form action="#" id="form-cb" method="POST">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="table-actions-wrapper">
                                        <?php if (array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) { ?>
                                        <button class="btn green table-group-action-submit submit"><i class="fa fa-check"></i> Update </button>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>                          
                                <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                    <thead>
                                       <tr role="row" class="heading">
                                            <th width="5%">No</th>
                                            <th width="1%">Aksi</th>
                                            <th width="10%">No. Faktur</th>
                                            <th width="10%">Tggl. Keberangkatan</th>
                                            <th width="10%">Cabang / Marketing</th>
                                            <!-- <th width="10%">Rekening</th> -->
                                            <!-- <th width="10%">Cabang</th> -->
                                            <th width="30%">Nama Pendaftar</th>
                                            <th width="5%">Tanggal Payment</th>
                                            <th width="30%">Jumlah Benefit</th>
                                            <th width="30%">Catatan</th>
                                            <th width="2%">
                                                <!-- <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                                    <span></span>
                                                </label> -->Status
                                            </th>
                                            <!-- <th width="55%">Status</th> -->
                                        </tr>
                                        <tr role="row" class="filter">
                                            <td> </td>
                                            <td>
                                                <div class="margin-bottom-5">
                                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                        <i class="fa fa-search"></i></button>
                                                </div>
                                                <button class="btn btn-sm red btn-outline filter-cancel">
                                                    <i class="fa fa-times"></i></button>
                                            </td>
                                            <td><input type="text" class="form-control form-filter input-sm" name="no_faktur"> </td>
                                            <td>
                                                <select name="nm_jadwal" class="form-control form-filter input-sm jadwal">
                                                    <option></option>                                                
                                                    <option value="0000-00-00">Tabungan</option>                                                
                                                    <?php
                                                    foreach ($schedule as $row) { ?> 
                                                    <option value="<?php echo $row->nm_jadwal; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td><!-- <input type="text" class="form-control form-filter input-sm" name="nm_marketing"> --> </td>
                                            <!-- <td><input type="text" class="form-control form-filter input-sm" name="rekening"> </td> -->
                                            <!-- <td><input type="text" class="form-control form-filter input-sm" name="nm_cabang"> </td> -->
                                            <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                            <td><input type="date" class="form-control form-filter input-sm" name="tgl_payment"> </td>
                                            <td><input type="text" class="form-control form-filter input-sm" name="jml_benefit"> </td>
                                            <td><input type="text" class="form-control form-filter input-sm" name="catatan"> </td>
                                            <td>
                                                <select name="status" class="form-control form-filter input-sm">
                                                    <option disabled selected></option>
                                                    <option value="0">Belum Lunas</option>
                                                    <option value="1">Lunas</option>
                                                </select> 
                                            </td>
                                            
                                            

                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>