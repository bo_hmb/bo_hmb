<form action="#" id="form-create" role="form">
    <div class="form-body">
        <span class="caption-subject font-dark bold uppercase">Data Benefit</span>
        <input type="hidden" class="form-control" name="id_benefit" value="<?php echo $id ?>">
        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->nm_marketing ?>">
                    <input type="hidden" name="id_jadwal" value="<?php echo $main->id_jadwal?>">
                    <label for="form_control_1">Nama Marketing
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly value="<?php echo $main->rekening ?>">
                        <label class="control-label">Rekening
                        </label>
                        
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->nm_cabang ?>">
                    <label for="form_control_1">Cabang
                    </label>
                </div>
            </div>
        </div>
        <span class="caption-subject font-dark bold uppercase">Data Pendaftar</span>
        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->no_pendaftaran?>">
                    <label for="form_control_1">No. Pendaftaran
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->nm_lengkap?>">
                    <label for="form_control_1">Nama Pendaftar
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker">
                        <input type="text" class="form-control" readonly value="<?php echo $main->tgl_payment?>">
                        <label class="control-label">Tanggal Payment
                            
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control jml_benefit" readonly value="<?php echo "Rp." . number_format($main->jml_benefit,0,',','.')?>">
                    <input type="hidden" id="jml_benefit" value="<?php echo ($main->jml_benefit) ?>">
                    <label for="form_control_1">Jumlah Benefit
                    </label>
                </div>
            </div>

            <div class="col-md-4">
               <!--  <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="pph" placeholder="Masukkan PPh">
                    <label for="form_control_1">PPh
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan PPh</span>
                </div> -->
                <div class="form-group">
                    <br>
                <div class="col-md-4">
                    <label class="mt-checkbox mt-checkbox-outline" for="1">
                        <input id="1" type="checkbox" value="2" name="pph" class="input-small pph"/>PPh
                        <span></span>
                    </label>
                </div>        
            </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control harga-pph" name="harga_pph" placeholder="Biaya PPh" readonly>
                    <label for="form_control_1">Biaya PPh
                    </label>
                </div>
            </div>
        </div>


        <span class="caption-subject font-dark bold uppercase">Masukkan Data Pembayaran</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_faktur" placeholder="No. Faktur Otomatis" readonly value="<?php echo $faktur ?>">
                    <label for="form_control_1">No. Faktur             
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" name="tgl_faktur" class="form-control" readonly value="<?php echo(date('Y-m-d')) ?>">
                        <label class="control-label">Tanggal Pembayaran
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" disabled>
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pembayaran </span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input"> 
                    <input type="text" class="form-control jml_bayar" name="total_bayar" placeholder="Masukkan Jumlah Bayar">
                    <label for="form_control_1">Jumlah Bayar
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Jumlah Bayar</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="catatan" placeholder="Masukkan Catatan">
                    <label for="form_control_1">Catatan    
                    <span class="required">*</span>                    
                    </label>
                    <span class="help-block">Masukkan Catatan</span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                    <label>Bukti Bayar
                    </label>
                    <span class="help-block">Masukkan Bukti Bayar</span>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>