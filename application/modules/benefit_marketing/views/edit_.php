<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <span class="caption-subject font-dark bold uppercase">Data Benefit</span>
        <input type="hidden" name="id" value="<?php echo $main->id?>">
        <input type="hidden" name="id_jadwal" value="<?php echo $main->id_jadwal?>">
        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $nm ?>">
                    <label for="form_control_1">Nama Marketing/Agen
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group" data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control" readonly value="<?php echo $rekening ?>">
                        <label class="control-label">Rekening
                        </label>
                        
                    </div>
                </div>
            </div>

        </div>
        <span class="caption-subject font-dark bold uppercase">Data Pendaftar</span>
        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->no_pendaftaran?>">
                    <label for="form_control_1">No. Pendaftaran
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" readonly value="<?php echo $main->nm_lengkap?>">
                    <label for="form_control_1">Nama Pendaftar
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker">
                        <input type="text" class="form-control" readonly value="<?php echo $main->tgl_payment?>">
                        <label class="control-label">Tanggal Payment
                            
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" style="display: none">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Data Pembayaran</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="no_faktur" placeholder="No. Faktur Otomatis" readonly value="<?php echo $main->no_faktur ?>">
                    <label for="form_control_1">No. Faktur             
                    </label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" name="tgl_faktur" class="form-control" readonly value="<?php echo(date('Y-m-d')) ?>">
                        <label class="control-label">Tanggal Pembayaran
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" disabled style="display: none">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Tanggal Pembayaran </span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input"> 
                    <input type="text" class="form-control money" name="jml_benefit" value="<?php echo $main->jml_benefit ?>">
                    <label for="form_control_1" style="color:red">Jumlah Benefit
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Jumlah Benefit</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="catatan" value="<?php echo $main->catatan ?>" readonly>
                    <label for="form_control_1">Catatan    
                    <span class="required">*</span>                    
                    </label>
                    <span class="help-block">Catatan</span>
                </div>
            </div>

        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>