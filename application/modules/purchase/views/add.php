            <form role="form" action="#" class="form-horizontal" id="form-create">
                <div class="form-group">
                    <label class="col-md-2 control-label">No Faktur</label>
                    <div class="col-md-6">
                        <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Toko</label>
                    <div class="col-md-6">
                        <input type="text" name="nm_toko" placeholder="Nama Toko" class="form-control" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Beli</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                            <input type="text" name="tgl_beli" class="form-control" placeholder="Tanggal" readonly="">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nama Barang</th>
                                    <th>Harga Beli</th>
                                    <th>Harga Jual</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" value="0" readonly="">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     