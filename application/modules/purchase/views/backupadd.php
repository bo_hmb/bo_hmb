        <div class="portlet-body form">
            <form role="form" action="#" id="form-create" class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-2 control-label">No Faktur</label>
                    <div class="col-md-6">
                        <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Toko</label>
                    <div class="col-md-6">
                        <input type="text" name="nm_toko" placeholder="Nama Toko" class="form-control" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Beli</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                            <input type="text" name="tgl_beli" class="form-control" placeholder="Tanggal">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-md-12">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th>Nama Barang</th>
                                    <th>Harga</th>
                                    <th>qty</th>
                                    <th>Subtotal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <!--elemet sebagai target append-->
                            <tbody id="itemlist">
                                <tr>
                                    <td id="brg_id">
                                        <select name="nm_barang[0]" class="form-control barang">
                                            <option></option>
                                            <?php foreach ($barang as $row) { ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_barang; ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td><input name="harga[0]" class="form-control" /></td>
                                    <td><input name="qty[0]" class="form-control" /></td>
                                    <td><input name="subtotal[0]" class="form-control" /></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>
                                        <button class="btn btn-small btn-default" onclick="additem(); return false"><i class="icon-plus"></i></button>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-8 control-label">Total Harga</label>
                    <div class="col-md-4">
                        <input type="text" placeholder="Harga" class="form-control money" /> 
                    </div>
                </div>
                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                    
           
        <script>
            var i = 1;
            function additem() {
//                menentukan target append
                var itemlist = document.getElementById('itemlist');
                
//                membuat element
                var row = document.createElement('tr');
                var v_nm_barang = document.createElement('td');
                var v_harga = document.createElement('td');
                var v_qty = document.createElement('td');
                var v_subtotal = document.createElement('td');
                var aksi = document.createElement('td');

//                meng append element
                itemlist.appendChild(row);
                row.appendChild(v_nm_barang);
                row.appendChild(v_harga);
                row.appendChild(v_qty);
                row.appendChild(v_subtotal);
                row.appendChild(aksi);

                
                //Create array of options to be added
                var array = <?php foreach ($barang as $row) { 
                                $result[] = [array(
                                  'id' => $row->id,
                                  'nama_barang' => $row->nm_barang,
                                  'harga_beli' => $row->harga_beli,
                                  'harga_jual' => $row->harga_jual,
                                )];
                            } 
                            echo json_encode($result);

                            ?>;


            

//                membuat element input
                var nm_barang = document.createElement('select');
                nm_barang.setAttribute('name', 'nm_barang[' + i + ']');
                nm_barang.setAttribute('class', 'form-control barang"' + i + '"');

                //Create and append the options
                for (var i = 0; i < array.length; i++) {
                    var option = document.createElement("option");
                    option.setAttribute("value", array[i]);
                    option.text = array[i];
                    nm_barang.appendChild(option);
                }


                var harga = document.createElement('input');
                harga.setAttribute('name', 'harga[' + i + ']');
                harga.setAttribute('class', 'form-control');

                var qty = document.createElement('input');
                qty.setAttribute('name', 'qty[' + i + ']');
                qty.setAttribute('class', 'form-control');

                var subtotal = document.createElement('input');
                subtotal.setAttribute('name', 'subtotal[' + i + ']');
                subtotal.setAttribute('class', 'form-control');

                var hapus = document.createElement('span');

//                meng append element input
                v_nm_barang.appendChild(nm_barang);
                v_harga.appendChild(harga);
                v_qty.appendChild(qty);
                v_subtotal.appendChild(subtotal);
                aksi.appendChild(hapus);

                hapus.innerHTML = '<button class="btn btn-small btn-default"><i class="icon-trash"></i></button>';
//                membuat aksi delete element
                hapus.onclick = function () {
                    row.parentNode.removeChild(row);
                };

                i++;
            }
        </script>