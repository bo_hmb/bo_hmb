            <form role="form" action="#" class="form-horizontal" id="form-edit">
                <div class="form-group">
                    <label class="col-md-2 control-label">No Faktur</label>
                    <div class="col-md-6">
                        <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" value="<?php echo $main->no_faktur ?>" readonly/> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Toko</label>
                    <div class="col-md-6">
                        <input type="text" name="nm_toko" placeholder="Nama Toko" class="form-control" value="<?php echo $main->nm_toko ?>"/> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Beli</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                            <input type="text" name="tgl_beli" class="form-control" placeholder="Tanggal" readonly="" value="<?php echo $main->tgl_beli ?>">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nama Barang</th>
                                    <th>Harga Beli</th>
                                    <th>Harga Jual</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                                <?php 
                                foreach ($barang as $row) { ?>

                                    <tr class="baris form-create-barang" id="form-create-barang" ">    
                                        <td align="center">
                                            <select id="barang" class="form-control barang" name="nm_barang[]" readonly>
                                                <option value="<?php echo $row->id ?>"><?php echo $row->nm_barang ?></option>
                                            </select>
                                        </td>
                                        <td align="center">
                                            <input id="harga_beli" class="form-control harga_beli money" name="harga_beli[]" type="text" value="<?php echo $row->harga_beli ?>" readonly>
                                        </td>
                                        <td align="center">
                                            <input id="harga_jual" class="form-control harga_jual money" name="harga_jual[]" type="text" value="<?php echo $row->harga_jual ?>" readonly>
                                        </td>
                                        <td align="center">
                                            <input id="jml" name="jml[]" class="form-control jml-edit money" type="text" placeholder="0" value="<?php echo $row->jml ?>" readonly>
                                        </td>
                                        <td align="center">
                                            <input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" readonly value="<?php echo $row->sub_harga ?>" readonly>
                                        </td>
                                    </td>
                                    <td>
                                        <!-- <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>
                                        <input id="rows" name="rows[]" value="" type="hidden"> -->
                                    </td>
                                        
                                    <?php } ?>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" value="<?php echo $main->total_bayar ?>" readonly="">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     