<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_benefit_cabang extends CI_Model 
	{
		

		var $table = 'benefit_cabang';
		var $table2 = 'pengeluaran_benefit_cabang';
		var $marketing = 'marketing';
		var $cabang = 'cabang';
		var $pendaftar = 'pendaftar';
		var $formulir = 'formulir';
		var $pengeluaran = 'pengeluaran';
		var $pembayaran = 'pembayaran';
		var $paket = 'paket';
		var $jadwal = 'jadwal';


	    var $column_order = array('');
	    var $column_search = array('');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_2($where) {
			$table2 = $this->pengeluaran;
			$this->db->where($where);
			$query=$this->db->get($table2);
			return $query;
		}

		public function get_detail($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										benefit_cabang.id,
										benefit_cabang.kd_cabang,
										cabang.nm_cabang,
										cabang.rekening,
										benefit_cabang.id_pendaftaran,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										benefit_cabang.tgl_payment,
										benefit_cabang.jml_benefit,
										benefit_cabang.status,
										benefit_cabang.active,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.tgl_keberangkatan
										FROM
										benefit_cabang
										INNER JOIN cabang ON benefit_cabang.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_cabang.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_cabang.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE benefit_cabang.id="'.$id.'"');
	        return $query->row();
		}

		public function _insert($data) {
			$table2 = $this->table2;
			$insert = $this->db->insert($table2, $data);
			return $insert;
		}

		public function _insert_benefit($data) {
			$table3 = $this->benefit_marketing;
			$insert = $this->db->insert($table3, $data);
			return $insert;
		}

		public function _insert_pengeluaran($data) {
			$table4 = $this->pengeluaran;
			$insert = $this->db->insert($table4, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' benefit_cabang.id,
								benefit_cabang.kd_cabang,
								cabang.nm_cabang,
								cabang.rekening,
								benefit_cabang.id_pendaftaran,
								pendaftar.no_pendaftaran,
								formulir.nm_lengkap,
								benefit_cabang.tgl_payment,
								benefit_cabang.jml_benefit,
								benefit_cabang.status,
								benefit_cabang.active,
								pembayaran.status_verifikasi,
								paket.nm_paket,
								jadwal.tgl_keberangkatan');
	    	$this->db->where($where);
	        $this->db->from($this->table);
	        $this->db->group_by('benefit_cabang.id');
	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->cabang, ''.$this->table.'.kd_cabang = '.$this->cabang.'.kd_cabang');
	    	$this->db->join($this->pendaftar, ''.$this->table.'.id_pendaftaran = '.$this->pendaftar.'.id');
	    	$this->db->join($this->formulir, ''.$this->pendaftar.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->pembayaran, ''.$this->table.'.no_faktur = '.$this->pembayaran.'.no_faktur');
	    	$this->db->join($this->paket, ''.$this->pendaftar.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from('benefit_cabang');
			$this->db->join('pembayaran','benefit_cabang.no_faktur=pembayaran.no_faktur');	        
	        // $this->db->group_by('benefit_cabang.id');
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_cabang($where) {
			$table = $this->cabang;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_jadwal_exp($where) {
			$table = $this->jadwal;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_export($where) {
			$query = $this->db->query(' SELECT
										benefit_cabang.id,
										benefit_cabang.kd_cabang,
										cabang.nm_cabang,
										cabang.rekening,
										benefit_cabang.id_pendaftaran,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										benefit_cabang.tgl_payment,
										benefit_cabang.jml_benefit,
										benefit_cabang.status,
										benefit_cabang.active,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.tgl_keberangkatan
										FROM
										benefit_cabang
										INNER JOIN cabang ON benefit_cabang.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_cabang.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_cabang.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id '.$where.' AND
										benefit_cabang.active = "1" AND benefit_cabang.jml_benefit <> "0" AND pembayaran.status_verifikasi = "1"
										GROUP BY benefit_cabang.id
										ORDER by cabang.nm_cabang ASC');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_all_export($id_jadwal) {
			$query = $this->db->query('SELECT kd_cabang,nm_cabang,sum(jml_benefit) AS benefit_belum_lunas FROM tv_benefit_cabang WHERE id_jadwal="'.$id_jadwal.'" 
									   AND status="0" GROUP BY kd_cabang');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_export($id_jadwal,$kd_cabang) {
			$query = $this->db->query(' SELECT kd_cabang,nm_cabang,sum(jml_benefit) AS benefit_belum_lunas FROM tv_benefit_cabang WHERE id_jadwal="'.$id_jadwal.'" AND
										kd_cabang = "'.$kd_cabang.'" AND status="0" GROUP BY kd_cabang');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_export_between($star,$until) {
			$query = $this->db->query(' SELECT
										benefit_cabang.id,
										benefit_cabang.kd_cabang,
										cabang.nm_cabang,
										cabang.rekening,
										benefit_cabang.id_pendaftaran,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										benefit_cabang.tgl_payment,
										benefit_cabang.jml_benefit,
										benefit_cabang.status,
										benefit_cabang.active,
										pembayaran.status_verifikasi,
										paket.nm_paket,
										jadwal.tgl_keberangkatan
										FROM
										benefit_cabang
										INNER JOIN cabang ON benefit_cabang.kd_cabang = cabang.kd_cabang
										INNER JOIN pendaftar ON benefit_cabang.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN pembayaran ON benefit_cabang.no_faktur = pembayaran.no_faktur
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id 
										WHERE benefit_cabang.active = "1" AND benefit_cabang.jml_benefit <> "0" AND pembayaran.status_verifikasi = "1" AND 
										benefit_cabang.tgl_payment BETWEEN "'.$star.'" AND "'.$until.'"
										GROUP BY benefit_cabang.id
										ORDER by cabang.nm_cabang ASC');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}

		public function get_sum_between($star,$until) {
			$query = $this->db->query(' SELECT kd_cabang,nm_cabang,rekening,sum(jml_benefit) AS benefit_belum_lunas FROM tv_benefit_cabang WHERE status="0" AND
										tgl_payment BETWEEN "'.$star.'" AND "'.$until.'" GROUP BY kd_cabang ');
			// $this->db->order_by('marketing.nm_marketing','ASC');			
			// $this->db->where($where);
			return $query;
		}
	}
