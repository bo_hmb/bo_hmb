<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>BENEFIT CABANG</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 50PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h2 style="text-align: center; font-weight: bold;">BENEFIT CABANG</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 50PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h3 style="text-align: center;">BETWEEN <?php echo $x=date('d-m-Y', strtotime($star)); ?> - <?php echo $x=date('d-m-Y', strtotime($until)); ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 40px;">Kode Cabang</th>
        <th style="width: 100px;">Nama Cabang</th>
        <!-- <th style="width: 120px;">Rekening</th> -->
        <th style="width: 40px;">No. Pendaftaran</th>
        <th style="width: 300px;">Nama Pendaftar</th>
        <th style="width: 80px;">Tanggal Payment</th>
        <th style="width: 100px;">Jumlah Benefit</th>
        <th style="width: 100px;">Status</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { 
        if  ($row->status == '0') {
            $status = 'Belum Lunas';
            $warna = 'background-color: #ff0000';
            } else {
               $status = 'Lunas';
               $warna = 'background-color: #39e600';
            }
        
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->kd_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <!-- <td style="border:1px solid #000;"><?php echo $row->rekening ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->no_pendaftaran ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_payment)) ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->jml_benefit,0,',','.') ?></td>
           <td style="border:1px solid #000;<?php echo $warna; ?>"><?php echo $status ?></td>      
        </tr>   
    <?php $nomor++;}?> 

</table>

<br>
            
<table>    
    <tr> 
    <?php $warna = 'background-color: #ff0000';            
            $warna2 = 'background-color: #39e600'; ?>          
        <th style="width: 20px;">No</th>        
        <th style="width: 40px;">Kode Cabang</th>
        <th style="width: 120px;">Nama Cabang</th>
        <th style="width: 200px;">Rekening</th>
        <!-- <th style="width: 100px;<?php echo $warna2; ?>">Benefit Lunas</th> -->
        <th style="width: 100px;<?php echo $warna; ?>">Benefit Belum Lunas</th>
    </tr>

    <?php $nomor=1; foreach($sum as $row) { 
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->kd_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->rekening ?></td>
           <!-- <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->benefit_lunas,0,',','.') ?></td>       -->
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->benefit_belum_lunas,0,',','.') ?></td>      
        </tr>   
    <?php $nomor++;}?> 

</table>
</body>
</html>