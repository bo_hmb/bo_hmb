            <form role="form" method="POST" action="<?php echo base_url(); ?>benefit_cabang/export_between" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
            <center>
                <div class="row">
                    <div class="form-group form-md-line-input">
                            <label class="col-md-3 control-label">Star Payment</label>
                            <div class="col-md-8">
                                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" name="star_payment" class="form-control" >                                    
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                            </div>
                        </div>
                    </div>
                <div>

                <div class="row">
                    <div class="form-group form-md-line-input">
                            <label class="col-md-3 control-label">Until Payment</label>
                            <div class="col-md-8">
                                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                        <input type="text" name="until_payment" class="form-control" >                                    
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                            </div>
                        </div>
                    </div>
                <div>
            </center>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     