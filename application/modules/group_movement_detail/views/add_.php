<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <input type="text" class="hidden" name="id_movement" value="<?php echo $group_movement?>">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="">
                        <input type="text" name="dat" class="form-control" readonly>
                        <label class="control-label">Date
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Input Date </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="route" placeholder="Input Route">
                    <label for="form_control_1">Route
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Route</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group">
                        <span class="help-block"> Time </span>
                    <div class="input-group">
                        <input type="text" id="clockface_1" value="00:00" class="form-control" name="tim"/>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" id="clockface_1_toggle">
                                <i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>                      
        </div>

        <div class="row">
             
            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="program" placeholder="Input Program">
                    <label for="form_control_1">Program
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Program</span>
                </div>
            </div>  
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="transportation" placeholder="Input Transportation">
                    <label for="form_control_1">Transportation
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Transportation</span>
                </div>
            </div>   
        </div>   

        <span class="caption-subject font-dark bold uppercase">Accomodation Detail</span>

        <div class="row">
            <br>
            <div class="col-md-6">
                <div class="form-group ">
                <label for="form_control_1">Hotel <span class="required">*</span></label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="multi-append" name="id_hotel[]" class="form-control pilihbanyak" multiple>
                            <option></option>
                            <?php
                                foreach ($hotel as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_hotel; ?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="meals_program" placeholder="Input Meals Program">
                    <label for="form_control_1">Meals Program
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Input Meals Program</span>
                </div>
            </div>             
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group date form_datetime form_datetime bs-datetime">
                        <input type="text" size="16" class="form-control" name="check_in" placeholder="Masukkan Check In" data-date-format="yyyy-mm-dd hh:mm:ss">
                        <label class="control-label">Check In <span class="required">*</span>
                        </label>
                            <span class="input-group-addon">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group date form_datetime form_datetime bs-datetime">
                        <input type="text" size="16" class="form-control" name="check_out" placeholder="Input Check Out" data-date-format="yyyy-mm-dd hh:mm:ss">
                        <label class="control-label">Check Out <span class="required">*</span>
                        </label>
                            <span class="input-group-addon">
                                <button class="btn default date-set" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>             
        </div>

    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>