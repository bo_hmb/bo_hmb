<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Group_movement_detail extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Group movement Detail';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_group_movement_detail');
            // $this->load->model('M_movement');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function group_movement($id)
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Group movement Detail';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['idmovement'] = $this->uri->segment(3);
            $this->data['group_movement'] = $this->M_group_movement_detail->get_group_movement(array('id' => $id))->row();
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            $dat = $this->input->post('dat',TRUE);
            $route = $this->input->post('route',TRUE);
            $program = $this->input->post('program',TRUE);
            $tim = $this->input->post('tim',TRUE);
            $transportation = $this->input->post('transportation',TRUE);
            $hotel = $this->input->post('hotel',TRUE);
            $check_in = $this->input->post('check_in',TRUE);
            $time_check_in = $this->input->post('time_check_in',TRUE);
            $check_out = $this->input->post('check_out',TRUE);
            $time_check_out = $this->input->post('time_check_out',TRUE);
            $meals_program = $this->input->post('meals_program',TRUE);
            
            $cols = array();
			if (!empty($dat)) { $cols['dat'] = $dat; }
            if (!empty($route)) { $cols['route'] = $route; }
            if (!empty($program)) { $cols['program'] = $program; }
            if (!empty($tim)) { $cols['tim'] = $tim; }
            if (!empty($transportation)) { $cols['transportation'] = $transportation; }
            if (!empty($hotel)) { $cols['hotel'] = $hotel; }
            if (!empty($check_in)) { $cols['check_in'] = $check_in; }
            if (!empty($check_out)) { $cols['check_out'] = $check_out; }
			if (!empty($meals_program)) { $cols['meals_program'] = $meals_program; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "group_movement_detail.active = '1' AND id_gorup_movement = '".$id."'";             
            } else {
             $where = "group_movement_detail.active = '1' AND id_gorup_movement = '".$id."'";             
            }


	        $list = $this->M_group_movement_detail->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_group_movement_detail->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';
                $hotel = $this->M_group_movement_detail->hotel($r->id_hotel);

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $x=date('d-m-Y', strtotime($r->dat)),
                    // $r->dat,
                    $r->route,
                    $r->program,
                    $r->tim,
                    $r->transportation,
                    $hotel->hotel,
                    $r->check_in,
                    $r->check_out,
                    $r->meals_program,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
			$data['title'] = 'Tambah Data Movedet';
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            $data['group_movement'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {
			// $this->validateInput();
            $this->ajaxRequest();

            if (count($this->input->post('id_hotel')) > 0) {
                $arrhotel = $this->input->post('id_hotel');
                foreach($arrhotel as $val)
                {
                    $hotelarr = @$hotelarr . $val. ",";
                }
            } else {
                $hotelarr ='';
            }

			$data = array(
				'id_gorup_movement' => $this->input->post('id_movement',TRUE),
                'dat' => $this->input->post('dat',TRUE),
                'route' => $this->input->post('route',TRUE),
                'program' => $this->input->post('program',TRUE),
                'tim' => $this->input->post('tim',TRUE),
                'transportation' => $this->input->post('transportation',TRUE),
                'id_hotel' => substr(trim($hotelarr), 0, -1),
                'check_in' => $this->input->post('check_in',TRUE),
                'check_out' => $this->input->post('check_out',TRUE),
                'meals_program' => $this->input->post('meals_program',TRUE),
                'active' => '1',
				);

			$query = $this->M_group_movement_detail->_insert($data);

            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_group_movement_detail->get_where(array('id' => $id))->row();
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            if (count($this->input->post('id_hotel')) > 0) {
                $arrhotel = $this->input->post('id_hotel');
                foreach($arrhotel as $val)
                {
                    $hotelarr = @$hotelarr . $val. ",";
                }
            } else {
                $hotelarr ='';
            }
            
            $id    = $this->input->post('id');

            $data = array(
                'dat' => $this->input->post('dat',TRUE),
                'route' => $this->input->post('route',TRUE),
                'program' => $this->input->post('program',TRUE),
                'tim' => $this->input->post('tim',TRUE),
                'transportation' => $this->input->post('transportation',TRUE),
                'id_hotel' => substr(trim($hotelarr), 0, -1),
                'check_in' => $this->input->post('check_in',TRUE),
                'check_out' => $this->input->post('check_out',TRUE),
                'meals_program' => $this->input->post('meals_program',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_group_movement_detail->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_group_movement_detail->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            //id_movement,jam,agenda
            $this->form_validation->set_rules('id_movement', 'nm hotel', 'trim|required');
            $this->form_validation->set_rules('jam', 'kota lokasi', 'trim|required');
            $this->form_validation->set_rules('agenda', 'alamat', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
