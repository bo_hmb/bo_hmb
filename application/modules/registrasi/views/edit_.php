<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">  
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                <div class="fg-line">
                    <label>Pilih Jamah</label>
                    <select id="id_formulir" name="id_formulir" class="form-control jadwal" data-placeholder="Pilih Jamaah">
                        <option disabled selected></option>
                            <option></option>
                            <?php
                                foreach ($formulir as $row) { ?>
                                <option <?php echo ($row->id === $main->id_formulir) ? 'selected' : ''; ?> value="<?php echo $row->id; ?>">NIK : <?php echo $row->no_ktp; ?> - <?php echo $row->nm_ktp; ?> / <?php echo $row->nm_lengkap; ?> - <?php echo $row->age; ?> Thn
                                </option>                    
                            <?php }?>
                    </select>
                </div>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis Travel
                        <span class="required">*</span>
                    </label>
                    <select class="form-contro jenis_travel" id="jenis_travel" name="jenis_travel">
                         <option></option>
                        <option <?php echo ("UMRAH" === $main->jenis_travel) ? 'selected' : ''; ?> value="UMRAH">UMRAH</option>
                        <option <?php echo ("HAJI KHUSUS KUOTA" === $main->jenis_travel) ? 'selected' : ''; ?> value="HAJI KHUSUS KUOTA">HAJI KHUSUS KUOTA</option>
                        <option <?php echo ("HAJI KHUSUS NON KUOTA" === $main->jenis_travel) ? 'selected' : ''; ?> value="HAJI KHUSUS NON KUOTA">HAJI KHUSUS NON KUOTA</option>
                        <option <?php echo ("TOUR/WISATA" === $main->jenis_travel) ? 'selected' : ''; ?> value="TOUR/WISATA">TOUR/WISATA</option>                     
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Paket</label>
                    <select class="form-control paket" name="id_paket" id="paket">
                        <option <?php echo ("1" === $main->id_paket) ? 'selected' : ''; ?>  value="1">Tabungan | -</option>
                        <?php 
                            // $packet = Modules::run('packet/get_where', array('paket.active' => '1','kd_cabang'=>$this->kode_cabang,'jenis_travel' => $main->jenis_travel))->result();    
                            foreach ($packet as $row) { ?> 
                            <option <?php echo ($row->id === $main->id_paket) ? 'selected' : ''; ?> value="<?php echo $row->id; ?>">
                                <?php if(array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) echo $row->kd_cabang; ?> | 
                                <?php echo $row->nm_paket; ?> | <?php echo $row->nm_jadwal; ?></option>    
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" placeholder="Nomor Pendaftaran" value="<?php echo $main->no_pendaftaran ?>">
                        <label for="form_control_1">Nomor Pendaftaran 
                                <span class="required">*</span>
                        </label>
                            <span class="input-group-btn">
                                <button class="btn default" type="button" onclick="generate()">
                                    <i class="fa fa-check"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" name="tgl_pendaftaran" class="form-control" value="<?php echo $main->tgl_pendaftaran ?>">
                        <label class="control-label">Tanggal Pendaftaran
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pendaftaran </span>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Data Mahram dan Biaya - Biaya</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_mahram" placeholder="Masukkan Nama Mahram" value="<?php echo $main->nm_mahram ?>">
                    <label for="form_control_1">Nama Mahram
                    </label>
                    <span class="help-block">Masukkan Nama Mahram</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Pilih Hubungan Mahram
                    </label>
                    <select class="form-contro jenis_travel" id="hub_mahram" name="hub_mahram">
                        <option <?php echo ("-" === $main->hub_mahram ) ? 'selected' : ''; ?> value="-">-</option>
                        <option <?php echo ("Suami" === $main->hub_mahram ) ? 'selected' : ''; ?> value="Suami">Suami</option>
                        <option <?php echo ("Istri" === $main->hub_mahram ) ? 'selected' : ''; ?> value="Istri">Istri</option>
                        <option <?php echo ("Anak" === $main->hub_mahram ) ? 'selected' : ''; ?> value="Anak">Anak</option>
                        <option <?php echo ("Orang Tua" === $main->hub_mahram ) ? 'selected' : ''; ?> value="Orang Tua">Orang Tua</option>
                        <option <?php echo ("Saudara" === $main->hub_mahram ) ? 'selected' : ''; ?> value="Saudara">Saudara</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="hp_mahram" placeholder="Masukkan No. HP Mahram" value="<?php echo $main->hp_mahram ?>">
                    <label for="form_control_1">No. HP Mahram
                    </label>
                    <span class="help-block">Masukkan No. HP Mahram</span>
                </div>
            </div>
        </div>

        

        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-checkbox mt-checkbox-outline" for="8">
                        <input id="8" type="checkbox" name="mahram" value="1" class="input-small mahram" <?php if ($main->biaya_mahram != "0") echo "checked"?> />Biaya Mahram
                        <span></span>
                    </label>
                </div>        
            </div>
            <input type="hidden" class="form-control biaya-mahram money" name="biaya_mahram" placeholder="Masukkan Biaya Mahram" value="<?php echo $main->biaya_mahram ?>" id="biaya_mahram">
            <input type="hidden" class="form-control biaya-mahram2" name="biaya_mahram2" placeholder="Masukkan Biaya Mahram" value="<?php echo $main->biaya_mahram ?>" id="biaya_mahram2">
            <input type="hidden" class="form-control biaya-progresif money" name="biaya_progresif" placeholder="Masukkan Progresif" value="<?php echo $main->biaya_progresif ?>" id="biaya_progresif">


            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-checkbox mt-checkbox-outline" for="9">
                        <input id="9" type="checkbox" name="progresif" value="1" class="input-small progresif" <?php if ($main->biaya_progresif != "0") echo "checked"?> />Biaya Asuransi
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control add_on money" name="add_on" value="<?php echo $main->add_on ?>">
                    <label for="form_control_1">Add On
                    </label>
                    <span class="help-block">Masukkan Add On</span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control overpayment money" name="overpayment" value="<?php echo $main->overpayment ?>">
                    <label for="form_control_1">Overpayment
                    </label>
                    <span class="help-block">Masukkan Overpayment</span>
                </div>
            </div>
            <input type="hidden" class="form-control biaya-progresif money" name="biaya_progresif" placeholder="Masukkan Progresif" value="<?php echo $main->biaya_progresif ?>" id="biaya_progresif">
        </div>
        <input type="hidden" class="form-control id_marketing" id="id_marketing" name="id_marketing" value="<?php echo $main->id_marketing ?>">

        <!-- <span class="caption-subject font-dark bold uppercase">Upgrade Hotel</span>

        <div class="row">
            <br>
            <div class="col-md-6">                
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *4</label>
                        <select id="id_up_hotel_b4" name="id_up_hotel_b4" class="form-control hotel_b4" data-placeholder="Pilih Hotel">
                            <option></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control hrg_up_hotel_b4 money" id="hrg_up_hotel_b4" name="biaya_up_hotel_b4" placeholder="Masukkan Biaya Upgrade Hotel *5" value="<?php echo $main->biaya_up_hotel_b4 ?>">
                    <label for="form_control_1">Biaya Upgrade Hotel *4
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *4</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *5</label>
                        <select id="id_up_hotel_b5" name="id_up_hotel_b5" class="form-control hotel_b5" data-placeholder="Pilih Hotel">
                            <option></option>
                            
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control hrg_up_hotel_b5 money" id="hrg_up_hotel_b5" name="biaya_up_hotel_b5" placeholder="Masukkan Biaya Upgrade Hotel *5" value="<?php echo $main->biaya_up_hotel_b5 ?>">
                    <label for="form_control_1">Biaya Upgrade Hotel *5
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Biaya Upgrade Hotel *5</span>
                </div>
            </div>
        </div> -->

        <div class="row">
            <div class="col-md-12">
                <span class="caption-subject font-dark bold uppercase">Upgrade Hotel Makkah </span>                
            </div>
        </div>

        <div class="row">
            
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_d" name="hotel_request_makkah" class="hotel_request_makkah default" value="Default" <?php if ($main->hotel_request_makkah==="Default") echo "checked"?> />Default
                        <span></span></label>
                        <input readonly type="hidden" class="status_request_makkah" id="status_request_makkah" name="status_request_makkah" value="<?php echo $main->status_up_hotel_makkah ?>">  
                        <input readonly type="hidden" class="status_request_madinah" id="status_request_madinah" name="status_request_madinah" value="<?php echo $main->status_up_hotel_madinah ?>"> 
                    </label>
                </div>        
            </div>
            <input type="hidden" class="form-control biaya-mahram money" name="biaya_mahram" placeholder="Masukkan Biaya Mahram" value="0" id="biaya_mahram">

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_4" name="hotel_request_makkah" class="hotel_request_makkah *4" value="*4" <?php if ($main->hotel_request_makkah==="*4") echo "checked"?>/>*4
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_5" name="hotel_request_makkah" class="hotel_request_makkah *5" value="*5" <?php if ($main->hotel_request_makkah==="*5") echo "checked"?>/>*5
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input readonly type="text" class="money biaya_request_makkah form-control" id="biaya_request_makkah" name="biaya_request_makkah" value="<?php echo $main->biaya_up_hotel_makkah ?>">    
                    <input readonly type="hidden" class="money biaya_request_madinah form-control" id="biaya_request_madinah" name="biaya_request_madinah" value="<?php echo $main->biaya_up_hotel_madinah ?>">               
                    <label for="form_control_1">Biaya Upgrade Hotel Makkah
                    </label>
                </div>
            </div>
        </div>

        
                <!-- <div class="col-md-2">
                    
                </div>  
                <div class="col-md-2">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_madinah_d" name="hotel_request_madinah" class="" value="Default" <?php if ($main->hotel_request_madinah==="Default") echo "checked"?> />Default
                        <span></span></label>
                        <input readonly type="hidden" class="status_request_madinah" id="status_request_madinah" name="status_request_madinah" value="<?php echo $main->status_up_hotel_madinah ?>">                   

                </div>   
                <div class="col-md-1">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_madinah_4" name="hotel_request_madinah" class="hotel_request_madinah *4" value="*4" <?php if ($main->hotel_request_madinah==="*4") echo "checked"?>/>*4
                        <span></span></label>
                </div>        
                <div class="col-md-1">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_madinah_5" name="hotel_request_madinah" class="hotel_request_madinah *5" value="*5" <?php if ($main->hotel_request_madinah==="*5") echo "checked"?> />*5
                        <span></span></label><br>
                 </div>   -->   


        <!-- <div class="row">   -->
            
            <!-- <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <label for="form_control_1">Biaya Upgrade Hotel Madinah
                    </label>
                </div>
            </div> -->
        <!-- </div>  -->
        
        <!-- <div class="row">
            <br>
            <div class="col-md-6">                
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *4</label>
                        <select id="id_up_hotel_b4" name="id_up_hotel_b4" class="form-control hotel_b4" data-placeholder="Pilih Hotel" readonly>
                            <option></option>
                        </select>
                    </div>
                    <input type="hidden" class="form-control hrg_up_hotel_b4 money" id="hrg_up_hotel_b4" name="biaya_up_hotel_b4" value="<?php echo $main->biaya_up_hotel_b4 ?>" readonly>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *4</label>
                        <select id="id_up_hotel_b4_madinah" name="id_up_hotel_b4_madinah" class="form-control hotel_b4_madinah" data-placeholder="Pilih Hotel" readonly>
                            <option></option>
                        </select>
                    </div>
                    <input type="hidden" class="form-control hrg_up_hotel_b4_madinah money" id="hrg_up_hotel_b4_madinah" name="biaya_up_hotel_b4_madinah" placeholder="" value="0" readonly>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">                
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *5</label>
                        <select id="id_up_hotel_b5" name="id_up_hotel_b5" class="form-control hotel_b5" data-placeholder="Pilih Hotel" readonly>
                            <option></option>                            
                        </select>
                    </div>
                    <input type="hidden" class="form-control hrg_up_hotel_b5 money" id="hrg_up_hotel_b5" name="biaya_up_hotel_b5" value="<?php echo $main->biaya_up_hotel_b5 ?>" readonly>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="fg-line">
                    <label>Upgrade Hotel *5</label>
                        <select id="id_up_hotel_b5_madinah" name="id_up_hotel_b5_madinah" class="form-control hotel_b5_madinah" data-placeholder="Pilih Hotel" readonly>
                            <option></option>                            
                        </select>
                    </div>
                    <input type="hidden" class="form-control hrg_up_hotel_b5_madinah money" id="hrg_up_hotel_b5_madinah" name="biaya_up_hotel_b5_madinah" placeholder="" value="0" readonly>                    
                </div>
            </div>
        </div> -->

        <?php 
        $paket= Modules::run('packet/get_where', array('paket.id'=>$main->id_paket))->row(); 
        ?>
        <input type="hidden" class="form-control biaya-paket money" id="biaya-paket" name="biaya_paket" value="<?php echo $paket->hrg_paket ?>">


        <div class="row">
            <div class="col-md-12">
                <span class="caption-subject font-dark bold uppercase">Tipe Kamar Makkah </span>                
            </div>
            <!-- <div class="col-md-6">
                <span class="caption-subject font-dark bold uppercase">Tipe Kamar Madinah</span>                
            </div> -->
        </div>

        <div class="row">
            <div class="form-group" id="toastTypeGroup">
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar double" value="Double" <?php if ($main->jenis_kamar==="Double") echo "checked"?>  />Double
                        <span></span></label>
                        <input type="hidden" class="money lama_hari" id="lama_hari" name="lama_hari" value="<?php echo $main->lama_hari ?>">
                        <input readonly type="hidden" class="status_jenis_kamar" id="status_jenis_kamar" name="status_jenis_kamar" value="<?php echo $main->status_upgrade_kamar ?>">
                    </div>        
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar triple" value="Triple" <?php if ($main->jenis_kamar==="Triple") echo "checked"?> />Triple
                        <span></span></label>
                    </div> 
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="jenis_kamar_quad_makkah" name="jenis_kamar" class="jenis_kamar quad" value="Quad" <?php if ($main->jenis_kamar==="Quad") echo "checked"?>/>Quad
                        <span></span></label>
                    </div> 
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar quint" value="Quint" <?php if ($main->jenis_kamar==="Quint") echo "checked"?>/>Quint
                        <span></span></label><br>
                    </div> 
                </div> 
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="jk_no_bed" name="jenis_kamar" class="jenis_kamar no_bed" value="NoBed" <?php if ($main->jenis_kamar==="NoBed") echo "checked"?>/>No Bed
                        <span></span></label>
                        <!-- <input type="text" class="money" id="quint"> -->
                    </div>        
                </div>  
                <!-- /// -->   
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                        <input readonly type="text" class="money jns_kmr_hrg_makkah form-control" id="jns_kmr_hrg_makkah" name="biaya_upgrade_kamar_makkah" value="<?php echo $main->biaya_upgrade_kamar ?>">               
                        <label for="form_control_1">Biaya Upgrade
                        <input readonly type="hidden" class="money jns_kmr_hrg_madinah form-control" id="jns_kmr_hrg_madinah" name="biaya_upgrade_kamar_madinah" value="<?php echo $main->biaya_upgrade_kamar_madinah ?>">
                    </div>
                </div>
            </div>            
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pilih Hubungan Keluarga
                    </label>
                    <select class="form-contro jenis_travel" id="hub_mahram" name="hubungan">
                        <option <?php echo ("-" === $main->hubungan ) ? 'selected' : ''; ?> value="-">-</option>
                        <option <?php echo ("Suami" === $main->hubungan ) ? 'selected' : ''; ?> value="Suami">Suami</option>
                        <option <?php echo ("Istri" === $main->hubungan ) ? 'selected' : ''; ?> value="Istri">Istri</option>
                        <option <?php echo ("Anak" === $main->hubungan ) ? 'selected' : ''; ?> value="Anak">Anak</option>
                        <option <?php echo ("Orang Tua" === $main->hubungan ) ? 'selected' : ''; ?> value="Orang Tua">Orang Tua</option>
                        <option <?php echo ("Saudara" === $main->hubungan ) ? 'selected' : ''; ?> value="Saudara">Saudara</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pilih Nama Keluarga</label>
                    <select class="form-control pilih" name="keluarga_terdekat" data-placeholder="Pilih Keluarga Terdekat">
                        <option></option>
                        <?php
                        foreach ($formulir as $row) { ?>
                         <option <?php echo ($row->nm_lengkap === $main->keluarga_terdekat) ? 'selected' : ''; ?> value="<?php echo $row->nm_lengkap; ?>">NIK : <?php echo $row->no_ktp; ?> - <?php echo $row->nm_ktp; ?> / <?php echo $row->nm_lengkap; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <!-- <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="keluarga_terdekat" placeholder="Masukkan Keluarga Terdekat" value="<?php echo $main->keluarga_terdekat ?>">
                    <label for="form_control_1">Nama Keluarga Terdekat
                    </label>
                    <span class="help-block">Masukkan Nama Keluarga Terdekat</span>
                </div> -->
            </div>            
        </div>
        <br>
        <span class="caption-subject font-dark bold uppercase">Kelengkapan Berkas</span>

        <div class="row">
            <br>            
            <div class="form-group">
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="1">
                        <input id="1" type="checkbox" value="1" name="fc_ktp" class="input-small" <?php if ($main->fc_ktp==='1') echo "checked"?>/>FC KTP
                        <span></span>
                    </label>
                </div>        
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="2">
                        <input id="2" type="checkbox" value="1" name="fc_aktakelahiran" class="input-small" <?php if ($main->fc_aktakelahiran==='1') echo "checked"?>/>FC Akta Kelahiran
                        <span></span>
                    </label>
                </div>      

                <div class="col-md-5">
                    <div class="form-group" id="toastTypeGroup">                        
                        <div class="col-md-6">
                            <label class="mt-radio mt-radio-outline">
                                <input type="radio" name="fc_bukunikah" value="1" <?php if ($main->fc_bukunikah==='1') echo "checked"?> />FC Buku Nikah
                                <span></span></label>
                                <!-- <input type="text" class="money" id="quad"> -->
                        </div>        
                        <div class="col-md-6">
                            <label class="mt-radio mt-radio-outline">
                                <input type="radio" name="fc_bukunikah" value="2" <?php if ($main->fc_bukunikah==='2') echo "checked"?> />Non Buku Nikah
                                <span></span></label><br>
                                <!-- <input type="text" class="money" id="quint"> -->
                        </div>     
                    </div>
                </div>

                <!-- <div class="col-md-2">
                    <label class="mt-checkbox mt-checkbox-outline" for="3">
                        <input id="3" type="checkbox" value="1" name="fc_bukunikah" class="input-small" <?php if ($main->fc_bukunikah==='1') echo "checked"?>/>FC Buku Nikah
                        <span></span>
                    </label>
                </div>    
                <div class="col-md-2">
                    <label class="mt-checkbox mt-checkbox-outline" for="7">
                        <input id="7" type="checkbox" value="2" name="fc_bukunikah" class="input-small" <?php if ($main->fc_bukunikah==='2') echo "checked"?>/> Non Buku Nikah
                        <span></span>
                    </label>
                </div>   -->              
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="4">
                        <input id="4" type="checkbox" value="1" name="pasport_asli" class="input-small" <?php if ($main->pasport_asli==='1') echo "checked"?> />Pasport Asli
                        <span></span>
                    </label>
                </div>        
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="5">
                        <input id="5" type="checkbox" value="1" name="buku_faksin_asli" class="input-small" <?php if ($main->buku_faksin_asli==='1') echo "checked"?>/>Buku Faksin Asli
                        <span></span>
                    </label>
                </div>         
                <div class="col-md-2">
                    &nbsp;&nbsp;&nbsp;
                    <label class="mt-checkbox mt-checkbox-outline" for="6">
                        <input id="6" type="checkbox" value="1" name="fc_kk" class="input-small" <?php if ($main->fc_kk==='1') echo "checked"?>  />FC KK
                        <span></span>
                    </label>
                </div>
                <div class="col-md-4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="mt-checkbox mt-checkbox-outline" for="7">
                        <input id="7" type="checkbox" value="1" name="pas_photo" class="input-small" <?php if ($main->pas_photo==='1') echo "checked"?> />Pas Foto
                        <span></span>
                    </label>
                </div>           
            </div>
        </div>

        <br>
        <span class="caption-subject font-dark bold uppercase">Barang Tambahan</span>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:30%">Nama Barang</th>
                            <th>Harga</th>
                            <th style="width:10%">Qty</th>
                            <th>Free</th>
                            <th>Sub Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="container">
                        
                            <?php 
                            foreach ($barang as $row) { ?>

                            <tr class="baris form-create-barang" id="form-create-barang">    
                                <td align="center">
                                    <select id="barang" class="form-control barang" name="nm_barang[]" readonly>
                                        <option value="<?php echo $row->id_barang ?>"><?php echo $row->nm_barang ?></option>
                                    </select>
                                </td>
                                <td align="center">
                                    <input id="harga_jual" class="form-control harga_jual money" name="harga_jual[]" type="text" value="<?php echo $row->harga_satuan ?>" readonly>
                                </td>
                                <td align="center">
                                    <input id="jml" name="jml[]" class="form-control jml money" type="text" placeholder="0" value="<?php echo $row->jml ?>" readonly>
                                </td>
                                <td align="center">
                                    <input type="checkbox" id="free[]" class="free" <?php if ($row->free != "0") echo "checked"?>>
                                </td>

                                <td align="center">
                                    <input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" readonly value="<?php echo $row->sub_harga ?>" readonly>
                                </td>
                            </td>
                            <td>
                                <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>
                                <input id="rows" name="rows[]" value="" type="hidden">
                                <input id="sts_free" name="sts_free[]" value="<?php echo $row->free ?>" type="hidden">
                            </td>
                                
                            <?php } ?>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                            <td align="center">
                                <input type="text" name="total_bayar" class="form-control total-bayar money" id="total-bayar" readonly="" value="<?php echo $main->biaya_order_barang ?>">
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
            </div>
        </div>

        <input type="hidden" id="age" name="age" value="<?php echo $main->age ?>">
        <input type="hidden" id="gender" name="gender" value="<?php echo $main->jk ?>">

        <?php 
            if ($main->age<= 144 && $main->age > 24) {
                                    $diskon = 15 ;
                                }   
                                if ($main->age<= 24) {
                                    $diskon = 75 ;
                                }  

                                if ($main->age > 144) {
                                    $diskon = 0 ;
                                } 
        ?>
        <input type="hidden" class="diskon" id="diskon" name="diskon" value="<?php echo $diskon ?>">

        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control biaya-normal money" id="biaya-normal" readonly value="<?php echo $x=$main->total_biaya + $main->potongan_paket ?>">
                    <!-- <input type="text" class="form-control total-biaya" readonly name="total_biaya" id="total_biaya"> -->
                    <label for="form_control_1">Biaya Normal
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control potongan money" id="potongan" <?php 
                    if (array_key_exists('Admin Manivest',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel) or array_key_exists('Admin Finance',$this->userLevel)) echo ""; else echo "readonly";
                    ?> name="potongan_paket" value="<?php echo $main->potongan_paket ?>">
                    <!-- <input type="text" class="form-control total-biaya" readonly name="total_biaya" id="total_biaya"> -->
                    <label for="form_control_1">Potongan Harga Paket
                    </label>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">                
                    <input type="text" class="form-control total-hrg-paket money" readonly name="total_biaya" id="total-hrg-paket" value="<?php echo $main->total_biaya ?>">
                    <label for="form_control_1">Total Biaya
                    </label>                   
                </div>
            </div>
            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>" 
                    <?php 
                    if (array_key_exists('CS',$this->userLevel) or array_key_exists('Admin CS',$this->userLevel) or array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Admin',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) echo ""; else echo "disabled=";
                    ?> ><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>

<script>
function generate() {
  document.getElementById('no_pendaftaran').value = "<?php echo $no_pendaftaran; ?>";
}
</script>