<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>MANIFEST REGISTRASI</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 550PX"> 
            <h2 style="text-align: center; font-weight: bold;">MANIFEST REGISTRASI</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 255PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h3 style="text-align: center;"><?php echo date('d-m-Y', strtotime($star)).' - '.date('d-m-Y', strtotime($end)); ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 40px;">No. Pendaftaran</th>
        <th style="width: 40px;">NIK</th>
        <th style="width: 10px;">TT</th>
        <th style="width: 200px;">Nama Lengkap</th>
        <th style="width: 100px;">HP</th>
        <th style="width: 80px;">No. Pasport</th>
        <th style="width: 200px;">Nama Pasport</th>
        <th style="width: 150px;">Paket</th>
        <th style="width: 100px;">Cabang</th>
        <th style="width: 100px;">Agen</th>
    </tr>
    <?php $nomor=1; foreach ($detail as $row) { ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_pendaftaran ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_ktp ?></td>
           <td style="border:1px solid #000;"><?php echo $row->title ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_ktp ?></td>
           <td style="border:1px solid #000;"><?php echo $row->hp ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_pasport ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_paket.' '.$row->nm_jadwal ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>
           
        </tr> 
      <?php $nomor++; } ?>
</table>
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 100px;">Cabang</th>
        <th style="width: 150px;">Jumlah</th>
    <tr>
    <?php $no=1; foreach ($group as $row) { ?>
    <tr>
        <td style="border:1px solid #000;"><?php echo $no; ?></td>
        <td style="border:1px solid #000;"><?php echo $row->nm_cabang; ?></td>
        <td style="border:1px solid #000;"><?php echo $row->jumlah; ?></td>
    </tr>
    <?php $no++; } ?>
</table>
</body>
</html>