            <form role="form" method="POST" action="<?php echo base_url(); ?>registrasi/export_where" target="_blank" class="form-horizontal" id="form-export">
            
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Start Date</label>
                        <div class="col-md-4">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_star" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    
                        <label class="col-md-2 control-label">To Date</label>
                        <div class="col-md-3">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_to" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Schedule</label>
                        <div class="col-md-8">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Select Schedule">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="0">Tabungan</option>
                                <option value="111">Haji 2019</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Work Area</label>
                        <div class="col-md-8">
                            <select id="cabang" name="cabang" class="form-control jadwal" data-placeholder="Select">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <?php
                                foreach ($workarea as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tabungan / Reguler</label>
                        <div class="col-md-8">
                            <select id="jenis" name="jenis" class="form-control jadwal" data-placeholder="Select">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="Tabungan">Tabungan</option>
                                <option value="Reguler">Reguler</option>
                            </select>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     