<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Customer Service</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Registrasi</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Registrasi</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            <div class="btn-group">
                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export-excel" href="javascript:;"> EXCEL SIPATUH </a>
                                    </li>
                                    <li>
                                        <a class="btn-export" href="javascript:;"> MANIFEST REGISTRASI </a>
                                    </li>
            
                                    <?php if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel)) { ?>
                                    <li>
                                        <a class="btn-export-where" href="javascript:;"> REGISTRASI </a>
                                    </li>
                                    <?php } ?>
                                    
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-container">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" >
                                        <th width="5px">No</th>
                                        <th width="1px">Aksi</th>
                                        <th width="15px">Jenis</th>
                                        <th width="200px">Paket</th>
                                        <th width="15px">No. Pendaftaran</th>
                                        <th width="15px">Tanggal Keberangkatan</th>
                                        <!-- <th width="10px">No. KTP</th> -->
                                        <th width="10px">Nama Sesuai KTP</th>
                                        <th width="10px">Nama Sesuai Pasport</th>
                                        <!-- <th width="10px">Tanggal Lahir</th>
                                        <th width="15px">Tanggal Pendaftaran</th>
                                        <th width="15px">Nama Mahram</th>
                                        <th width="15px">No. HP Mahram</th>
                                        <th width="15px">Hubungan Mahram</th>
                                        <th width="15px">Keluarga Terdekat</th>
                                        <th width="15px">Hubungan Keluarga</th>
                                        <th width="15px">Biaya Mahram</th>
                                        <th width="15px">Biaya Progresif</th>
                                        <th width="15px">Upgrade Hotel *4</th>
                                        <th width="15px">Biaya Upgr. Hotel *4</th>
                                        <th width="15px">Upgrade Hotel *5</th>
                                        <th width="15px">Biaya Upgr. Hotel *5</th> -->
                                        <th width="15px">Jenis Kamar</th>
                                        <!-- <th width="15px">Biaya Upgrade Kamar</th>
                                        <th width="15px">Biaya Order Barang</th> -->
                                        <th width="15px">FC KTP</th>
                                        <th width="15px">FC Akta Lahir</th>
                                        <th width="15px">FC Buku Nikah</th>
                                        <th width="15px">FC KK</th>
                                        <th width="15px">Buku Vaksin Asli</th>
                                        <th width="15px">Paspor Asli</th>
                                        <th width="15px">Pas Foto</th>
                                        <th width="15px">Total Biaya</th>
                                        <th width="15px">Status</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <!-- <td>
                                            <select class="form-control form-filter input-sm"name="jenis_travel">
                                                 <option></option>
                                                <option value="UMRAH">UMRAH</option>
                                                <option value="HAJI KHUSUS KUOTA">HAJI KHUSUS KUOTA</option>
                                                <option value="HAJI KHUSUS NON KUOTA">HAJI KHUSUS NON KUOTA</option>
                                                <option value="TOUR/WISATA">TOUR/WISATA</option>                        
                                            </select>
                                        </td> -->
                                        <td>
                                            <select name="jenis" class="form-control form-filter input-sm pilih">
                                                <option disabled selected></option>
                                                    <option value="Reguler">Reguler</option>   
                                                    <option value="Tabungan">Tabungan</option>   
                                            </select>
                                        </td>
                                        <td>                                            
                                            <select class="form-control form-filter input-sm pilih" name="nm_paket">
                                                <option value=""></option>                                                
                                                <option value="Tabungan">Tabungan</option>                                                
                                                <?php 
                                                    $packet = Modules::run('packet/get_where', array('paket.active' => '1','paket.kd_cabang'=>$this->kode_cabang))->result();
                                                    foreach ($packet as $row) { ?> 
                                                    <option value="<?php echo $row->nm_paket; ?>"><?php echo $row->nm_paket; ?> | <?php echo $row->nm_jadwal; ?></option>    
                                                <?php } ?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_pendaftaran"> </td>                                         
                                        <td><select name="tgl_keberangkatan" class="form-control form-filter input-sm pilih">
                                                <option value=""></option>                                                
                                                <?php
                                                 $schedule= Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
                                                foreach ($schedule as $row) { ?>
                                                <option value="<?php echo $row->tgl_keberangkatan; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                                <?php
                                                }
                                                ?>
                                            </select> 
                                        </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="no_ktp"> </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_ktp"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        <!-- <td> </td>
                                        <td><input type="date" class="form-control form-filter input-sm" name="tgl_pendaftaran"> </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>-->
                                        <td></td> 
                                        <td></td>
                                        <!-- <td></td>
                                        <td></td> -->
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <!-- <select name="status" class="form-control form-filter input-sm">
                                                <option value=""></option>
                                                <option value="Belum Lunas">Belum Lunas</option>
                                                <option value="Registrasi">Registrasi</option>
                                                <option value="Lunas">Lunas</option>
                                            </select>   -->
                                        </td>
                                        
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>