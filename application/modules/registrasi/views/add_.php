<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">  
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                <div class="fg-line">
                    <label>Pilih Jamah</label>
                    <select id="id_formulir" name="id_formulir" class="form-contro id_formulir jadwal" data-placeholder="Pilih Jamaah">
                        <option disabled selected></option>
                        <option></option>
                        <?php
                        foreach ($formulir as $row) { ?>
                        <option value="<?php echo $row->id; ?>">NIK : <?php echo $row->no_ktp; ?> - <?php echo $row->nm_ktp; ?> / <?php echo $row->nm_lengkap; ?> - <?php echo $row->age; ?> Thn / <?php echo $row->no_pasport; ?></option>                    
                        <?php
                        }
                        ?>
                    </select>
                </div>
                </div>
            </div>
        </div>
        <div class="row">  
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jenis Travel
                        <span class="required">*</span>
                    </label>
                    <select class="form-contro jenis_travel" id="jenis_travel" name="jenis_travel">
                         <option></option>
                        <option value="UMRAH">UMRAH</option>
                        <option value="HAJI KHUSUS KUOTA">HAJI KHUSUS KUOTA</option>
                        <option value="HAJI KHUSUS NON KUOTA">HAJI KHUSUS NON KUOTA</option>
                        <option value="TOUR/WISATA">TOUR/WISATA</option>                        
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Paket</label>
                    <select class="form-control paket" name="id_paket" id="paket">
                        <option></option>
                        
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" placeholder="Nomor Pendaftaran" >
                        <label for="form_control_1">Nomor Pendaftaran 
                                <span class="required">*</span>
                        </label>
                            <span class="input-group-btn">
                                <button class="btn default" type="button" onclick="generate()">
                                    <i class="fa fa-check"></i>
                                </button>
                            </span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" name="tgl_pendaftaran" class="form-control" value="<?php echo date('Y-m-d') ?>">
                        <label class="control-label">Tanggal Pendaftaran
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Pendaftaran </span>
                </div>
            </div>
        </div>

        <span class="caption-subject font-dark bold uppercase">Data Mahram dan Biaya - Biaya</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_mahram" placeholder="Masukkan Nama Mahram">
                    <label for="form_control_1">Nama Mahram
                    </label>
                    <span class="help-block">Masukkan Nama Mahram</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Pilih Hubungan Mahram
                    </label>
                    <select class="form-contro jenis_travel" id="hub_mahram" name="hub_mahram">
                        <option value="-">-</option>
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option value="Orang Tua">Orang Tua</option>
                        <option value="Saudara">Saudara</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="hp_mahram" placeholder="Masukkan No. HP Mahram">
                    <label for="form_control_1">No. HP Mahram
                    </label>
                    <span class="help-block">Masukkan No. HP Mahram</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-checkbox mt-checkbox-outline" for="8">
                        <input id="8" type="checkbox" name="mahram" value="1" class="input-small mahram" />Biaya Mahram
                        <span></span>
                    </label>
                </div>        
            </div>
            <input type="hidden" class="form-control biaya-mahram money" name="biaya_mahram" placeholder="Masukkan Biaya Mahram" value="0" id="biaya_mahram">

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-checkbox mt-checkbox-outline" for="9">
                        <input id="9" type="checkbox" name="progresif" value="1" class="input-small progresif" />Biaya Asuransi
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control add_on money" name="add_on" value="0">
                    <label for="form_control_1">Add On
                    </label>
                    <span class="help-block">Masukkan Add On</span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control overpayment money" name="overpayment" value="0">
                    <label for="form_control_1">Overpayment
                    </label>
                    <span class="help-block">Masukkan Overpayment</span>
                </div>
            </div>
            <input type="hidden" class="form-control biaya-progresif money" name="biaya_progresif" placeholder="Masukkan Progresif" value="0" id="biaya_progresif">
        </div>       

        <div class="row">
            <div class="col-md-12">
                <span class="caption-subject font-dark bold uppercase">Upgrade Hotel</span>                
            </div>
        </div>

        <div class="row">
            
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_d" name="hotel_request_makkah" class="hotel_request_makkah default" value="Default" checked />Default
                        <span></span></label>
                        <input readonly type="hidden" class="status_request_makkah" id="status_request_makkah" name="status_request_makkah" value="1">                   
                        <input readonly type="hidden" class="status_jenis_kamar_madinah" id="status_jenis_kamar_madinah" name="status_jenis_kamar_madinah" value="1">
                    </label>
                </div>        
            </div>
            <input type="hidden" class="form-control biaya-mahram money" name="biaya_mahram" placeholder="Masukkan Biaya Mahram" value="0" id="biaya_mahram">

            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_4" name="hotel_request_makkah" class="hotel_request_makkah *4" value="*4" />*4
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-2">
                <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="hotel_request_makkah_5" name="hotel_request_makkah" class="hotel_request_makkah *5" value="*5" />*5
                        <span></span>
                    </label>
                </div>        
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input readonly type="text" class="money biaya_request_makkah form-control" id="biaya_request_makkah" name="biaya_request_makkah" value="0">                   
                    <label for="form_control_1">Biaya Upgrade Hotel Makkah
                    </label>
                </div>
            </div>
            <input type="hidden" class="form-control biaya-progresif money" name="biaya_progresif" placeholder="Masukkan Progresif" value="0" id="biaya_progresif">
            <input type="hidden" class="money biaya_request_madinah form-control" id="biaya_request_madinah" name="biaya_request_madinah" value="0">
            <input type="hidden" class="form-control biaya-paket money" id="biaya-paket" name="biaya_paket" placeholder="Masukkan Biaya Paket">
            <input type="hidden" class="form-control id_marketing" id="id_marketing" name="id_marketing">
        </div>
        <!-- ////////// -->

        <div class="row">
            <div class="col-md-12">
                <span class="caption-subject font-dark bold uppercase">Tipe Kamar</span>                
            </div>
            <!-- <div class="col-md-6">
                <span class="caption-subject font-dark bold uppercase">Tipe Kamar Madinah</span>                
            </div> -->
        </div>

        <div class="row">
            <div class="form-group" id="toastTypeGroup">
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar double" value="Double" />Double
                        <span></span></label>
                        <!-- <input type="text" class="money" id="double"> -->
                        <input type="hidden" class="money lama_hari" id="lama_hari" name="lama_hari" value="0">
                        <input type="hidden" class="status_jenis_kamar" id="status_jenis_kamar" name="status_jenis_kamar" value="1">                   
                        <input type="hidden" class="status_jenis_kamar_madinah" id="status_jenis_kamar_madinah" name="status_jenis_kamar_madinah" value="1">                   

                    </div>        
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar triple" value="Triple" />Triple
                        <span></span></label>
                        <!-- <input type="text" class="money" id="triple"> -->
                    </div>        
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="jk_quad" name="jenis_kamar" class="jenis_kamar quad" value="Quad" />Quad
                        <span></span></label>
                        <!-- <input type="text" class="money" id="quad"> -->
                    </div>        
                </div>        
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="jenis_kamar" class="jenis_kamar quint" value="Quint" />Quint
                        <span></span></label>
                        <!-- <input type="text" class="money" id="quint"> -->
                    </div>        
                </div>  
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                    <label class="mt-radio mt-radio-outline">
                        <input type="radio" id="jk_no_bed" name="jenis_kamar" class="jenis_kamar no_bed" value="NoBed" />No Bed
                        <span></span></label>
                        <!-- <input type="text" class="money" id="quint"> -->
                    </div>        
                </div>  
                <div class="col-md-2">
                    <div class="form-group form-md-line-input">
                        <input readonly type="text" class="money jns_kmr_hrg_makkah form-control" id="jns_kmr_hrg_makkah" name="biaya_upgrade_kamar_makkah" value="0">                    
                        <label for="form_control_1">Biaya Upgrade
                        </label>
                        <input type="hidden" class="money jns_kmr_hrg_madinah form-control" id="jns_kmr_hrg_madinah" name="biaya_upgrade_kamar_madinah" value="0">

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pilih Hubungan Keluarga
                    </label>
                    <select class="form-control jenis_travel" id="hubungan" name="hubungan">
                        <option value="-">-</option>
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option value="Orang Tua">Orang Tua</option>
                        <option value="Saudara">Saudara</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                

                <div class="form-group">
                    <label>Pilih Nama Keluarga</label>
                    <select class="form-control pilih" name="keluarga_terdekat" data-placeholder="Pilih Keluarga Terdekat">
                        <option></option>
                        <?php
                        foreach ($formulir as $row) { ?>
                        <option value="<?php echo $row->nm_lengkap; ?>">NIK : <?php echo $row->no_ktp; ?> - <?php echo $row->nm_ktp; ?> / <?php echo $row->nm_lengkap; ?></option>                    
                        <?php
                        }
                        ?>
                    </select>
                </div>

                <!-- <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="keluarga_terdekat" placeholder="Masukkan Keluarga Terdekat">
                    <label for="form_control_1">Nama Keluarga Terdekat
                    </label>
                    <span class="help-block">Masukkan Nama Keluarga Terdekat</span>

                </div> -->
            </div>
        </div>

        <br>
        <span class="caption-subject font-dark bold uppercase">Kelengkapan Berkas</span>

        <div class="row">
            <br>            
            <div class="form-group">
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="1">
                        <input id="1" type="checkbox" value="1" name="fc_ktp" class="input-small" />FC KTP
                        <span></span>
                    </label>
                </div>        
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="2">
                        <input id="2" type="checkbox" value="1" name="fc_aktakelahiran" class="input-small" />FC Akta Kelahiran
                        <span></span>
                    </label>
                </div>   

                <div class="col-md-5">
                    <div class="form-group" id="toastTypeGroup">                        
                        <div class="col-md-6">
                            <label class="mt-radio mt-radio-outline">
                                <input type="radio" name="fc_bukunikah" value="1" />FC Buku Nikah
                                <span></span></label>
                                <!-- <input type="text" class="money" id="quad"> -->
                        </div>        
                        <div class="col-md-6">
                            <label class="mt-radio mt-radio-outline">
                                <input type="radio" name="fc_bukunikah" value="2" />Non Buku Nikah
                                <span></span></label><br>
                                <!-- <input type="text" class="money" id="quint"> -->
                        </div>     
                    </div>
                </div>

                <!-- <div class="col-md-2">
                    <label class="mt-checkbox mt-checkbox-outline" for="3">
                        <input id="3" type="checkbox" value="1" name="fc_bukunikah" class="input-small" />FC Buku Nikah
                        <span></span>
                    </label>
                </div> 
                <div class="col-md-2">
                    <label class="mt-checkbox mt-checkbox-outline" for="7">
                        <input id="7" type="checkbox" value="2" name="fc_bukunikah" class="input-small" /> Non Buku Nikah
                        <span></span>
                    </label>
                </div>  -->           
            </div>

            <div class="form-group">
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="4">
                        <input id="4" type="checkbox" value="1" name="pasport_asli" class="input-small" />Pasport Asli
                        <span></span>
                    </label>
                </div>        
                <div class="col-md-3">
                    <label class="mt-checkbox mt-checkbox-outline" for="5">
                        <input id="5" type="checkbox" value="1" name="buku_faksin_asli" class="input-small" />Buku Vaksin Asli
                        <span></span>
                    </label>
                </div>         
                <div class="col-md-2">
                    &nbsp;&nbsp;&nbsp;
                    <label class="mt-checkbox mt-checkbox-outline" for="6">
                        <input id="6" type="checkbox" value="1" name="fc_kk" class="input-small" />FC KK
                        <span></span>
                    </label>
                </div>
                <div class="col-md-4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="mt-checkbox mt-checkbox-outline" for="7">
                        <input id="7" type="checkbox" value="1" name="pas_photo" class="input-small" />Pas Foto
                        <span></span>
                    </label>
                </div>            
            </div>
        </div>
       
        <br>
        <span class="caption-subject font-dark bold uppercase">Barang Tambahan</span>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:30%">Nama Barang</th>
                            <th>Harga</th>
                            <th style="width:10%">Qty</th>
                            <th>Free</th>
                            <th>Sub Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="container">                        
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga</strong></td>
                            <td align="center">
                                <input type="text" name="total_bayar" class="form-control total-bayar tot-byr money" id="total-bayar" readonly="" value="0">
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
            </div>
        </div>

          
                    <input type="hidden" id="age" name="age">
                    <input type="hidden" id="gender" name="gender">
                    <input type="hidden" class="diskon" id="diskon" name="diskon" value="0">
                    

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control biaya-normal money" id="biaya-normal" readonly name="xx" placeholder="Total Biaya">
                    <!-- <input type="text" class="form-control total-biaya" readonly name="total_biaya" id="total_biaya"> -->
                    <label for="form_control_1">Biaya Normal
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control potongan money" id="potongan" <?php 
                    if (array_key_exists('Admin Finance',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) echo ""; else echo "readonly";
                    ?> name="potongan" placeholder="Potongan Harga Paket" value="0">
                    <!-- <input type="text" class="form-control total-biaya" readonly name="total_biaya" id="total_biaya"> -->
                    <label for="form_control_1">Potongan Harga Paket
                    </label>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control total-hrg-paket money" id="total-hrg-paket" readonly name="total_biaya" placeholder="Total Biaya">
                    <!-- <input type="text" class="form-control total-biaya" readonly name="total_biaya" id="total_biaya"> -->
                    <label for="form_control_1">Total Biaya
                    </label>
                </div>
            </div>
            
        </div>


    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>

<script>

function generate() {
  document.getElementById('no_pendaftaran').value = "<?php echo $no_pendaftaran; ?>";
}
</script>