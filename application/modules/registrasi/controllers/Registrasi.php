<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Registrasi extends AdminController {

        public function __construct()
        {
            parent::__construct();
            date_default_timezone_set('Asia/Makassar');
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");
            $this->bulan = date('m');
            $this->tahun = date('Y');
            $this->curdate = date('Y-m-d H:i:s');

            $this->dollar = 14000;

            // Module components            
            $this->data['module'] = 'Registrasi';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_registrasi');
            $this->load->model('M_barang_order');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components

            $this->data['pageTitle'] = 'Registrasi';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {

            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $jenis = $this->input->post('jenis',TRUE);
            $jenis_travel = $this->input->post('jenis_travel',TRUE);
            $nm_paket = $this->input->post('nm_paket',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);
            $no_ktp = $this->input->post('no_ktp',TRUE);
            $tgl_pendaftaran = $this->input->post('tgl_pendaftaran',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $nm_ktp = $this->input->post('nm_ktp',TRUE);
            $jenis_kamar = $this->input->post('jenis_kamar',TRUE);
            // $status = $this->input->post('status',TRUE);

            $cols = array();
            if (!empty($no_pendaftaran)) { $cols['pendaftar.no_pendaftaran'] = $no_pendaftaran; }    
            if (!empty($jenis)) { $cols['jenis'] = $jenis; }    
            if (!empty($jenis_travel)) { $cols['paket.jenis_travel'] = $jenis_travel; }    
            if (!empty($nm_paket)) { $cols['paket.nm_paket'] = $nm_paket; }    
            if (!empty($tgl_keberangkatan)) { $cols['jadwal.tgl_keberangkatan'] = $tgl_keberangkatan; }    
            if (!empty($no_ktp)) { $cols['formulir.no_ktp'] = $no_ktp; }    
            if (!empty($tgl_pendaftaran)) { $cols['pendaftar.tgl_pendaftaran'] = $tgl_pendaftaran; }    
            if (!empty($nm_lengkap)) { $cols['formulir.nm_lengkap'] = $nm_lengkap; }    
            if (!empty($nm_ktp)) { $cols['formulir.nm_ktp'] = $nm_ktp; }    
            if (!empty($jenis_kamar)) { $cols['formulir.jenis_kamar'] = $jenis_kamar; }    
            // if (!empty($status)) { $cols['status'] = $status; }    

            if(array_key_exists('Admin CS', $this->userLevel) or 
               array_key_exists('Admin Manivest', $this->userLevel) or
               array_key_exists('Admin Finance', $this->userLevel) or
               array_key_exists('Super Admin', $this->userLevel))  {

             $where = "(pendaftar.active = '1' OR pendaftar.active = '3')";
            } else {
             $where = "(pendaftar.active = '1' OR pendaftar.active = '3') AND pendaftar.kd_office = '$this->kode_cabang'";      
            }      


            $list = $this->M_registrasi->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_registrasi->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">';
                

                if(array_key_exists('Admin CS', $this->userLevel) or 
                   array_key_exists('Admin Manivest', $this->userLevel) or
                   array_key_exists('Admin Finance', $this->userLevel) or
                   array_key_exists('Super Admin', $this->userLevel))  {
                    $btn_action .= '
                                    <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body"     data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>';
                    $num_payment = $this->M_registrasi->get_payment(array('id_pendaftaran' => $r->id, 'active' => '1'))->num_rows();
                    if ($num_payment == 0) 
                    $btn_action .= '                
                                    <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>';
                } else {
                    if ($r->stts==0) $btn_action .= '
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body"     data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>';
                }
                $btn_action .= '</div>';

                if($r->fc_ktp == '1')
                {
                    $status_fc_ktp = 'V';
                    $label_fc_ktp = 'success';
                }else{
                    $status_fc_ktp = 'X';
                    $label_fc_ktp = 'danger';
                }
                //
                if($r->fc_aktakelahiran == '1')
                {
                    $status_fc_aktakelahiran = 'V';
                    $label_fc_aktakelahiran = 'success';
                }else{
                    $status_fc_aktakelahiran = 'X';
                    $label_fc_aktakelahiran = 'danger';
                }
                //
                if($r->fc_bukunikah == '1')
                {
                    $status_fc_bukunikah = 'V';
                    $label_fc_bukunikah = 'success';
                }else if($r->fc_bukunikah == '2') {
                    $status_fc_bukunikah = '-';
                    $label_fc_bukunikah = 'info';
                }else{
                    $status_fc_bukunikah = 'X';
                    $label_fc_bukunikah = 'danger';
                }
                //
                if($r->buku_faksin_asli == '1')
                {
                    $status_buku_faksin_asli = 'V';
                    $label_buku_faksin_asli = 'success';
                }else{
                    $status_buku_faksin_asli = 'X';
                    $label_buku_faksin_asli = 'danger';
                }
                //
                if($r->pasport_asli == '1')
                {
                    $status_pasport_asli = 'V';
                    $label_pasport_asli = 'success';
                }else{
                    $status_pasport_asli = 'X';
                    $label_pasport_asli = 'danger';
                }
                //
                if($r->fc_kk == '1')
                {
                    $status_fc_kk = 'V';
                    $label_fc_kk = 'success';
                }else{
                    $status_fc_kk = 'X';
                    $label_fc_kk = 'danger';
                }
                //
                if($r->pas_photo == '1')
                {
                    $status_pas_photo = 'V';
                    $label_pas_photo = 'success';
                }else{
                    $status_pas_photo = 'X';
                    $label_pas_photo = 'danger';
                }
                //
                if ($r->active == '1') {
                    if($r->status == 'Lunas')
                    {
                        $active = 'Lunas';
                        $label = 'success';
                    }else if($r->status == 'Registrasi'){
                        $active = 'Registrasi';
                        $label = 'danger';
                    }else if($r->status == 'Belum Lunas'){
                        $active = 'Belum Lunas';
                        $label = 'danger';
                    }else if($r->status == 'Refund'){
                        $active = 'Refund';
                        $label = 'danger';
                    }
                    $bt='';
                } else if ($r->active == '3') {
                    $active = 'Waiting Approve CS';
                    if(array_key_exists('Admin CS', $this->userLevel) OR array_key_exists('Super Admin', $this->userLevel))
                    $bt='<button class="btn btn-xs btn-outline red btn-approve" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-check"></i> Approve</button>'; else 
                    $bt='';
                    $label = 'warning';
                }


                if ($r->tgl_keberangkatan == '0000-00-00') {
                    $j='Paket Tabungan';
                } else {
                    $j=date('d-m-Y', strtotime($r->tgl_keberangkatan));
                }

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    //$r->jenis_travel,
                    $r->jenis,
                    $x=$r->nm_paket.'|'.$r->nm_jadwal,
                    $r->no_pendaftaran,
                    $j,
                    // $r->no_ktp,
                    $r->nm_ktp,
                    $r->nm_lengkap,
                    //$x=date('d-m-Y', strtotime($r->tgl_lahir)),
                    //$x=date('d-m-Y', strtotime($r->tgl_pendaftaran)),
                    //$r->nm_mahram,
                   // $r->hp_mahram,
                    //$r->hub_mahram,
                    //$r->keluarga_terdekat,
                    //$r->hubungan,
                    // $a="Rp." . number_format($r->biaya_mahram,0,',','.'),
                    // $b="Rp." . number_format($r->biaya_progresif,0,',','.'),
                    // $r->hotel_b4,
                    // $c="Rp." . number_format($r->biaya_up_hotel_b4,0,',','.'),
                    // $r->hotel_b5,
                    // $d="Rp." . number_format($r->biaya_up_hotel_b5,0,',','.'),
                    $x='MK:'.$r->jenis_kamar.' MD:'.$r->jenis_kamar_madinah,
                    // $e="Rp." . number_format($r->biaya_upgrade_kamar,0,',','.'),
                    // $f="Rp." . number_format($r->biaya_order_barang,0,',','.'),
                    '<span class="label label-sm label-'.$label_fc_ktp.'">'.$status_fc_ktp.'</span>',
                    '<span class="label label-sm label-'.$label_fc_aktakelahiran.'">'.$status_fc_aktakelahiran.'</span>',
                    '<span class="label label-sm label-'.$label_fc_bukunikah.'">'.$status_fc_bukunikah.'</span>',
                    '<span class="label label-sm label-'.$label_fc_kk.'">'.$status_fc_kk.'</span>',
                    '<span class="label label-sm label-'.$label_buku_faksin_asli.'">'.$status_buku_faksin_asli.'</span>',
                    '<span class="label label-sm label-'.$label_pasport_asli.'">'.$status_pasport_asli.'</span>',
                    '<span class="label label-sm label-'.$label_pas_photo.'">'.$status_pas_photo.'</span>',
                    $g="Rp." . number_format($r->total_biaya,0,',','.'),
                    '<span class="label label-sm label-'.$label.'">'.$active.'</span><br>'.$bt,
                    //$hotel_makkah->nm_hotel, label label-sm label-info
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        public function load_detail()
        {

            $id = $this->input->get('id');

            $data['main'] = $this->M_registrasi->get_detail($id);

            return response($this->load->view('detail_', $data, TRUE), 'html');
        }

        public function load_add_form()
        {
            $data['title'] = 'Add Data Registrasi';
            $data['packet'] = Modules::run('packet/get_where', array('paket.active' => '1','kd_cabang'=>$this->kode_cabang,'use'=>'1'))->result();    
            // $data['workarea'] = $this->M_workarea->get()->result();     
            $data['pesawat'] = Modules::run('pesawat/get_where', array('pesawat.active' => '1'))->result();
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();

            if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel) or array_key_exists('Admin', $this->userLevel)){
                $data['formulir'] = $this->M_registrasi->get_formulir_where('formulir.active="1"')->result();
            } else {
                $data['formulir'] = $this->M_registrasi->get_formulir_where('formulir.active="1" AND marketing.kd_cabang="'. $this->kode_cabang.'"')->result();
            }
            $cek = $this->M_registrasi->get_where(array('kd_office' => $this->kode_cabang,'active' => '1'))->num_rows();
            $data['no_pendaftaran'] = $this->kode_cabang.'-'.date('d').date('m').date("y").sprintf('%03d',$cek+1);

            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function add()
        {

            $this->ajaxRequest();
            // $this->validateInput();
            $main = $this->M_registrasi->get_where(array('id_formulir' => $this->input->post('id_formulir'),'id_paket' => $this->input->post('id_paket'),'active' => '1'))->num_rows();
            if ($main == 0) {
                if(array_key_exists('Admin CS', $this->userLevel) OR array_key_exists('Super Admin', $this->userLevel)){
                    if ($this->input->post('id_paket') == '1') {
                        $jenis='Tabungan'; 
                        $active = '1';         
                    } else {
                        $jenis='Reguler';
                        $active = '1';         
                    }   
                } else {
                    if ($this->input->post('id_paket') == '1') {
                        $jenis='Tabungan'; 
                        $active = '3';         
                    } else {
                        $jenis='Reguler';
                        $active = '1';         
                    } 
                }
                $data = array(
                    'id_formulir' => $this->input->post('id_formulir',TRUE),
                    'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                    'jenis_travel' => $this->input->post('jenis_travel',TRUE),
                    'id_paket' => $this->input->post('id_paket',TRUE),
                    'potongan_paket' => preg_replace("/[^0-9\.]/", "", $this->input->post('potongan',TRUE)),
                    'tgl_pendaftaran' => $this->input->post('tgl_pendaftaran',TRUE),
                    'nm_mahram' => $this->input->post('nm_mahram',TRUE),
                    'hp_mahram' => $this->input->post('hp_mahram',TRUE),
                    'hub_mahram' => $this->input->post('hub_mahram',TRUE),
                    'keluarga_terdekat' => $this->input->post('keluarga_terdekat',TRUE),
                    'hubungan' => $this->input->post('hubungan',TRUE),
                    'biaya_mahram' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_mahram',TRUE)),
                    'biaya_progresif' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_progresif',TRUE)),
                    // 'id_up_hotel_b4' => $this->input->post('id_up_hotel_b4',TRUE),
                    // 'id_up_hotel_b5' => $this->input->post('id_up_hotel_b5',TRUE),
                    'jenis_kamar' => $this->input->post('jenis_kamar',TRUE),
                    'jenis_kamar_madinah' => 'Quad',
                    'biaya_upgrade_kamar' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_upgrade_kamar_makkah',TRUE)),
                    'status_upgrade_kamar' => $this->input->post('status_jenis_kamar',TRUE),
                    'biaya_upgrade_kamar_madinah' => '0',
                    'status_upgrade_kamar_madinah' => '0',
                    'biaya_order_barang' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('total_bayar',TRUE)),
                    'add_on' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('add_on',TRUE)),
                    'fc_ktp' => $this->input->post('fc_ktp',TRUE),
                    'fc_aktakelahiran' => $this->input->post('fc_aktakelahiran',TRUE),
                    'fc_bukunikah' => $this->input->post('fc_bukunikah',TRUE),
                    'buku_faksin_asli' => $this->input->post('buku_faksin_asli',TRUE),
                    'pasport_asli' => $this->input->post('pasport_asli',TRUE),
                    'fc_kk' => $this->input->post('fc_kk',TRUE),
                    'pas_photo' => $this->input->post('pas_photo',TRUE),
                    //'total_biaya' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_biaya',TRUE)),
                    'hotel_request_makkah' => $this->input->post('hotel_request_makkah',TRUE),                
                    'biaya_up_hotel_makkah' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_request_makkah',TRUE)),
                    'status_up_hotel_makkah' => $this->input->post('status_request_makkah',TRUE),                
                    'overpayment' => preg_replace("/[^0-9\.]/", "", $this->input->post('overpayment',TRUE)),               
                    'hotel_request_madinah' => 'Default',                
                    'biaya_up_hotel_madinah' =>  '0',
                    'status_up_hotel_madinah' => '0',                
                    'active' => $active,
                    'jenis' => $jenis,
                    'id_user' => $this->currentUser->id,
                    'kd_office' => $this->kode_cabang,
                    );

                $query = $this->M_registrasi->_insert($data);

                $rowPaket = Modules::run('packet/get_where', array('paket.id'=>$this->input->post('id_paket',TRUE)))->row();   

                $data_notif = array(
                        'user_id' => $this->currentUser->id,
                        'for_user' => '3',
                        'activity_type' => 'Regist '.$this->input->post('no_pendaftaran',TRUE).' Paket '.$rowPaket->nm_paket.' '.$rowPaket->nm_jadwal,
                        'activity_uri_1' => 'registrasi',
                        'activity_time' => $this->curdate,
                        'deleted' => '1',
                        'kdoffice' => $this->kode_cabang,
                        'reading' => '1',
                        );
                $query_notif = $this->M_registrasi->_insert_notif($data_notif);

                if ($this->input->post('status_jenis_kamar') == '0' or $this->input->post('status_request_makkah') == '0') {
                    $row_kamar = $this->M_registrasi->get_paket(array('id' => $this->input->post('id_paket')))->row();
                    $data_notif_kamar = array(
                            'user_id' => $this->currentUser->id,
                            'for_user' => '5',
                            'activity_type' => 'Upgrade Hote/Kamar '.$this->input->post('no_pendaftaran',TRUE),
                            'activity_uri_1' => 'request/schedule/'.$row_kamar->id_jadwal,
                            'activity_time' => $this->curdate,
                            'deleted' => '1',
                            'kdoffice' => $this->kode_cabang,
                            'reading' => '1',
                            );
                    $query_notif = $this->M_registrasi->_insert_notif($data_notif_kamar);
                }


                if ($this->input->post('nm_barang') != null) {
                    $cek = count($this->input->post('nm_barang'));
                    for ($i=0; $i < $cek; $i++) { 
                        $data_order_detail = array(
                            'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                            'id_barang' => $this->input->post('nm_barang',TRUE)[$i],
                            'harga_satuan' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga_jual',TRUE))[$i],
                            'jml' => $this->input->post('jml',TRUE)[$i],
                            'free' => $this->input->post('sts_free',TRUE)[$i],
                            'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                            'active' => '1',
                            'kd_office' => $this->kode_cabang,
                            );

                        $query1 = $this->M_barang_order->_insert($data_order_detail);

                        // $where = array(
                        // 'id' => $this->input->post('nm_barang',TRUE)[$i],
                        // );

                        // $data = array(
                        // 'stok' => $this->input->post('cekstok',TRUE)[$i] - $this->input->post('jml',TRUE)[$i],
                        // );
                        // $querystok = Modules::run('baggage/get_update_stok', $where, $data);
                    }
                }
                $query=true; 
            } else $query=false;
            // Check if query was success
            if ($query == true) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan, terdapat indikasi data duplikat');
            }
            
            return response($response, 'json');
        }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $no_pendaftaran = $this->input->get('no_pendaftaran');
            $data['main'] = $this->M_registrasi->get_detail($id);
            $main = $this->M_registrasi->get_where(array('id' => $id))->row();
            if(array_key_exists('Admin CS', $this->userLevel) or 
               array_key_exists('Admin Manivest', $this->userLevel) or
               array_key_exists('Admin Finance', $this->userLevel) or
               array_key_exists('Super Admin', $this->userLevel))  {
               $data['packet'] = Modules::run('packet/get_where', array('paket.active' => '1'))->result();    
            } else {
               $data['packet'] = Modules::run('packet/get_where', array('paket.active' => '1','kd_cabang'=>$this->kode_cabang))->result();
            }
            // $data['workarea'] = $this->M_workarea->get()->result();     
            $data['pesawat'] = Modules::run('pesawat/get_where', array('pesawat.active' => '1'))->result();
            $data['hotel'] = Modules::run('hotel/get_hotel_where', array('hotel.active' => '1'))->result();

            $data['formulir'] = $this->M_registrasi->get_formulir_where('formulir.active="1"')->result();
            $data['barang'] = $this->M_barang_order->get_order_formulir($main->no_pendaftaran);
            $cek = $this->M_registrasi->get_where(array('kd_office' => $this->kode_cabang,'active' => '1'))->num_rows();
            $data['no_pendaftaran'] = $this->kode_cabang.'-'.date('d').date('m').date("y").sprintf('%03d',$cek+1);
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            $id = $this->input->post('id');
            $row = $this->M_registrasi->get_detail($id);

             if ($this->input->post('id_paket') == '1') $jenis='Tabungan'; else
             if ($row->jenis == 'Tabungan' && $this->input->post('id_paket') != '1') $jenis='Tabungan'; else           
             if ($row->jenis == 'Reguler' && $this->input->post('id_paket') != '1') $jenis='Reguler';           

            $data = array(
                'id_formulir' => $this->input->post('id_formulir',TRUE),
                'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                'jenis_travel' => $this->input->post('jenis_travel',TRUE),
                'id_paket' => $this->input->post('id_paket',TRUE),
                'potongan_paket' => preg_replace("/[^0-9\.]/", "", $this->input->post('potongan_paket',TRUE)),
                'tgl_pendaftaran' => $this->input->post('tgl_pendaftaran',TRUE),
                'nm_mahram' => $this->input->post('nm_mahram',TRUE),
                'hp_mahram' => $this->input->post('hp_mahram',TRUE),
                'hub_mahram' => $this->input->post('hub_mahram',TRUE),
                'keluarga_terdekat' => $this->input->post('keluarga_terdekat',TRUE),
                'hubungan' => $this->input->post('hubungan',TRUE),
                'biaya_mahram' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_mahram2',TRUE)),
                'biaya_progresif' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_progresif',TRUE)),
                // 'id_up_hotel_b4' => $this->input->post('id_up_hotel_b4',TRUE),
                // 'id_up_hotel_b5' => $this->input->post('id_up_hotel_b5',TRUE),
                'jenis_kamar' => $this->input->post('jenis_kamar',TRUE),
                'jenis_kamar_madinah' => 'Quad',
                'biaya_upgrade_kamar' => preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_upgrade_kamar_makkah',TRUE)),
                'status_upgrade_kamar' => $this->input->post('status_jenis_kamar',TRUE),
                'biaya_upgrade_kamar_madinah' => '0',
                'status_upgrade_kamar_madinah' => '0',
                'biaya_order_barang' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('total_bayar',TRUE)),
                'add_on' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('add_on',TRUE)),
                'fc_ktp' => $this->input->post('fc_ktp',TRUE),
                'fc_aktakelahiran' => $this->input->post('fc_aktakelahiran',TRUE),
                'fc_bukunikah' => $this->input->post('fc_bukunikah',TRUE),
                'buku_faksin_asli' => $this->input->post('buku_faksin_asli',TRUE),
                'pasport_asli' => $this->input->post('pasport_asli',TRUE),
                'fc_kk' => $this->input->post('fc_kk',TRUE),
                'pas_photo' => $this->input->post('pas_photo',TRUE),
                //'total_biaya' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_biaya',TRUE)),
                'hotel_request_makkah' => $this->input->post('hotel_request_makkah',TRUE),                
                'biaya_up_hotel_makkah' =>  preg_replace("/[^0-9\.]/", "", $this->input->post('biaya_request_makkah',TRUE)),
                'status_up_hotel_makkah' => $this->input->post('status_request_makkah',TRUE),                
                'overpayment' => preg_replace("/[^0-9\.]/", "", $this->input->post('overpayment',TRUE)),               
                'hotel_request_madinah' => 'Default',                
                'biaya_up_hotel_madinah' =>  '0',
                'status_up_hotel_madinah' => '0', 
                'Jenis' => $jenis,
                'id_user_lastupdate' => $this->currentUser->id,                
                'user_update' => $this->currentUser->id,                
                );

            $num_row_payment = $this->M_registrasi->get_payment(array('id_pendaftaran' => $id, 'active' => '1'))->num_rows();
            if ($num_row_payment == 0 && $jenis == 'Tabungan') {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Jamaah belum melakukan pembayaran tabungan');
                } else {
                    $query = $this->M_registrasi->_update(array('id' => $id), $data);  
                    $query_del = $this->M_barang_order->delete_by_id($this->input->post('no_pendaftaran'));
                    if ($this->input->post('nm_barang') != null) {
                        $cek = count($this->input->post('nm_barang'));

                        for ($i=0; $i < $cek; $i++) { 
                            $data_order_detail = array(
                                'no_pendaftaran' => $this->input->post('no_pendaftaran',TRUE),
                                'id_barang' => $this->input->post('nm_barang',TRUE)[$i],
                                'harga_satuan' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga_jual',TRUE))[$i],
                                'jml' => $this->input->post('jml',TRUE)[$i],
                                'free' => $this->input->post('sts_free',TRUE)[$i],
                                'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                                'active' => '1',
                                'kd_office' => $this->kode_cabang,
                                );

                            $query1 = $this->M_barang_order->_insert($data_order_detail);
                        }

                    }
                    if ($query) {
                        $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
                    } else {
                        $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
                    }
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_registrasi->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function approve()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '1',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_registrasi->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

        public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules 
            $this->form_validation->set_rules('jenis_travel', 'jenis_travel', 'trim|required');
            $this->form_validation->set_rules('tgl_pendaftaran', 'tgl_pendaftaran', 'trim|required');
            $this->form_validation->set_rules('jenis_kamar', 'jenis_kamar', 'trim|required');
            $this->form_validation->set_rules('id_paket', 'id_paket', 'trim|required');
            

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function get_nm_packet()
        {
            $jenis_travel = $this->input->post('id');
            $data = Modules::run('packet/get_where', array('paket.active' => '1','paket.jenis_travel' => $jenis_travel,'paket.kd_cabang' => $this->kode_cabang,'use' => '1'))->result();
            echo json_encode($data);
        }

        public function get_hrg_paket()
        {
            $id = $this->input->post('id');
            $data = Modules::run('packet/get_where', array('paket.id' => $id))->row();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_paket_barang()
        {
            $id = $this->input->post('id');
            $data = $this->M_barang_order->get_paket_barang($id);
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b4);
            $data = Modules::run('hotel/get_hotel_where_in', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel_b5()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b5);
            $data = Modules::run('hotel/get_hotel_where_in', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel_b4_makkah()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b4);
            $data = Modules::run('hotel/get_hotel_where_in_makkah', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel_b5_makkah()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b5);
            $data = Modules::run('hotel/get_hotel_where_in_makkah', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel_b4_madinah()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b4);
            $data = Modules::run('hotel/get_hotel_where_in_madinah', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hotel_b5_madinah()
        {
            $paket_id = $this->input->post('id');
            $id_packet = Modules::run('packet/get_where', array('paket.id' => $paket_id))->row();
            $data_explode = explode(",", $id_packet->id_up_hotel_b5);
            $data = Modules::run('hotel/get_hotel_where_in_madinah', $data_explode)->result();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_hrg_hotel()
        {
            $id = $this->input->post('id');
            $data = Modules::run('hotel/get_hotel_where', array('id' => $id))->row();
            echo json_encode($data);
        }

        public function load_export()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function load_export_where()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            $data['workarea'] = Modules::run('workarea/_get_where', array('active' => '1'))->result();    
            return response($this->load->view('export_where', $data, TRUE), 'html');
        }

        public function load_export_excel()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export_excel', $data, TRUE), 'html');
        }

        public function cetak_excel()
        {
            $id = $this->input->post('id_jadwal');
            $data['main'] = $this->M_registrasi->get_export_excel($id);
            $this->load->view('registrasi/print_view_excel', $data);
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');
            //$data['main'] = $id;
            $data['main'] = $this->M_registrasi->get_jadwal_exp(array('id' => $id ))->row(); 

            if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Admin CS', $this->userLevel)) {
                $data['detail'] = $this->M_registrasi->get_export(array('active' => '1','id_jadwal' => $id,'jenis_travel' => 'UMRAH' ))->result();
            } else {
                $data['detail'] = $this->M_registrasi->get_export(array('active' => '1','id_jadwal' => $id,'kd_office' => "$this->kode_cabang",'jenis_travel' => 'UMRAH'))->result();
            }    


            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('registrasi/print_export', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_where()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $star = $this->input->post('date_star');
            $end = $this->input->post('date_to');
            $id_jadwal = $this->input->post('id_jadwal');
            $kd_cabang = $this->input->post('cabang');
            $jenis = $this->input->post('jenis');
            //$data['main'] = $id;

            if ($kd_cabang == 'All') $cabang = ''; else $cabang = ' kd_office = "'.$kd_cabang.'" AND';
            if ($id_jadwal == 'All') $jadwal = ''; else $jadwal = ' id_jadwal="'.$id_jadwal.'" AND';
            if ($jenis == 'All') $jenis_travel = ''; else $jenis_travel = ' jenis="'.$jenis.'" AND';

            $where = $cabang.$jadwal.$jenis_travel.' tgl_pendaftaran BETWEEN "'.$star.'" AND "'.$end.'"';

            $data['star']  = $this->input->post('date_star');
            $data['end']  = $this->input->post('date_to');
            
            

            $data['detail'] = $this->M_registrasi->get_export($where.' AND jenis_travel = "UMRAH"')->result();
            $data['group'] = $this->M_registrasi->get_export_group($where.' AND jenis_travel = "UMRAH"')->result();
             


            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('registrasi/print_export_where', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->setFooter('{PAGENO} dari {nb}');

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function get_umur_formulir()
        {
            $id = $this->input->post('id');
            // $data = Modules::run('packet/get_where', array('paket.jenis_travel' => $jenis_travel,'paket.kd_cabang' => $this->kode_cabang))->result();
            $data = $this->M_registrasi->get_umur($id);
            echo json_encode($data);
        }

        public function get_formulir()
        {
            $id = $this->input->post('id');
            $data = $this->M_registrasi->get_formulir_where('formulir.id = "'.$id.'"')->row();
            echo json_encode($data);
        }

    }
?>
