<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Group_manivest_detail extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Group Manivest Detail';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_group_manivest_detail');
            // $this->load->model('M_movement');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function group_manivest()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Group Manivest Detail';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['idmovement'] = $this->uri->segment(3);
            $this->data['group_manivest'] = $this->M_group_manivest_detail->get_group_manivest(array('id' => $this->uri->segment(3)))->row();
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
			// $id_movement = $this->input->post('id_movement',TRUE);
            $jam = $this->input->post('jam',TRUE);
            $agenda = $this->input->post('agenda',TRUE);
            
            
            $cols = array();
			// if (!empty($id_movement)) { $cols['id_movement'] = $id_movement; }
			if (!empty($jam)) { $cols['jam'] = $jam; }
			if (!empty($agenda)) { $cols['agenda'] = $agenda; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
              $where = "pendaftar.active = '1' AND jadwal.id = '".$id."'";
            } else {
              $where = "pendaftar.active = '1' AND jadwal.id = '".$id."'";
            }


	        $list = $this->M_group_manivest_detail->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_group_manivest_detail->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                // <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                $btn_action='';
                if (array_key_exists('Admin Manivest',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) 
                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id_formulir.'"><i class="fa fa-edit"></i></button>
                             </div>';
                if($r->stts == '1')
                {
                    $status = 'OK';
                    $label = 'success';
                    $checkbox = '<span class="label label-sm label-'.$label.'">'.$status.'</span>';
                }else{
                    $checkbox = '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="checkbox[]" type="checkbox" class="checkboxes" value="'.$r->id_pendaftar.'"/><span></span></label>';
                    $status = 'Belum Lunas';
                    $label = 'danger';
                }

                $btn_action.='';

                $records["data"][] = array(
                    $no,                    
                    $r->title,
                    // $r->no_pendaftaran,
                    $r->nm_lengkap,
                    $r->no_pasport,
                    $r->tgl_lahir,
                    $r->tmp_lahir,
                    $r->create_pasport,
                    $r->exp_pasport,
                    $r->kota_paspor,
                    // $r->nm_cabang,
                    // $r->age,
                    // $r->hubungan,
                    $r->nm_marketing,
                    $checkbox.''.$btn_action,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

        function update_multiple()
        {
            // db transaction 
            $this->db->trans_begin();

            // prospect detail
            $totdata = $this->input->post('checkbox');
            if($totdata != null) {
                // $this->load->model('prospect/m_prospect_detail');
                foreach ($totdata as $key => $value) {
                    $data = array(
                            'stts' => '1',
                            'user_update' => $this->currentUser->id,
                    );

                    $query = $this->M_group_manivest_detail->_update(array('id' => $this->input->post('checkbox')[$key]), $data);
                }
            }else { $queryDetail = TRUE; }

            // db transaction
            // $this->db->trans_complete();

            // Check if query was success
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                // generate an error... or use the log_message() function to log your error
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }else{
                $this->db->trans_commit();
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Update successfully');
            }

            return response($response, 'json');

        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
			$data['title'] = 'Tambah Data Movedet';
            $data['movement'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			// $this->validateInput();

			$data = array(
				'id_movement' => $this->input->post('id_movement',TRUE),
                'jam' => $this->input->post('jam',TRUE),
                'agenda' => $this->input->post('agenda',TRUE),
                'active' => '1',
				);

			$query = $this->M_group_manivest_detail->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_group_manivest_detail->get_where_form(array('id' => $id))->row();
            return response($this->load->view('edit_formulir_mini', $data, TRUE), 'html');
        }

        public function edit_mini()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id = $this->input->post('id');
            
                $data = array(
                'nm_lengkap' => $this->input->post('nm_lengkap',TRUE),
                'tmp_lahir' => $this->input->post('tmp_lahir',TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
                'jk' => $this->input->post('jk',TRUE),
                'create_pasport' => $this->input->post('create_pasport',TRUE),
                'no_pasport' => $this->input->post('no_pasport',TRUE),
                'exp_pasport' => $this->input->post('exp_pasport',TRUE),
                'kota_paspor' => $this->input->post('kota_paspor',TRUE),
                );
            $query = $this->M_group_manivest_detail->_update_formulir(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_group_manivest_detail->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            //id_movement,jam,agenda
            $this->form_validation->set_rules('id_movement', 'nm hotel', 'trim|required');
            $this->form_validation->set_rules('jam', 'kota lokasi', 'trim|required');
            $this->form_validation->set_rules('agenda', 'alamat', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

    }
?>
