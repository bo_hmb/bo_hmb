<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Mainivest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Manivest Detail</span>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Group Manivest <?php  echo $group_manivest->group_code ?></span>
                        </div>
                        
                    </div>
                     <script>
                    id_group = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="table-container">                            
                            <form action="#" id="form-cb" method="POST">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="table-actions-wrapper">
                                        <?php if (array_key_exists('Admin Manivest',$this->userLevel) or array_key_exists('Super Admin',$this->userLevel)) { ?>
                                        <button class="btn green table-group-action-submit submit"><i class="fa fa-check"></i> Update </button>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div> 
                                <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="5%">No</th>
                                            <th width="1%">Title</th>
                                            <!-- <th width="10%">No. Registrasi</th> -->
                                            <th width="10%">Nama Paspor</th>
                                            <th width="10%">No.Pasport</th>
                                            <th width="10%">Tggl. Lahir</th>
                                            <th width="10%">Tempat Lahir</th>
                                            <th width="10%">Tanggal Issued</th>
                                            <th width="10%">Expired Pasport</th>
                                            <th width="10%">Kota Issued</th>
                                            <!-- <th width="10%">Cabang</th> -->
                                            <!-- <th width="10%">Umur</th> -->
                                            <!-- <th width="10%">Hubungan Terdekat</th> -->
                                            <th width="10%">Marketing</th>
                                            <th width="1%">Aksi</th>
                                        </tr>
                                        <tr role="row" class="filter">

                                            <td> </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="margin-bottom-5">
                                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                        <i class="fa fa-search"></i></button>
                                                </div>
                                                <button class="btn btn-sm red btn-outline filter-cancel">
                                                    <i class="fa fa-times"></i></button>
                                                <select name="stts" class="form-control form-filter input-sm">
                                                    <option disabled selected></option>
                                                    <option value="0">Belum</option>
                                                    <option value="1">Aman</option>
                                                </select>
                                            </td>

                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>