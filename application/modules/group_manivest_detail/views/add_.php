<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            
            <input type="text" class="hidden" name="id_movement" value="<?php echo $movement?>">

            <div class="col-md-4">
                <div class="form-group">
                        <span class="help-block"> Masukkan Jam </span>
                    <div class="input-group">
                        <input type="text" id="clockface_2" value="00:00" class="form-control" readonly="" name="jam"/>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" id="clockface_2_toggle">
                                <i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="agenda" placeholder="Masukkan Agenda">
                    <label for="form_control_1">Agenda
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Agenda</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>