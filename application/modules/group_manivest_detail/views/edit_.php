<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            
            <input type="text" class="hidden" name="id_movement" value="<?php echo $main->id?>">

            <div class="col-md-4">
                <div class="form-group">
                        <span class="help-block"> Masukkan Jam </span>
                    <div class="input-group">
                        <input type="text" id="clockface_2" value="<?php echo $main->jam ?>" class="form-control" readonly="" name="jam"/>
                        <span class="input-group-btn">
                            <button class="btn default" type="button" id="clockface_2_toggle">
                                <i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="agenda" placeholder="Masukkan Agenda" value="<?php echo $main->agenda ?>">
                    <label for="form_control_1">Agenda
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Agenda</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>