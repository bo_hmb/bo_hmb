<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_group_manivest_detail extends CI_Model 
	{
		

		var $table = 'pendaftar';
		var $formulir = 'formulir';
		var $paket = 'paket';
		var $jadwal = 'jadwal';
		var $marketing = 'marketing';
		var $cabang = 'cabang';

	    var $order = array('no_pendaftaran' => 'ASC'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_form($where) {
			$table = $this->formulir;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function _update_formulir($where, $data) {
			$table = $this->formulir;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' pendaftar.id AS id_pendaftar,
	    						jadwal.id,
								(SELECT 
											IF(
												(TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan))<=144 AND 
												(TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan)) >24,"CHILD",
												
													IF((TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan))<=24,"INFANT",
														IF(formulir.jk="L","MR","MRS"))
													
												)
										) AS title,
								formulir.id AS id_formulir, 
								formulir.nm_lengkap,
								formulir.jk,
								formulir.no_pasport,
								formulir.tgl_lahir,
								formulir.tmp_lahir,
								formulir.exp_pasport,
								formulir.tgl_lahir,
								formulir.create_pasport,
								formulir.kota_paspor,
								cabang.nm_cabang,
								(YEAR(CURDATE())-YEAR(formulir.tgl_lahir)) AS age,
								pendaftar.no_pendaftaran,
								pendaftar.hubungan,
								marketing.nm_marketing,
								pendaftar.tgl_pendaftaran,
								pendaftar.stts,
								pendaftar.active
								');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// join table
	    	$this->db->join($this->formulir, ''.$this->table.'.id_formulir = '.$this->formulir.'.id');
	    	$this->db->join($this->paket, ''.$this->table.'.id_paket = '.$this->paket.'.id');
	    	$this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	    	$this->db->join($this->marketing, ''.$this->formulir.'.id_marketing = '.$this->marketing.'.id');
	    	$this->db->join($this->cabang, ''.$this->table.'.kd_office = '.$this->cabang.'.kd_cabang');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from('pendaftar,jadwal');	       
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_group_manivest() {
			$this->db->order_by('id');
			$query=$this->db->get('group_manivest');
			return $query;
		}
	}
