<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_spending extends CI_Model 
	{
		

		var $table = 'pengeluaran';
		var $pengeluaran_detail = 'pengeluaran_detail';
		var $jadwal = 'jadwal';

	    var $column_order = array('id','no_faktur','tgl_faktur','jenis','diterima','total_biaya');
	    var $column_search = array('id','no_faktur','tgl_faktur','jenis','diterima','total_biaya');
	    var $order = array('tgl_faktur' => 'desc'); 

		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_sum() {
			$table = $this->table;
			$query=$this->db->get($table);
			return $query;
		}

		public function get_detail($where) {
			$table = $this->pengeluaran_detail;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_cetak($id) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.nm_jadwal,pengeluaran.no_faktur,pengeluaran.tgl_faktur,
										pengeluaran.jenis,pengeluaran.diterima,pengeluaran.total_biaya,
										pengeluaran.active FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE pengeluaran.id="'.$id.'"');
	        return $query->row();
		}

		public function get_schedule() {
			$results = array();
	        $query = $this->db->query(' SELECT
										jadwal.id,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										jadwal.lama_hari,
										jadwal.active,
										paket.nm_paket
										FROM
										jadwal
										INNER JOIN paket ON jadwal.id = paket.id_jadwal
										WHERE
										jadwal.active = 1 AND paket.kd_cabang="MKS"');
	        return $query;
		}

		public function get_by_schedule($where) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pengeluaran_detail.id,
										pengeluaran.id_jadwal,
										jadwal.nm_jadwal,
										pengeluaran_detail.no_faktur,
										pengeluaran.tgl_faktur,
										pengeluaran.jenis,
										pengeluaran.diterima,
										pengeluaran_detail.item,
										pengeluaran_detail.deskripsi,
										pengeluaran_detail.harga,
										pengeluaran_detail.jml,
										pengeluaran_detail.sub_harga,
										pengeluaran.from_bank,
										pengeluaran_detail.active
										FROM
										pengeluaran_detail
										INNER JOIN pengeluaran ON pengeluaran_detail.no_faktur = pengeluaran.no_faktur
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE pengeluaran.active="1" AND pengeluaran_detail.active="1" '.$where.' 
										GROUP BY pengeluaran_detail.id
										ORDER BY pengeluaran.tgl_faktur,pengeluaran_detail.no_faktur
										');
	        return $query->result();
		}

		public function get_by_date($start,$to,$where) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pengeluaran_detail.id,
										pengeluaran.id_jadwal,
										pengeluaran_detail.no_faktur,
										pengeluaran.tgl_faktur,
										pengeluaran.jenis,
										pengeluaran.diterima,
										pengeluaran_detail.item,
										pengeluaran_detail.deskripsi,
										pengeluaran_detail.harga,
										pengeluaran_detail.jml,
										pengeluaran.from_bank,
										pengeluaran_detail.sub_harga,
										pengeluaran_detail.active,
										jadwal.nm_jadwal
										FROM
										pengeluaran_detail
										INNER JOIN pengeluaran ON pengeluaran_detail.no_faktur = pengeluaran.no_faktur
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id
										WHERE pengeluaran.tgl_faktur BETWEEN "'.$start.'" AND "'.$to.'" AND pengeluaran.active="1" AND pengeluaran_detail.active = "1" '.$where.'
										GROUP BY pengeluaran_detail.id
										ORDER BY pengeluaran.tgl_faktur,pengeluaran_detail.no_faktur
										');
	        return $query->result();
		}

		public function get_sum_schedule($where) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.id AS id_jadwal,jadwal.nm_jadwal,
										SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE pengeluaran.status="1" AND pengeluaran.active="1" '.$where.'');
	        return $query->row();
		}

		public function get_sum_schedule_group($where) {
			$results = array();
	        $query = $this->db->query(' SELECT jenis,SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE pengeluaran.status="1" AND pengeluaran.active="1" '.$where.'
										GROUP BY jenis');
	        return $query->result();
		}

		public function get_sum_date($start,$to,$where) {
			$results = array();
	        $query = $this->db->query(' SELECT pengeluaran.id,jadwal.id AS id_jadwal,jadwal.nm_jadwal,pengeluaran.from_bank,
										SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE pengeluaran.status="1" AND pengeluaran.active="1" AND tgl_faktur BETWEEN "'.$start.'" AND "'.$to.'" '.$where.'');
	        return $query->row();
		}

		public function get_sum_date_group($start,$to,$where) {
			$results = array();
	        $query = $this->db->query(' SELECT jenis,SUM(total_biaya) AS total_biaya FROM pengeluaran
										INNER JOIN jadwal ON pengeluaran.id_jadwal = jadwal.id 
										WHERE pengeluaran.status="1" AND pengeluaran.active="1" AND tgl_faktur BETWEEN "'.$start.'" AND "'.$to.'" '.$where.' 
										GROUP BY jenis');
	        return $query->result();
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _insert_detail($data) {
			$table = $this->pengeluaran_detail;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function _update_detail($where, $data) {
			$table = $this->pengeluaran_detail;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

		public function delete_by_id($id)
		{
			$this->db->where('no_faktur', $id);
			$this->db->delete('pengeluaran_detail');
		}

	    private function _get_datatables_query($where, $cols)
	    {
	    	$this->db->select(' pengeluaran.id,
								jadwal.nm_jadwal,
								pengeluaran.no_faktur,
								pengeluaran.tgl_faktur,
								pengeluaran.jenis,
								pengeluaran.diterima,
								pengeluaran.total_biaya,
								pengeluaran.from_bank,
								pengeluaran.status,
								pengeluaran.active
	    					');
	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	$this->db->join($this->jadwal, ''.$this->table.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_export_month($where) {
			$query = $this->db->query(' SELECT
										pembayaran.id,
										pembayaran.no_faktur,
										pembayaran.jenis,
										pembayaran.id_pendaftaran,
										pembayaran.tgl_bayar,
										pembayaran.jml_bayar,
										pembayaran.path_bukti_bayar,
										pembayaran.catatan,
										pembayaran.status_verifikasi,
										pembayaran.tunai,
										pembayaran.via_bank,
										pembayaran.active,
										pendaftar.kd_office,
										pendaftar.no_pendaftaran,
										formulir.nm_lengkap,
										formulir.id_marketing,
										cabang.nm_cabang,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										marketing.nm_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE
										pembayaran.active="1" '.$where.' 
										ORDER BY tgl_bayar ASC
										');

			return $query;
		}

		public function get_sum_export_month($where) {
			$query = $this->db->query(' SELECT
										pembayaran.via_bank,
										Sum(pembayaran.jml_bayar) AS count_jml_bayar,
										pendaftar.id_paket,
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										pendaftar.kd_office,
										cabang.nm_cabang,
										pendaftar.id_formulir,
										formulir.id_marketing
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										WHERE
										pembayaran.active="1" '.$where.'
										GROUP BY via_bank
										');

			return $query;
		}

		public function get_sum_by_scheedule($where) {
			$query = $this->db->query(' SELECT										
										jadwal.nm_jadwal,
										jadwal.tgl_keberangkatan,
										SUM(jml_bayar) as count_jml_bayar
										FROM
										pembayaran
										INNER JOIN pendaftar ON pembayaran.id_pendaftaran = pendaftar.id
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										WHERE
										pembayaran.active="1" '.$where.'
										GROUP BY id_jadwal
										');

			return $query;
		}
	}
