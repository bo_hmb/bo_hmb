<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Expenditure</span>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Expenditure</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                    </button>
                            <div class="btn-group">
                                <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export1"> Based On Schedule </a>
                                    </li>
                                    <li>
                                        <a class="btn-export_date"> Base On Date </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a class="btn-export-report"> Report </a>
                                    </li>
                                    <!-- <li>
                                        <a href="javascript:;"> All </a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <th width="10%">Relasi Pengeluaran</th>
                                        <th width="10%">No Faktur</th>
                                        <th width="10%">Tanggal Faktur</th>
                                        <th width="10%">Jenis Spending</th>
                                        <th width="10%">Diterima Oleh</th>
                                        <th width="15%">Total Biaya</th>
                                        <th width="10%">From Bank</th>
                                        <th width="10%">Status</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <td>
                                            <select name="nm_jadwal" class="form-control form-filter input-sm">
                                                <option></option>                                                
                                                <?php
                                                 $schedule= Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
                                                foreach ($schedule as $row) { ?>
                                                <option value="<?php echo $row->nm_jadwal; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                                <?php
                                                }
                                                ?>
                                            </select> 
                                            <!-- <input type="text" class="form-control form-filter input-sm" name="nm_jadwal">  -->
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_faktur"> </td>
                                        <td><input type="date" class="form-control form-filter input-sm" name="tgl_faktur"> </td>
                                        <td>
                                            <select class="form-control form-filter input-sm" name="jenis">
                                                <option value="">Pilih</option>
                                                <option value="Expenditure Handling">Expenditure Handling</option>
                                                <option value="Operasional Haji">Operasional Haji</option>
                                                <option value="Operasional Umroh">Operasional Umroh</option>
                                                <option value="Operasional Tour">Operasional Tour</option>
                                                <option value="Administrasi dan Umum">Administrasi dan Umum</option>
                                                <option value="Pembayaran Hutang">Pembayaran Hutang</option>                                
                                                <option value="Patty Cash">Patty Cash</option>
                                                <option value="Gaji dan Insentif">Gaji dan Insentif</option>
                                                <option value="Pinjaman Usaha">Pinjaman Usaha</option>
                                                <option value="Sumbangan dan Kegiata Sosial">Sumbangan dan Kegiata Sosial</option>
                                                <option value="Peralatan dan Perlengkapan Kantor">Peralatan dan Perlengkapan Kantor</option>
                                                <option value="Entertaint">Entertaint</option>
                                                <option value="Percetakan">Percetakan</option>
                                                <option value="Promosi dan Pengembangan">Promosi dan Pengembangan</option>
                                                <option value="Mutasi Antar Bank">Mutasi Antar Bank</option>
                                                <option value="Pinjaman Karyawan">Pinjaman Karyawan</option>
                                                <option value="Mutasi Rekening">Mutasi Rekening</option>
                                                <option value="Persediaan Perlengkapan">Persediaan Perlengkapan</option>
                                                <option value="Profit Sharing">Profit Sharing</option>
                                                <option value="Lain-lain">Lain-lain</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="diterima"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="total_biaya"> </td>
                                        <td>
                                            <select name="from_bank" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="Mandiri">Mandiri</option>
                                                <option value="Mandiri 2">Mandiri 2</option>
                                                <option value="Mandiri 3">Mandiri 3</option>
                                                <option value="Mandiri 4">Mandiri 4</option>
                                                <option value="Mandiri 5">Mandiri 5</option>
                                                <option value="Mandiri 6">Mandiri 6</option>
                                                <option value="Mandiri 7">Mandiri 7</option>
                                                <option value="Mandiri 8">Mandiri 8</option>
                                                <option value="Mandiri 9 (USD)">Mandiri 9 (USD)</option>
                                                <option value="BRI">BRI</option>
                                                <option value="BNI Syariah">BNI Syariah</option>
                                            </select>  
                                        </td>
                                        <td>
                                            <select name="status" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="0">Disagree</option>
                                                <option value="2">Agree</option>
                                                <option value="1">Payment</option>
                                            </select>  
                                        </td>

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>