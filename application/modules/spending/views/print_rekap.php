<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>DATA REKAP</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h2 style="text-align: center; font-weight: bold;">DATA REKAP</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <!-- <h5 style="text-align: center;">Start <?php echo $x=date('d-m-Y', strtotime($date_star)) ?>  to <?php echo $x=date('d-m-Y', strtotime($date_to)).' From '.$bank.' Bank' ?></h5> -->
        </th>
    </tr>
</table>
<hr>


<h3 style="text-align: center; font-weight: bold;">INCOME</h3>
<br>
<table>    
    <tr>           
        <th style="width:20px;">No</th>
        <th style="width:80px;">Bank</th>
        <th style="width:90px;">Cabang</th>
        <th style="width:90px;">No. Faktur</th>
        <th style="width:240px;">Nama Jamaah</th>
        <th style="width:80px;">Tanggal Bayar</th>
        <th style="width:110px;">Jumlah Bayar</th>
        <th style="width:250px;">Catatan</th>
        <th style="width:150px;">Nama Agen</th>
    </tr>

    <?php $nomor=1; foreach($detail_in as $row) { ?>
        <?php 
        if ($row->tgl_keberangkatan == '') {
            $tgl='';
        } else if ($row->tgl_keberangkatan == '0000-00-00') {
            $tgl='Tabungan';
        } else {
            $tgl= date('d M Y', strtotime($row->tgl_keberangkatan));
        }

         ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->via_bank ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_faktur ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_bayar)) ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->jml_bayar,0,',','.') ?></td>
           <td style="border:1px solid #000;"><?php echo $row->catatan ?> <?php echo $tgl ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>
        </tr>   
    <?php $nomor++;}?> 

</table>

<br>
<h3 style="font-weight: bold;">Rekapitulasi</h3>            
<table>    
    <tr>          
        <!-- <th style="width: 20px;">No</th>         -->
        <th style="width: 150px;">Bank</th>
        <th style="width: 150px;">Total Pembayaran</th>
    </tr>

    <?php $total=0; $nomor=1; foreach($sum_in as $row) { 
            ?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->via_bank ?></td>     
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->count_jml_bayar,0,',','.') ?></td>      
        </tr>   
    <?php $total = $total + $row->count_jml_bayar ; $nomor++;}?> 
        <tr>
            <td style="border:1px solid #000;">Total</td>
            <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($total,0,',','.') ?></td> 
        </tr>
        

</table>
<br>
<table>    
    <tr>          
        <!-- <th style="width: 20px;">No</th>         -->
        <th style="width: 150px;">Nama Jadwal</th>
        <th style="width: 150px;">Total Pembayaran</th>
    </tr>

    <?php $tot=0; $nomor=1; foreach($sum_by_schedule_in as $row) { 
            ?>
        <tr>                                                
           <!-- <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->nm_jadwal ?></td>     
           <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($row->count_jml_bayar,0,',','.') ?></td>      
        </tr>   
    <?php $tot=$tot+$row->count_jml_bayar; $nomor++;}?> 
        <tr>
            <td style="border:1px solid #000;">Total</td>
            <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($tot,0,',','.') ?></td> 
        </tr>

</table>



<h3 style="text-align: center; font-weight: bold;">EXPENDITURE</h3>
<br>    
<table>    
    <tr>           
        <th style="width: 10px;">No</th>
        <th style="width: 90px;">Tanggal Faktur</th>
        <th style="width: 190px;">Jenis Faktur</th>
        <th style="width: 180px;">Nama Jadwal</th>
        <th style="width: 320px;">Item</th>
        <th style="width: 310px;">Deskripsi</th>
        <th style="width: 150px;">Diterima Oleh</th>
        <th style="width: 50px;">From Bank</th>
        <th style="width: 100px;">Total Biaya</th>
    </tr>

    <?php $nomor=1; foreach($detail_ex as $row) {
        ?>
        <tr>                                                
           <td style="border:1px solid #000;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_faktur)) ?></td>
           <td style="border:1px solid #000;"><?php echo $row->jenis ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_jadwal ?></td>
           <td style="border:1px solid #000;"><?php echo $row->item ?></td>
           <td style="border:1px solid #000;"><?php echo $row->deskripsi ?></td>
           <td style="border:1px solid #000;"><?php echo $row->diterima ?></td>
           <td style="border:1px solid #000;"><?php echo $row->from_bank ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->sub_harga,0,',','.') ?></td>
        </tr>   
    <?php $nomor++;}?> 
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>                
        <td></td>                
        <td></td>                
        <td></td>                
        <td  style="border:1px solid #000;text-align: center;">Total</td>
        <td  style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->total_biaya,0,',','.');?></td>
    </tr>
</table>
<br>
<table>
    <?php $no=1; foreach ($detail_group as $row) { ?>
    <tr>
        <td style="border:1px solid #000;"><?php echo $no; ?></td>
        <td style="border:1px solid #000;"><?php echo $row->jenis; ?></td>
        <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->total_biaya,0,',','.') ?></td>
    </tr>
    <?php $no++; } ?>
    <tr>
        <td style="border:1px solid #000;text-align: center;" colspan="2">Total</td>
        <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->total_biaya,0,',','.');?></td>
    </tr>
</table>
<br>
<h3 style="text-align: left; font-weight: bold;">REKAPITULASI</h3>

<table>
    <tr>
        <td style="border:1px solid #000;">Pemasukan</td>
        <td style="border:1px solid #000;">Pengeluaran</td>
        <td style="border:1px solid #000;">Margin</td>
    </tr>
    <tr>
        <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($tot,0,',','.') ?></td>
        <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($main->total_biaya,0,',','.');?></td>
        <td style="border:1px solid #000;text-align: right;"><?php echo 'Rp. '.$t=number_format($tot - $main->total_biaya,0,',','.') ?></td>
    </tr>
    
</table>

</body>
</html>