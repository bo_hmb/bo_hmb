<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>DATA EXPENDITURE</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h2 style="text-align: center; font-weight: bold;">DATA EXPENDITURE</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 80%"> 
            <h5 style="text-align: center;"><?php echo $main->nm_jadwal.' From '.$bank.' Bank'; ?></h5>
        </th>
    </tr>
</table>



<!-- <p style="text-align: center;">Jl. Poros Makassar - Maros, Km. 3, Karoada, Turikale Kab. Maros</p> -->
<hr>
<!-- N Kop -->

<!-- <h3 style="text-align: center; font-weight: bold;">Hasil Diagnosa</h3> -->

<br>    
<table>    
    <tr>           
        <th style="width: 10px;">No</th>
        <th style="width: 90px;">Tanggal Faktur</th>
        <th style="width: 190px;">Jenis Faktur</th>
        <th style="width: 320px;">Item</th>
        <th style="width: 310px;">Deskripsi</th>
        <th style="width: 200px;">Diterima Oleh</th>
        <th style="width: 100px;">From Bank</th>
        <th style="width: 100px;">Total Biaya</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $x=date('d-m-Y', strtotime($row->tgl_faktur)) ?></td>
           <td style="border:1px solid #000;"><?php echo $row->jenis ?></td>
           <td style="border:1px solid #000;"><?php echo $row->item ?></td>
           <td style="border:1px solid #000;"><?php echo $row->deskripsi ?></td>
           <td style="border:1px solid #000;"><?php echo $row->diterima ?></td>
           <td style="border:1px solid #000;"><?php echo $row->from_bank ?></td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->sub_harga,0,',','.') ?></td>
        </tr>   
    <?php $nomor++;}?> 
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>                
        <td></td>                
        <td></td>                
        <td  style="border:1px solid #000;text-align: center;">Total</td>
        <td  style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->total_biaya,0,',','.');?></td>
    </tr>
</table>
<br>
<table>
    <?php $no=1; foreach ($detail_group as $row) { ?>
    <tr>
        <td style="border:1px solid #000;"><?php echo $no; ?></td>
        <td style="border:1px solid #000;"><?php echo $row->jenis; ?></td>
        <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->total_biaya,0,',','.') ?></td>
    </tr>
    <?php $no++; } ?>
    <tr>
        <td style="border:1px solid #000;text-align: center;" colspan="2">Total</td>
        <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->total_biaya,0,',','.');?></td>
    </tr>
</table>
<br>
<p style="text-align: right;">Makassar, <?php echo date('d-m-Y'); ?></p>

<br>
<br>
<br>
<table>
    <tr>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 182px">Dibuat Oleh</th>
        <th style="border:0px solid #000; width: 530px"></th>
        <th style="border:0px solid #000; width: 182px">Mengetahui</th>        
    </tr>
</table>
<br>
<br>
<br>

<table>
    <tr>
        <th style="border:0px solid #000; width: 150px"></th>
        <th style="border:0px solid #000; width: 182px"><?php echo $this->currentUser->first_name; ?></th>
        <th style="border:0px solid #000; width: 530px"></th>
        <th style="border:0px solid #000; width: 182px">Yuliansyah Siregar</th>
            
    </tr>
</table>

</body>
</html>