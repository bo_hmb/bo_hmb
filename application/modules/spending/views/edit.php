            <form role="form" action="#" class="form-horizontal" id="form-edit">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">No Faktur</label>
                        <div class="col-md-3">
                            <input type="text" name="no_faktur" placeholder="Nomor Faktur" class="form-control" value="<?php echo $main->no_faktur ?>" readonly/> 
                        </div>
                    
                        <label class="col-md-2 control-label">Jenis Expenditure</label>
                        <div class="col-md-3">
                            <select class="form-control sp jadwal" name="jenis">
                                <option value="-">Pilih</option>
                                <option value="Expenditure Handling" <?php if ($main->jenis == 'Expenditure Handling') {echo "selected";}  ?>>Expenditure Handling</option>
                                <option value="Operasional Haji" <?php if ($main->jenis == 'Operasional Haji') {echo "selected";}  ?>>Operasional Haji</option>
                                <option value="Operasional Umroh" <?php if ($main->jenis == 'Operasional Umroh') {echo "selected";}  ?>>Operasional Umroh</option>
                                <option value="Operasional Tour" <?php if ($main->jenis == 'Operasional Tour') {echo "selected";}  ?>>Operasional Tour</option>
                                <option value="Administrasi dan Umum" <?php if ($main->jenis == 'Administrasi dan Umum' ) {echo "selected";}  ?>>Administrasi dan Umum</option>
                                <option value="Pembayaran Hutang" <?php if ($main->jenis == 'Pembayaran Hutang') {echo "selected";}  ?>>Pembayaran Hutang</option>                                
                                <option value="Patty Cash" <?php if ($main->jenis == 'Patty Cash') {echo "selected";}  ?>>Patty Cash</option>
                                <option value="Gaji dan Insentif" <?php if ($main->jenis == 'Gaji dan Insentif') {echo "selected";}  ?>>Gaji dan Insentif</option>
                                <option value="Pinjaman Usaha" <?php if ($main->jenis == 'Pinjaman Usaha') {echo "selected";}  ?>>Pinjaman Usaha</option>
                                <option value="Sumbangan dan Kegiata Sosial" <?php if ($main->jenis == 'Sumbangan dan Kegiata Sosial' ) {echo "selected";}  ?>>Sumbangan dan Kegiata Sosial</option>
                                <option value="Peralatan dan Perlengkapan Kantor" <?php if ($main->jenis == 'Peralatan dan Perlengkapan Kantor' ) {echo "selected";}  ?>>Peralatan dan Perlengkapan Kantor</option>
                                <option value="Entertaint" <?php if ($main->jenis == 'Entertaint') {echo "selected";}  ?>>Entertaint</option>
                                <option value="Percetakan" <?php if ($main->jenis == 'Percetakan') {echo "selected";}  ?>>Percetakan</option>
                                <option value="Promosi dan Pengembangan" <?php if ($main->jenis == 'Promosi dan Pengembangan') {echo "selected";}  ?>>Promosi dan Pengembangan</option>
                                <option value="Mutasi Antar Bank" <?php if ($main->jenis == 'Mutasi Antar Bank') {echo "selected";}  ?>>Mutasi Antar Bank</option>
                                <option value="Pinjaman Karyawan" <?php if ($main->jenis == 'Pinjaman Karyawan') {echo "selected";}  ?>>Pinjaman Karyawan</option>
                                <option value="Mutasi Rekening" <?php if ($main->jenis == 'Mutasi Rekening') {echo "selected";}  ?>>Mutasi Rekening</option>
                                <option value="Persediaan Perlengkapan" <?php if ($main->jenis == 'Persediaan Perlengkapan') {echo "selected";}  ?>>Persediaan Perlengkapan</option>
                                <option value="Profit Sharing" <?php if ($main->jenis == 'Profit Sharing') {echo "selected";}  ?>>Profit Sharing</option>
                                <option value="Lain-lain" <?php if ($main->jenis == 'Lain-lain') {echo "selected";}  ?>>Lain-lain</option>                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal Faktur</label>
                        <div class="col-md-3">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="tgl_faktur" class="form-control" placeholder="Tanggal" value="<?php echo $main->tgl_faktur; ?>">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <label class="col-md-2 control-label">Diterima Oleh</label>
                        <div class="col-md-3">
                            <input type="text" name="diterima" value="<?php echo $main->diterima; ?>" placeholder="Diterima Oleh" class="form-control" /> 
                        </div>
                    </div>
                </div>
                <div class="row">      
                        <div class="form-group">
                        <label class="col-md-2 control-label">Nama Jadwal</label>
                        <div class="col-md-3">
                            <select id="id_jadwal" name="id_jadwal" class="form-control pilih" data-placeholder="Pilih Jadwal">
                                <option <?php if($main->id_jadwal === '1'){echo "selected";} ?>  value="1">Umum</option>
                                <option <?php if($main->id_jadwal === '0'){echo "selected";} ?>  value="0">Tabungan</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                    <!-- <?php if ($row->nm_jadwal == 'Tabungan') $nm_jadwal = 'Umum'; else $nm_jadwal = $row->nm_jadwal;?>                                         -->
                                <option value="<?php echo $row->id; ?>" <?php if($main->id_jadwal === $row->id){echo "selected";} ?> ><?php echo $nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php if ($main->jenis == 'Expenditure Handling' ) { ?>
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-3">
                            <select class="form-control pilih" name="status">
                                <option value="0"  <?php if ($main->status == '0' ) {echo "selected";}  ?> >Disagree</option>
                                <option value="2"  <?php if ($main->status == '2' ) {echo "selected";}  ?> >Agree</option>
                                <option value="1"  <?php if ($main->status == '1' ) {echo "selected";}  ?> >Payment</option>
                            </select>
                        </div>
                        <?php } else { ?>
                            <input type="hidden" name="status" value="<?php echo $main->status ?>">
                        <?php } ?>
                        <!-- </div> -->
                     
                        <div class="form-group">                        
                        <label class="col-md-2 control-label">Pilih Bank</label>
                        <div class="col-md-3">
                            <select class="form-control pilih" name="from_bank">
                                <option value="Mandiri" <?php if ($main->from_bank == 'Mandiri') {echo "selected";}  ?>>Mandiri</option>
                                <option value="Mandiri 2" <?php if ($main->from_bank == 'Mandiri 2') {echo "selected";}  ?>>Mandiri 2</option>
                                <option value="Mandiri 3" <?php if ($main->from_bank == 'Mandiri 3') {echo "selected";}  ?>>Mandiri 3</option>
                                <option value="Mandiri 4" <?php if ($main->from_bank == 'Mandiri 4') {echo "selected";}  ?>>Mandiri 4</option>
                                <option value="Mandiri 5" <?php if ($main->from_bank == 'Mandiri 5') {echo "selected";}  ?>>Mandiri 5</option>
                                <option value="Mandiri 6" <?php if ($main->from_bank == 'Mandiri 6') {echo "selected";}  ?>>Mandiri 6</option>
                                <option value="Mandiri 7" <?php if ($main->from_bank == 'Mandiri 7') {echo "selected";}  ?>>Mandiri 7</option>
                                <option value="Mandiri 8" <?php if ($main->from_bank == 'Mandiri 8') {echo "selected";}  ?>>Mandiri 8</option>
                                <option value="Mandiri 9 (USD)" <?php if ($main->from_bank == 'Mandiri 9 (USD)') {echo "selected";}  ?>>Mandiri 9 (USD)</option>
                                <option value="BRI" <?php if ($main->from_bank == 'BRI' ) {echo "selected";}  ?>>BRI</option>
                                <option value="BNI Syariah" <?php if ($main->from_bank == 'BNI Syariah' ) {echo "selected";}  ?>>BNI Syariah</option>
                            </select>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Item</th>
                                    <th>Deskripsi</th>
                                    <th>Harga Satuan</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                                <?php foreach ($detail as $row) { ?>                                
                                <tr class="baris form-create-barang" id="form-create-barang">
                                    <input type="hidden" value="<?php echo $row->id ?>" name="iddetail[]">
                                    <td align="center">
                                        <input id="item" class="form-control item" name="item[]" type="text" value="<?php echo $row->item ?>" ></td>
                                    <td align="center">
                                        <input id="deskripsi" class="form-control deskripsi" name="deskripsi[]" type="text" value="<?php echo $row->deskripsi ?>"></td>
                                    <td align="center">
                                        <input id="harga" class="form-control harga money" name="harga[]" type="text" value="<?php echo $row->harga ?>" readonly></td>
                                    <td align="center">
                                        <input id="jml" name="jml[]" class="form-control jml money" type="text" placeholder="0" value="<?php echo $row->jml ?>" readonly></td>
                                    <td align="center">
                                        <input id="sub_harga" class="form-control sub_harga money" name="sub_harga[]" type="text" value="<?php echo $row->sub_harga ?>" readonly></td>
                                    <td>
                                        <!-- <button type="button" class="btn btn-circle btn-danger" id="hapus[]"><i class="icon-trash"></i></button> -->
                                        <button type="button" class="btn btn-circle btn-danger" id="hapus"><i class="icon-trash"></i></button>
                                        <input id="rows[]" name="rows[]" value="" type="hidden">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga </strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total_biaya money" id="total_biaya" value="<?php echo $main->total_biaya ?>" readonly>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
                        
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     