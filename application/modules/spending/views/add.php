            <form role="form" action="#" class="form-horizontal" id="form-create">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">No Faktur Otomatis</label>
                        <div class="col-md-3">
                            <input type="text" name="no_faktur" placeholder="Nomor Faktur Otomatis" class="form-control" value="Otomatis" readonly/> 
                        </div>
                    
                        <label class="col-md-2 control-label">Jenis Expenditure</label>
                        <div class="col-md-3">
                            <select class="form-control sp jadwal" name="jenis">
                                <option value="-">Pilih</option>
                                <option value="Expenditure Handling">Expenditure Handling</option>
                                <option value="Operasional Haji">Operasional Haji</option>
                                <option value="Operasional Umroh">Operasional Umroh</option>
                                <option value="Operasional Tour">Operasional Tour</option>
                                <option value="Administrasi dan Umum">Administrasi dan Umum</option>
                                <option value="Pembayaran Hutang">Pembayaran Hutang</option>                                
                                <option value="Patty Cash">Patty Cash</option>
                                <option value="Gaji dan Insentif">Gaji dan Insentif</option>
                                <option value="Pinjaman Usaha">Pinjaman Usaha</option>
                                <option value="Sumbangan dan Kegiata Sosial">Sumbangan dan Kegiata Sosial</option>
                                <option value="Peralatan dan Perlengkapan Kantor">Peralatan dan Perlengkapan Kantor</option>
                                <option value="Entertaint">Entertaint</option>
                                <option value="Percetakan">Percetakan</option>
                                <option value="Promosi dan Pengembangan">Promosi dan Pengembangan</option>
                                <option value="Mutasi Antar Bank">Mutasi Antar Bank</option>
                                <option value="Pinjaman Karyawan">Pinjaman Karyawan</option>
                                <option value="Mutasi Rekening">Mutasi Rekening</option>
                                <option value="Persediaan Perlengkapan">Persediaan Perlengkapan</option>
                                <option value="Profit Sharing">Profit Sharing</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal Faktur</label>
                        <div class="col-md-3">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="tgl_faktur" class="form-control" placeholder="Tanggal" value="<?php echo(date('Y-m-d')) ?>">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button" > <!-- disabled -->
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <label class="col-md-2 control-label">Diterima Oleh</label>
                        <div class="col-md-3">
                            <input type="text" name="diterima" placeholder="Diterima Oleh" class="form-control" /> 
                        </div>
                    </div>
                </div>
                <div class="row">      
                        <div class="form-group">
                        <label class="col-md-2 control-label">Nama Jadwal</label>
                        <div class="col-md-3">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Pilih Jadwal">
                                <option disabled selected></option>
                                <option value="1">Umum</option>
                                <option value="0">Tabungan</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_paket.' - '.$row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <label class="col-md-2 control-label">Pilih Bank</label>
                        <div class="col-md-3">
                            <select class="form-control sp jadwal" name="from_bank">
                                <option value="Mandiri">Mandiri</option>
                                <option value="Mandiri 2">Mandiri 2</option>
                                <option value="Mandiri 3">Mandiri 3</option>
                                <option value="Mandiri 4">Mandiri 4</option>
                                <option value="Mandiri 5">Mandiri 5</option>
                                <option value="Mandiri 6">Mandiri 6</option>
                                <option value="Mandiri 7">Mandiri 7</option>
                                <option value="Mandiri 8">Mandiri 8</option>
                                <option value="Mandiri 9 (USD)">Mandiri 9 (USD)</option>
                                <option value="BRI">BRI</option>
                                <option value="BNI Syariah">BNI Syariah</option>
                            </select>
                        </div>
                        </div>
                </div>

                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Item</th>
                                    <th>Deskripsi</th>
                                    <th>Harga Satuan</th>
                                    <th style="width:10%">Qty</th>
                                    <th>Sub Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="4" align="right" style="font-size:14px;vertical-align:middle;"><strong>Total Harga </strong></td>
                                    <td align="center">
                                        <input type="text" name="total_bayar" class="form-control total_biaya money" id="total_biaya" value="0" readonly="">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i>Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     