            <form role="form" method="POST" action="<?php echo base_url(); ?>spending/export_date" target="_blank" class="form-horizontal" id="form-export">
            <!-- <form role="form" action="#" class="form-horizontal" id="form-export"> -->
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Select Start Date</label>
                        <div class="col-md-4">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_star" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    
                        <label class="col-md-2 control-label">Select To Date</label>
                        <div class="col-md-3">
                            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" name="date_to" class="form-control" placeholder="Select" readonly="">
                                <span class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Jenis Expenditure</label>
                        <div class="col-md-8">
                            <div class="input-group select2-bootstrap-append">
                                <select id="multi-append" name="jenis[]" class="form-control pilihbanyak" multiple>
                                    <option value="All">All</option>
                                    <option value="Expenditure Handling">Expenditure Handling</option>
                                    <option value="Operasional Haji">Operasional Haji</option>
                                    <option value="Operasional Umroh">Operasional Umroh</option>
                                    <option value="Operasional Tour">Operasional Tour</option>
                                    <option value="Administrasi dan Umum">Administrasi dan Umum</option>
                                    <option value="Pembayaran Hutang">Pembayaran Hutang</option>                                
                                    <option value="Patty Cash">Patty Cash</option>
                                    <option value="Gaji dan Insentif">Gaji dan Insentif</option>
                                    <option value="Pinjaman Usaha">Pinjaman Usaha</option>
                                    <option value="Sumbangan dan Kegiata Sosial">Sumbangan dan Kegiata Sosial</option>
                                    <option value="Peralatan dan Perlengkapan Kantor">Peralatan dan Perlengkapan Kantor</option>
                                    <option value="Entertaint">Entertaint</option>
                                    <option value="Percetakan">Percetakan</option>
                                    <option value="Promosi dan Pengembangan">Promosi dan Pengembangan</option>
                                    <option value="Mutasi Antar Bank">Mutasi Antar Bank</option>
                                    <option value="Pinjaman Karyawan">Pinjaman Karyawan</option>
                                    <option value="Mutasi Rekening">Mutasi Rekening</option>
                                    <option value="Persediaan Perlengkapan">Persediaan Perlengkapan</option>
                                    <option value="Profit Sharing">Profit Sharing</option>
                                    <option value="Lain-lain">Lain-lain</option>
                                </select>
                            </div>
                            
                            <!-- <select class="form-control sp jadwal" name="jenis">
                                <option value="All">All</option>
                                <option value="Expenditure Handling">Expenditure Handling</option>
                                

                                <option value="Operasional Haji">Operasional Haji</option>
                                <option value="Operasional Umroh">Operasional Umroh</option>
                                <option value="Operasional Tour">Operasional Tour</option>
                                <option value="Administrasi dan Umum">Administrasi dan Umum</option>
                                <option value="Pembayaran Hutang">Pembayaran Hutang</option>                                
                                <option value="Patty Cash">Patty Cash</option>
                                <option value="Gaji dan Insentif">Gaji dan Insentif</option>
                                <option value="Pinjaman Usaha">Pinjaman Usaha</option>
                                <option value="Sumbangan Sosial">Sumbangan Sosial</option>
                                <option value="Peralatan dan Perlengkapan Kantor">Peralatan dan Perlengkapan Kantor</option>
                                <option value="Entertaint">Entertaint</option>
                                <option value="Promosi & Percetakan">Promosi & Percetakan</option>
                                <option value="Pengembangan">Pengembangan</option>
                                <option value="Mutasi Antar Bank">Mutasi Antar Bank</option>
                                <option value="Pinjaman Karyawan">Pinjaman Karyawan</option>
                                <option value="Mutasi Rekening">Mutasi Rekening</option>
                                <option value="Persediaan Perlengkapan">Persediaan Perlengkapan</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select> -->
                        </div>
                        
                    </div>
                </div>

                <!-- <div class="row">
                    <div class="col-md-12">                
                        <div class="form-group">
                            <label for="multi-append" class="control-label">Jenis Expenditure 2</label>
                            
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">From Bank</label>
                        <div class="col-md-8">
                            <select class="form-control sp jadwal" name="bank">
                                <option value="All">All</option>
                                <option value="Mandiri">Mandiri</option>
                                <option value="Mandiri 2">Mandiri 2</option>
                                <option value="Mandiri 3">Mandiri 3</option>
                                <option value="Mandiri 4">Mandiri 4</option>
                                <option value="Mandiri 5">Mandiri 5</option>
                                <option value="Mandiri 6">Mandiri 6</option>
                                <option value="Mandiri 7">Mandiri 7</option>
                                <option value="Mandiri 8">Mandiri 8</option>
                                <option value="Mandiri 9 (USD)">Mandiri 9 (USD)</option>
                                <option value="BRI">BRI</option>
                                <option value="BNI Syariah">BNI Syariah</option>
                                <option value="BSM">BSM</option>
                            </select>
                        </div>
                        
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" href="<?php echo base_url(); ?>spending/export1" target="_blank"><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     