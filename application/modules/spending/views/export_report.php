            <form role="form" method="POST" action="<?php echo base_url(); ?>spending/export_report" target="_blank" class="form-horizontal" id="form-export">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Select Schedule</label>
                        <div class="col-md-8">
                            <select id="id_jadwal" name="id_jadwal" class="form-control jadwal" data-placeholder="Select Schedule">
                                <option disabled selected></option>
                                <option value="All">All</option>                                
                                <option value="0">Tabungan</option>
                                <?php
                                foreach ($schedule as $row) { ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>   
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Month</label>
                        <div class="col-md-8">
                            <select id="kd_cabang" name="bulan" class="form-control jadwal" data-placeholder="Select Month">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="1">Januari</option>                                
                                <option value="2">Februari</option>                                
                                <option value="3">Maret</option>                                
                                <option value="4">April</option>                                
                                <option value="5">Mei</option>                                
                                <option value="6">Juni</option>                                
                                <option value="7">Juli</option>                                
                                <option value="8">Agustus</option>                                
                                <option value="9">September</option>                                
                                <option value="10">Oktober</option>                                
                                <option value="11">November</option>                                
                                <option value="12">Desember</option>                                
                            </select>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Year</label>
                        <div class="col-md-8">
                            <select id="kd_cabang" name="tahun" class="form-control jadwal" data-placeholder="Select Year">
                                <option disabled selected></option>
                                <option value="All">All</option>
                                <option value="2018">2018</option>                                
                                <option value="2019">2019</option>                                
                            </select>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 control-label">From Bank</label>
                        <div class="col-md-8">
                            <select class="form-control sp " name="bank">
                                <option value="All">All</option>
                                <option value="Mandiri">Mandiri</option>
                                <option value="Mandiri 2">Mandiri 2</option>
                                <option value="Mandiri 3">Mandiri 3</option>
                                <option value="Mandiri 4">Mandiri 4</option>
                                <option value="Mandiri 5">Mandiri 5</option>
                                <option value="Mandiri 6">Mandiri 6</option>
                                <option value="Mandiri 7">Mandiri 7</option>
                                <option value="Mandiri 8">Mandiri 8</option>
                                <option value="Mandiri 9 (USD)">Mandiri 9 (USD)</option>
                                <option value="BRI">BRI</option>
                                <option value="BNI Syariah">BNI Syariah</option>
                                <option value="BSM">BSM</option>
                            </select>
                        </div>
                        
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green"><i class="fa fa-file-pdf-o"></i> Export</button>
                                <!-- <a type="submit" class="btn btn-outline green" "><i class="fa fa-file-pdf-o"></i> Export</a> -->
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Exit</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>