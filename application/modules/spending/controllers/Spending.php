<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Spending extends AdminController {

        public function __construct()
        {
            parent::__construct();
            date_default_timezone_set('Asia/Makassar');
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");
			$this->bulan = date('m');
			$this->tahun = date('Y');
            $this->curdate = date('Y-m-d H:i:s');
            // Module components            
            $this->data['module'] = 'Expenditure';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            $this->load->model('M_spending');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
                $this->kode_cabang = $this->currentUser->kode_cabang;
            }
        }

        public function index()
        {
            // Page components
            
            $this->data['pageTitle'] = 'Spending';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {

            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $no_faktur = $this->input->post('no_faktur',TRUE);
            $tgl_faktur = $this->input->post('tgl_faktur',TRUE);
            $jenis = $this->input->post('jenis',TRUE);
            $diterima = $this->input->post('diterima',TRUE);
            $total_biaya = $this->input->post('total_biaya',TRUE);
            $from_bank = $this->input->post('from_bank',TRUE);
            $status = $this->input->post('status',TRUE);
            
            $cols = array();
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($no_faktur)) { $cols['pengeluaran.no_faktur'] = $no_faktur; }
            if (!empty($tgl_faktur)) { $cols['pengeluaran.tgl_faktur'] = $tgl_faktur; }
            if (!empty($jenis)) { $cols['pengeluaran.jenis'] = $jenis; }
            if (!empty($diterima)) { $cols['diterima'] = $diterima; }
            if (!empty($from_bank)) { $cols['from_bank'] = $from_bank; }
            if (!empty($total_biaya)) { $cols['total_biaya'] = $total_biaya; }
            if ($status != null) { $cols['status'] = $status; }
            
            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "pengeluaran.active = '1'";              
            } else {
             $where = "pengeluaran.active = '1'";             
            }

            $list = $this->M_spending->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_spending->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
                $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <a type="button" class="btn btn-xs btn-outline blue tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'spending/cetak/'.$r->id.'" target="_blank"><i class="fa fa-print"></i></a>
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->no_faktur.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';
                if($r->status == '1')
                {
                    $active = 'Payment';
                    $label = 'success';
                }else if($r->status == '2'){
                    $active = 'Agree';
                    $label = 'info';
                }else if($r->status == '0'){
                    $active = 'Disagree';
                    $label = 'danger';
                }

                if ($r->nm_jadwal == 'Tabungan') {
                    $nm_jadwal = $r->nm_jadwal;//'Umum';
                } else {
                    $nm_jadwal = $r->nm_jadwal;
                }

                $records["data"][] = array(
                    $no,   
                    $btn_action,
                    $nm_jadwal,
                    $r->no_faktur,
                    $x=date('d-m-Y', strtotime($r->tgl_faktur)),
                    $r->jenis,
                    $r->diterima,
                    $t="Rp." . number_format($r->total_biaya,0,',','.'),
                    $r->from_bank,
                    '<span class="label label-sm label-'.$label.'">'.$active.'</span>',

                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_barang_order->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Barang Beli';
            
            $data['schedule'] = $this->M_spending->get_schedule()->result();
            // Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
			return response($this->load->view('add', $data, TRUE), 'html');
		}


		public function add()
	    {

			$this->validateInput();

            $cek = $this->M_spending->get_where_sum()->num_rows();
            $faktur = 'EP-'.date('m').date("Y").sprintf('%03d',$cek+1);

			$data_order = array(
				'no_faktur' => $faktur,//$this->input->post('no_faktur',TRUE),
                'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                'jenis' => $this->input->post('jenis',TRUE),
                'diterima' => $this->input->post('diterima',TRUE),
                'total_biaya' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_bayar')),
                'from_bank' => $this->input->post('from_bank',TRUE),
                'status' => '1',
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_spending->_insert($data_order);

            $cek = count($this->input->post('item'));

            for ($i=0; $i < $cek; $i++) { 
                $data_order_detail = array(
                    'no_faktur' => $faktur,
                    'item' => $this->input->post('item',TRUE)[$i],
                    'deskripsi' => $this->input->post('deskripsi',TRUE)[$i],
                    'harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga',TRUE))[$i],
                    'jml' => $this->input->post('jml',TRUE)[$i],
                    'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                    'active' => '1',
                    );

                $query1 = $this->M_spending->_insert_detail($data_order_detail);
            }
            
            // insert to notification
            $data_notification = array(
                'user_id' => $this->currentUser->id,
                'for_user' => '9',
                'activity_type' => 'Expenditure '.$this->input->post('jenis',TRUE),
                'activity_uri_1' => 'spending',
                'activity_time' => $this->curdate,
                'deleted' => '1',
                'kdoffice' => $this->kode_cabang,
                'reading' => '1',

                );
            $notification = Modules::run('notification/insert', $data_notification);


            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {
            $id = $this->input->get('id');
            $data['main'] = $this->M_spending->get_where(array('id' => $id))->row();
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();
            $data['detail'] = $this->M_spending->get_detail(array('no_faktur' => $data['main']->no_faktur,'active' => '1'))->result();
            return response($this->load->view('edit', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data_order = array(
                'no_faktur' => $this->input->post('no_faktur',TRUE),
                'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'tgl_faktur' => $this->input->post('tgl_faktur',TRUE),
                'jenis' => $this->input->post('jenis',TRUE),
                'diterima' => $this->input->post('diterima',TRUE),
                'total_biaya' => preg_replace("/[^0-9\.]/", "", $this->input->post('total_bayar')),
                'from_bank' => $this->input->post('from_bank',TRUE),
                'status' => $this->input->post('status',TRUE),
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_spending->_update(array('id' => $id), $data_order);

            // $cek = count($this->input->post('item'));

            // for ($i=0; $i < $cek; $i++) { 
            //     $data_order_detail = array(
            //         'no_faktur' => $this->input->post('no_faktur',TRUE),
            //         'item' => $this->input->post('item',TRUE)[$i],
            //         'deskripsi' => $this->input->post('deskripsi',TRUE)[$i],
            //         'harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga',TRUE))[$i],
            //         'jml' => $this->input->post('jml',TRUE)[$i],
            //         'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
            //         );
            //     $iddet = $this->input->post('iddetail',TRUE)[$i];
            //     $query1 = $this->M_spending->_update_detail(array('no_faktur' => $this->input->post('no_faktur',TRUE), 'id' => $iddet), $data_order_detail);
            // }

            $query_del = $this->M_spending->delete_by_id($this->input->post('no_faktur'));
            
            if ($this->input->post('item') != null) {
            $cek = count($this->input->post('item'));

                for ($i=0; $i < $cek; $i++) { 
                    $data_order_detail = array(
                        'no_faktur' => $this->input->post('no_faktur',TRUE),
                        'item' => $this->input->post('item',TRUE)[$i],
                        'deskripsi' => $this->input->post('deskripsi',TRUE)[$i],
                        'harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('harga',TRUE))[$i],
                        'jml' => $this->input->post('jml',TRUE)[$i],
                        'sub_harga' => preg_replace("/[^0-9\.]/", "", $this->input->post('sub_harga',TRUE))[$i],
                        'active' => '1',
                        );
                    $query1 = $this->M_spending->_insert_detail($data_order_detail);
                }
            }

            // insert to notification
            // $data_notification = array(
            //     'activity_time' => $this->curdate,
            //     'kdoffice' => $this->kode_cabang,
            //     'activity_type' => $this->input->post('jenis',TRUE),
            //     'activity_uri_1' => $this->uri->segment(1),
            //     'deleted' => '1',
            //     'user_id' => $this->currentUser->id,
            //     );
            // $notification = Modules::run('notification/insert', $data_notification);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            // $id = $this->input->post('id');
            $no_faktur = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            $query = $this->M_spending->_update(array('no_faktur' => $no_faktur), $data);

            $data_order_detail = array(
                'active' => '0',
                );
            $query1 = $this->M_spending->_update_detail(array('no_faktur' => $this->input->post('id',TRUE)), $data_order_detail);
            // die(print_r($no_faktur));
            
            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('no_faktur', 'no faktur', 'trim|required');
            $this->form_validation->set_rules('tgl_faktur', 'tgl_faktur', 'trim|required');
            $this->form_validation->set_rules('jenis', 'jenis', 'trim|required');
            $this->form_validation->set_rules('diterima', 'diterima', 'trim|required');
            $this->form_validation->set_rules('total_bayar', 'total_bayar', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function load_export1()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export1', $data, TRUE), 'html');
        }

        public function load_export_report()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export_report', $data, TRUE), 'html');
        }

        public function load_export_date()
        {
            $data['title'] = '';
            //$data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export_date', $data, TRUE), 'html');
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_spending->get_cetak($id);
            $data['detail'] = $this->M_spending->get_detail(array('active' => '1','no_faktur' => $data['main']->no_faktur))->result();


            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('spending/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf(); 

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export1()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');            
            $jenis = $this->input->post('jenis');            
            $bank = $this->input->post('bank');            
            
            if ($jenis == 'All') $where='pengeluaran.id_jadwal='.$id.''; else $where='pengeluaran.id_jadwal='.$id.' AND pengeluaran.jenis="'.$jenis.'"';
            if ($bank == 'All') $where.=''; else $where.=' AND pengeluaran.from_bank="'.$bank.'"';
            $data['main'] = $this->M_spending->get_sum_schedule('AND '.$where);            
            $data['detail'] = $this->M_spending->get_by_schedule('AND '.$where);
            $data['detail_group'] = $this->M_spending->get_sum_schedule_group('AND '.$where);            
            $data['bank'] = $bank;            

            //$this->load->view('spending/print_export1', $data);
            $html = $this->load->view('spending/print_export1', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_report()
        {

            $data = [];

            $data['title'] = "Report";

            $bulan = $this->input->post('bulan');
            $tahun = $this->input->post('tahun');
            $via_bank = $this->input->post('bank');
            $id_jadwal = $this->input->post('id_jadwal');

            if ($id_jadwal == 'All') $Filterid_jadwal = ''; else $Filterid_jadwal='AND id_jadwal = "'.$id_jadwal.'" ';

            if ($bulan == 'All') 
                {
                    $FilterBulan_in = ''; 
                    $FilterBulan_ex = ''; 
                } else {
                    $FilterBulan_in='AND MONTH(tgl_bayar) = "'.$bulan.'" ';
                    $FilterBulan_ex='AND MONTH(pengeluaran.tgl_faktur) = "'.$bulan.'" ';
                }
            if ($tahun == 'All') 
                {
                    $Filtertahun_in = ''; 
                    $Filtertahun_ex = ''; 
                } else {
                    $Filtertahun_in='AND YEAR(tgl_bayar) = "'.$tahun.'" ';
                    $Filtertahun_ex='AND YEAR(pengeluaran.tgl_faktur) = "'.$tahun.'" ';
                }
            if ($via_bank == 'All') 
                {
                    $Filtervia_bank_in = ''; 
                    $Filtervia_bank_ex = ''; 
                } else {
                    $Filtervia_bank_in='AND via_bank = "'.$via_bank.'" ';
                    $Filtervia_bank_ex='AND from_bank = "'.$via_bank.'" ';
                }

            $whereFilter_in = $FilterBulan_in.' '.$Filtertahun_in.' '.$Filtervia_bank_in.' '.$Filterid_jadwal;
            $whereFilter_ex = $FilterBulan_ex.' '.$Filtertahun_ex.' '.$Filtervia_bank_ex.' '.$Filterid_jadwal;

            //get data Income
            $data ['detail_in'] = $this->M_spending->get_export_month($whereFilter_in)->result();                
            $data ['sum_in'] = $this->M_spending->get_sum_export_month($whereFilter_in)->result();   
            $data ['sum_by_schedule_in'] = $this->M_spending->get_sum_by_scheedule($whereFilter_in)->result();  
            //batas get data Income

            //get data Expenditure 
            //AND pengeluaran.id_jadwal="176" AND MONTH(pengeluaran.tgl_faktur)="09" AND YEAR(pengeluaran.tgl_faktur)="2019" AND from_bank="Mandiri"
            $data['main'] = $this->M_spending->get_sum_schedule($whereFilter_ex);            
            $data['detail_ex'] = $this->M_spending->get_by_schedule($whereFilter_ex);
            $data['detail_group'] = $this->M_spending->get_sum_schedule_group($whereFilter_ex);            
            $data['bank'] = $bank;  
            //batas get data Expenditure 

            //$this->load->view('spending/print_export1', $data);
            $html = $this->load->view('spending/print_rekap', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_date()
        {

            $data = [];

            $data['title'] = "Report";

            $arr_jenis = $this->input->post('jenis');
            foreach($arr_jenis as $val)
            {
                $jenis_arr = @$jenis_arr .'"'. $val.'"' .",";
            }
            $fixjenis=substr(trim($jenis_arr), 0, -1);

            //get data main
            $jenis = $this->input->post('jenis');            
            $bank = $this->input->post('bank');            
            $data['bank'] = $bank;            
            if ($fixjenis == '"All"') $where=''; else $where='AND pengeluaran.jenis in ('.$fixjenis.') ';
            if ($bank == 'All') $where.=''; else $where.=' AND pengeluaran.from_bank="'.$bank.'"';

            $data['main'] = $this->M_spending->get_sum_date($this->input->post('date_star'),$this->input->post('date_to'),$where);            
            $data['detail'] = $this->M_spending->get_by_date($this->input->post('date_star'),$this->input->post('date_to'),$where);
            $data['detail_group'] = $this->M_spending->get_sum_date_group($this->input->post('date_star'),$this->input->post('date_to'),$where);
            $data['date_star'] = $this->input->post('date_star');
            $data['date_to'] = $this->input->post('date_to');

            //$this->load->view('spending/print_export1', $data);
            $html = $this->load->view('spending/print_export_date', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "report.pdf";
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>
