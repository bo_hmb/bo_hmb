<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_request extends CI_Model 
	{
		//id_jadwal,hari,tgl,lokasi

		var $table = 'pendaftar';
		var $formulir = 'formulir';
		var $paket = 'paket';
		var $jadwal = 'jadwal';
		var $tv_request = 'tv_request';

	    var $column_order = array('id','id_jadwal','hari','tgl','lokasi');
	    var $column_search = array('id','id_jadwal','hari','tgl','lokasi');
	    var $order = array('id' => 'desc'); 



		public function __construct() {
			parent::__construct();
		}

		public function get($order_by) {
			$table = $this->table;
			$this->db->order_by($order_by);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where($where) {
			$table = $this->tv_request;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_jadwal($where) {
			$table = $this->jadwal;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function _insert($data) {
			$table = $this->table;
			$insert = $this->db->insert($table, $data);
			return $insert;
		}

		public function _update($where, $data) {
			$table = $this->table;
			$this->db->where($where);
			$update = $this->db->update($table, $data);
			return $update;
		}

	    private function _get_datatables_query($where, $cols)
	    {

	    	// $this->db->select(' pendaftar.id,
						// 		pendaftar.no_pendaftaran,
						// 		formulir.jk,
						// 		formulir.nm_lengkap,
						// 		pendaftar.jenis_kamar,
						// 		pendaftar.hotel_request_makkah,
						// 		pendaftar.hotel_request_madinah,
						// 		CONCAT(pendaftar.hubungan," ",pendaftar.keluarga_terdekat) AS keluarga,
						// 		paket.nm_paket,
						// 		jadwal.tgl_keberangkatan ');
	    	$this->db->where($where);
	        $this->db->from($this->tv_request);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	    	// $this->db->join($this->formulir, ''.$this->table.'.id_formulir = '.$this->formulir.'.id');
	    	// $this->db->join($this->paket, ''.$this->table.'.id_paket = '.$this->paket.'.id');
	    	// $this->db->join($this->jadwal, ''.$this->paket.'.id_jadwal = '.$this->jadwal.'.id');
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {
	    	$this->db->where($where);
	        $this->db->from($this->tv_request);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
	}
