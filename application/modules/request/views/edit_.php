<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="group_code" value="<?php echo $main->no_pendaftaran ?>">
                    <label for="form_control_1">No. Pendaftaran</label>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="brach_code" value="<?php echo $main->nm_lengkap ?>">
                    <label for="form_control_1">Nama Lengkap</label>
                </div>
            </div>   
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="brach" value="<?php echo $main->jk ?>">
                    <label for="form_control_1">L/P</label>
                </div>
            </div>              
        </div> 


        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="group_code" value="<?php echo $main->hp ?>">
                    <label for="form_control_1">No. Telepon</label>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="group_code" value="<?php echo $main->nm_paket ?>">
                    <label for="form_control_1">Paket</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="brach_code" value="<?php echo $main->keluarga ?>">
                    <label for="form_control_1">Keluarga Terdekat</label>
                </div>
            </div>   
        </div> 

        <span class="caption-subject font-dark bold uppercase">Data Request</span>

        <div class="row">
            <br>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" value="<?php echo $main->jenis_kamar ?> - <?php echo $t="Rp." . number_format($main->biaya_upgrade_kamar,0,',','.') ?>">
                    <label for="form_control_1">Jenis Kamar Makkah</label>
                        <select class="form-control jeniskelamin" name="status_upgrade_kamar">
                            <option <?php echo ("0" === $main->status_upgrade_kamar) ? 'selected' : ''; ?> value="0">Belum Terverifikasi</option>
                            <option <?php echo ("1" === $main->status_upgrade_kamar) ? 'selected' : ''; ?> value="1">Terverifikasi</option>
                        </select>
                </div>
            </div> 

            <!-- <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" value="<?php echo $main->jenis_kamar ?> - <?php echo $t="Rp." . number_format($main->biaya_upgrade_kamar_madinah,0,',','.') ?>">
                    <label for="form_control_1">Jenis Kamar Madinah</label>
                
                        <select class="form-control jeniskelamin" name="status_upgrade_kamar_madinah">
                            <option <?php echo ("0" === $main->status_upgrade_kamar_madinah) ? 'selected' : ''; ?> value="0">Belum Terverifikasi</option>
                            <option <?php echo ("1" === $main->status_upgrade_kamar_madinah) ? 'selected' : ''; ?> value="1">Terverifikasi</option>
                        </select>
                        

                </div>
            </div>      -->        

            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" value="<?php echo $main->hotel_request_makkah ?> - <?php echo $t="Rp." . number_format($main->biaya_up_hotel_makkah,0,',','.') ?>">
                    <label for="form_control_1">Request Hotel Makkah</label>
                
                        <select class="form-control jeniskelamin" name="status_up_hotel_makkah">
                            <option <?php echo ("0" === $main->status_up_hotel_makkah) ? 'selected' : ''; ?> value="0">Belum Terverifikasi</option>
                            <option <?php echo ("1" === $main->status_up_hotel_makkah) ? 'selected' : ''; ?> value="1">Terverifikasi</option>
                        </select>
                        

                </div>
            </div> 

            <!-- <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" value="<?php echo $main->hotel_request_madinah ?> - <?php echo $t="Rp." . number_format($main->biaya_up_hotel_madinah,0,',','.') ?>">
                    <label for="form_control_1">Request Hotel Madinah</label>
                
                        <select class="form-control jeniskelamin" name="status_up_hotel_madinah">
                            <option <?php echo ("0" === $main->status_up_hotel_madinah) ? 'selected' : ''; ?> value="0">Belum Terverifikasi</option>
                            <option <?php echo ("1" === $main->status_up_hotel_madinah) ? 'selected' : ''; ?> value="1">Terverifikasi</option>
                        </select>
                </div>
            </div>  -->
        </div> 
            
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>