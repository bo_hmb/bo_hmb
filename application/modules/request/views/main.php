<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>schedule_request">Mainivest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>schedule_request"> List Request</a>
                    <i class="fa fa-circle"></i>
                    
                </li> 
                <li>
                    <a href="#"><?php echo $jadwal->nm_jadwal ?></a>
                </li> 
                
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data  List Request <?php echo $jadwal->nm_jadwal ?></span>
                        </div>
                           <div class="actions">
                                <div class="btn-group">
                                <!-- <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown"> -->
                                <a class="btn red btn-outline btn-circle btn-export" href="<?php echo base_url(); ?>request/cetak/<?php echo $jadwal->id ?>" target="_blank">
                                    <i class="fa fa-share"></i>
                                    <span class="hidden-xs"> Export </span>
                                    <!-- <i class="fa fa-angle-down"></i> -->
                                </a>

                                <!-- <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="btn-export"> Based On Schedule </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;"> PDF </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="javascript:;"> Print </a>
                                    </li>
                                </ul> -->
                            </div>
                            </div>
                        
                    </div>
                    <script>
                    id_jadwal = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <th width="15%">No. Pendaftaran</th>
                                        <th width="15%">Nama Lengkap</th>
                                        <th width="13%">No. HP</th>
                                        <th width="13%">L/P</th>
                                        <th width="15%">Jenis Kamar</th>
                                        <!-- <th width="15%">Jenis Kamar Madinah</th> -->
                                        <th width="15%">Request Hotel</th>
                                        <!-- <th width="15%">Request Hotel Madinah</th> -->
                                        <th width="15%">Keluarga Terdekat</th>
                                        <th width="15%">Paket</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <td> </td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_pendaftaran"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="hp"> </td>
                                        <td>
                                            <select class="form-control form-filter input-sm"name="jk">
                                                <option></option>
                                                <option value="L">L</option>
                                                <option value="P">P</option>                                                
                                            </select>
                                        <td>
                                            <select class="form-control form-filter input-sm"name="jenis_kamar">
                                                <option></option>
                                                <option value="Double">Double</option>
                                                <option value="Triple">Triple</option>
                                                <option value="Quad">Quad</option>
                                                <option value="Quint">Quint</option>
                                            </select>
                                        </td>
                                        <!-- <td>
                                            <select class="form-control form-filter input-sm"name="jenis_kamar_madinah">
                                                <option></option>
                                                <option value="Double">Double</option>
                                                <option value="Triple">Triple</option>
                                                <option value="Quad">Quad</option>
                                                <option value="Quint">Quint</option>
                                            </select>
                                        </td> -->
                                        <td><input type="text" class="form-control form-filter input-sm" name="hotel_request_makkah"> </td>
                                        <!-- <td><input type="text" class="form-control form-filter input-sm" name="hotel_request_madinah"> </td> -->
                                        <td> </td>
                                        <td>                                            
                                            <select class="form-control form-filter input-sm" name="nm_paket">
                                                <option></option>

                                                <?php 
                                                    $packet = Modules::run('packet/get_where', array('paket.active' => '1','paket.kd_cabang'=>$this->kode_cabang))->result();
                                                    foreach ($packet as $row) { ?> 
                                                    <option value="<?php echo $row->nm_paket; ?>"><?php echo $row->nm_paket; ?></option>    
                                                <?php } ?>
                                            </select>
                                        </td>
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>