<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=List_Request.xls");
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>LIST REQUEST</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 200PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h2 style="text-align: center; font-weight: bold;">LIST REQUEST</h2>
        </th>
    </tr>
    <tr>
        <th style="border:0px solid #000;"></th>
        <th style="border:0px solid #000; width: 200PX"></th>
        <th style="border:0px solid #000; width: 500PX">                 
            <h3 style="text-align: center;">Keberangkatan <?php echo $main->nm_jadwal; ?></h3>
        </th>
    </tr>
</table>
<hr>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>        
        <th style="width: 100px;">No. Pendaftaran</th>
        <th style="width: 250px;">Nama Lengkap</th>
        <th style="width: 150px;">No. HP</th>
        <th style="width: 30px;">J/K</th>
        <th style="width: 100px;">Paket</th>
        <th style="width: 50px;">Jenis Kamar</th>
        <!-- <th style="width: 50px;">Kamar Madinah</th> -->
        <th style="width: 70px;">Jenis Hotel</th>
        <!-- <th style="width: 70px;">Htl. Madinah</th> -->
        <th style="width: 250px;">Keluarga Terdekat</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) { 


                if(($row->jenis_kamar == 'Double') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar = 'background-color: #ff0000';
                if(($row->jenis_kamar == 'Double') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar = 'background-color: #00b8e6';

                if(($row->jenis_kamar == 'Triple') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar = 'background-color: #ff0000';
                if(($row->jenis_kamar == 'Triple') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar = 'background-color: #00b8e6';

                if(($row->jenis_kamar == 'Quad') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar = 'background-color: #ff0000';
                if(($row->jenis_kamar == 'Quad') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar = 'background-color: #00b8e6';

                if(($row->jenis_kamar == 'Quint') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar = 'background-color: #ff0000';
                if(($row->jenis_kamar == 'Quint') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar = 'background-color: #00b8e6';
                ///
                if(($row->jenis_kamar_madinah == 'Double') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar_madinah = 'background-color: #ff0000';
                if(($row->jenis_kamar_madinah == 'Double') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar_madinah = 'background-color: #00b8e6';

                if(($row->jenis_kamar_madinah == 'Triple') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar_madinah = 'background-color: #ff0000';
                if(($row->jenis_kamar_madinah == 'Triple') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar_madinah = 'background-color: #00b8e6';

                if(($row->jenis_kamar_madinah == 'Quad') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar_madinah = 'background-color: #ff0000';
                if(($row->jenis_kamar_madinah == 'Quad') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar_madinah = 'background-color: #00b8e6';

                if(($row->jenis_kamar_madinah == 'Quint') AND ($row->status_upgrade_kamar == '0')) $warna_jenis_kamar_madinah = 'background-color: #ff0000';
                if(($row->jenis_kamar_madinah == 'Quint') AND ($row->status_upgrade_kamar == '1')) $warna_jenis_kamar_madinah = 'background-color: #00b8e6';
                ///
                if(($row->hotel_request_makkah == 'Default') AND ($row->status_up_hotel_makkah == '0')) $warna_makkah = 'background-color: #ff0000';
                if(($row->hotel_request_makkah == 'Default') AND ($row->status_up_hotel_makkah == '1')) $warna_makkah = 'background-color: #00b8e6';

                if(($row->hotel_request_makkah == '*4') AND ($row->status_up_hotel_makkah == '0')) $warna_makkah = 'background-color: #ff0000';
                if(($row->hotel_request_makkah == '*4') AND ($row->status_up_hotel_makkah == '1')) $warna_makkah = 'background-color: #00b8e6';

                if(($row->hotel_request_makkah == '*5') AND ($row->status_up_hotel_makkah == '0')) $warna_makkah = 'background-color: #ff0000';
                if(($row->hotel_request_makkah == '*5') AND ($row->status_up_hotel_makkah == '1')) $warna_makkah = 'background-color: #00b8e6';
                ///
                if(($row->hotel_request_madinah == 'Default') AND ($row->status_up_hotel_madinah == '0')) $warna_madinah = 'background-color: #ff0000';
                if(($row->hotel_request_madinah == 'Default') AND ($row->status_up_hotel_madinah == '1')) $warna_madinah = 'background-color: #00b8e6';

                if(($row->hotel_request_madinah == '*4') AND ($row->status_up_hotel_madinah == '0')) $warna_madinah = 'background-color: #ff0000';
                if(($row->hotel_request_madinah == '*4') AND ($row->status_up_hotel_madinah == '1')) $warna_madinah = 'background-color: #00b8e6';

                if(($row->hotel_request_madinah == '*5') AND ($row->status_up_hotel_madinah == '0')) $warna_madinah = 'background-color: #ff0000';
                if(($row->hotel_request_madinah == '*5') AND ($row->status_up_hotel_madinah == '1')) $warna_madinah = 'background-color: #00b8e6';
        
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_pendaftaran ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $row->hp ?></td>
           <td style="border:1px solid #000;"><?php echo $row->jk ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_paket ?></td>
           <td style="border:1px solid #000;<?php echo $warna_jenis_kamar; ?>"><?php echo $row->jenis_kamar ?></td>
           <!-- <td style="border:1px solid #000;<?php echo $warna_jenis_kamar_madinah; ?>"><?php echo $row->jenis_kamar_madinah ?></td> -->
           <td style="border:1px solid #000;<?php echo $warna_makkah; ?>"><?php echo $row->hotel_request_makkah ?></td>
           <!-- <td style="border:1px solid #000;<?php echo $warna_madinah; ?>"><?php echo $row->hotel_request_madinah ?></td> -->
           <td style="border:1px solid #000;"><?php echo $row->keluarga ?></td>
           <!-- <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->benefit,0,',','.') ?></td> -->
           <!-- <td style="border:1px solid #000;<?php echo $warna; ?>"><?php echo $status ?></td>       -->
        </tr>   
    <?php $nomor++;}?> 

</table>

<br>
            
<!-- <table>    
    <tr> 
    <?php $warna = 'background-color: #ff0000';            
            $warna2 = 'background-color: #39e600'; ?>          
        <th style="width: 20px;">No</th>        
        <th style="width: 40px;">ID Marketing</th>
        <th style="width: 150px;">Nama Marketing</th>
        <th style="width: 100px;<?php echo $warna2; ?>">Benefit Lunas</th>
        <th style="width: 100px;<?php echo $warna; ?>">Benefit Belum Lunas</th>
    </tr>

    <?php $nomor=1; foreach($sum as $row) { 
            ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nik ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->benefit_lunas,0,',','.') ?></td>      
           <td style="border:1px solid #000;"><?php echo 'Rp. '.$t=number_format($row->benefit_belum_lunas,0,',','.') ?></td>      
        </tr>   
    <?php $nomor++;}?> 

</table> -->
</body>
</html>