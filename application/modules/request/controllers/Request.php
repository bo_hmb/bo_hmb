<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Request extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Request';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            $this->load->model('M_request');
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                // $this->data['userGroup'] = $this->userLevel;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function schedule()
        {
            // Page components

            $this->data['pageTitle'] = 'Request';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['id_data'] = $this->uri->segment(3);
            $this->data['jadwal'] = $this->M_schedule->get_where(array('id' => $this->uri->segment(3)))->row();
            $this->data['content'] = $this->load->view('main', $this->data, true);
            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $jk = $this->input->post('jk',TRUE);
            $jenis_kamar = $this->input->post('jenis_kamar',TRUE);
            $hotel_request_makkah = $this->input->post('hotel_request_makkah',TRUE);
            $hotel_request_madinah = $this->input->post('hotel_request_madinah',TRUE);
            $keluarga = $this->input->post('keluarga',TRUE);           
            $nm_paket = $this->input->post('nm_paket',TRUE);           

            $cols = array();
            if (!empty($no_pendaftaran)) { $cols['no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($jk)) { $cols['jk'] = $jk; }
            if (!empty($jenis_kamar)) { $cols['jenis_kamar'] = $jenis_kamar; }
            if (!empty($hotel_request_makkah)) { $cols['hotel_request_makkah'] = $hotel_request_makkah; }
            if (!empty($hotel_request_madinah)) { $cols['hotel_request_madinah'] = $hotel_request_madinah; }
            if (!empty($keluarga)) { $cols['keluarga'] = $keluarga; }
            if (!empty($nm_paket)) { $cols['nm_paket'] = $nm_paket; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1' AND id_jadwal = ".$id."";
            } else {
             $where = "active = '1' AND id_jadwal = ".$id."";
            }

	        $list = $this->M_request->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_request->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                
                                
                            </div>';
                if(($r->jenis_kamar == 'Double') AND ($r->status_upgrade_kamar == '0')) $label = 'danger';
                if(($r->jenis_kamar == 'Double') AND ($r->status_upgrade_kamar == '1')) $label = 'success';

                if(($r->jenis_kamar == 'Triple') AND ($r->status_upgrade_kamar == '0')) $label = 'danger';
                if(($r->jenis_kamar == 'Triple') AND ($r->status_upgrade_kamar == '1')) $label = 'success';

                if(($r->jenis_kamar == 'Quad') AND ($r->status_upgrade_kamar == '0')) $label = 'danger';
                if(($r->jenis_kamar == 'Quad') AND ($r->status_upgrade_kamar == '1')) $label = 'success';

                if(($r->jenis_kamar == 'Quint') AND ($r->status_upgrade_kamar == '0')) $label = 'danger';
                if(($r->jenis_kamar == 'Quint') AND ($r->status_upgrade_kamar == '1')) $label = 'success';
                ///
                if(($r->jenis_kamar_madinah == 'Double') AND ($r->status_upgrade_kamar_madinah == '0')) $label_kamar_madinah = 'danger';
                if(($r->jenis_kamar_madinah == 'Double') AND ($r->status_upgrade_kamar_madinah == '1')) $label_kamar_madinah = 'success';

                if(($r->jenis_kamar_madinah == 'Triple') AND ($r->status_upgrade_kamar_madinah == '0')) $label_kamar_madinah = 'danger';
                if(($r->jenis_kamar_madinah == 'Triple') AND ($r->status_upgrade_kamar_madinah == '1')) $label_kamar_madinah = 'success';

                if(($r->jenis_kamar_madinah == 'Quad') AND ($r->status_upgrade_kamar_madinah == '0')) $label_kamar_madinah = 'danger';
                if(($r->jenis_kamar_madinah == 'Quad') AND ($r->status_upgrade_kamar_madinah == '1')) $label_kamar_madinah = 'success';

                if(($r->jenis_kamar_madinah == 'Quint') AND ($r->status_upgrade_kamar_madinah == '0')) $label_kamar_madinah = 'danger';
                if(($r->jenis_kamar_madinah == 'Quint') AND ($r->status_upgrade_kamar_madinah == '1')) $label_kamar_madinah = 'success';
                ///
                if(($r->hotel_request_makkah == 'Default') AND ($r->status_up_hotel_makkah == '0')) $label_hotel_makkah = 'danger';
                if(($r->hotel_request_makkah == 'Default') AND ($r->status_up_hotel_makkah == '1')) $label_hotel_makkah = 'success';

                if(($r->hotel_request_makkah == '*4') AND ($r->status_up_hotel_makkah == '0')) $label_hotel_makkah = 'danger';
                if(($r->hotel_request_makkah == '*4') AND ($r->status_up_hotel_makkah == '1')) $label_hotel_makkah = 'success';

                if(($r->hotel_request_makkah == '*5') AND ($r->status_up_hotel_makkah == '0')) $label_hotel_makkah = 'danger';
                if(($r->hotel_request_makkah == '*5') AND ($r->status_up_hotel_makkah == '1')) $label_hotel_makkah = 'success';
                ///
                if(($r->hotel_request_madinah == 'Default') AND ($r->status_up_hotel_madinah == '0')) $label_hotel_madinah = 'danger';
                if(($r->hotel_request_madinah == 'Default') AND ($r->status_up_hotel_madinah == '1')) $label_hotel_madinah = 'success';

                if(($r->hotel_request_madinah == '*4') AND ($r->status_up_hotel_madinah == '0')) $label_hotel_madinah = 'danger';
                if(($r->hotel_request_madinah == '*4') AND ($r->status_up_hotel_madinah == '1')) $label_hotel_madinah = 'success';

                if(($r->hotel_request_madinah == '*5') AND ($r->status_up_hotel_madinah == '0')) $label_hotel_madinah = 'danger';
                if(($r->hotel_request_madinah == '*5') AND ($r->status_up_hotel_madinah == '1')) $label_hotel_madinah = 'success';


                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    $r->no_pendaftaran,
                    $r->nm_lengkap,
                    $r->hp,
                    $r->jk,
                    // $r->jenis_kamar,
                    '<span class="label label-sm label-'.$label.'">'.$r->jenis_kamar.'</span>',
                    // '<span class="label label-sm label-'.$label_kamar_madinah.'">'.$r->jenis_kamar_madinah.'</span>',
                    '<span class="label label-sm label-'.$label_hotel_makkah.'">'.$r->hotel_request_makkah.'</span>',
                    // '<span class="label label-sm label-'.$label_hotel_madinah.'">'.$r->hotel_request_madinah.'</span>',

                    // $r->hotel_request_makkah,                    
                    // $r->hotel_request_madinah,
                    $r->keluarga,
                    $r->nm_paket,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
            $data['id_jadwal'] = $id;
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

			$data = array(
				'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'group_code' => $this->input->post('group_code',TRUE),
                'brach' => $this->input->post('brach',TRUE),
                'brach_code' => $this->input->post('brach_code',TRUE),
                'total_pax' => $this->input->post('total_pax',TRUE),
                'personn_incharge' => $this->input->post('personn_incharge',TRUE),
                'dep_date' => $this->input->post('dep_date',TRUE),
                'return_date' => $this->input->post('return_date',TRUE),
                'carrier' => $this->input->post('carrier',TRUE),
                'pnr' => $this->input->post('pnr',TRUE),
                'flight_detail' => $this->input->post('flight_detail',TRUE),
                'bus' => $this->input->post('bus',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_request->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_request->get_where(array('id' => $id))->row();
            // $data['schedule'] = $this->M_schedule->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            $id = $this->input->post('id');

            $data = array(
                'status_upgrade_kamar' => $this->input->post('status_upgrade_kamar',TRUE),
                'status_upgrade_kamar_madinah' => '0',
                'status_up_hotel_makkah' => $this->input->post('status_up_hotel_makkah',TRUE),
                'status_up_hotel_madinah' => '0', 
                'user_req_update' => $this->currentUser->id,               
            );

            $query = $this->M_request->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_req_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_request->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            $this->form_validation->set_rules('group_code', 'group code', 'trim|required');
            $this->form_validation->set_rules('brach', 'brach', 'trim|required');
           
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cetak($id=null)
        {

            //$cek =  $this->M_payment->get_where_payment(array('active'=>'1','id_pendaftaran'=>$id,'status_verifikasi'=>0))->num_rows();

            
                    $data = [];

                    $data['title'] = "Report";

                    //get data main
                    
                    $data['main'] = $this->M_request->get_where_jadwal(array('id' => $id ))->row();
                    $data['detail'] = $this->M_request->get_where(array('id_jadwal' => $id))->result();

                    // $html = $this->load->view('welcome_message', $data, true);
                    $html = $this->load->view('request/print_export', $data, TRUE);
                
                    //this the the PDF filename that user will get to download
                    $pdfFilePath = "report.pdf";

                    //mPDF versi 7
                    $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'A4-L',
                                        'orientation' => 'L'
                                    ]);

                    $mpdf->WriteHTML($html);
                    $mpdf->Output();    
              
        }

        public function cetak_excel($id=null)
        {
            $data['main'] = $this->M_request->get_where_jadwal(array('id' => $id ))->row();
            $data['detail'] = $this->M_request->get_where(array('id_jadwal' => $id))->result();

            // $html = $this->load->view('welcome_message', $data, true);
            $this->load->view('request/print_export_excel', $data);
        }

    }
?>
