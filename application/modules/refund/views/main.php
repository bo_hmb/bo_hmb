<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Finance</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Refund / Reschedule</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Refund / Reschedule</span>
                        </div>
                        <div class="actions">
                            <button id="add-btn-refund" class="btn sbold red"> Add Refund
                                <i class="fa fa-plus"></i>
                            </button>
                            <button id="add-btn" class="btn sbold green"> Add Reschedule
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <!-- <th width="1%">Aksi</th> -->
                                        <th width="10%">Nama Cabang</th>
                                        <th width="13%">No. Pendaftaran</th>
                                        <th width="10%">No. KTP</th>
                                        <th width="10%">Nama Lengkap</th> 
                                        <th width="10%">Jenis</th>
                                        <th width="15%">Keterangan</th>
                                        <th width="15%">Alasan</th>
                                        <th width="10%">Refund</th>
                                    </tr>
                                    <tr role="row" class="filter">
                                        <!-- <td> </td> -->
                                        <td>
                                            <div class="margin-bottom-5">
                                                <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                    <i class="fa fa-search"></i></button>
                                            </div>
                                            <button class="btn btn-sm red btn-outline filter-cancel">
                                                <i class="fa fa-times"></i></button>
                                        </td>
                                        <td>
                                            <select name="nm_cabang" class="form-control form-filter input-sm">
                                                <option></option>
                                                <?php
                                                    $cabang= $this->M_refund->get_cabang(array('active' => '1'))->result();
                                                    foreach ($cabang as $row) { ?>
                                                    <option value="<?php echo $row->nm_cabang; ?>"><?php echo $row->nm_cabang; ?></option>   
                                                <?php }?>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_pendaftaran"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="no_ktp"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="nm_lengkap"> </td>
                                        <td>
                                            <select name="jenis" class="form-control form-filter input-sm">
                                                <option></option>
                                                <option value="Refund">Refund</option>
                                                <option value="Reschedule">Reschedule</option>
                                            </select> 
                                        </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="ket"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="alasan"> </td>
                                        <td><input type="text" class="form-control form-filter input-sm" name="biaya"> </td>
                                        

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>