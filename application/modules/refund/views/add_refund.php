<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <input type="hidden" name="id_paket_lama" class="id_paket_lama" id="id_paket_lama">
            <input type="hidden" name="id_paket_baru" id="id_paket_baru" class="id_paket_baru">
            <input type="hidden" name="jenis" id="jenis" value="Refund">
            <input type="hidden" name="ket" id="ket" value="Refund">
            <div class="col-md-9">        
                <div class="form-group">
                <div class="fg-line">
                    <label>Pilih Data Registrasi</label>
                    <select id="id_registrsi" name="id_registrsi" class="form-control pilih id_registrsi" data-placeholder="Pilih">
                        <option disabled selected></option>
                        <option></option>
                        <?php
                        foreach ($regist as $row) { ?>
                        <option value="<?php echo $row->id; ?>">NIK : <?php echo $row->no_ktp; ?> - <?php echo $row->nm_ktp; ?> / <?php echo $row->nm_lengkap; ?></option>                    
                        <?php
                        }
                        ?>
                    </select>
                </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control h_" name="h_" id="h_" placeholder="H -">
                    <label for="form_control_1">H - Keberangkatan
                    </label>
                    <span class="help-block">H - Keberangkatan</span>
                </div>
            </div>
        </div>
        <div class="row">
            <br>            
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control total_biaya money" name="total_biaya" id="total_biaya" placeholder="Total Biaya">
                    <label for="form_control_1">Total Biaya
                    </label>
                    <span class="help-block">Total Biaya</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control telah_bayar money" name="telah_bayar" id="telah_bayar" placeholder="Telah Bayar">
                    <label for="form_control_1">Telah Bayar
                    </label>
                    <span class="help-block">Telah Bayar</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control penalti money" name="penalti" id="penalti" placeholder="Biaya Pinalti Pembatalan">
                    <label for="form_control_1">Biaya Pinalti Pembatalan
                    </label>
                    <span class="help-block">Biaya Pinalti Pembatalan</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control refund money" name="refund" id="refund" placeholder="Refund">
                    <label for="form_control_1">Refund
                    </label>
                    <span class="help-block">Refund</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="alasan" placeholder="Masukkan Alasan">
                    <label for="form_control_1">Alasan
                    </label>
                    <span class="help-block">Masukkan Alasan</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="file" class="form-control" name="path_bukti_bayar" id="path_bukti_bayar" placeholder="Masukkan Bukti Bayar">
                    <label>Bukti Bayar
                    </label>
                    <span class="help-block">Masukkan Bukti Bayar</span>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Refund</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>