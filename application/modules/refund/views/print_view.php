<?php
function terbilang($x) {
      $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
      if ($x < 12)
        return " " . $angka[$x];
      elseif ($x < 20)
        return terbilang($x - 10) . " belas";
      elseif ($x < 100)
        return terbilang($x / 10) . " puluh" . terbilang($x % 10);
      elseif ($x < 200)
        return " seratus" . terbilang($x - 100);
      elseif ($x < 1000)
        return terbilang($x / 100) . " ratus" . terbilang($x % 100);
      elseif ($x < 2000)
        return " seribu" . terbilang($x - 1000);
      elseif ($x < 1000000)
        return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
      elseif ($x < 1000000000)
        return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    }
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>DATA INCOME</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<?php $kota = $this->M_payment->get_kota_where(array('id_kota' => $cabang->kota))->row(); ?> 
<table>
    <tr>
        <td style="border:0px solid #000;width: 50px;">
        <table>    
            <tr>           
                <td style="border:0px solid #000;width: 90px;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="100" width="97"></td>
            </tr>
        </table>
        </td>
        <td>
        <table>    
            <tr>    
                <td style="border:0px solid #000; width: 330px">PT. ZNH INTERNASIONAL INDONESIA</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nomor </td>
                <td style="border:0px solid #000; width: 200px">: 
                    <?php 
                    if ($detail_row->no_faktur == null) {
                        echo '-';
                    } else {
                        echo $detail_row->no_faktur;
                    }

                    ?>
                    
                </td>
            </tr>
            <?php 
                if ($main->tgl_keberangkatan == '0000-00-00') {
                    $tgl_keberangkatan='Paket Tabungan';
                } else {
                    $tgl_keberangkatan=date('d-m-Y', strtotime($main->tgl_keberangkatan));
                }
            ?>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Jl. Adhyaksa Baru, Ruko Zamrud 2, No K1/K2</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Tgl. Keberangkatan </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $tgl_keberangkatan;?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Panakukang, Makassar, Sul-sel</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nama Agen </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $main->nm_marketing ?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Telp : 0411-4679170</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nama Jamaah </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $main->nm_lengkap ?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Kantor Cabang : <?php echo $cabang->nm_cabang ?></td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px"></td>
                <td style="border:0px solid #000; width: 200px"></td>
            </tr>
        </table>
        </td>
    </tr>
</table>
<hr>
<br> 
<table>    
    <tr>     
        <th style="border:0px solid #000; width: 182px"></th>
        <th style="border:0px solid #000; width: 80px"></th>
        <th style="border:0px solid #000; width: 140px"><u> <h2>KWITANSI</h2> </u></th>
        <th style="border:0px solid #000; width: 80px"></th>
        <th style="border:0px solid #000; width: 182px"></th>  
    </tr>
</table>
<br> 
<table>    
    <tr>            
        <td style="width: 210px"> 
            Telah Diterima Dari
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $main->nm_lengkap ?>
        </td>
    <tr>            
        <td style="width: 210px"> 
            Sejumlah
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo 'Rp. '.$t=number_format($detail_row->jml_bayar,0,',','.').',-' ?> <br>
            <i><?php echo ucwords(terbilang($detail_row->jml_bayar)).' Rupiah' ?></i>
        </td>

    </tr>

    <tr>            
        <td style="width: 210px"> 
            Tunai / Transfer
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $detail_row->tunai; ?>
        </td>
    </tr>
</table>
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 400px;">Keterangan</th>
        <th style="width: 250px;">Jumlah</th>
    </tr>

        <?php 
            if ($main->id_paket == '1') {
                $hrg_paket=0;
                $total_biaya=$main->total_biaya ; 
                $potongan=$main->potongan_paket;                                 
            } else { 
                $hrg_paket=$main->hrg_paket;
                $total_biaya=$main->total_biaya ;
                $potongan=$main->potongan_paket;                 
                
            }


            if ($umur->age > 144 && $main->id_paket != '0') {
                $hrg_paket=$main->hrg_paket;
                $total_biaya=$main->total_biaya + $main->potongan_paket;
                $potongan=$main->potongan_paket;                 
            }
            if ($umur->age <= 144 && $main->id_paket != '0') {
                $hrg_paket=$main->hrg_paket - $main->potongan_paket;
                $total_biaya=$main->total_biaya ;
                $potongan=0; 
            }
            
        ?>        
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">1</td>
           <td style="border:1px solid #000;">Harga Paket</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($hrg_paket,0,',','.').',-' ?></td>
        </tr>   

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">2</td>
           <td style="border:1px solid #000;">Biaya Mahram</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_mahram,0,',','.').',-' ?></td>
        </tr>  

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">3</td>
           <td style="border:1px solid #000;">Biaya Progresif</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_progresif,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">4</td>
           <td style="border:1px solid #000;">Biaya Upgrade Hotel</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format(($main->biaya_up_hotel_makkah + $main->biaya_up_hotel_madinah),0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">5</td>
           <td style="border:1px solid #000;">Biaya Upgrade Kamar</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format(($main->biaya_upgrade_kamar + $main->biaya_upgrade_kamar_madinah) ,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">6</td>
           <td style="border:1px solid #000;">Biaya Order Barang</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_order_barang,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">7</td>
           <td style="border:1px solid #000;">Add On</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->add_on,0,',','.').',-' ?></td>
        </tr>

        <?php if ($main->id_paket == '1') { ?>
            <tr>                                                
               <td style="border:1px solid #000;text-align: center;">8</td>
               <td style="border:1px solid #000;">Setoran Awal Paket Tabungan</td>
               <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($detail_row->jml_bayar,0,',','.').',-' ?></td>
            </tr>
        <?php } ?>
        

        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;">Sub Total</td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;"><?php echo $t=number_format($total_biaya,0,',','.').',-';?></td>
        </tr>
        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;">Potongan Pembayaran</td>
            <td  style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($potongan,0,',','.').',-';?></td>
        </tr>
        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;">Total</td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;"><?php echo $t=number_format($total_biaya - $potongan,0,',','.').',-';?></td>
        </tr>

        <?php foreach($detail as $row) {?>        
        <tr>
            <td style="border:1px solid #000;"></td>
            <td style="border:1px solid #000;text-align: right;"><?php echo date('d-m-Y', strtotime($detail_row->tgl_bayar)).' / '.$row->tunai.' / '.$row->catatan ?></td>
            <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->jml_bayar,0,',','.').',-';?></td>
        </tr>
        <?php }?> 

        <tr>                                                
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;text-align: right;">Sisa Pembayaran Terutang</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->sisa,0,',','.').',-' ?></td>
        </tr> 
</table>
<br>
    <p style="text-align: right;"><?php echo $kota->name_regencies; ?>, <?php echo date('d-m-Y'); ?></p>
<br>
<table>
    <tr>
        <th style="border:0px solid #000; width: 250px"></th>
        <th style="border:0px solid #000; width: 182px">Dibuat Oleh</th>
    </tr>
</table>
<br>
<br>
<br>

<table>
    <tr>
        <th style="border:0px solid #000; width: 250px"></th>
        <th style="border:0px solid #000; width: 182px"><?php echo $this->currentUser->first_name; ?><br>Finance Officer</th>
        
    </tr>
</table>
<br>
<table>    
    <tr>            
        <td style="width: 210px"> 
            *Status
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $main->status ?>
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            *Batas Pembayaran
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php 
                if ($main->batas_pelunasan == '0000-00-00') {
                    $batas_pelunasan='-';
                } else {
                    $batas_pelunasan=date('d-m-Y', strtotime($main->batas_pelunasan));
                }
            ?>
            <?php echo $batas_pelunasan ?> <br>            
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            *Rekening
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            PT. ZNH INTERNASIONAL INDONESIA <br>
            Mandiri 152.00.28.29.2999
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            
        </td>
        <td style="width: 1px"> 
            
        </td>
        <td> 
            BRI 107.301.000.3033.07
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td>
            <i>*Jika jamaah tidak melunasi pembayaran setelah melewati batas pembayaran, maka jamaah dianggap mengundurkan diri dan DP di anggap hangus.</i>
        </td>

    </tr>
    <tr>
        <td>
            <i>*LEMBAR PERTAMA : UNTUK JAMAAH</i><br>
        </td>

    </tr>
</table>   
<pagebreak>
<!-- ////////////////// -->
<table>
    <tr>
        <td style="border:0px solid #000;width: 50px;">
        <table>    
            <tr>           
                <td style="border:0px solid #000;width: 90px;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="100" width="97"></td>
            </tr>
        </table>
        </td>
        <td>
        <table>    
            <tr>    
                <td style="border:0px solid #000; width: 330px">PT. ZNH INTERNASIONAL INDONESIA</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nomor </td>
                <td style="border:0px solid #000; width: 200px">: 
                    <?php 
                    if ($detail_row->no_faktur == null) {
                        echo '-';
                    } else {
                        echo $detail_row->no_faktur;
                    }

                    ?>
                    
                </td>
            </tr>
            <?php 
                if ($main->tgl_keberangkatan == '0000-00-00') {
                    $tgl_keberangkatan='Paket Tabungan';
                } else {
                    $tgl_keberangkatan=date('d-m-Y', strtotime($main->tgl_keberangkatan));
                }
            ?>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Jl. Adhyaksa Baru, Ruko Zamrud 2, No K1/K2</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Tgl. Keberangkatan </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $tgl_keberangkatan;?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Panakukang, Makassar, Sul-sel</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nama Agen </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $main->nm_marketing ?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Telp : 0411-4679170</td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px">Nama Jamaah </td>
                <td style="border:0px solid #000; width: 200px">: <?php echo $main->nm_lengkap ?></td>
            </tr>
            <tr>           
                <td style="border:0px solid #000; width: 330px">Kantor Cabang : <?php echo $cabang->nm_cabang ?></td>
                <td style="border:0px solid #000; width: 0px"></td>
                <td style="border:0px solid #000; width: 140px"></td>
                <td style="border:0px solid #000; width: 200px"></td>
            </tr>
        </table>
        </td>
    </tr>
</table>
<hr>
<br> 
<table>    
    <tr>     
        <th style="border:0px solid #000; width: 182px"></th>
        <th style="border:0px solid #000; width: 80px"></th>
        <th style="border:0px solid #000; width: 140px"><u> <h2>KWITANSI</h2> </u></th>
        <th style="border:0px solid #000; width: 80px"></th>
        <th style="border:0px solid #000; width: 182px"></th>  
    </tr>
</table>
<br> 
<table>    
    <tr>            
        <td style="width: 210px"> 
            Telah Diterima Dari
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $main->nm_lengkap ?>
        </td>
    <tr>            
        <td style="width: 210px"> 
            Sejumlah
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo 'Rp. '.$t=number_format($detail_row->jml_bayar,0,',','.').',-' ?> <br>
            <i><?php echo ucwords(terbilang($detail_row->jml_bayar)).' Rupiah' ?></i>
        </td>

    </tr>

    <tr>            
        <td style="width: 210px"> 
            Tunai / Transfer
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $detail_row->tunai; ?>
        </td>
    </tr>
</table>
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 400px;">Keterangan</th>
        <th style="width: 250px;">Jumlah</th>
    </tr>

        <?php 
            if ($main->id_paket == '1') {
                $hrg_paket=0;
                $total_biaya=$main->total_biaya ; 
                $potongan=$main->potongan_paket;                                 
            } else { 
                $hrg_paket=$main->hrg_paket;
                $total_biaya=$main->total_biaya ;
                $potongan=$main->potongan_paket;                 
                
            }


            if ($umur->age > 144 && $main->id_paket != '0') {
                $hrg_paket=$main->hrg_paket;
                $total_biaya=$main->total_biaya + $main->potongan_paket;
                $potongan=$main->potongan_paket;                 
            }
            if ($umur->age <= 144 && $main->id_paket != '0') {
                $hrg_paket=$main->hrg_paket - $main->potongan_paket;
                $total_biaya=$main->total_biaya ;
                $potongan=0; 
            }
            
        ?>        
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">1</td>
           <td style="border:1px solid #000;">Harga Paket</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($hrg_paket,0,',','.').',-' ?></td>
        </tr>   

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">2</td>
           <td style="border:1px solid #000;">Biaya Mahram</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_mahram,0,',','.').',-' ?></td>
        </tr>  

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">3</td>
           <td style="border:1px solid #000;">Biaya Progresif</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_progresif,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">4</td>
           <td style="border:1px solid #000;">Biaya Upgrade Hotel</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format(($main->biaya_up_hotel_makkah + $main->biaya_up_hotel_madinah),0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">5</td>
           <td style="border:1px solid #000;">Biaya Upgrade Kamar</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format(($main->biaya_upgrade_kamar + $main->biaya_upgrade_kamar_madinah) ,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">6</td>
           <td style="border:1px solid #000;">Biaya Order Barang</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->biaya_order_barang,0,',','.').',-' ?></td>
        </tr> 

        <tr>                                                
           <td style="border:1px solid #000;text-align: center;">7</td>
           <td style="border:1px solid #000;">Add On</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->add_on,0,',','.').',-' ?></td>
        </tr>

        <?php if ($main->id_paket == '1') { ?>
            <tr>                                                
               <td style="border:1px solid #000;text-align: center;">8</td>
               <td style="border:1px solid #000;">Setoran Awal Paket Tabungan</td>
               <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($detail_row->jml_bayar,0,',','.').',-' ?></td>
            </tr>
        <?php } ?>
        

        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;">Sub Total</td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;"><?php echo $t=number_format($total_biaya,0,',','.').',-';?></td>
        </tr>
        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;">Potongan Pembayaran</td>
            <td  style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($potongan,0,',','.').',-';?></td>
        </tr>
        <tr>
            <td style="border:1px solid #000;"></td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;">Total</td>
            <td  style="border:1px solid #000;text-align: right;font-weight: bold;"><?php echo $t=number_format($total_biaya - $potongan,0,',','.').',-';?></td>
        </tr>

        <?php foreach($detail as $row) {?>        
        <tr>
            <td style="border:1px solid #000;"></td>
            <td style="border:1px solid #000;text-align: right;"><?php echo date('d-m-Y', strtotime($detail_row->tgl_bayar)).' / '.$row->tunai.' / '.$row->catatan ?></td>
            <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($row->jml_bayar,0,',','.').',-';?></td>
        </tr>
        <?php }?> 

        <tr>                                                
           <td style="border:1px solid #000;"></td>
           <td style="border:1px solid #000;text-align: right;">Sisa Pembayaran Terutang</td>
           <td style="border:1px solid #000;text-align: right;"><?php echo $t=number_format($main->sisa,0,',','.').',-' ?></td>
        </tr> 
</table>
<br>
    <p style="text-align: right;"><?php echo $kota->name_regencies; ?>, <?php echo date('d-m-Y'); ?></p>
<br>
<table>
    <tr>
        <th style="border:0px solid #000; width: 250px"></th>
        <th style="border:0px solid #000; width: 182px">Dibuat Oleh</th>
    </tr>
</table>
<br>
<br>
<br>

<table>
    <tr>
        <th style="border:0px solid #000; width: 250px"></th>
        <th style="border:0px solid #000; width: 182px"><?php echo $this->currentUser->first_name; ?><br>Finance Officer</th>
        
    </tr>
</table>
<br>
<table>    
    <tr>            
        <td style="width: 210px"> 
            *Status
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php echo $main->status ?>
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            *Batas Pembayaran
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            <?php 
                if ($main->batas_pelunasan == '0000-00-00') {
                    $batas_pelunasan='-';
                } else {
                    $batas_pelunasan=date('d-m-Y', strtotime($main->batas_pelunasan));
                }
            ?>
            <?php echo $batas_pelunasan ?> <br>            
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            *Rekening
        </td>
        <td style="width: 1px"> 
            :
        </td>
        <td> 
            PT. ZNH INTERNASIONAL INDONESIA <br>
            Mandiri 152.00.28.29.2999
        </td>
    </tr>
    <tr>            
        <td style="width: 210px"> 
            
        </td>
        <td style="width: 1px"> 
            
        </td>
        <td> 
            BRI 107.301.000.3033.07
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td>
            <i>*Jika jamaah tidak melunasi pembayaran setelah melewati batas pembayaran, maka jamaah dianggap mengundurkan diri dan DP di anggap hangus.</i>
        </td>

    </tr>
    <tr>
        <td style="background-color: #ff0000">
            <i>*LEMBAR KEDUA : UNTUK ARSIP FINANCE</i><br>
        </td>
    </tr>
</table>  

</body>
</html>