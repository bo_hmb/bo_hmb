<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Refund extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');
            // $this->kode_cabang = $this->currentUser->kode_cabang;

            // Module components            
            $this->data['module'] = 'Refund';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_refund');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                // $this->userLevel = $userGroup;                
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            $this->data['pageTitle'] = 'Refund';
            $this->data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result(); 
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);
            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_cabang = $this->input->post('nm_cabang',TRUE);
            $no_pendaftaran = $this->input->post('no_pendaftaran',TRUE);
            $no_ktp = $this->input->post('no_ktp',TRUE);
            $nm_lengkap = $this->input->post('nm_lengkap',TRUE);
            $jenis = $this->input->post('jenis',TRUE);
            $ket = $this->input->post('ket',TRUE);
            $alasan = $this->input->post('alasan',TRUE);

			$cols = array();
			if (!empty($nm_cabang)) { $cols['nm_cabang'] = $nm_cabang; }
            if (!empty($no_pendaftaran)) { $cols['no_pendaftaran'] = $no_pendaftaran; }
            if (!empty($no_ktp)) { $cols['no_ktp'] = $no_ktp; }
            if (!empty($nm_lengkap)) { $cols['nm_lengkap'] = $nm_lengkap; }   
			if (!empty($jenis)) { $cols['refund.jenis'] = $jenis; }
            if (!empty($ket)) { $cols['ket'] = $ket; }   
			if (!empty($alasan)) { $cols['alasan'] = $alasan; }   

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "refund.active = '1'";              
            } else if(array_key_exists('Admin Finance', $this->userLevel)) {
             $where = "refund.active = '1'";              
            } else if(array_key_exists('Finance', $this->userLevel)) {
             $where = "refund.active = '1'";              //AND kd_office = '$this->kode_cabang'
            }       


	        $list = $this->M_refund->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_refund->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;


                
                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                            </div>';
            
                $records["data"][] = array(
                    $no,   
                    // $btn_action,
                    $r->nm_cabang,
                    $r->no_pendaftaran,
                    $r->no_ktp,
                    $r->nm_lengkap,
                    $r->jenis,
                    $r->ket,
                    $r->alasan,
                    $t="Rp." . number_format($r->biaya,0,',','.'),
                );


            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_add_form()
		{
			$data['title'] = 'Add Data Registrasi';
            $data['packet'] = Modules::run('packet/get_where', array('paket.active' => '1','kd_cabang'=>$this->kode_cabang))->result();
            $data['regist'] = $this->M_refund->get_registrasi();
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

        public function load_add_form_refund()
        {
            $data['title'] = 'Add Data Registrasi';
            $data['packet'] = Modules::run('packet/get_where', array('paket.active' => '1','kd_cabang'=>$this->kode_cabang))->result();
            $data['regist'] = $this->M_refund->get_registrasi();
            return response($this->load->view('add_refund', $data, TRUE), 'html');
        }

		public function add()
	    {
            $this->ajaxRequest();
			$config['upload_path']="./upload";
            $config['allowed_types']='gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload',$config);
            if($this->upload->do_upload("file")){
                $data = array('upload_data' => $this->upload->data());
                $image = $data['upload_data']['file_name']; 

                $id = $this->input->post('id_registrsi');
                $data1 = array(
                        'active' => '0',
                        );
                $query1 = $this->M_refund->_update(array('id_pendaftar' => $id), $data1); 

                $data = array(
                        'id_pendaftar' => $this->input->post('id_registrsi',TRUE),
                        'id_paket_lama' => $this->input->post('id_paket_lama',TRUE),
                        'id_paket_baru' => $this->input->post('id_paket_baru',TRUE),
                        'jenis' => $this->input->post('jenis',TRUE),
                        'biaya' => preg_replace("/[^0-9\.]/", "", $this->input->post('refund',TRUE)),
                        'ket' => $this->input->post('ket',TRUE),
                        'alasan' => $this->input->post('alasan',TRUE),
                        'path_bukti_bayar' => $image,
                        'active' => '1',
                        'id_user' => $this->currentUser->id,
                        );
                $query = $this->M_refund->_insert($data);

                $data2 = array(
                        'id_paket' => $this->input->post('id_paket_baru',TRUE),
                        'id_user_lastupdate' => $this->currentUser->id,
                        );
                $query2 = $this->M_refund->_update_regist(array('id' => $id), $data2); 

                $cek = $this->M_refund->get_where_payment(array('active'=>'1'))->num_rows();
                $faktur = 'IO-'.date('m').date("Y").sprintf('%03d',$cek+1);
                $data3 = array(
                            'no_faktur' => $faktur,
                            'jenis' => 'IO',
                            'id_pendaftaran' => 0,
                            'tgl_bayar' => date('Y-m-d'),
                            'jml_bayar' => preg_replace("/[^0-9\.]/", "", $this->input->post('penalti',TRUE)),
                            'path_bukti_bayar' => $image,
                            'catatan' => 'Biaya Reschedule',
                            'tunai' =>  $this->input->post('tunai',TRUE),
                            'active' => '1',
                            'status_verifikasi' => '1',
                            'id_user' => $this->currentUser->id,
                            );
                $query3 = $this->M_refund->_insert_pembayaran($data3);
            } 
         
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_refund->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function load_payment_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_refund->get_detail($id);
            return response($this->load->view('add_', $data, TRUE), 'html');
        }

        public function load_export()
        {
            $data['title'] = '';
            $data['schedule'] = Modules::run('schedule/get_schedule_where', array('active' => '1'))->result();    
            return response($this->load->view('export', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'nM_refund' => $this->input->post('nM_refund',TRUE),
                'kota_lokasi' => $this->input->post('kota_lokasi',TRUE),
                'alamat' => $this->input->post('alamat',TRUE),
                'jrk_mkkh_masjidl' => $this->input->post('jrk_mkkh_masjidl',TRUE),
                'jrk_mdinh_msjidl' => $this->input->post('jrk_mdinh_msjidl',TRUE),
                'bintang' => $this->input->post('bintang',TRUE),
                'biaya_sewa_kamar' => $this->input->post('biaya_sewa_kamar',TRUE),
                'biaya_sewa_hotel' => $this->input->post('biaya_sewa_hotel',TRUE),
                'biaya_upgrade_hotel' => $this->input->post('biaya_upgrade_hotel',TRUE),
                'biaya_upgrade_kamar' => $this->input->post('biaya_upgrade_kamar',TRUE),
            );

            $query = $this->M_refund->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_refund->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('tgl_bayar', 'tgl_bayar', 'trim|required');
            $this->form_validation->set_rules('jml_bayar', 'jml_bayar', 'trim|required|numeric');
            // $this->form_validation->set_rules('path_bukti_bayar', 'path_bukti_bayar', 'trim|required');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function get_paket_where()
        {
            $id = $this->input->post('id');
            $regist = $this->M_refund->get_registrasi_where('where pendaftar.id='.$id.'')->row();
            $data = Modules::run('packet/get_where', array('paket.id' => $regist->id_paket))->row();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function get_paket_baru()
        {
            $id = $this->input->post('id');
            $data = Modules::run('packet/get_where', array('paket.id' => $id))->row();
            // $this->output->enable_profiler(TRUE);
            echo json_encode($data);
        }

        public function cek_payment()
        {
            $id = $this->input->post('id');
            $data = $this->M_refund->get_registrasi_where('where pendaftar.id='.$id.'')->row();
            echo json_encode($data);
        }

        public function cetak($id=null)
        {

            //$cek =  $this->M_refund->get_where_payment(array('active'=>'1','id_pendaftaran'=>$id,'status_verifikasi'=>0))->num_rows();

            
                    $data = [];

                    $data['title'] = "Report";

                    //get data main
                    
                    $data['main'] = $this->M_refund->get_detail($id);
                    $data['umur'] = $this->M_refund->get_umur($data['main']->id_formulir);
                    $data['detail'] = $this->M_refund->get_payment_detail($id);
                    $data['detail_row'] = $this->M_refund->get_payment_detail_row($id);
                    $data['cabang'] = $this->M_refund->get_cabang(array('kd_cabang' => $this->kode_cabang))->row();


                    // $html = $this->load->view('welcome_message', $data, true);
                    $html = $this->load->view('payment/print_view', $data, TRUE);
                
                    //this the the PDF filename that user will get to download
                    $pdfFilePath = "report.pdf";

                    //mPDF versi 7
                    $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal',
                                     
                                    ]);

                    $mpdf->WriteHTML($html);
                    $mpdf->Output();    
              
        }

        public function export()
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            $id = $this->input->post('id_jadwal');
            //$data['main'] = $id;
            $data['main'] = $this->M_refund->get_jadwal_exp(array('id' => $id ))->row(); 
            $data ['detail'] = $this->M_refund->get_export(array('active' => '1','id_jadwal' => $id ))->result();
            $data ['total'] = $this->M_refund->get_sum_export(array('active' => '1','id_jadwal' => $id ))->row();

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('payment/print_export', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>

