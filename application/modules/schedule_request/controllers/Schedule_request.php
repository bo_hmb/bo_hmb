<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Schedule_request extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Schedule Request';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Schedule Request';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_jadwal = $this->input->post('nm_jadwal',TRUE);
            $tgl_keberangkatan = $this->input->post('tgl_keberangkatan',TRUE);
            $tgl_kepulangan = $this->input->post('tgl_kepulangan',TRUE); 

            $cols = array();
            if (!empty($nm_jadwal)) { $cols['nm_jadwal'] = $nm_jadwal; }
            if (!empty($tgl_keberangkatan)) { $cols['tgl_keberangkatan'] = $tgl_keberangkatan; }
            if (!empty($tgl_kepulangan)) { $cols['tgl_kepulangan'] = $tgl_kepulangan; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1'";
            } else {
             $where = "active = '1'";
            }


            $list = $this->M_schedule->get_datatables($where, $cols);
            $last_query = $this->db->last_query();
            // die(print_r($last_query));

            $iTotalRecords = $this->M_schedule->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) { 
                $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-search"></i></button>
                               </div>
                               <a type="button" class="btn btn-xs btn-outline red tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'request/cetak/'.$r->id.'" target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                               <a type="button" class="btn btn-xs btn-outline green tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" href="'.base_url().'request/cetak_excel/'.$r->id.'" target="_blank"><i class="fa fa-file-excel-o"></i></a>
                               ';
                            
                // selisih hari data-id="'.$r->id.'"
                // $lama_hari = ((abs(strtotime($r->tgl_kepulangan) - strtotime($r->tgl_keberangkatan)))/(60*60*24));

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_jadwal,
                    $x=date('d-m-Y', strtotime($r->tgl_keberangkatan)),
                    $x=date('d-m-Y', strtotime($r->tgl_kepulangan)),
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Hotel';
            $data['schedule'] = $this->M_schedule->get()->result();
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

			$data = array(
                
				'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'hari' => $this->input->post('hari',TRUE),
                'tgl' => $this->input->post('tgl',TRUE),
                'lokasi' => $this->input->post('lokasi',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_movement->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_movement->get_where(array('id' => $id))->row();
            $data['schedule'] = $this->M_schedule->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'hari' => $this->input->post('hari',TRUE),
                'tgl' => $this->input->post('tgl',TRUE),
                'lokasi' => $this->input->post('lokasi',TRUE),
            );

            $query = $this->M_movement->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                );

            // die(print_r($data));
            $query = $this->M_movement->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('id_jadwal', '', 'trim|required');
            $this->form_validation->set_rules('hari', '', 'trim|required');
            $this->form_validation->set_rules('tgl', 'tgl', 'trim|required');
            $this->form_validation->set_rules('lokasi', '', 'trim|required');
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function cetak($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_schedule->get_where_request($id);
            $data ['detail'] = $this->M_schedule->get_where_detail_request($id);

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('schedule_request/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'A4-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

    }
?>
