<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_schedule extends CI_Model 
	{

		var $table = 'jadwal';
		var $group_manivest = 'group_manivest';
		var $order = array('tgl_keberangkatan' => 'ASC'); 
		
		
		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$this->db->order_by('id');
			$query=$this->db->get('jadwal');
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		public function get_where_group_manivest($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										group_manivest.id,
										group_manivest.id_jadwal,
										group_manivest.group_code,
										group_manivest.brach,
										group_manivest.brach_code,
										(SELECT COUNT(id) from group_manivest_detail WHERE id_group_manivest=group_manivest.id) AS total_pax,
										group_manivest.personn_incharge,
										group_manivest.dep_date,
										group_manivest.return_date,
										group_manivest.carrier,
										group_manivest.pnr,
										group_manivest.flight_detail,
										group_manivest.bus,
										group_manivest.active,
										group_manivest.id_user,
										group_manivest.create_at,
										group_manivest.update_at
										FROM
										group_manivest
										WHERE id_jadwal="'.$id.'"');
	        return $query->row();
		}

		public function get_where_detail_group_manivest($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pendaftar.id,
										jadwal.id AS id_jadwal,
										(SELECT IF((YEAR(CURDATE())-YEAR(formulir.tgl_lahir))<10,"Child",IF(formulir.jk="L","Mr","Ms"))) AS title,
										formulir.nm_lengkap,
										formulir.jk,
										formulir.no_pasport,
										formulir.tgl_lahir,
										formulir.tmp_lahir,
										formulir.exp_pasport,
										cabang.nm_cabang,
										(YEAR(CURDATE())-YEAR(formulir.tgl_lahir)) AS age,
										pendaftar.hubungan,
										marketing.nm_marketing,
										pendaftar.tgl_pendaftaran,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE id_jadwal="'.$id.'"');
	        return $query->result();
		}

		private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }
		
	}
