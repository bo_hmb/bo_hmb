<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>GROUP MANIVEST</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<h2 style="text-align: center; font-weight: bold;">GROUP MANIVEST</h2>
<table>
    <tr>
       <td >
        <table>    
            <tr>            
                <td style="border:1px solid #000;width: 300px;font-size: 14px;background-color: #e6f2ff"> 
                    <h5>BRANCH DETAIL</h5>
                </td>
                <td style="border:1px solid #000;width: 300px;font-size: 14px;background-color: #b3ffb3"> 
                    <h5>FLIGHT DETAIL</h5>
                </td>
                <td style="border:1px solid #000;width: 250px;font-size: 14px;background-color: #FFFF00"> 
                    <h5>UPDATING : <?php echo date('d-m-Y H:m:s', strtotime($master->update_at)) ?></h5>
                </td>
            </tr>
        </table>

        <table>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Group Code</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->group_code;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Dep. Date</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo date('d-m-Y', strtotime($master->dep_date));?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Branch</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->brach;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Return Date</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo date('d-m-Y', strtotime($master->return_date));?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Branch Code</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->brach_code;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Carrier</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->carrier;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Total Pax</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->total_pax;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>PNR</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->pnr;?></h5>
                </td>
            </tr>
            <tr>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Person In Charge</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->personn_incharge;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>Flight Detail</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>:</h5>
                </td>
                <td style="border:0px solid #000;width: 150px;font-size: 14px;"> 
                    <h5><?php echo $master->flight_detail;?></h5>
                </td>
            </tr>
        </table>
      </th>

        <th style="border:0px solid #000; width: 170px">
            <img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="100" width="100">
        </th>
    </tr>
</table>
<!-- N Kop -->
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 40px;">Title</th>
        <th style="width: 200px;">Full Name</th>
        <th style="width: 40px;">No. Pasport</th>
        <th style="width: 70px;">Birth Date</th>
        <th style="width: 100px;">Place Birth</th>
        <th style="width: 70px;">Exp. Date</th>
        <th style="width: 70px;">Issuing Office</th>
        <th style="width: 30px;">Age</th>
        <th style="width: 70px;">Relationship</th>
        <th style="width: 70px;">Agen</th>
        <th style="width: 120px;">Note</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->title ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_lengkap ?></td>
           <td style="border:1px solid #000;"><?php echo $row->no_pasport ?></td>
           <td style="border:1px solid #000;"><?php echo date('d-m-Y', strtotime($row->tgl_lahir))?></td>
           <td style="border:1px solid #000;"><?php echo $row->tmp_lahir ?></td>
           <td style="border:1px solid #000;"><?php echo date('d-m-Y', strtotime($row->exp_pasport))?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_cabang ?></td>
           <td style="border:1px solid #000;"><?php echo $row->age ?></td>
           <td style="border:1px solid #000;"><?php echo $row->hubungan ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>  
           <td style="border:1px solid #000;"></td>      
        </tr>   
    <?php $nomor++;}?> 
</table>

</body>
</html>