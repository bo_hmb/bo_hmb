<form action="#" id="form-create" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="fg-line">
                    <label>Schedule</label>
                    <select id="id_jadwal" name="id_jadwal" class="form-control select" data-placeholder="Select Schedule">
                        <option disabled selected></option>
                        <?php
                        foreach ($schedule as $row) { ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->nm_jadwal; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="DD" data-date-start-date="+0d">
                        <input type="text" id="hari" name="hari" class="form-control" readonly>
                        <label class="control-label">Hari
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Hari </span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl" name="tgl" class="form-control" readonly>
                        <label class="control-label">Tanggal
                            <span class="required">*</span>
                        </label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                    <span class="help-block"> Masukkan Tanggal Keberangkatan </span>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="lokasi" placeholder="Masukkan lokasi">
                    <label for="form_control_1">lokasi
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan lokasi</span>
                </div>
            </div>            
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>