<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Group_manivest extends AdminController {

        public function __construct()
        {
            parent::__construct();
            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Group Manifest';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            $this->load->model('M_group_manivest');
            $this->load->model('M_schedule');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }
                $this->userLevel = $userGroup;
            }
        }

        public function schedule()
        {
            // Page components
            $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Group_manivest';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);;
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            // $this->data['id_data'] = $this->uri->segment(3);
            $this->data['jadwal'] = $this->M_schedule->get_where(array('id' => $this->uri->segment(3)))->row();
            $this->data['cek'] = count($this->M_group_manivest->get_where(array('id_jadwal' => $this->uri->segment(3)))->result());
            $this->data['content'] = $this->load->view('main', $this->data, true);
            // Render page
            $this->renderPage();
        }

        public function load_data($id)
        {
            
            //$id_jadwal = $this->input->post('id_jadwal',TRUE);
            $group_code = $this->input->post('group_code',TRUE);
            $brach = $this->input->post('brach',TRUE);
            $brach_code = $this->input->post('brach_code',TRUE);
            $total_pax = $this->input->post('total_pax',TRUE);
            $personn_incharge = $this->input->post('personn_incharge',TRUE);
            $dep_date = $this->input->post('dep_date',TRUE);
            $return_date = $this->input->post('return_date',TRUE);
            $carrier = $this->input->post('carrier',TRUE);
            $pnr = $this->input->post('pnr',TRUE);
            $flight_detail = $this->input->post('flight_detail',TRUE);
            $bus = $this->input->post('bus',TRUE);

			$cols = array();
			//if (!empty($id_jadwal)) { $cols['id_jadwal'] = $id_jadwal; }
			if (!empty($group_code)) { $cols['group_code'] = $group_code; }
			if (!empty($brach)) { $cols['brach'] = $brach; }
			if (!empty($brach_code)) { $cols['brach_code'] = $brach_code; }
            if (!empty($total_pax)) { $cols['total_pax'] = $total_pax; }
            if (!empty($personn_incharge)) { $cols['personn_incharge'] = $personn_incharge; }
            if (!empty($dep_date)) { $cols['dep_date'] = $dep_date; }
            if (!empty($return_date)) { $cols['return_date'] = $return_date; }
            if (!empty($carrier)) { $cols['carrier'] = $carrier; }
            if (!empty($pnr)) { $cols['pnr'] = $pnr; }
            if (!empty($flight_detail)) { $cols['flight_detail'] = $flight_detail; }
            if (!empty($bus)) { $cols['bus'] = $bus; }

            if(array_key_exists('Super Admin', $this->userLevel)) {
             $where = "active = '1' AND group_manivest.id_jadwal = '".$id."'";
            } else {
             $where = "active = '1' AND group_manivest.id_jadwal = '".$id."'";
            }

	        $list = $this->M_group_manivest->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_group_manivest->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs blue btn-outline btn-agenda tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id_jadwal.'"><i class="fa fa-search"></i></button>

                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>
                            </div>';

                $records["data"][] = array(
                    $no, 
                    $btn_action,
                    $r->group_code,
                    $r->brach,
                    $r->brach_code,
                    $r->total_pax,
                    $r->personn_incharge,
                    $d=date('d-m-Y', strtotime($r->dep_date)),
                    $e=date('d-m-Y', strtotime($r->return_date)),
                    $r->carrier,
                    $r->pnr,
                    $r->flight_detail,
                    $r->bus,
                );

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->m_customer->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form($id)
		{
            $data['id_jadwal'] = $id;
            $data['main'] = $this->M_schedule->get_where(array('id' => $id ))->row();
			return response($this->load->view('add_', $data, TRUE), 'html');
		}

		public function add()
	    {

			// $this->validateInput();

			$data = array(
				'id_jadwal' => $this->input->post('id_jadwal',TRUE),
                'group_code' => $this->input->post('group_code',TRUE),
                'brach' => $this->input->post('brach',TRUE),
                'brach_code' => $this->input->post('brach_code',TRUE),
                'total_pax' => $this->input->post('total_pax',TRUE),
                'personn_incharge' => $this->input->post('personn_incharge',TRUE),
                'dep_date' => $this->input->post('dep_date',TRUE),
                'return_date' => $this->input->post('return_date',TRUE),
                'carrier' => $this->input->post('carrier',TRUE),
                'pnr' => $this->input->post('pnr',TRUE),
                'flight_detail' => $this->input->post('flight_detail',TRUE),
                'bus' => $this->input->post('bus',TRUE),
                'active' => '1',
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_group_manivest->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_group_manivest->get_where(array('id' => $id))->row();
            $data['schedule'] = $this->M_schedule->get()->result();
            return response($this->load->view('edit_', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();
            // Validate the submitted data
            $this->validateInput();
            // Preparing the data before update
            $id    = $this->input->post('id');
            $data = array(
                'group_code' => $this->input->post('group_code',TRUE),
                'brach' => $this->input->post('brach',TRUE),
                'brach_code' => $this->input->post('brach_code',TRUE),
                'total_pax' => $this->input->post('total_pax',TRUE),
                'personn_incharge' => $this->input->post('personn_incharge',TRUE),
                'dep_date' => $this->input->post('dep_date',TRUE),
                'return_date' => $this->input->post('return_date',TRUE),
                'carrier' => $this->input->post('carrier',TRUE),
                'pnr' => $this->input->post('pnr',TRUE),
                'flight_detail' => $this->input->post('flight_detail',TRUE),
                'bus' => $this->input->post('bus',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_group_manivest->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }


        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_group_manivest->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            $this->form_validation->set_rules('group_code', 'group code', 'trim|required');
            $this->form_validation->set_rules('brach', 'brach', 'trim|required');
           
            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function export_pdf($id=null)
        {

            $data = [];

            $data['title'] = "Report";

            //get data main
            
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_schedule->get_where_group_manivest($id);
            $data ['detail'] = $this->M_schedule->get_where_detail_group_manivest($id);

            // $html = $this->load->view('welcome_message', $data, true);
            $html = $this->load->view('group_manivest/print_view', $data, TRUE);
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = "GM.pdf";

            //mPDF versi 7
            $mpdf = new \Mpdf\Mpdf([
                                        'mode' => 'utf-8',
                                        'format' => 'Legal-L',
                                        'orientation' => 'L'
                                    ]);

            $mpdf->WriteHTML($html);
            $mpdf->Output();       
        }

        public function export_excel($id)
        {
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_schedule->get_where_group_manivest($id);
            $data ['detail'] = $this->M_schedule->get_where_detail_group_manivest($id);

            // $html = $this->load->view('welcome_message', $data, true);
            $this->load->view('group_manivest/print_view_excel', $data);
        }

        public function export_excel_sv($id)
        {
            $data['main'] = $this->M_schedule->get_where(array('id' => $id))->row();
            $data ['master'] = $this->M_schedule->get_where_group_manivest($id);
            $data ['detail'] = $this->M_schedule->get_where_detail_group_manivest2($id);

            // $html = $this->load->view('welcome_message', $data, true);
            $this->load->view('group_manivest/print_view_excel_2', $data);
        }

    }
?>
