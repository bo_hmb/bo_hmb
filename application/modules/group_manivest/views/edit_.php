<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="group_code" placeholder="Masukkan Group Code" value="<?php echo $main->group_code ?>">
                    <label for="form_control_1">Group Code</label>
                    <span class="help-block">Masukkan Group Code</span>
                </div>
            </div> 
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="brach_code" placeholder="Masukkan Brach Code" value="<?php echo $main->brach_code ?>">
                    <label for="form_control_1">Brach Code</label>
                    <span class="help-block">Masukkan Brach Code</span>
                </div>
            </div>   
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="brach" placeholder="Masukkan Brach" value="<?php echo $main->brach ?>">
                    <label for="form_control_1">Brach</label>
                    <span class="help-block">Masukkan Brach</span>
                </div>
            </div>  
            <div class="col-md-3">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="personn_incharge" placeholder="Masukkan Person In Charge" value="<?php echo $main->personn_incharge ?>">
                    <label for="form_control_1">Person In Charge</label>
                    <span class="help-block">Masukkan Person In Charge</span>
                </div>
            </div> 
        </div> 

        <span class="caption-subject font-dark bold uppercase">Data Flight Detail</span>

        <div class="row">
            <br>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="personn_incharge" placeholder="Masukkan Person In Charge" value="<?php echo $main->personn_incharge ?>">
                    <label for="form_control_1">Person In Charge</label>
                    <span class="help-block">Masukkan Person In Charge</span>
                </div>
            </div> 
           <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl" name="dep_date" class="form-control" readonly value="<?php echo $main->dep_date ?>">
                        <label class="control-label">Dep. Date</label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <span class="help-block"> Masukkan Dep. Date </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                        <input type="text" id="tgl" name="return_date" class="form-control" readonly value="<?php echo $main->return_date ?>">
                        <label class="control-label">Return Date</label>
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                    <span class="help-block"> Masukkan Return Date</span>
                </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="carrier" placeholder="Masukkan Carrier" value="<?php echo $main->carrier ?>">
                    <label for="form_control_1">Carrier</label>
                    <span class="help-block">Masukkan Carrier</span>
                </div>
            </div> 
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="pnr" placeholder="Masukkan PNR" value="<?php echo $main->pnr ?>">
                    <label for="form_control_1">PNR</label>
                    <span class="help-block">Masukkan PNR</span>
                </div>
            </div>   
            <div class="col-md-4">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="flight_detail" placeholder="Masukkan Flight Detail" value="<?php echo $main->flight_detail ?>">
                    <label for="form_control_1">Flight Detail</label>
                    <span class="help-block">Masukkan Flight Detail</span>
                </div>
            </div>  
        </div>

        <span class="caption-subject font-dark bold uppercase">Data Transportation Detail</span>

        <div class="row">
            <br>
            <div class="col-md-12">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="bus" placeholder="Masukkan Bus" value="<?php echo $main->bus ?>">
                    <label for="form_conWtrol_1">Bus</label>
                    <span class="help-block">Masukkan Bus</span>
                </div>
            </div> 
        </div> 
            
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>