<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="">Main Menu</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Mainivest</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Group Manifest</a>
                    <i class="fa fa-circle"></i>
                    
                </li> 
                <li>
                    <a href="#"><?php echo $jadwal->nm_jadwal ?></a>
                </li> 
                
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <br>
                <!-- Begin: Demo Datatable 1 -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-dark sbold uppercase">Data Group Manifest <?php echo $jadwal->nm_jadwal ?></span>
                        </div>
                           <div class="actions">
                            <?php if ($cek == 0) { ?>
                                <button id="add-btn" class="btn sbold green"> Add
                                        <i class="fa fa-plus"></i>
                                </button>
                            <?php } ?>
                            <?php if ($cek != 0) { ?>
                                <div class="btn-group">
                                    <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                        <span class="hidden-xs"> Manivest </span>
                                        <i class="fa fa-share"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="<?php echo base_url() ?>group_manivest/export_excel/<?php echo $this->uri->segment(3); ?>" target="_blank"> Export Excel </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>group_manivest/export_excel_sv/<?php echo $this->uri->segment(3); ?>" target="_blank"> Export Excel SV-KL </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() ?>group_manivest/export_pdf/<?php echo $this->uri->segment(3); ?>" target="_blank"> Export PDF </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <?php } ?>
                        
                    </div>
                    <script>
                    id_jadwal = <?php echo $this->uri->segment(3); ?>;
                    </script>
                    <div class="portlet-body">
                        <div class="">                            
                            <table class="table table-striped table-bordered table-hover table-checkable table-responsive" id="datatable_ajax">
                                <thead>
                                    <tr role="row" class="heading">
                                        <th width="5%">No</th>
                                        <th width="1%">Aksi</th>
                                        <th width="15%">Group Code</th>
                                        <th width="15%">Brach</th>
                                        <th width="15%">Brach Code</th>
                                        <th width="15%">Total Pax</th>
                                        <th width="15%">Personn Incharge</th>
                                        <th width="15%">Dep Date</th>
                                        <th width="15%">Return Date</th>
                                        <th width="15%">Carrier</th>
                                        <th width="15%">Pnr</th>
                                        <th width="15%">Flight Detail</th>
                                        <th width="15%">Bus</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End: Demo Datatable 1 -->
                
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>