<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Group_Manifest.xls");
?>

<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>GROUP MANIVEST</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }

    </style>
</head>
<body>
<!-- kop -->
<h2 style="text-align: center; font-weight: bold;">GROUP MANIFEST</h2>
<table>
    <tr>
       <td >
        <table>
            <tr>    
                <td></td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;background-color: #b3ffb3"> 
                    <h5>GROUP CODE</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;background-color: #b3ffb3"> 
                    <h5>: <?php echo $master->group_code;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;background-color: #FFFF00"> 
                    <h5>DEP. DATE</h5>
                </td>
                <td style="border:0px solid #000;width: 120px;font-size: 14px;background-color: #FFFF00"> 
                    <h5>: <?php echo date('d-m-Y', strtotime($master->dep_date));?></h5>
                </td>
            </tr>
            <tr>
                <td></td>                
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>TOTAL PAX</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>: <?php echo $master->total_pax;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>RETURN DATE</h5>
                </td>
                <td style="border:0px solid #000;width: 120px;font-size: 14px;"> 
                    <h5>: <?php echo date('d-m-Y', strtotime($master->return_date));?></h5>
                </td>
            </tr>
            <tr>
                <td></td>                               
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>FLIGHT DETAIL</h5>
                </td>
                <td style="border:0px solid #000;width: 1px;font-size: 14px;"> 
                    <h5>: <?php echo $master->flight_detail;?></h5>
                </td>
                <td style="border:0px solid #000;width: 139px;font-size: 14px;"> 
                    <h5>LAST UPDATE</h5>
                </td>
                <td style="border:0px solid #000;width: 120px;font-size: 14px;"> 
                    <h5>: <?php echo date('d-m-Y H:m:s', strtotime($master->update_at)) ?></h5>
                </td>
            </tr>
            
        </table>
      </th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 100px"></th>
        <th style="border:0px solid #000; width: 155px">
            <center><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="100" width="100"></center>
        </th>
    </tr>
</table>
<!-- N Kop -->
<br>
<table>    
    <tr>           
        <th style="width: 20px;">No</th>
        <th style="width: 40px;">Title</th>
        <th style="width: 250px;">Full NameX</th>
        <th style="width: 90px;">No. Pasport</th>
        <th style="width: 70px;">Birth Date</th>
        <th style="width: 100px;">Place Birth</th>
        <th style="width: 70px;">Date Issue</th>
        <th style="width: 70px;">Exp. Date</th>
        <th style="width: 70px;">Issuing Office</th>
        <th style="width: 30px;">Age</th>
        <th style="width: 200px;">Relationship</th>
        <th style="width: 150px;">Agen</th>
        <th style="width: 120px;">Note</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {?>

        <?php if ($row->no_pasport == "") $no_pasport="-"; else $no_pasport=$row->no_pasport; ?>
        <?php if ($row->exp_pasport == "0000-00-00") $exp_pas="-"; else $exp_pas=date('d-M-Y', strtotime($row->exp_pasport)); ?>
        
        <?php $create_pas="-"; if ($row->create_pasport == "0000-00-00") $exp_pas="-"; else $create_pas=date('d-M-Y', strtotime($row->create_pasport)); ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $row->title ?></td>
           <td style="text-transform: uppercase; border:1px solid #000;"><?php echo strtoupper($row->nm_lengkap) ?></td>
           <td style="border:1px solid #000;"><?php echo $no_pasport ?></td>
           <td style="border:1px solid #000;"><?php echo $x="'".date('d-M-Y', strtotime($row->tgl_lahir))?></td>
           <td style="text-transform: uppercase; border:1px solid #000;"><?php echo strtoupper($row->tmp_lahir) ?></td>
           <td style="border:1px solid #000;"><?php echo $x="'".$create_pas?></td>
           <td style="border:1px solid #000;"><?php echo $x="'".$exp_pas?></td>
           <td style="text-transform: uppercase; border:1px solid #000;"><?php echo strtoupper($row->kota_paspor) ?></td>
           <td style="border:1px solid #000;"><?php echo $row->age ?></td>
           <td style="border:1px solid #000;"><?php echo $row->keluarga ?></td>
           <td style="border:1px solid #000;"><?php echo $row->nm_marketing ?></td>  
           <td style="border:1px solid #000;"><?php echo $row->status ?></td>      
        </tr>   
    <?php $nomor++;}?> 
</table>

</body>
</html>