<?php

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Manifest_sv.xls");
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/logo.png" /> 
  
<title>GROUP MANIVEST</title>
  
    <style>
      
        table{
              
        border-collapse: collapse;
              
        /*width: 100%;*/
              
        /*margin: 0 auto;*/
          
        }
          
        table th{
              
        border:1px solid #000;
              
        font-weight: bold;
              
        text-align: center;
        font-size: 12px;
          
        }
          
        table td{
              
        border:0px solid #000;
              
        padding: 3px;
        font-size: 13px;
              
        vertical-align: top;
          
        }
    </style>
</head>
<body>
<!-- kop -->
<table>    
    <tr>           
        <th style="border:0px solid #000;"><img src="<?php echo base_url() ?>assets/img/logokop.png" class="img-responsive" alt="" height="50" width="50"></th>
        <th style="border:0px solid #000; width: 300PX"></th>
        <th style="border:0px solid #000; width: 500PX"> 
            <h1 style="text-align: center; font-weight: bold;">MANIFEST SV KUALA LUMPUR</h1>
        </th>
    </tr>
</table>
<!-- N Kop -->
<br>
<table>  
    <tr>           
        <td style="border:0px solid #000;width: 20px;"></td>
        <td style="border:0px solid #000;width: 320px;">
            RETAJ ELSAFWA TRAVEL & SERVICES SDN BHD <br>
            RE  : NAME LIST FOR : HMB TRAVEL <br>
            DEP :<br>
            RTN :<br>
            PNR :<br><br><br>
            MUASSASAH :<br>
            CONTACT PERSON :<br>
            MOBILE NO :<br>
        </td>
        <td style="border:0px solid #000;width: 20px;"></td>
        <td style="border:0px solid #000;width: 800px;">
            <table>
                <tr>
                    <td style="border:1px solid #000;width: 990px;">
                        <b>REMINDER</b><br>
                        <b>ALL THE NAMES MUST BE SORT ALPHABETICALLY (MENGIKUT ABJAD)</b><br>
                        <b>ACCEPT FOR INFANT NAMES INSERT BELOW THE FATHER OR MOTHER NAME'S</b><br>
                        <b>COPY PASSPORT FOR INFANT NEEDED</b><br>
                    </td>
                </tr>
            </table>
        </td>
    </tr> 

    <tr>           
        <th style="width: 20px;">NO</th>
        <th style="width: 320px;">NAME</th>
        <th style="width: 20px;">BIL</th>
        <th style="width: 800px;">APIS DATA</th>
        <th style="width: 800px;">NOTE</th>
    </tr>

    <?php $nomor=1; foreach($detail as $row) {
        if ($row->jk == 'L') $jk='M'; if ($row->jk == 'P') $jk='F';
        ?>
        <tr>                                                
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;"><?php echo $x=strtoupper($row->nm_lengkap) ?> <?php echo $x=strtoupper($row->title) ?></td>
           <td style="border:1px solid #000;text-align: center;"><?php echo $nomor ?></td>
           <td style="border:1px solid #000;">SRDOCSSVHK1-P-IDN-
                                             <?php echo $row->no_pasport ?>-IDN-<?php echo $jk ?>-
                                             <?php echo $x=strtoupper(date('dMy', strtotime($row->exp_pasport)))?>-
                                             <?php echo $x=strtoupper($row->nm_lengkap) ?>/P<?php echo $nomor ?>
           </td>
           <td><?php echo $row->status ?></td>
        </tr>   
    <?php $nomor++;}?> 
</table>

</body>
</html>