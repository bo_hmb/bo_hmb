<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class M_schedule extends CI_Model 
	{

		var $table = 'jadwal';
		
		public function __construct() {
			parent::__construct();
		}

		public function get() {
			$this->db->order_by('id');
			$query=$this->db->get('jadwal');
			return $query;
		}

		public function get_where($where) {
			$table = $this->table;
			$this->db->where($where);
			$query=$this->db->get($table);
			return $query;
		}

		private function _get_datatables_query($where, $cols)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);

	        if(!empty($cols)){

		        foreach ($cols as $col => $value)
		        {
        			$this->db->like($col, $value);
		        }

		    }

	        if(isset($_POST['order'])) // here order processing
	        {
	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	        } 
	        else if(isset($this->order))
	        {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	    }
	 
	    public function get_datatables($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_all($where)
	    {

	    	$this->db->where($where);
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }

	    public function count_filtered($where, $cols)
	    {
	        $this->_get_datatables_query($where, $cols);
	        return $this->db->count_all_results();
	    }

	    public function get_where_group_manivest($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										group_manivest.id,
										group_manivest.id_jadwal,
										group_manivest.group_code,
										group_manivest.brach,
										group_manivest.brach_code,
										(SELECT COUNT(id) from group_manivest_detail WHERE id_group_manivest=group_manivest.id) AS total_pax,
										group_manivest.personn_incharge,
										group_manivest.dep_date,
										group_manivest.return_date,
										group_manivest.carrier,
										group_manivest.pnr,
										group_manivest.flight_detail,
										group_manivest.bus,
										group_manivest.active,
										group_manivest.id_user,
										group_manivest.create_at,
										group_manivest.update_at
										FROM
										group_manivest
										WHERE id_jadwal="'.$id.'"');
	        return $query->row();
		}

		public function get_where_detail_group_manivest($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pendaftar.id,
										jadwal.id AS id_jadwal,
										(SELECT 
											IF(
												(TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan))<=144 AND 
												(TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan)) >24,"CHILD",
												
													IF((TIMESTAMPDIFF(MONTH, tgl_lahir, jadwal.tgl_kepulangan))<=24,"INFANT",
														IF(formulir.jk="L","MR","MRS"))
													
												)
										) AS title,
										formulir.nm_lengkap,
										formulir.jk,
										formulir.no_pasport,
										formulir.tgl_lahir,
										formulir.tmp_lahir,
										formulir.create_pasport,
										formulir.exp_pasport,
										cabang.nm_cabang,
										(YEAR(CURDATE())-YEAR(formulir.tgl_lahir)) AS age,
										concat(pendaftar.hubungan, " ", pendaftar.keluarga_terdekat ) AS keluarga,
										marketing.nm_marketing,
										pendaftar.tgl_pendaftaran,
										( case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) = 0	THEN "Registrasi"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+
														(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah)+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Belum Lunas"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+
														(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah)+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Lunas"
												
												ELSE "Registrasi"
											END
										) AS status,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE id_jadwal="'.$id.'" AND pendaftar.active = 1 order by pendaftar.no_pendaftaran ASC');
	        return $query->result();
		}

		public function get_where_detail_group_manivest2($id) {
			$results = array();
	        $query = $this->db->query(' SELECT
										pendaftar.id,
										jadwal.id AS id_jadwal,
										(SELECT 
											IF(
												(YEAR(CURDATE())-YEAR(formulir.tgl_lahir))<12 AND 
												(YEAR(CURDATE())-YEAR(formulir.tgl_lahir)) >2,"Child",
												
													IF((YEAR(CURDATE())-YEAR(formulir.tgl_lahir))<=2,"Infant",
														IF(formulir.jk="L","Mr","Mrs"))
													
												)
										) AS title,
										formulir.nm_lengkap,
										formulir.jk,
										formulir.no_pasport,
										formulir.tgl_lahir,
										formulir.tmp_lahir,
										formulir.create_pasport,
										formulir.exp_pasport,
										cabang.nm_cabang,
										(YEAR(CURDATE())-YEAR(formulir.tgl_lahir)) AS age,
										concat(pendaftar.hubungan, " ", pendaftar.keluarga_terdekat ) AS keluarga,
										marketing.nm_marketing,
										pendaftar.tgl_pendaftaran,
										( case 
												WHEN (SELECT jenis FROM refund WHERE refund.id_pendaftar=pendaftar.id AND refund.active=1)	= "Refund" THEN "Refund"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) = 0	THEN "Registrasi"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) > 0	AND 
														(SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) < 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+
														(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah)+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Belum Lunas"
												WHEN (SELECT SUM(jml_bayar) FROM pembayaran WHERE id_pendaftaran=pendaftar.id AND status_verifikasi=1) >= 
														((paket.hrg_paket-pendaftar.potongan_paket)+pendaftar.biaya_mahram+pendaftar.biaya_progresif+
														(pendaftar.biaya_up_hotel_makkah*pendaftar.status_up_hotel_makkah)+
														(pendaftar.biaya_up_hotel_madinah*pendaftar.status_up_hotel_madinah)+
														(pendaftar.biaya_upgrade_kamar*pendaftar.status_upgrade_kamar)+
														(pendaftar.biaya_upgrade_kamar_madinah*pendaftar.status_upgrade_kamar_madinah)+
														pendaftar.biaya_order_barang+pendaftar.add_on) THEN "Lunas"
												
												ELSE "Registrasi"
											END
										) AS status,
										pendaftar.active
										FROM
										pendaftar
										INNER JOIN formulir ON pendaftar.id_formulir = formulir.id
										INNER JOIN paket ON pendaftar.id_paket = paket.id
										INNER JOIN jadwal ON paket.id_jadwal = jadwal.id
										INNER JOIN marketing ON formulir.id_marketing = marketing.id
										INNER JOIN cabang ON pendaftar.kd_office = cabang.kd_cabang
										WHERE id_jadwal="'.$id.'" AND pendaftar.active = 1 order by pendaftar.no_pendaftaran ASC');
	        return $query->result();
		}
		
	}
