<form action="#" id="form-edit" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="nm_barang" placeholder="Masukkan Nama Baggage" value="<?php echo $main->nm_barang ?>">
                    <label for="form_control_1">Nama Baggage
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Nama Baggage</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="satuan" placeholder="Masukkan Satuan Barang" value="<?php echo $main->satuan ?>">
                    <label for="form_control_1">Satuan
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Satuan Barang</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <select class="form-control jeniskelamin" name="untuk_jk">
                        <option <?php echo ("L" === $main->untuk_jk) ? 'selected' : ''; ?> value="L">Laki-Laki</option>
                        <option <?php echo ("P" === $main->untuk_jk) ? 'selected' : ''; ?> value="P">Perempuan</option>
                    </select>
                    <label>Untuk Jenis Kelamin
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Silahkan pilih jenis kelamin</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="stok" placeholder="Masukkan Jumlah Stok" value="<?php echo $main->stok ?>">
                    <label for="form_control_1">Stok
                    </label>
                    <span class="help-block">Masukkan Jumlah Stok</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="harga_beli" placeholder="Masukkan Harga Beli" value="<?php echo $main->harga_beli ?>">
                    <label for="form_control_1">Harga Beli</label>
                    <span class="help-block">Masukkan Harga Beli</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-md-line-input">
                    <input type="text" class="form-control" name="harga_jual" placeholder="Masukkan Harga Jual" value="<?php echo $main->harga_jual ?>">
                    <label>Harga Jual
                        <span class="required">*</span>
                    </label>
                    <span class="help-block">Masukkan Harga Jual</span>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <dov class="pull-right">
                    <button type="submit" class="btn btn-outline green submit" name="id" value="<?php echo $main->id; ?>"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                </dov>
            </div>
        </div>
    </div>
</form>