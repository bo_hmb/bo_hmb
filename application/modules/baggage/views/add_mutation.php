            <form role="form" action="#" class="form-horizontal" id="form-mutasi">
                <div class="form-group">
                    <label class="col-md-2 control-label">Lokasi Tujuan</label>
                    <div class="col-md-6">
                        <select class="form-control" name="kd_office" required="">
                            <option> -pilih- </option>
                            <?php foreach ($office as $row) { ?>
                                <option value="<?php echo $row->kd_cabang; ?>" <?php if($row->kd_cabang === $kode_cabang){echo "disabled";} ?>><?php echo $row->nm_cabang ?></option>
                            <?php } ?>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">No Mutasi</label>
                    <div class="col-md-6">
                        <input type="text" name="no_mutasi" value="<?php echo $no_mutasi; ?>" placeholder="No Mutasi" class="form-control" /> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Tanggal Mutasi</label>
                    <div class="col-md-6">
                        <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                            <input type="text" name="tgl_mutasi" class="form-control" placeholder="Tanggal" readonly="">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:30%">Nama Barang</th>
                                    <th>Harga Beli</th>
                                    <th>Harga Jual</th>
                                    <th>Kode Office</th>
                                    <th>Stok</th>
                                    <th>Jumlah</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="container">
                            </tbody>
                            
                        </table>
                        <button type="button" class="btn btn-danger pull-right" id="add_row"><i class="fa fa-plus"></i> Tambah Item Baru</button>
                    </div>
                </div>

                <hr />
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                             <dov class="pull-right">
                                <button type="submit" class="btn btn-outline green submit"><i class="fa fa-save"></i> Simpan</button>
                                <button type="button" class="btn red btn-outline" data-dismiss="modal"><i class="fa fa-times"></i> Keluar</button>
                            </dov>
                        </div>
                    </div>
                </div>
            </form>
     