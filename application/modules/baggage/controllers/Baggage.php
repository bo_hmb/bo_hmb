<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Baggage extends AdminController {

        public function __construct()
        {
            parent::__construct();

            $this->tanggal = date("Y-m-d");
            $this->jam = date("H:i:s");

			$this->bulan = date('m');
			$this->tahun = date('Y');

            // Module components            
            $this->data['module'] = 'Barang';
            $this->data['pluginCss'] = $this->load->view('assets/_pluginCss', $this->data, true);
            $this->data['pluginJs'] = $this->load->view('assets/_pluginJs', $this->data, true);
            
            $this->load->model('M_baggage');
            // $this->output->enable_profiler(TRUE);
            if($this->cekCurrentUser > 0) {
                $this->group = $this->ion_auth->get_users_groups( $this->currentUser->id)->result();
                $this->kode_cabang = $this->currentUser->kode_cabang;
                foreach ($this->group as $key => $value) {
                    $userGroup[$value->name] = $value->description;
                }

                $this->userLevel = $userGroup;
            }
        }

        public function index()
        {
            // Page components
            // $this->data['userGroup'] = $this->userLevel;

            $this->data['pageTitle'] = 'Barang';
            $this->data['pageCss'] = $this->load->view('assets/_pageCss', $this->data, true);
            $this->data['pageJs'] = $this->load->view('assets/_pageJs', $this->data, true);
            $this->data['content'] = $this->load->view('main', $this->data, true);

            // Render page
            $this->renderPage();
        }

        public function load_data()
        {
            $nm_barang = $this->input->post('nm_barang',TRUE);
            $satuan = $this->input->post('satuan',TRUE);
            $kd_office = $this->input->post('kd_office',TRUE);
            $untuk_jk = $this->input->post('untuk_jk',TRUE);
            $stok = $this->input->post('stok',TRUE);
            $harga_beli = $this->input->post('harga_beli',TRUE);
            $harga_jual = $this->input->post('harga_jual',TRUE);

			$cols = array();
			if (!empty($nm_barang)) { $cols['nm_barang'] = $nm_barang; }
            if (!empty($satuan)) { $cols['satuan'] = $satuan; }
			if (!empty($kd_office)) { $cols['kd_office'] = $kd_office; }
			if (!empty($untuk_jk)) { $cols['untuk_jk'] = $untuk_jk; }
			if (!empty($stok)) { $cols['stok'] = $stok; }
            if (!empty($harga_beli)) { $cols['harga_beli'] = $harga_beli; }
            if (!empty($harga_jual)) { $cols['harga_jual'] = $harga_jual; }   

            if(array_key_exists('Admin Baggage', $this->userLevel) or 
               array_key_exists('Admin', $this->userLevel) or
               array_key_exists('Super Admin', $this->userLevel))  {

             $where = "active = '1'";
            } else {
             $where = "active = '1' AND kd_office = '$this->kode_cabang'";  
            }


	        $list = $this->M_baggage->get_datatables($where, $cols);
			$last_query = $this->db->last_query();
			// die(print_r($last_query));

            $iTotalRecords = $this->M_baggage->count_all($where);
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);
            
            $records = array();
            $records["data"] = array(); 

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;

            $no = $iDisplayStart;
            foreach ($list as $r) {
	            $no++;

                $btn_action = '<div class="btn-group btn-group-xs btn-group-solid">
                                <button type="button" class="btn btn-xs yellow btn-outline btn-edit tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-xs btn-outline red btn-update-status" data-id="'.$r->id.'" data-toggle="confirmation" data-placement="top" data-btn-ok-label="Yes" data-btn-ok-icon="icon-user-following" data-btn-ok-class="btn-success" data-btn-cancel-label="No" data-btn-cancel-icon="icon-user-unfollow" data-btn-cancel-class="btn-danger"><i class="fa fa-trash"></i></button>';
                
                if(array_key_exists('Super Admin', $this->userLevel)) {
                $btn_action .= '<button type="button" class="btn btn-xs blue btn-outline btn-mutation tooltips" data-container="body" data-placement="top" data-original-title="Tooltip in top" data-id="'.$r->id.'"><i class="fa fa-truck"></i></button>';
                }

                $btn_action .= '</div>';

                if(array_key_exists('Admin Baggage', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { 

                $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_barang,
                    $r->satuan,
                    $r->kd_office,
                    $r->untuk_jk,
                    $r->stok,
                    $t="Rp." . number_format($r->harga_beli,0,',','.'),
                    $t="Rp." . number_format($r->harga_jual,0,',','.'),
                );

                } else {
                  $records["data"][] = array(
                    $no,                    
                    $btn_action,
                    $r->nm_barang,
                    $r->satuan,
                    $r->kd_office,
                    $r->untuk_jk,
                    $r->stok,
                    $t="Rp." . number_format($r->harga_jual,0,',','.'),
                );
  
                }

            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;
            
            echo json_encode($records);
        }

		public function load_detail()
		{

            $id = $this->input->get('id');

			$data['main'] = $this->M_baggage->get_where(array('id' => $id ))->row();

			return response($this->load->view('detail', $data, TRUE), 'html');
		}

		public function load_add_form()
		{
			$data['title'] = 'Tambah Data Baggage';
			return response($this->load->view('add_baggage', $data, TRUE), 'html');
		}

		public function add()
	    {

			$this->validateInput();

			$data = array(
				'nm_barang' => $this->input->post('nm_barang',TRUE),
                'satuan' => $this->input->post('satuan',TRUE),
                'untuk_jk' => $this->input->post('untuk_jk',TRUE),
                'stok' => $this->input->post('stok',TRUE),
                'harga_beli' => $this->input->post('harga_beli',TRUE),
                'harga_jual' => $this->input->post('harga_jual',TRUE),
                'active' => '1',
                'kd_office' => $this->kode_cabang,
                'id_user' => $this->currentUser->id,
				);

			$query = $this->M_baggage->_insert($data);
            
            // Check if query was success
            if ($query) {
                $response = array('status' => true, 'action' => 'Success', 'message' => 'Data berhasil ditambahkan');
            } else {
                $response = array('status' => false, 'action' => 'Failed', 'message' => 'Data gagal ditambahkan');
            }
			
			return response($response, 'json');
	    }

        public function load_edit_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_baggage->get_where(array('id' => $id))->row();
            return response($this->load->view('edit_baggage', $data, TRUE), 'html');
        }

        public function edit()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');

            $data = array(
                'nm_barang' => $this->input->post('nm_barang',TRUE),
                'satuan' => $this->input->post('satuan',TRUE),
                'untuk_jk' => $this->input->post('untuk_jk',TRUE),
                'stok' => $this->input->post('stok',TRUE),
                'harga_beli' => $this->input->post('harga_beli',TRUE),
                'harga_jual' => $this->input->post('harga_jual',TRUE),
                'user_update' => $this->currentUser->id,
            );

            $query = $this->M_baggage->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function load_mutation_form()
        {

            $id = $this->input->get('id');
            $data['main'] = $this->M_baggage->get_where(array('id' => $id))->row();
            $data['office'] = Modules::run('workarea/get_workarea');
            return response($this->load->view('mutation', $data, TRUE), 'html');
        }

        public function mutation()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            $this->validateInput();

            // Preparing the data before update
            $id    = $this->input->post('id');
            $kd_office = $this->input->post('kd_office');
            $nm_barang = $this->input->post('nm_barang');
            $where = 'kd_office = "'.$kd_office.'" and nm_barang = "'.$nm_barang.'"';
            $main = $this->M_baggage->get_where($where)->row();
            $count = $this->M_baggage->get_where($where)->num_rows();
            if ($count >= 1) {
                $data = array(
                    'nm_barang' => $this->input->post('nm_barang',TRUE),
                    'satuan' => $this->input->post('satuan',TRUE),
                    'untuk_jk' => $this->input->post('untuk_jk',TRUE),
                    'stok' => $this->input->post('stok',TRUE) + $main->stok,
                    'harga_beli' => $this->input->post('harga_beli',TRUE),
                    'harga_jual' => $this->input->post('harga_jual',TRUE),
                    'kd_office' => $this->input->post('kd_office',TRUE),
                    'id_user' => $this->currentUser->id,
                );

                $query = $this->M_baggage->_update(array('id' => $main->id), $data);
            }else{
                $data = array(
                'nm_barang' => $this->input->post('nm_barang',TRUE),
                'satuan' => $this->input->post('satuan',TRUE),
                'untuk_jk' => $this->input->post('untuk_jk',TRUE),
                'stok' => $this->input->post('stok',TRUE),
                'harga_beli' => $this->input->post('harga_beli',TRUE),
                'harga_jual' => $this->input->post('harga_jual',TRUE),
                'active' => '1',
                'kd_office' => $this->input->post('kd_office',TRUE),
                'id_user' => $this->currentUser->id,
                );

                $query = $this->M_baggage->_insert($data);
            }

            $data = array(
                'id_barang' => $id,
                'kd_asal' => $this->input->post('kd_asal',TRUE),
                'kd_tujuan' => $this->input->post('kd_office',TRUE),
                'stok' => $this->input->post('stok',TRUE),
                'id_user' => $this->currentUser->id,
                'kd_office' => $this->input->post('kd_office',TRUE),
            );

            $query = $this->M_baggage->_insert_his($data);
            
            // update stok
            $where1 = array(
            'id' => $id,
            );

            $data1 = array(
            'stok' => $this->input->post('cekstok',TRUE) - $this->input->post('stok',TRUE),
            );

            $querystok = Modules::run('baggage/get_update_stok', $where1, $data1);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function load_add_mutasi()
        {
            $data['kode_cabang'] = $this->kode_cabang;
            $data['office'] = Modules::run('workarea/get_workarea');


            $cek = $this->M_baggage->get_where_his(array('kd_office' => $this->kode_cabang))->num_rows();
            $data['no_mutasi'] = $this->kode_cabang.'-'.date('d').date('m').date("y").sprintf('%03d',$cek+1);

            return response($this->load->view('add_mutation', $data, TRUE), 'html');
        }

        public function mutasi()
        {
            // Check if ajax request
            $this->ajaxRequest();

            // Validate the submitted data
            // $this->validateInput();

            // Preparing the data before update
            
            $cek = count($this->input->post('nm_barang'));

            for ($i=0; $i < $cek; $i++) { 

                // $where = array(
                //     'kd_office' => $this->input->post('kd_office'),
                //     'nm_barang' => $this->input->post('nama_barang')[$i],
                //     );

                // $main = $this->M_baggage->get_where($where)->row();

                // $data = array(
                //     'stok' => $this->input->post('jml',TRUE)[$i] + $main->stok,
                //     'id_user' => $this->currentUser->id,
                // );

                // $query = $this->M_baggage->_update($where, $data);

                // update stok
                $wherestok = array(
                'id' => $this->input->post('nm_barang',TRUE)[$i],
                );

                $datastok = array(
                'stok' => $this->input->post('cekstok',TRUE)[$i] - $this->input->post('jml',TRUE)[$i],
                );
                $querystok = Modules::run('baggage/get_update_stok', $wherestok, $datastok);
                
                // history
                $data_his = array(
                    'id_barang' => $this->input->post('nm_barang',TRUE)[$i],
                    'kd_asal' => $this->input->post('kd_asal',TRUE)[$i],
                    'kd_tujuan' => $this->input->post('kd_office',TRUE),
                    'no_mutasi' => $this->input->post('no_mutasi',TRUE),
                    'tgl_mutasi' => $this->input->post('tgl_mutasi',TRUE),
                    'stok' => $this->input->post('jml',TRUE)[$i],
                    'id_user' => $this->currentUser->id,
                    'kd_office' => $this->kode_cabang,
                );

                $query_his = $this->M_baggage->_insert_his($data_his);


            }

            $data_mutasi = array(
            'no_mutasi' => $this->input->post('no_mutasi',TRUE),
            'tgl_mutasi' => $this->input->post('tgl_mutasi',TRUE),
            'kd_office' => $this->kode_cabang,
            'kd_asal' => $this->kode_cabang,
            'kd_tujuan' => $this->input->post('kd_office',TRUE),
            'id_user' => $this->currentUser->id,
            'app' => 'pending',
            );

            $query_mutasi = $this->M_baggage->_insert_mutasi($data_mutasi);

            

            // Check if query was success
            if ($query_mutasi) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'updated successfully');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Failed to update');
            }

            // Return the result to the view
            return response($results);
        }

        public function delete()
        {
            $id = $this->input->post('id');
            $data = array(
                'active' => '0',
                'user_update' => $this->currentUser->id,
                );

            // die(print_r($data));
            $query = $this->M_baggage->_update(array('id' => $id), $data);

            // Check if query was success
            if ($query) {
                $results = array('status' => true, 'action' => 'Success', 'message' => 'berhasil');
            } else {
                $results = array('status' => false, 'action' => 'Failed', 'message' => 'Gagal');
            }

            // Return the result to the view
            return response($results, 'json');
        }

	    public function validateInput()
        {
            // Load form validation library
            $this->load->library('form_validation');

            // Set validation rules	
            $this->form_validation->set_rules('nm_barang', 'nm barang', 'trim|required');
            $this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
            $this->form_validation->set_rules('untuk_jk', 'untuk jk', 'trim|required');
            $this->form_validation->set_rules('stok', 'stok', 'trim|required|numeric');
            $this->form_validation->set_rules('harga_beli', 'harga beli', 'trim|required|numeric');
            $this->form_validation->set_rules('harga_jual', 'harga jual', 'trim|required|numeric');

            // Run the validation
            if ($this->form_validation->run() === false) {

                $response = array(
                    'status' => false,
                    'action' => 'Failed',
                    'message' => $this->form_validation->error_string('<h5>', '</h5>')
                );

                return response($response, 'json');

            }
        }

        public function get_where($where)
        {
            $query = $this->M_baggage->get_where($where);
            return $query;
        }

        public function get_update_stok($where, $data)
        {
            $query = $this->M_baggage->_update($where, $data);
            return $query;
        }

        public function get_barang()
        {
            $query = $this->M_baggage->get_where(array('active' => '1' ))->result();
                foreach ($query as $row){
                   $result[] = array(
                  'nm_barang' => $row->nm_barang,
                  'harga_beli' => $row->harga_beli,
                  'harga_jual' => $row->harga_jual,
                );
            }

            echo json_encode($result);
        }

        public function load_barang()
        {   
            $json = [];


            $this->load->database();

            // if(!empty($this->input->get("q"))){

                // $this->db->like('nm_barang', $this->input->get("q"));
                $this->db->where('kd_office', $this->kode_cabang);
                 $this->db->where('active', '1');
                $query = $this->db->select('id,nm_barang as text')
                            ->limit(10)
                            ->get("barang");
                $json = $query->result();
            // }

            
            echo json_encode($json);

        }

        public function get_select_barang()
        {
            $id = $this->input->post("id");
            $data = Modules::run('baggage/get_where', array('active' => '1', 'id' => $id))->row();
            echo json_encode($data);
        }

    }
?>
