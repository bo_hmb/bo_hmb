
<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>
    <!-- END SIDEBAR TOGGLER BUTTON -->   
    <li class="nav-item <?php if($this->uri->segment(1)=="dashboard"){echo "active";}?>">
        <a href="<?php echo base_url() ?>dashboard" class="nav-item">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>

    <!-- sperator -->
    <li class="heading">
        <h3 class="uppercase">Main Menu</h3>
    </li>

<?php if(array_key_exists('Admin CS', $this->userLevel) or array_key_exists('CS', $this->userLevel) or 
         array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Manivest', $this->userLevel) or 
         array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>

    <li class="nav-item <?php if($this->uri->segment(1)=="formulir" or $this->uri->segment(1)=="registrasi"){echo "active ";echo "open";}?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-user-following"></i>
            <span class="title">Customer Service</span>
            <span class="arrow <?php if($this->uri->segment(1)=="formulir" or $this->uri->segment(1)=="registrasi"){echo "open";}?>"></span>
        </a>
        <ul class="sub-menu" <?php if($this->uri->segment(1)=="formulir" or $this->uri->segment(1)=="registrasi"){echo " style='display: block;' ";}?>>
            <?php if(array_key_exists('Admin CS', $this->userLevel) or array_key_exists('CS', $this->userLevel) or 
                     array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Manivest', $this->userLevel) or 
                     array_key_exists('Super Admin', $this->userLevel)) { ?>

                    <li class="nav-item <?php if($this->uri->segment(1)=="formulir"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>formulir" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Form</span>
                        </a>
                    </li>
            <?php } ?>

            <?php if(array_key_exists('Admin CS', $this->userLevel) or array_key_exists('CS', $this->userLevel) or 
                     array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Manivest', $this->userLevel) or 
                     array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>        
                    <li class="nav-item <?php if($this->uri->segment(1)=="registrasi"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>registrasi" class="nav-link ">
                            <i class="icon-bulb"></i>
                            <span class="title">Registration</span>

                        </a>
                    </li>
            <?php } ?>

        </ul>
    </li>
<?php } ?>

<?php if(array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Manivest', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>

    <li class="nav-item <?php if($this->uri->segment(1)=="schedule_movement" or $this->uri->segment(1)=="movement" or $this->uri->segment(1)=="movdet" 
                        or $this->uri->segment(1)=="schedule_group_manivest" or $this->uri->segment(1)=="group_manivest" 
                        or $this->uri->segment(1)=="group_manivest_detail" or $this->uri->segment(1)=="schedule_group_movement"
                        or $this->uri->segment(1)=="group_movement" or $this->uri->segment(1)=="group_movement_detail" or $this->uri->segment(1)=="reservation"
                        or $this->uri->segment(1)=="schedule_request" or $this->uri->segment(1)=="request" or $this->uri->segment(1)=="request_detail"
                        ){echo "active ";echo "open";}?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-list"></i>
            <span class="title">Manifest</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" >
            
            <li class="nav-item <?php if($this->uri->segment(1)=="schedule_movement" or $this->uri->segment(1)=="movement" or $this->uri->segment(1)=="movdet"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>schedule_movement" class="nav-link ">
                    <span class="title">Itinerary</span>
                </a>
            </li>
            <li class="nav-item <?php if($this->uri->segment(1)=="schedule_group_manivest" or $this->uri->segment(1)=="group_manivest"  or $this->uri->segment(1)=="group_manivest_detail"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>schedule_group_manivest" class="nav-link ">
                    <span class="title">Group Manifest</span>
                </a>
            </li>
            <li class="nav-item <?php if($this->uri->segment(1)=="schedule_request" or $this->uri->segment(1)=="request"  or $this->uri->segment(1)=="request_detail"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>schedule_request" class="nav-link ">
                    <span class="title">List Request</span>
                </a>
            </li>
            <li class="nav-item <?php if($this->uri->segment(1)=="schedule_group_movement" or $this->uri->segment(1)=="group_movement"  or $this->uri->segment(1)=="group_movement_detail" or $this->uri->segment(1)=="reservation" ){echo "active";}?>">
                <a href="<?php echo base_url(); ?>schedule_group_movement" class="nav-link ">
                    <span class="title">Group Movement</span>
                </a>
            </li>
            
        </ul>
    </li>
<?php } ?>

<?php if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="payment" or $this->uri->segment(1)=="benefit_marketing" or 
                                 $this->uri->segment(1)=="benefit_cabang" or $this->uri->segment(1)=="benefit_ref_area" or 
                                 $this->uri->segment(1)=="income_detail" or $this->uri->segment(1)=="refund" or $this->uri->segment(1)=="benefit_bom" or
                                 $this->uri->segment(1)=="spending" or $this->uri->segment(1)=="payment_detail" or $this->uri->segment(1)=="income_other")
                                 {echo "active ";echo "open";}?>">

        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-credit-card"></i>
            <span class="title">Finance</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" <?php if($this->uri->segment(1)=="payment" or $this->uri->segment(1)=="payment_detail"){echo " style='display: block;' ";}?>>

            <li class="nav-item <?php if($this->uri->segment(1)=="payment" or $this->uri->segment(1)=="income_detail" or $this->uri->segment(1)=="payment_detail" or $this->uri->segment(1)=="income_other")
                                 {echo "active ";echo "open";}?>">
                <a href="javascript:;" class="nav-link nav-toggle">Income
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu" <?php if($this->uri->segment(1)=="payment" or $this->uri->segment(1)=="payment_detail"){echo " style='display: block;' ";}?>>
                    <li class="nav-item <?php if($this->uri->segment(1)=="payment" or $this->uri->segment(1)=="payment_detail" ) {echo "active";}?>">
                        <a href="<?php echo base_url(); ?>payment" class="nav-link ">
                            <span class="title">Income</span>
                        </a>
                    </li>
                    <?php if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
                    <li class="nav-item <?php if($this->uri->segment(1)=="income_detail"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>income_detail" class="nav-link ">
                            <span class="title">Detail <!-- installment --></span>
                        </a>
                    </li>
                    <?php } ?>

                   <!--  <li class="nav-item <?php if($this->uri->segment(1)=="income_other"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>income_other" class="nav-link ">
                            <span class="title">Other</span>
                        </a>
                    </li> -->
                </ul>
            </li>

            <?php if(array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
                
                <li class="nav-item <?php if($this->uri->segment(1)=="benefit_marketing"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>benefit_marketing" class="nav-link ">
                        <span class="title">Benefit <!-- installment --></span>
                    </a>
                </li>
                <!-- <li class="nav-item <?php if($this->uri->segment(1)=="benefit_cabang"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>benefit_cabang" class="nav-link ">
                        <span class="title">Benefit Area </span>
                    </a>
                </li> -->

                <!-- <li class="nav-item <?php if($this->uri->segment(1)=="benefit_ref_area"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>benefit_ref_area" class="nav-link ">
                        <span class="title">Benefit Referensi Area</span>
                    </a>
                </li> 

                <li class="nav-item <?php if($this->uri->segment(1)=="benefit_bom"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>benefit_bom" class="nav-link ">
                        <span class="title">Benefit BOM</span>
                    </a>
                </li>  -->

                <li class="nav-item <?php if($this->uri->segment(1)=="spending"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>spending" class="nav-link ">
                        <span class="title">Expenditure <!-- installment --></span>
                    </a>
                </li>

                <li class="nav-item <?php if($this->uri->segment(1)=="refund"){echo "active";}?>">
                    <a href="<?php echo base_url(); ?>refund" class="nav-link ">
                        <span class="title">Refund/Reschedule</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </li>
<?php } ?>


<?php if(array_key_exists('Admin CS', $this->userLevel) OR array_key_exists('Admin Marketing', $this->userLevel) or array_key_exists('Marketing', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="marketing" or $this->uri->segment(1)=="structure_marketing" or $this->uri->segment(1)=="chart_marketing"){echo "active ";echo "open";}?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-graph"></i>
            <span class="title">Marketing</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu" <?php if($this->uri->segment(1)=="marketing" or $this->uri->segment(1)=="chart_marketing" or $this->uri->segment(1)=="structure_marketing"){echo " style='display: block;' ";}?>>
            <li class="nav-item <?php if($this->uri->segment(1)=="marketing"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>marketing" class="nav-link ">
                    <span class="title">Marketing</span>
                </a>
            </li>
            <li class="nav-item <?php if($this->uri->segment(1)=="structure_marketing"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>structure_marketing" class="nav-link ">
                    <span class="title">Structure Marketing</span>
                </a>
            </li>
            <li class="nav-item <?php if($this->uri->segment(1)=="chart_marketing"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>chart_marketing" class="nav-link ">
                    <span class="title">Chart Marketing</span>
                </a>
            </li>
        </ul>
    </li>
<?php } ?>


<?php if(array_key_exists('Admin', $this->userLevel) or array_key_exists('Handling', $this->userLevel) or 
         array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>

    <li class="nav-item <?php if($this->uri->segment(1)=="expenditure_handling" or $this->uri->segment(1)=="sale"){echo "active ";echo "open";}?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-briefcase"></i>
            <span class="title">Handling</span>
            <span class="arrow <?php if($this->uri->segment(1)=="expenditure_handling" or $this->uri->segment(1)=="take_baggage"){echo "open";}?>"></span>
        </a>
        <ul class="sub-menu" <?php if($this->uri->segment(1)=="expenditure_handling" or $this->uri->segment(1)=="take_baggage"){echo " style='display: block;' ";}?>>
            <?php if(array_key_exists('Admin', $this->userLevel) or array_key_exists('Handling', $this->userLevel) or 
                     array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>

                    <li class="nav-item <?php if($this->uri->segment(1)=="expenditure_handling"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>expenditure_handling" class="nav-link ">
                            <!-- <i class="icon-paper-plane"></i> -->
                            <span class="title">Expenditure Handling</span>
                        </a>
                    </li>
            <?php } ?>

            <?php if(array_key_exists('Handling', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>        
                    <li class="nav-item <?php if($this->uri->segment(1)=="take_baggage"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>take_baggage" class="nav-link ">
                            <!-- <i class="icon-bulb"></i> -->
                            <span class="title">Pegambilan Barang</span>

                        </a>
                    </li>
                    <li class="nav-item <?php if($this->uri->segment(1)=="sale"){echo "active";}?>">
                        <a href="<?php echo base_url(); ?>sale" class="nav-link ">
                            <!-- <i class="icon-bulb"></i> -->
                            <span class="title">Penjualan Barang</span>

                        </a>
                    </li>
            <?php } ?>

        </ul>
    </li>
<?php } ?>

<!-- 
<?php if(array_key_exists('Handling', $this->userLevel) or array_key_exists('Admin', $this->userLevel) or array_key_exists('Admin Finance', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="expenditure_handling"){echo "active";}?>">
        <a href="<?php echo base_url() ?>expenditure_handling" class="nav-item">
            <i class="icon-paper-plane"></i>
            <span class="title">Expenditure Handling</span>
        </a>
    </li>
<?php } ?> -->

    <!-- sperator -->
    <li class="heading">
        <h3 class="uppercase">Master Data</h3>
    </li>

<?php if(array_key_exists('Super Admin', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="schedule"){echo "active";}?>">
        <a href="<?php echo base_url(); ?>schedule">
            <i class="icon-calendar"></i>
            <span class="title">Schedule</span>
        </a>
    </li>
    <li class="nav-item <?php if($this->uri->segment(1)=="packet"){echo "active";}?>">
        <a href="<?php echo base_url(); ?>packet" class="nav-item">
            <i class="icon-bag"></i>
            <span class="title">Packet</span>
        </a>
    </li>
<?php } ?>

<?php if(array_key_exists('Admin Manivest', $this->userLevel) or array_key_exists('Manivest', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="hotel"){echo "active";}?>">
        <a href="<?php echo base_url(); ?>hotel">
            <i class="icon-globe"></i>
            <span class="title">Hotel</span>
        </a>
    </li>
<?php } ?>

<?php if(array_key_exists('Admin Baggage', $this->userLevel) or array_key_exists('Baggage', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="baggage" or $this->uri->segment(1)=="purchase" or $this->uri->segment(1)=="baggage_mutation"){echo "active ";echo "open";}?>">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-briefcase"></i>
            <span class="title">Barang</span>
            <span class="arrow <?php if($this->uri->segment(1)=="baggage" or $this->uri->segment(1)=="purchase" ){echo "open";}?>"></span>
        </a>
        <ul class="sub-menu" <?php if($this->uri->segment(1)=="baggage" or $this->uri->segment(1)=="purchase" or $this->uri->segment(1)=="baggage_mutation"){echo " style='display: block;' ";}?>>

            <li class="nav-item <?php if($this->uri->segment(1)=="baggage"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>baggage" class="nav-link ">
                    <span class="title">Barang</span>
                </a>
            </li>
            <!-- <li class="nav-item <?php if($this->uri->segment(1)=="barang_beli"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>barang_beli" class="nav-link ">
                    <span class="title">Baggage beli</span>
                </a>
            </li> -->
            <?php if(array_key_exists('Admin Baggage', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
            <li class="nav-item <?php if($this->uri->segment(1)=="purchase"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>purchase" class="nav-link ">
                    <span class="title">Pembelian Barang</span>
                </a>
            </li>

            <li class="nav-item <?php if($this->uri->segment(1)=="baggage_mutation"){echo "active";}?>">
                <a href="<?php echo base_url(); ?>baggage_mutation" class="nav-link ">
                    <span class="title">Barang Mutasi</span>
                </a>
            </li>
            <?php } ?>
        </ul>
    </li>
<?php } ?>

<?php if(array_key_exists('Admin', $this->userLevel) or array_key_exists('Super Admin', $this->userLevel)) { ?>
    <li class="nav-item <?php if($this->uri->segment(1)=="workarea"){echo "active";}?>">
        <a href="<?php echo base_url(); ?>workarea" class="nav-item">
            <i class="icon-pointer"></i>
            <span class="title">Work Area</span>
        </a>
    </li>


    <!-- sperator -->
    <li class="heading">
        <h3 class="uppercase">Setting</h3>
    </li>

    <li class="nav-item <?php if($this->uri->segment(1)=="users"){echo "active";}?>">
        <a href="<?php echo base_url(); ?>users" class="nav-item">
            <i class="icon-user"></i>
            <span class="title">Users</span>
        </a>
    </li>
<?php } ?>

</ul>
<!-- END SIDEBAR MENU -->