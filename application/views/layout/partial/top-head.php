<div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?php echo base_url() ?>">
                            <img src="<?php echo base_url() ?>assets/img/header.png" alt="logo" class="logo-default" /> 
                        </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <?php //$expenditure = Modules::run('expenditure_handling/count_all', array('jenis' => 'Expenditure Handling')); 

                            function time_since($dateTime)
                            {
                               date_default_timezone_set('Asia/Makassar');
                                $chunks = array(
                                    array(60*60*24*365, 'tahun'),
                                    array(60*60*24*30, 'bulan'),
                                    array(60*60*24, 'hari'),
                                    array(60*60, 'jam'),
                                    array(60, 'menit')
                                );

                                $today = time();
                                $since = $today - $dateTime;

                                if ($since > 604800) {
                                    $print = date("M jS", $dateTime);
                                    if ($since > 31536000) {
                                        $print .= ", ".date("Y", $dateTime);
                                        return $print;
                                    }
                                }

                                for ($i=0, $j = count($chunks); $i < $j; $i++) { 
                                    $seconds = $chunks[$i][0];
                                    $name = $chunks[$i][1];
                                    if (($count = floor($since/$seconds)) != 0)
                                        break;
                                }

                                $print = ($count == 1) ? '1 '.$name : "$count {$name}";
                                return $print.' yang lalu';
                            }


                            if(array_key_exists('Super Admin', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1'" )->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id order by id DESC")->result();                        
                            } else if(array_key_exists('Admin CS', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='3'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where for_user='3' order by id DESC")->result();                        
                            } else if(array_key_exists('Admin Manivest', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='5'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='5' order by id DESC")->result();                        
                            } else if(array_key_exists('Admin Baggage', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='7'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='7' order by id DESC")->result();                        
                            } else if(array_key_exists('Admin Finance', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='9'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='9' order by id DESC")->result();                        
                            } else if(array_key_exists('Admin Marketing', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='11'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='11' order by id DESC")->result();                        
                            } else if(array_key_exists('Admin Hotel', $this->userLevel)){
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='13'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='13' order by id DESC")->result();                        
                            } else {
                                $notifcount = $this->db->query("SELECT count(notification.id) as jumlah FROM notification WHERE reading='1' AND  for_user='X'")->row();
                                $notif = $this->db->query(" SELECT
                                                            notification.id,
                                                            notification.user_id,
                                                            notification.activity_type,
                                                            notification.activity_uri_1,
                                                            notification.activity_time,
                                                            notification.position,
                                                            notification.kdoffice,
                                                            notification.for_user,
                                                            notification.reading,
                                                            users.first_name,
                                                            posisi.posisi
                                                            FROM
                                                            notification
                                                            INNER JOIN users ON notification.user_id = users.id
                                                            INNER JOIN posisi ON notification.for_user = posisi.id where notification.for_user='X' order by id DESC")->result();                        
                            } 

                        ?>
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default" id="count" ><?php echo $notifcount->jumlah; ?> </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <?php if ($notifcount->jumlah >= 1) { ?>
                                            <?php foreach ($notif as $row) { ?>
                                                <li>
                                                    <a onclick="reading(<?php echo $row->id; ?>)" href="<?php echo base_url(); ?><?php echo $row->activity_uri_1; ?>">
                                                        <span class="details">
                                                                <small <?php if ($row->reading == 1) echo "style='color:red;'"; ?>><i class="fa fa-bell-o"> </i>&nbsp;<?php echo $row->first_name; ?> menambah data <?php echo $row->activity_type; ?>
                                                                    <br>                                                                
                                                                    <i><?php echo $x=date('d M y h:m', strtotime($row->activity_time)); ?></i>    
                                                                </small>
                                                        </span>

                                                    </a>
                                                </li>
                                            <?php } } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>


                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <!-- <img alt="" class="img-circle" src="<?php echo base_url() ?>assets/assets/layouts/layout/img/avatar3_small.jpg" /> -->
                                    <span class="username username-hide-on-mobile"> <?php echo $this->currentUser->fullName; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!-- <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url(); ?>auth/logout">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>                            
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>

<script type="text/javascript">
    function reading(id) {
       var  url = "<?php echo site_url('dashboard/kurangi/')?>"+id;
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON"
        }); 
    }   
</script>